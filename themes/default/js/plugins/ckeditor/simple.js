CKEDITOR.replace('summary', {
    toolbar: [{
        name: 'document',
         items: ['Source', '-', 'DocProps', 'Preview', '-', 'Templates', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt', '-', 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat', '-', 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', 'Iframe', 'Syntaxhighlight', 'InsertPre', '-',  'Youtube', 'MediaEmbed', 'Image', 'Flash', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'TransformTextSwitcher', 'TransformTextToLowercase', 'TransformTextToUppercase', 'TransformTextCapitalize', '-', 'BidiLtr', 'BidiRtl', '-', 'Link', 'Unlink', '-', 'Table', '-', 'SpecialChar', '-', 'HorizontalRule', '-', 'Smiley', 'TextColor', 'BGColor', 'UIColor', '-', 'Maximize', 'PlaceHolder', 'ShowBlocks', 'Styles', 'Format', 'Font', 'FontSize', 'LetterSpacing', 'lineheight', 'Zoom']
    }],
    height: 100
});
CKEDITOR.config.allowedContent = true;
CKEDITOR.disableAutoInline = true;