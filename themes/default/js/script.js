// autocomplet : this function will be executed every time we change the text
function autocomplet() {
	var min_length = 0; // min caracters to display the autocomplete
	var keyword = $('.select2-search__field').val();
	if (keyword.length >= min_length) {
		$.ajax({
			url: 'http://localhost/pixel/cosine/panel/media_ajax',
			type: 'POST',
			data: {keyword:keyword},
			success:function(data){
				$('.select2-results__options').show();
				$('.select2-results__options').html(data);
			}
		});
	} else {
		$('.select2-results__options').hide();
	}
}

// set_item : this function will be executed when we select an item
function set_item(item) {
	// change input value
	$('.select2-search__field').val(item);
	// hide proposition list
	$('.select2-results__options').hide();
}
function set_item_id(item) {
	$('.search_id').val(item);
	$('.select2-results__options').hide();
}