<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CodeIgniter Mail_Template Library
 *
 * Generate Mail_Template's in your CodeIgniter applications.
 *
 * @package       CodeIgniter
 * @subpackage    Libraries
 * @category      Libraries
 * @author        Arwendra Adi Putra
 * @license       MIT License
 * @link          https://github.com/pixelcostudios/Mail_Template
 */

class Mail_Template  {

  public function mail_forgot($data_array = array())
  {
      extract($data_array);

      $subject                      = $data_array['subject'];
      $user_fullname                = $data_array['user_fullname'];
      $user_email                   = $data_array['user_email'];
      $user_forgotten_password_code = $data_array['user_forgotten_password_code'];
      $themes                       = $data_array['themes'];
      $facebook                     = $data_array['facebook'];
      $google_plus                  = $data_array['google_plus'];
      $twitter                      = $data_array['twitter'];
      $instagram                    = $data_array['instagram'];
      $powered                      = $data_array['powered'];
      $company                      = $data_array['company'];

      $message = '<!doctype html>
      <html>
      <head>
      <meta name="viewport" content="width=device-width" />
      <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
      <title></title>
      <style>
      img {border: none;-ms-interpolation-mode: bicubic;max-width: 100%; }

      body {
      background-color: #f6f6f6;
      font-family: "Roboto";
      -webkit-font-smoothing: antialiased;
      font-size: 13px;
      line-height: 1.4;
      margin: 0;
      padding: 0; 
      -ms-text-size-adjust: 100%;
      -webkit-text-size-adjust: 100%; }

      table {
      border-collapse: separate;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
      width: 100%; }
      table td {
      font-family: "Roboto";
      font-size: 13px;
      vertical-align: top; }

      .body {background-color: #F2F2F2;width: 100%; padding-top:20px;}

      .header {
      display: block;
      margin: 0 auto !important;
      max-width: 580px;
      width: 580px; 
      background: #fff;}

      .container {
      display: block;
      margin: 0 auto !important;
      max-width: 570px;
      padding: 5px;
      width: 570px; 
      background: #fff;
      text-align: center;
      }
      .container.reset p{font-size: 20px;font-weight: 400;}

      .content {
      box-sizing: border-box;
      display: block;
      margin: 0 auto;
      max-width: 580px;
      padding: 10px; }

      h1,
      h2,
      h3,
      h4 {
      color: #000000;
      font-family: "Roboto";
      font-weight: 400;
      line-height: 1.4;
      margin: 0;
      margin-bottom: 30px; }

      h1 {
      font-size: 35px;
      font-weight: 300;
      text-align: center;
      text-transform: capitalize; }

      p,
      ul,
      ol {
      font-family: "Roboto";
      font-size: 14px;
      font-weight: normal;
      margin: 0;
      margin-bottom: 10px; }
      p li,
      ul li,
      ol li {list-style-position: inside;margin-left: 5px; }
      p{line-height: 25px;margin-bottom: 5px;}
      a {color: #3498db;text-decoration: underline; }

      .align-center {text-align: center; }
      .align-right {text-align: right; }
      .align-left { text-align: left; }
      .clear {clear: both; }
      .mt0 {margin-top: 0; }
      .mb0 {margin-bottom: 0; }

      .btn {
      display: inline-block;
      padding: 6px 12px;
      margin-bottom: 0;
      font-size: 14px;
      font-weight: 400;
      line-height: 1.42857143;
      text-align: center;
      white-space: nowrap;
      vertical-align: middle;
      -ms-touch-action: manipulation;
      touch-action: manipulation;
      cursor: pointer;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      background-image: none;
      border: 1px solid transparent;
      border-radius: 3px;
      background-color: #1ab394;
      border-color: #1ab394;
      color: #FFFFFF;
      text-decoration: none;
      }
      .btn:hover {background:#169C82;}
      .content-block{padding: 20px 0;color: #999999;}
      .content-block img{border-radius: 3px;}
      @media only screen and (max-width: 620px) {
      .header {
      display: block;
      margin: 0 auto !important;
      max-width: 100%;
      width: 100%; 
      background: #fff;}

      .container {
      display: block;
      margin: 0 auto !important;
      max-width: 100%;
      padding: 5px 0;
      width: 100%; 
      background: #fff;
      text-align: center;
      }
      .container p{padding: 0 10px;}
      }

      @media all {
      }

      </style>
      </head>
      <body class="">
      <table border="0" cellpadding="0" cellspacing="0" class="body">
        <tr>
          <td class="header"><img src="'.base_url().'themes/'.$themes.'/images/bg_forgot.jpg" width="100%"></td>
        </tr>
        <tr>
          <td class="container reset"><p>Reset Password</p></td>
        </tr>
        <tr>
          <td class="container"><p><strong> Hi '.$user_fullname.',</strong></p></td>
        </tr>
        <tr>
          <td class="container"><p>A request to reset password was received from your email '.$user_email.'</p></td>
        </tr>
        <tr>
          <td class="container"><p>Click link change password to reset password.</p></td>
        </tr>
        <tr>
          <td class="container"><p><a href="'.base_url().'panel/forgot_change/'.$user_forgotten_password_code.'.html" class="btn">Change Password</a></p></td>
        </tr>
        <tr>
          <td class="content-block" align="center">
            Follow '.$company.' Social Media & Get Update
            <br>
            <br>
            <a href="'.$facebook.'"><img src="'.base_url().'themes/'.$themes.'/images/32-facebook.png"></a>&nbsp;&nbsp;
            <a href="'.$google_plus.'"><img src="'.base_url().'themes/'.$themes.'/images/32-googleplus.png"></a>&nbsp;&nbsp;
            <a href="'.$instagram.'"><img src="'.base_url().'themes/'.$themes.'/images/32-instagram.png"></a></a>&nbsp;&nbsp;
            <a href="'.$twitter.'"><img src="'.base_url().'themes/'.$themes.'/images/32-twitter.png"></a>
            <br>
            <br>
            <span class="apple-link align-center">Powered by '.$powered.'</span>
          </td>
        </tr>
      </table>
        </body>
      </html>';
      return $message;
  }
  public function mail_activation($data_array = array())
  {
      extract($data_array);

      $subject                      = $data_array['subject'];
      $user_fullname                = $data_array['user_fullname'];
      $user_email                   = $data_array['user_email'];
      $user_activation_code         = $data_array['user_activation_code'];
      $themes                       = $data_array['themes'];
      $facebook                     = $data_array['facebook'];
      $google_plus                  = $data_array['google_plus'];
      $twitter                      = $data_array['twitter'];
      $instagram                    = $data_array['instagram'];
      $powered                      = $data_array['powered'];
      $company                      = $data_array['company'];

      $message = '<!doctype html>
      <html>
      <head>
      <meta name="viewport" content="width=device-width" />
      <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
      <title></title>
      <style>
      img {border: none;-ms-interpolation-mode: bicubic;max-width: 100%; }

      body {
      background-color: #f6f6f6;
      font-family: "Roboto";
      -webkit-font-smoothing: antialiased;
      font-size: 13px;
      line-height: 1.4;
      margin: 0;
      padding: 0; 
      -ms-text-size-adjust: 100%;
      -webkit-text-size-adjust: 100%; }

      table {
      border-collapse: separate;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
      width: 100%; }
      table td {
      font-family: "Roboto";
      font-size: 13px;
      vertical-align: top; }

      .body {background-color: #F2F2F2;width: 100%; padding-top:20px;}

      .header {
      display: block;
      margin: 0 auto !important;
      max-width: 580px;
      width: 580px; 
      background: #fff;}

      .container {
      display: block;
      margin: 0 auto !important;
      max-width: 570px;
      padding: 5px;
      width: 570px; 
      background: #fff;
      text-align: center;
      }
      .container.reset p{font-size: 20px;font-weight: 400;}

      .content {
      box-sizing: border-box;
      display: block;
      margin: 0 auto;
      max-width: 580px;
      padding: 10px; }

      h1,
      h2,
      h3,
      h4 {
      color: #000000;
      font-family: "Roboto";
      font-weight: 400;
      line-height: 1.4;
      margin: 0;
      margin-bottom: 30px; }

      h1 {
      font-size: 35px;
      font-weight: 300;
      text-align: center;
      text-transform: capitalize; }

      p,
      ul,
      ol {
      font-family: "Roboto";
      font-size: 14px;
      font-weight: normal;
      margin: 0;
      margin-bottom: 10px; }
      p li,
      ul li,
      ol li {list-style-position: inside;margin-left: 5px; }
      p{line-height: 25px;margin-bottom: 5px;}
      a {color: #3498db;text-decoration: underline; }

      .align-center {text-align: center; }
      .align-right {text-align: right; }
      .align-left { text-align: left; }
      .clear {clear: both; }
      .mt0 {margin-top: 0; }
      .mb0 {margin-bottom: 0; }

      .btn {
      display: inline-block;
      padding: 6px 12px;
      margin-bottom: 0;
      font-size: 14px;
      font-weight: 400;
      line-height: 1.42857143;
      text-align: center;
      white-space: nowrap;
      vertical-align: middle;
      -ms-touch-action: manipulation;
      touch-action: manipulation;
      cursor: pointer;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      background-image: none;
      border: 1px solid transparent;
      border-radius: 3px;
      background-color: #1ab394;
      border-color: #1ab394;
      color: #FFFFFF;
      text-decoration: none;
      }
      .btn:hover {background:#169C82;}
      .content-block{padding: 20px 0;color: #999999;}
      .content-block img{border-radius: 3px;}
      @media only screen and (max-width: 620px) {
      .header {
      display: block;
      margin: 0 auto !important;
      max-width: 100%;
      width: 100%; 
      background: #fff;}

      .container {
      display: block;
      margin: 0 auto !important;
      max-width: 100%;
      padding: 5px 0;
      width: 100%; 
      background: #fff;
      text-align: center;
      }
      .container p{padding: 0 10px;}
      }

      @media all {
      }

      </style>
      </head>
      <body class="">
      <table border="0" cellpadding="0" cellspacing="0" class="body">
        <tr>
          <td class="header"><img src="'.base_url().'themes/'.$themes.'/images/bg_activation.jpg" width="100%"></td>
        </tr>
        <tr>
          <td class="container reset"><p>Activation Registration</p></td>
        </tr>
        <tr>
          <td class="container"><p><strong> Hi '.$user_fullname.',</strong></p></td>
        </tr>
        <tr>
          <td class="container"><p>A request to activation registration was received from your email '.$user_email.'</p></td>
        </tr>
        <tr>
          <td class="container"><p>Click button Activation to activated Account.</p></td>
        </tr>
        <tr>
          <td class="container"><p><a href="'.base_url().'panel/register_activation/'.$user_activation_code.'.html" class="btn">Activation</a></p></td>
        </tr>
        <tr>
          <td class="content-block" align="center">
            Follow '.$company.' Social Media & Get Update
            <br>
            <br>
            <a href="'.$facebook.'"><img src="'.base_url().'themes/'.$themes.'/images/32-facebook.png"></a>&nbsp;&nbsp;
            <a href="'.$google_plus.'"><img src="'.base_url().'themes/'.$themes.'/images/32-googleplus.png"></a>&nbsp;&nbsp;
            <a href="'.$instagram.'"><img src="'.base_url().'themes/'.$themes.'/images/32-instagram.png"></a></a>&nbsp;&nbsp;
            <a href="'.$twitter.'"><img src="'.base_url().'themes/'.$themes.'/images/32-twitter.png"></a>
            <br>
            <br>
            <span class="apple-link align-center">Powered by '.$powered.'</span>
          </td>
        </tr>
      </table>
        </body>
      </html>';
      return $message;
  }
  public function mail_account_forgot($data_array = array())
  {
      extract($data_array);

      $subject                      = $data_array['subject'];
      $user_fullname                = $data_array['user_fullname'];
      $user_email                   = $data_array['user_email'];
      $user_forgotten_password_code = $data_array['user_forgotten_password_code'];
      $themes                       = $data_array['themes'];
      $facebook                     = $data_array['facebook'];
      $google_plus                  = $data_array['google_plus'];
      $twitter                      = $data_array['twitter'];
      $instagram                    = $data_array['instagram'];
      $powered                      = $data_array['powered'];
      $company                      = $data_array['company'];

      $message = '<!doctype html>
      <html>
      <head>
      <meta name="viewport" content="width=device-width" />
      <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
      <title></title>
      <style>
      img {border: none;-ms-interpolation-mode: bicubic;max-width: 100%; }

      body {
      background-color: #f6f6f6;
      font-family: "Roboto";
      -webkit-font-smoothing: antialiased;
      font-size: 13px;
      line-height: 1.4;
      margin: 0;
      padding: 0; 
      -ms-text-size-adjust: 100%;
      -webkit-text-size-adjust: 100%; }

      table {
      border-collapse: separate;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
      width: 100%; }
      table td {
      font-family: "Roboto";
      font-size: 13px;
      vertical-align: top; }

      .body {background-color: #F2F2F2;width: 100%; padding-top:20px;}

      .header {
      display: block;
      margin: 0 auto !important;
      max-width: 580px;
      width: 580px; 
      background: #fff;}

      .container {
      display: block;
      margin: 0 auto !important;
      max-width: 570px;
      padding: 5px;
      width: 570px; 
      background: #fff;
      text-align: center;
      }
      .container.reset p{font-size: 20px;font-weight: 400;}

      .content {
      box-sizing: border-box;
      display: block;
      margin: 0 auto;
      max-width: 580px;
      padding: 10px; }

      h1,
      h2,
      h3,
      h4 {
      color: #000000;
      font-family: "Roboto";
      font-weight: 400;
      line-height: 1.4;
      margin: 0;
      margin-bottom: 30px; }

      h1 {
      font-size: 35px;
      font-weight: 300;
      text-align: center;
      text-transform: capitalize; }

      p,
      ul,
      ol {
      font-family: "Roboto";
      font-size: 14px;
      font-weight: normal;
      margin: 0;
      margin-bottom: 10px; }
      p li,
      ul li,
      ol li {list-style-position: inside;margin-left: 5px; }
      p{line-height: 25px;margin-bottom: 5px;}
      a {color: #3498db;text-decoration: underline; }

      .align-center {text-align: center; }
      .align-right {text-align: right; }
      .align-left { text-align: left; }
      .clear {clear: both; }
      .mt0 {margin-top: 0; }
      .mb0 {margin-bottom: 0; }

      .btn {
      display: inline-block;
      padding: 6px 12px;
      margin-bottom: 0;
      font-size: 14px;
      font-weight: 400;
      line-height: 1.42857143;
      text-align: center;
      white-space: nowrap;
      vertical-align: middle;
      -ms-touch-action: manipulation;
      touch-action: manipulation;
      cursor: pointer;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      background-image: none;
      border: 1px solid transparent;
      border-radius: 3px;
      background-color: #1ab394;
      border-color: #1ab394;
      color: #FFFFFF;
      text-decoration: none;
      }
      .btn:hover {background:#169C82;}
      .content-block{padding: 20px 0;color: #999999;}
      .content-block img{border-radius: 3px;}
      @media only screen and (max-width: 620px) {
      .header {
      display: block;
      margin: 0 auto !important;
      max-width: 100%;
      width: 100%; 
      background: #fff;}

      .container {
      display: block;
      margin: 0 auto !important;
      max-width: 100%;
      padding: 5px 0;
      width: 100%; 
      background: #fff;
      text-align: center;
      }
      .container p{padding: 0 10px;}
      }

      @media all {
      }

      </style>
      </head>
      <body class="">
      <table border="0" cellpadding="0" cellspacing="0" class="body">
        <tr>
          <td class="header"><img src="'.base_url().'themes/'.$themes.'/images/bg_forgot.jpg" width="100%"></td>
        </tr>
        <tr>
          <td class="container reset"><p>Reset Password</p></td>
        </tr>
        <tr>
          <td class="container"><p><strong> Hi '.$user_fullname.',</strong></p></td>
        </tr>
        <tr>
          <td class="container"><p>A request to reset password was received from your email '.$user_email.'</p></td>
        </tr>
        <tr>
          <td class="container"><p>Click link change password to reset password.</p></td>
        </tr>
        <tr>
          <td class="container"><p><a href="'.base_url().'account/forgot_change/'.$user_forgotten_password_code.'.html" class="btn">Change Password</a></p></td>
        </tr>
        <tr>
          <td class="content-block" align="center">
            Follow '.$company.' Social Media & Get Update
            <br>
            <br>
            <a href="'.$facebook.'"><img src="'.base_url().'themes/'.$themes.'/images/32-facebook.png"></a>&nbsp;&nbsp;
            <a href="'.$google_plus.'"><img src="'.base_url().'themes/'.$themes.'/images/32-googleplus.png"></a>&nbsp;&nbsp;
            <a href="'.$instagram.'"><img src="'.base_url().'themes/'.$themes.'/images/32-instagram.png"></a></a>&nbsp;&nbsp;
            <a href="'.$twitter.'"><img src="'.base_url().'themes/'.$themes.'/images/32-twitter.png"></a>
            <br>
            <br>
            <span class="apple-link align-center">Powered by '.$powered.'</span>
          </td>
        </tr>
      </table>
        </body>
      </html>';
      return $message;
  }
  public function mail_account_activation($data_array = array())
  {
      extract($data_array);

      $subject                      = $data_array['subject'];
      $user_fullname                = $data_array['user_fullname'];
      $user_email                   = $data_array['user_email'];
      $user_activation_code         = $data_array['user_activation_code'];
      $themes                       = $data_array['themes'];
      $facebook                     = $data_array['facebook'];
      $google_plus                  = $data_array['google_plus'];
      $twitter                      = $data_array['twitter'];
      $instagram                    = $data_array['instagram'];
      $powered                      = $data_array['powered'];
      $company                      = $data_array['company'];

      $message = '<!doctype html>
      <html>
      <head>
      <meta name="viewport" content="width=device-width" />
      <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
      <title></title>
      <style>
      img {border: none;-ms-interpolation-mode: bicubic;max-width: 100%; }

      body {
      background-color: #f6f6f6;
      font-family: "Roboto";
      -webkit-font-smoothing: antialiased;
      font-size: 13px;
      line-height: 1.4;
      margin: 0;
      padding: 0; 
      -ms-text-size-adjust: 100%;
      -webkit-text-size-adjust: 100%; }

      table {
      border-collapse: separate;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
      width: 100%; }
      table td {
      font-family: "Roboto";
      font-size: 13px;
      vertical-align: top; }

      .body {background-color: #F2F2F2;width: 100%; padding-top:20px;}

      .header {
      display: block;
      margin: 0 auto !important;
      max-width: 580px;
      width: 580px; 
      background: #fff;}

      .container {
      display: block;
      margin: 0 auto !important;
      max-width: 570px;
      padding: 5px;
      width: 570px; 
      background: #fff;
      text-align: center;
      }
      .container.reset p{font-size: 20px;font-weight: 400;}

      .content {
      box-sizing: border-box;
      display: block;
      margin: 0 auto;
      max-width: 580px;
      padding: 10px; }

      h1,
      h2,
      h3,
      h4 {
      color: #000000;
      font-family: "Roboto";
      font-weight: 400;
      line-height: 1.4;
      margin: 0;
      margin-bottom: 30px; }

      h1 {
      font-size: 35px;
      font-weight: 300;
      text-align: center;
      text-transform: capitalize; }

      p,
      ul,
      ol {
      font-family: "Roboto";
      font-size: 14px;
      font-weight: normal;
      margin: 0;
      margin-bottom: 10px; }
      p li,
      ul li,
      ol li {list-style-position: inside;margin-left: 5px; }
      p{line-height: 25px;margin-bottom: 5px;}
      a {color: #3498db;text-decoration: underline; }

      .align-center {text-align: center; }
      .align-right {text-align: right; }
      .align-left { text-align: left; }
      .clear {clear: both; }
      .mt0 {margin-top: 0; }
      .mb0 {margin-bottom: 0; }

      .btn {
      display: inline-block;
      padding: 6px 12px;
      margin-bottom: 0;
      font-size: 14px;
      font-weight: 400;
      line-height: 1.42857143;
      text-align: center;
      white-space: nowrap;
      vertical-align: middle;
      -ms-touch-action: manipulation;
      touch-action: manipulation;
      cursor: pointer;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      background-image: none;
      border: 1px solid transparent;
      border-radius: 3px;
      background-color: #1ab394;
      border-color: #1ab394;
      color: #FFFFFF;
      text-decoration: none;
      }
      .btn:hover {background:#169C82;}
      .content-block{padding: 20px 0;color: #999999;}
      .content-block img{border-radius: 3px;}
      @media only screen and (max-width: 620px) {
      .header {
      display: block;
      margin: 0 auto !important;
      max-width: 100%;
      width: 100%; 
      background: #fff;}

      .container {
      display: block;
      margin: 0 auto !important;
      max-width: 100%;
      padding: 5px 0;
      width: 100%; 
      background: #fff;
      text-align: center;
      }
      .container p{padding: 0 10px;}
      }

      @media all {
      }

      </style>
      </head>
      <body class="">
      <table border="0" cellpadding="0" cellspacing="0" class="body">
        <tr>
          <td class="header"><img src="'.base_url().'themes/'.$themes.'/images/bg_activation.jpg" width="100%"></td>
        </tr>
        <tr>
          <td class="container reset"><p>Activation Registration</p></td>
        </tr>
        <tr>
          <td class="container"><p><strong> Hi '.$user_fullname.',</strong></p></td>
        </tr>
        <tr>
          <td class="container"><p>A request to activation registration was received from your email '.$user_email.'</p></td>
        </tr>
        <tr>
          <td class="container"><p>Click button Activation to activated Account.</p></td>
        </tr>
        <tr>
          <td class="container"><p><a href="'.base_url().'account/account_activation/'.$user_activation_code.'.html" class="btn">Activation</a></p></td>
        </tr>
        <tr>
          <td class="content-block" align="center">
            Follow '.$company.' Social Media & Get Update
            <br>
            <br>
            <a href="'.$facebook.'"><img src="'.base_url().'themes/'.$themes.'/images/32-facebook.png"></a>&nbsp;&nbsp;
            <a href="'.$google_plus.'"><img src="'.base_url().'themes/'.$themes.'/images/32-googleplus.png"></a>&nbsp;&nbsp;
            <a href="'.$instagram.'"><img src="'.base_url().'themes/'.$themes.'/images/32-instagram.png"></a></a>&nbsp;&nbsp;
            <a href="'.$twitter.'"><img src="'.base_url().'themes/'.$themes.'/images/32-twitter.png"></a>
            <br>
            <br>
            <span class="apple-link align-center">Powered by '.$powered.'</span>
          </td>
        </tr>
      </table>
        </body>
      </html>';
      return $message;
  }
  public function mail_voucher($data_array = array())
  {
      extract($data_array);

      $subject                      = $data_array['subject'];
      $user_fullname                = $data_array['user_fullname'];
      $themes                       = $data_array['themes'];
      $facebook                     = $data_array['facebook'];
      $google_plus                  = $data_array['google_plus'];
      $twitter                      = $data_array['twitter'];
      $instagram                    = $data_array['instagram'];
      $powered                      = $data_array['powered'];
      $company                      = $data_array['company'];

      $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
          <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '"  />
          <title>'.$subject.'</title>
          <style type="text/css">
              body {
                  background-color: #f6f6f6;
                  font-family: Arial, Verdana, Helvetica, sans-serif;
                  font-size: 13px;
                  text-align:center;
              }
              p{
                font-size: 36px;
                  text-align:center;
              }
              .content {
                  box-sizing: border-box;
                  display: block;
                  Margin: 0 auto;
                  max-width: 580px;
                  padding: 10px;
              }
              .code{
                font-size: 18px;
              }
              .white{
                width: 100%;
                background-color: #fff;
              }
              .white strong.name{
                margin: 20px 0 0;
                display: block;
              }
              .tranparent{
                width: 100%;
                margin: 20px 0 0;
              }
              .white td{
                padding: 8px;
                line-height: 25px;
              }
              .address{
                width: 100%;
              }
              .address td{
                border: 1px solid #ddd;
              }
          </style>
      </head>
      <body>
      <div class="content" style="text-align:center;">
      <img src="'.base_url().'themes/'.$themes.'/images/bg.jpg" width="100%">
      <table class="white">
          <tr>
            <td align="center"><strong class="name">Hi '.$user_fullname.',</strong></td>
          </tr>
          <tr>
          <td>Berikut ini adalah code voucher yang disertakan dalam file PDF dan bisa kamu gunakan di merchant yang telah ditunjuk.
          </td>
        </tr>
        <tr>
          <td></td>
        </tr>
      </table>
      <table class="tranparent align-center">
          <tr>
            <td align="center">
        Follow '.$company.' Social Media & Get Update
            <br>
            <br>
            <a href=""><img src="'.base_url().'themes/'.$themes.'/images/32-facebook.png"></a>&nbsp;&nbsp;
            <a href=""><img src="'.base_url().'themes/'.$themes.'/images/32-googleplus.png"></a>&nbsp;&nbsp;
            <a href=""><img src="'.base_url().'themes/'.$themes.'/images/32-instagram.png"></a></a>&nbsp;&nbsp;
            <a href=""><img src="'.base_url().'themes/'.$themes.'/images/32-twitter.png"></a>
            <br>
            <br>
            <span class="apple-link align-center">Powered by '.$powered.'</span>
            </td>
          </tr>
      </table>
      </div>
      </body>
      </html>';
      return $message;
  }
  public function mail_voucher_code($data_array = array())
  {
      extract($data_array);

      $subject                      = $data_array['subject'];
      $user_fullname                = $data_array['user_fullname'];
      $voucher                      = $data_array['voucher'];
      $voucher_date                 = $data_array['voucher_date'];
      $merchant_name                = $data_array['merchant_name'];
      $merchant_address             = $data_array['merchant_address'];
      $merchant_phone               = $data_array['merchant_phone'];
      $merchant_map                 = $data_array['merchant_map'];
      $themes                       = $data_array['themes'];
      $facebook                     = $data_array['facebook'];
      $google_plus                  = $data_array['google_plus'];
      $twitter                      = $data_array['twitter'];
      $instagram                    = $data_array['instagram'];
      $powered                      = $data_array['powered'];
      $company                      = $data_array['company'];

      $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
          <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '"  />
          <title>'.$subject.'</title>
          <style type="text/css">
              body {
                  background-color: #f6f6f6;
                  font-family: Arial, Verdana, Helvetica, sans-serif;
                  font-size: 13px;
                  text-align:center;
              }
              p{
                font-size: 36px;
                  text-align:center;
              }
              .content {
                  box-sizing: border-box;
                  display: block;
                  Margin: 0 auto;
                  max-width: 580px;
                  padding: 10px;
              }
              .code{
                font-size: 18px;
              }
              .white{
                width: 100%;
                background-color: #fff;
              }
              .white strong.name{
                margin: 20px 0 0;
                display: block;
              }
              .tranparent{
                width: 100%;
                margin: 20px 0 0;
              }
              .white td{
                padding: 8px;
                line-height: 25px;
              }
              .address{
                width: 100%;
              }
              .address td{
                border: 1px solid #ddd;
              }
          </style>
      </head>
      <body>
      <div class="content" style="text-align:center;">
      <img src="'.base_url().'themes/'.$themes.'/images/bg.jpg" width="100%">
      <table class="white">
          <tr>
            <td align="center"><strong class="name">Hi '.$user_fullname.',</strong></td>
          </tr>
          <tr>
            <td align="center">Berikut ini adalah code voucher kamu yang bisa kamu gunakan di merchant yang telah ditunjuk.</td>
          </tr>
          <tr>
            <td align="center" class="code">'.$voucher.'</td>
          </tr>
          <tr>
            <td align="center">Gunakan voucher ini sebelum expired pada tanggal '.$voucher_date.'.</td>
          </tr> 
          <tr>
            <td>
              <table class="address">
                <tr>
                  <td align="left">Nama Merchant</td>
                  <td>:</td>
                  <td align="left">'.$merchant_name.'</td>
                </tr>
                <tr>
                  <td align="left">Alamat Merchant</td>
                  <td>:</td>
                  <td align="left">'.$merchant_address.'</td>
                </tr>
                <tr>
                  <td align="left">Phone</td>
                  <td>:</td>
                  <td align="left">'.$merchant_phone.'</td>
                </tr>
                <tr>
                  <td align="left">Google Map</td>
                  <td>:</td>
                  <td align="left"><a href="'.$merchant_map.'">Link Google Map</a></td>
                </tr>
              </table>
            </td>
          </tr> 
      </table>
      <table class="tranparent align-center">
          <tr>
            <td align="center">
        Follow '.$company.' Social Media & Get Update
            <br>
            <br>
            <a href=""><img src="'.base_url().'themes/'.$themes.'/images/32-facebook.png"></a>&nbsp;&nbsp;
            <a href=""><img src="'.base_url().'themes/'.$themes.'/images/32-googleplus.png"></a>&nbsp;&nbsp;
            <a href=""><img src="'.base_url().'themes/'.$themes.'/images/32-instagram.png"></a></a>&nbsp;&nbsp;
            <a href=""><img src="'.base_url().'themes/'.$themes.'/images/32-twitter.png"></a>
            <br>
            <br>
            <span class="apple-link align-center">Powered by '.$powered.'</span>
            </td>
          </tr>
      </table>
      </div>
      </body>
      </html>';
      return $message;
  }
  public function mail_contact($data_array = array())
  {
      extract($data_array);

      $subject                      = $data_array['subject'];
      $user_fullname                = $data_array['user_fullname'];
      $user_email                   = $data_array['user_email'];
      $user_phone                   = $data_array['user_phone'];
      $user_subject                 = $data_array['user_subject'];
      $user_departement             = $data_array['user_departement'];
      $user_message                 = $data_array['user_message'];
      $themes                       = $data_array['themes'];
      $facebook                     = $data_array['facebook'];
      $google_plus                  = $data_array['google_plus'];
      $twitter                      = $data_array['twitter'];
      $instagram                    = $data_array['instagram'];
      $powered                      = $data_array['powered'];
      $company                      = $data_array['company'];

      $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
          <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '"  />
          <title>'.$subject.'</title>
          <style type="text/css">
              body {
                  background-color: #f6f6f6;
                  font-family: Arial, Verdana, Helvetica, sans-serif;
                  font-size: 13px;
                  text-align:center;
              }
              p{
                font-size: 36px;
                  text-align:center;
              }
              .content {
                  box-sizing: border-box;
                  display: block;
                  Margin: 0 auto;
                  max-width: 580px;
                  padding: 10px;
              }
              .code{
                font-size: 18px;
              }
              .white{
                width: 100%;
                background-color: #fff;
              }
              .white strong.name{
                margin: 20px 0 0;
                display: block;
              }
              .tranparent{
                width: 100%;
                margin: 20px 0 0;
              }
              .white td{
                padding: 8px;
                line-height: 25px;
              }
              .address{
                width: 100%;
              }
              .address td{
                border: 1px solid #ddd;
              }
          </style>
      </head>
      <body>
      <div class="content" style="text-align:center;">
      <img src="'.base_url().'themes/'.$themes.'/images/bg_contact.jpg" width="100%">
      <table class="white">
          <tr>
            <td align="center"><strong class="name">Hi '.$company.',</strong></td>
          </tr>
          <tr>
          <td>Seseorang telah menghubungi anda melalui email ini, berikut detail dari pesan yang dikirim:
          </td>
        </tr>
        <tr>
          <td>
            <table>
              <tr>
                <td>Name</td>
                <td>:</td>
                <td>'.$user_fullname.'</td>
              </tr>
              <tr>
                <td>Email</td>
                <td>:</td>
                <td>'.$user_email.'</td>
              </tr>
              <tr>
                <td>Email</td>
                <td>:</td>
                <td>'.$user_phone.'</td>
              </tr>
              <tr>
                <td>Subject</td>
                <td>:</td>
                <td>'.$user_subject.'</td>
              </tr>
              <tr>
                <td>Departement</td>
                <td>:</td>
                <td>'.$user_departement.'</td>
              </tr>
              <tr>
                <td>Message</td>
                <td>:</td>
                <td>'.$user_message.'</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td></td>
        </tr>
      </table>
      <table class="tranparent align-center">
          <tr>
            <td align="center">
        Follow '.$company.' Social Media & Get Update
            <br>
            <br>
            <a href=""><img src="'.base_url().'themes/'.$themes.'/images/32-facebook.png"></a>&nbsp;&nbsp;
            <a href=""><img src="'.base_url().'themes/'.$themes.'/images/32-googleplus.png"></a>&nbsp;&nbsp;
            <a href=""><img src="'.base_url().'themes/'.$themes.'/images/32-instagram.png"></a></a>&nbsp;&nbsp;
            <a href=""><img src="'.base_url().'themes/'.$themes.'/images/32-twitter.png"></a>
            <br>
            <br>
            <span class="apple-link align-center">Powered by '.$powered.'</span>
            </td>
          </tr>
      </table>
      </div>
      </body>
      </html>';
      return $message;
  }
}
