<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CodeIgniter Mail_Template Library
 *
 * Generate Mail_Template's in your CodeIgniter applications.
 *
 * @package       CodeIgniter
 * @subpackage    Libraries
 * @category      Libraries
 * @author        Arwendra Adi Putra
 * @license       MIT License
 * @link          https://github.com/pixelcostudios/Mail_Template
 */
class Mail_theme {

  public function deal_voucher()
  {
      $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
          <meta name="viewport" content="width=device-width" />
          <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" 
          <title>Voucher Email</title>
          <style>
            @import url("https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,900");
            /* -------------------------------------
                GLOBAL RESETS
            ------------------------------------- */
            img {
              border: none;
              -ms-interpolation-mode: bicubic;
              max-width: 100%; }

            body {
              background-color: #f6f6f6;
             font-family: "Source Sans Pro, sans-serif;
              -webkit-font-smoothing: antialiased;
              font-size: 14px;
              line-height: 1.4;
              margin: 0;
              padding: 0; 
              -ms-text-size-adjust: 100%;
              -webkit-text-size-adjust: 100%; }

            table {
              border-collapse: separate;
              mso-table-lspace: 0pt;
              mso-table-rspace: 0pt;
              width: 100%; }
              table td {
                font-family: "Source Sans Pro", sans-serif;
                font-size: 14px;
                vertical-align: top; }

            /* -------------------------------------
                BODY & CONTAINER
            ------------------------------------- */

            .body {
              background-color: #F2F2F2;
              width: 100%; }

            /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
            .container {
              display: block;
              Margin: 0 auto !important;
              /* makes it centered */
              max-width: 580px;
              padding: 10px;
              width: 580px; }

            /* This should also be a block element, so that it will fill 100% of the .container */
            .content {
              box-sizing: border-box;
              display: block;
              Margin: 0 auto;
              max-width: 580px;
              padding: 10px; }
              .address{
                margin: 0 0 20px;
                border-collapse: separate;
              }
              .address td{
                padding: 5px;
                border: 1px solid #F2F2F2;
              }
            /* -------------------------------------
                HEADER, FOOTER, MAIN
            ------------------------------------- */
            .main {
              background: #fff;
              border-radius: 3px;
              width: 100%; }
            .header{
              width: 100%;        
            }
            .header td{
              font-size: 20px;
              vertical-align: middle;
            }
            .wrapper {
              box-sizing: border-box;
              padding: 20px 20px 0px; }

            .footer {
              clear: both;
              padding-top: 10px;
              text-align: center;
              width: 100%; }
              .footer td,
              .footer p,
              .footer span,
              .footer a {
                color: #999999;
                font-size: 12px;}
                .footer-line{
                  margin: 0;
                  height: 20px;
                  background: #FEDD0F;
                }
            /* -------------------------------------
                TYPOGRAPHY
            ------------------------------------- */
            h1,
            h2,
            h3,
            h4 {
              color: #000000;
              font-family: "Source Sans Pro", sans-serif;
              font-weight: 400;
              line-height: 1.4;
              margin: 0;
              Margin-bottom: 30px; }

            h1 {
              font-size: 35px;
              font-weight: 300;
              text-align: center;
              text-transform: capitalize; }

            p,
            ul,
            ol {
              font-family: "Source Sans Pro", sans-serif;
              font-size: 14px;
              font-weight: normal;
              margin: 0;
              Margin-bottom: 10px; }
              p li,
              ul li,
              ol li {
                list-style-position: inside;
                margin-left: 5px; }
              p{
                    line-height: 25px;
              }
            a {
              color: #3498db;
              text-decoration: underline; }
              .code
              {
                color: #0058A4;
                margin: 0 0 10px 0;
              }
              .code td
              {
                font-size: 34px;
                font-weight: bold;
                letter-spacing: 2px;
              }
              .download{
                background: #CCCCCC;
              }
              .download td{
                padding: 5px;
                vertical-align:middle;
              }
            /* -------------------------------------
                OTHER STYLES THAT MIGHT BE USEFUL
            ------------------------------------- */
            .last {
              margin-bottom: 0; }

            .first {
              margin-top: 0; }

            .align-center {
              text-align: center; }

            .align-right {
              text-align: right; }

            .align-left {
              text-align: left; }

            .clear {
              clear: both; }

            .mt0 {
              margin-top: 0; }

            .mb0 {
              margin-bottom: 0; }

            .preheader {
              color: transparent;
              display: none;
              height: 0;
              max-height: 0;
              max-width: 0;
              opacity: 0;
              overflow: hidden;
              mso-hide: all;
              visibility: hidden;
              width: 0; }

            .powered-by a {
              text-decoration: none; }

            hr {
              border: 0;
              border-bottom: 1px solid #f6f6f6;
              Margin: 20px 0; }
              hr.code-hr{
                border-bottom: 1px solid #808080;
                width: 170px;
              }
            /* -------------------------------------
                RESPONSIVE AND MOBILE FRIENDLY STYLES
            ------------------------------------- */
            @media only screen and (max-width: 620px) {
              table[class=body] h1 {
                font-size: 28px !important;
                margin-bottom: 10px !important; }
              table[class=body] p,
              table[class=body] ul,
              table[class=body] ol,
              table[class=body] td,
              table[class=body] span,
              table[class=body] a {
                font-size: 16px !important; }
              table[class=body] .wrapper,
              table[class=body] .article {
                padding: 10px !important; }
              table[class=body] .content {
                padding: 0 !important; }
              table[class=body] .container {
                padding: 0 !important;
                width: 100% !important; }
              table[class=body] .main {
                border-left-width: 0 !important;
                border-radius: 0 !important;
                border-right-width: 0 !important; }
              table[class=body] .btn table {
                width: 100% !important; }
              table[class=body] .btn a {
                width: 100% !important; }
              table[class=body] .img-responsive {
                height: auto !important;
                max-width: 100% !important;
                width: auto !important; }}

            /* -------------------------------------
                PRESERVE THESE STYLES IN THE HEAD
            ------------------------------------- */
            @media all {
              .ExternalClass {
                width: 100%; }
              .ExternalClass,
              .ExternalClass p,
              .ExternalClass span,
              .ExternalClass font,
              .ExternalClass td,
              .ExternalClass div {
                line-height: 100%; }
              .apple-link a {
                color: inherit !important;
                font-family: inherit !important;
                font-size: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
                text-decoration: none !important; } 
              .btn-primary table td:hover {
                background-color: #34495e !important; }
              .btn-primary a:hover {
                background-color: #34495e !important;
                border-color: #34495e !important; } }

          </style>
        </head>
        <body class="">
          <table border="0" cellpadding="0" cellspacing="0" class="body">
            <tr>
              <td>&nbsp;</td>
              <td class="container">
                <div class="content">

                  <!-- START CENTERED WHITE CONTAINER -->
                  <img src="bg.jpg">
                  <table class="main">            
                    <!-- START MAIN CONTENT AREA -->
                    <tr>
                      <td class="wrapper">
                        <table border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td align="center">
                              <p class="mt0 mb0">&nbsp;</p>
                              <p><strong> Hi Diego,</strong></p>
                              <p class="mt0 mb0">&nbsp;</p>
                              <p>Berikut ini adalah code voucher kamu yang bisa kamu gunakan di merchant yang telah ditunjuk.</p>
                              <table border="0" cellpadding="0" cellspacing="0" class="code">
                                <tbody>
                                  <tr>
                                    <td align="center">
                                      9OpUFXUi1N
                                      <hr class="code-hr">
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <p>Gunakan voucher ini sebelum expired pada tanggal 2017-01-01 12:12:12.</p>
                            </td>
                          </tr>
                        </table>
                        <table class="address">
                          <tr>
                            <td>Nama Merchant</td>
                            <td>:</td>
                            <td>Milk n Chese</td>
                          </tr>
                          <tr>
                            <td>Alamat Merchant</td>
                            <td>:</td>
                            <td>Jl. Utama No 73, Pugeran, Meguwoharjo, Depok, Sleman</td>
                          </tr>
                          <tr>
                            <td>Phone</td>
                            <td>:</td>
                            <td>085643688710p</td>
                          </tr>
                          <tr>
                            <td>Google Map</td>
                            <td>:</td>
                            <td><a href="">Link Google Map</a></td>
                          </tr>
                        </table>
                      </td>
                    </tr>

                    <!-- <tr class="download">
                      <td>
                        <table>
                          <tr>
                            <td valign="center">Download Aplikasi Deal Jogja disini</td>
                            <td align="right" style="padding-bottom: 0;"><img src="icon_google_play.png" width="200"></td>
                          </tr>
                        </table>
                      </td>
                    </tr> -->
                    <!-- END MAIN CONTENT AREA -->
                    </table>

                  <!-- START FOOTER -->
                  <div class="footer">
                    <table border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td class="content-block" align="center">
                          Follow Deal Jogja Social Media & Get Update
                          <br>
                          <br>
                          <a href=""><img src="32-facebook.png"></a>&nbsp;&nbsp;
                          <a href=""><img src="32-googleplus.png"></a>&nbsp;&nbsp;
                          <a href=""><img src="32-instagram.png"></a></a>&nbsp;&nbsp;
                          <a href=""><img src="32-twitter.png"></a>
                          <br>
                          <br>
                          <span class="apple-link align-center">Powered by @kulinerjojga</span>
                        </td>
                      </tr>
                    </table>
                    
                  </div>
                  <!-- END FOOTER -->
                  
                <!-- END CENTERED WHITE CONTAINER -->
                </div>
              </td>
              <td>&nbsp;</td>
            </tr>
          </table>
          <!-- <div class="footer-line"></div> -->
        </body>
      </html>
      ';
      return $message;
  }
}
