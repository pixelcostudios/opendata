<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Web extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        header('Content-type=application/json; charset=utf-8');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
		$this->load->library('ion_auth');
        $this->load->helper(array('form', 'url', 'string'));
		$this->load->helper(array('path'));
    }
 	public function index_get()
    {
    	$this->response([
            'status' => TRUE,
            'error' => FALSE,
            'message' => 'Request Web API Not found'
        ], REST_Controller::HTTP_OK);
    }
    public function index_post()
    {
        $this->response([
            'status' => TRUE,
            'error' => FALSE,
            'message' => 'Request Web API Post Not found'
        ], REST_Controller::HTTP_OK);
    }
    public function post_list_get()
    {
        $this->load->model('model_web');
        
        $post       = $this->model_web->post_list('post','0');
        
        if(count($post)>0)
        {
            // foreach($post as $row)
            // {
            //     $data = [
            //         'outlet_name'      => $row->outlet_name,
            //         'outlet_region'    => $row->outlet_region,
            //         'outlet_address'   => $row->outlet_adddress,
            //         'outlet_city'      => $row->outlet_city,
            //         'outlet_province'  => $row->outlet_province,
            //         'outlet_avatar'    => $row->outlet_avatar,
            //         'outlet_latitude'  => $row->outlet_latitude,
            //         'outlet_longitude' => $row->outlet_longitude
            //     ];
            //     $result[] = $data;
            // }
            $json_status = ([
                'status' => TRUE,
                'message' => 'List Post',
                'page'  => '1',
                'total'  => count($post),
                'data' => $post
            ]);
            $this->response($json_status, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response([
                'status' => FALSE,
                'message' => 'No Post were found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }
    public function post_detail_get()
    {
        $this->load->model('themes/'.$this->config->item('themes').'/model_api');
        $this->load->model('themes/'.$this->config->item('themes').'/model_post');
        
        $post = $this->model_api->po_id($this->uri->segment(4));
        if(count($post)>0)
        {
            foreach($post as $row)
            {
                $thumbnail             = $this->model_post->ip_value($row->post_id,'post','ASC','ipost');
                $thumbnail             = count($thumbnail>0) ? base_url().'upload/'.$thumbnail : base_url().'upload/default.png';
                $data = array([
                    'post_id'          => $row->post_id,
                    'post_title'       => $row->post_title,
                    'post_slug'        => $row->post_slug,
                    'post_content'     => $row->post_content,
                    'date'             => $row->date,
                    'thumbnail'        => $thumbnail
                ]);
                $result = $data;
            }
            $json_status = ([
                'status' => TRUE,
                'message' => 'Post Detail',
                'data' => $result
            ]);
            $this->response($json_status, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response([
                'status' => FALSE,
                'message' => 'No Post were found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }
    public function user_login_post()
    {
    	$this->load->model('themes/'.$this->config->item('themes').'/model_api');
    	
    	$this->load->library('form_validation');
        $this->form_validation->set_rules('user_email', 'User Email', 'trim|required');
        $this->form_validation->set_rules('user_password', 'Password', 'required');
        if ($this->form_validation->run() === FALSE)
        {
			$this->load->helper('form');
            $this->response([
                'status' => FALSE,
	            'error' => TRUE,
                // 'message' => 'Login Failed',
                'message' => 'Login credentials are wrong. Please try again!'
	        ], REST_Controller::HTTP_OK);
        }
        else
        {
            $remember = (bool) $this->input->post('remember');
            $identity = $this->input->post('user_email');
            $password = $this->input->post('user_password');
            if ($this->ion_auth->login($identity, $password, $remember))
            {
                $user_unique            = $this->model_api->char_random('25');
                $data_array = array(
                    'user_unique'       => $user_unique
                );
                $row_user   = $this->model_api->user_detail_email($identity);
                $this->model_api->user_update($row_user->user_id,$data_array);
                $outlet     = $this->model_api->outlet_by_user($row_user->user_id);

                foreach($outlet as $row_key)
                {
                    $data_outlet = array(
                        'outlet_id'    => $row_key->outlet_id,
                        'outlet_name'  => $row_key->outlet_name,
                        'outlet_province'  => $row_key->outlet_province,
                    );
                    $json_outlet[]  = $data_outlet;
                }
                $json_result = array(
                    'user_id'           => $row_user->user_id,
                    'user_unique'       => $user_unique,
                    'user_role'         => $row_user->role_name,
                    'user_gender'       => $row_user->user_gender,
                    'user_first_name'   => $row_user->user_first_name,
                    'user_last_name'    => $row_user->user_last_name,
                    'user_email'        => $row_user->user_email,
                    'user_mobile'       => $row_user->user_mobile,
                    'user_address'      => $row_user->user_address,
                    'user_city'         => $row_user->user_city,
                    'user_province'     => $row_user->user_province,
                    'user_country'      => $row_user->user_country,
                    'user_postal'       => $row_user->user_postal,
                    'user_avatar'       => base_url().'upload/avatar/'.$row_user->user_avatar,
                    'user_join'         => $row_user->user_join
                );
            	$this->response([
                    'status'            => TRUE,
		            'error'             => FALSE,
		            'message'           => 'Login Successfully',
                    'uid'               => $user_unique,
                    'data'              => $json_result,
                    'outlet'            => $json_outlet
		            
		        ], REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response([
                    'status' => FALSE,
		            'error' => FALSE,
		            'message' => $this->ion_auth->errors()
		        ], REST_Controller::HTTP_OK);
            }
        }
    }
    public function item_by_outlet_get()
    {
        $this->load->model('themes/'.$this->config->item('themes').'/model_api');
        $this->load->model('themes/'.$this->config->item('themes').'/model_post');
        
        //$post = $this->model_api->post_news_type_public('product','insurance');
        $id          = $this->uri->segment(4);
        $post        = $this->model_api->item_by_outlet($id);
        $cart_date   = date('Y-m-d');
        $order_data  = $this->model_api->transaction_outlet_last_order($id,$cart_date);
        $order       = ($order_data!='') ? $order_data: 0;

        if(count($post)>0)
        {
            foreach($post as $row)
            {
                $data = [
                    'item_id'          => $row->items_id,
                    'item_name'        => $row->item_name,
                    'item_price'       => $row->items_price,
                    'item_avatar'      => $row->item_icon
                ];
                $result[] = $data;
            }
            $json_status = ([
                'status' => TRUE,
                'message' => 'Items Arfa Barbershop',
                'count'  => count($post),
                'order'  => $order,
                'data' => $result
            ]);
            $this->response($json_status, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response([
                'status' => FALSE,
                'message' => 'No Post were found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }
    public function outlet_by_user_get()
    {
        $this->load->model('themes/'.$this->config->item('themes').'/model_api');
        
        $id = (int)$this->uri->segment(4);
        
        if($id == 0)
        {
            $this->response([
                'status' => TRUE,
                'message' => 'Request Not found',
                'count' => '0'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            $post = $this->model_api->outlet_by_user($id);

            if(count($post) == 0)
            {
                $this->response([
                    'status' => TRUE,
                    'message' => 'List Outlet Not Found',
                    'count' => '0'
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                foreach($post as $row_key)
                {
                    $data = [
                        'outlet_id'         => $row_key->outlet_id,
                        'outlet_name'       => $row_key->outlet_name,
                        'outlet_avatar'     => base_url().'upload/'.$row_key->outlet_avatar,
                        'outlet_join'       => $row_key->outlet_date
                    ];
                    $json_result[] = $data;
                }
                $json_status = ([
                    'status' => TRUE,
                    'message' => 'List Outlet',
                    'count' => count($post),
                    'data' => $json_result
                ]);
                $this->response($json_status, REST_Controller::HTTP_OK);
            }
        }
    }
    public function pilot_by_outlet_get()
    {
        $this->load->model('themes/'.$this->config->item('themes').'/model_api');
        
        $id     = (int)$this->uri->segment(4);
        $date   = date('Y-m-d');
        
        if($id == 0)
        {
            $this->response([
                'status'    => TRUE,
                'message'   => 'Request Not found',
                'count'     => '0'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            $post = $this->model_api->pilot_by_outlet($id,'4');

            if(count($post) == 0)
            {
                $this->response([
                    'status'    => TRUE,
                    'message'   => 'List Pilot Not Found',
                    'count'     => count($post)
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                foreach($post as $row_key)
                {
                    $data = [
                        'pilot_id'         => $row_key->user_id,
                        'pilot_name'       => $row_key->user_first_name.' '.$row_key->user_last_name,
                        'pilot_status'     => $this->model_api->presence_check($row_key->user_id,$id,$date),
                        'pilot_avatar'     => base_url().'upload/avatar/'.$row_key->user_avatar,
                        'pilot_join'       => $row_key->user_join
                    ];
                    $json_result[] = $data;
                }
                $json_status = ([
                    'status'    => TRUE,
                    'message'   => 'List Pilot',
                    'count'     => count($post),
                    'data'      => $json_result
                ]);
                $this->response($json_status, REST_Controller::HTTP_OK);
            }
        }
    }
    public function outlet_by_partner_get()
    {
        $this->load->model('themes/'.$this->config->item('themes').'/model_api');
        
        $id = (int)$this->uri->segment(4);
        
        if($id == 0)
        {
            $this->response([
                'status' => TRUE,
                'message' => 'Request Not found',
                'count' => '0'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            $post = $this->model_api->outlet_by_user($id);

            if(count($post) == 0)
            {
                $this->response([
                    'status' => TRUE,
                    'message' => 'List Outlet Not Found',
                    'count' => count($post)
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                foreach($post as $row_key)
                {
                    $data = [
                        'outlet_id'         => $row_key->outlet_id,
                        'outlet_name'       => $row_key->outlet_name,
                        'outlet_province'   => $row_key->outlet_province
                    ];
                    $json_result[] = $data;
                }
                $json_status = ([
                    'status' => TRUE,
                    'message' => 'List Outlet',
                    'count' => count($post),
                    'data' => $json_result
                ]);
                $this->response($json_status, REST_Controller::HTTP_OK);
            }
        }
    }
    public function transaction_by_pilot_get()
    {
        $this->load->model('themes/'.$this->config->item('themes').'/model_api');
        
        $id          = (int)$this->uri->segment(4);
        $cart_date   = $this->uri->segment(5);
        //$cart_date   = $this->uri->segment(6);
        
        if($id == 0)
        {
            $this->response([
                'status' => TRUE,
                'message' => 'Request Not found',
                'count'     => count($post),
                'data'      => array()
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            $post = $this->model_api->transaction_list($id,$cart_date);

            if(count($post) == 0)
            {
                $this->response([
                    'status' => TRUE,
                    'message' => 'Transaction Not Found',
                    'count'     => count($post),
                    'data'      => array()
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                foreach($post as $row_key)
                {
                    $item       = $this->model_api->transaction_item_list($row_key->cart_id);

                    $data = [
                        'cart_id'       => $row_key->cart_id,
                        'cart_code'     => $row_key->cart_code,
                        'cart_count'    => count($item),
                        'cart_pilot'    => $row_key->user_first_name.' '.$row_key->user_last_name,
                        'cart_date'     => date('d M Y',strtotime($row_key->cart_date_time)),
                        'cart_time'     => date('H:i:s',strtotime($row_key->cart_date_time))
                    ];
                    $json_result[] = $data;
                }
                $json_status = ([
                    'status'    => TRUE,
                    'message'   => 'List Transaction',
                    'count'     => count($post),
                    'data'      => $json_result
                ]);
                $this->response($json_status, REST_Controller::HTTP_OK);
            }
        }
    }
    public function transaction_update_post()
    {
        $this->load->model('themes/'.$this->config->item('themes').'/model_api');
        
        $user_id        = $this->input->post('user_id');
        $outlet_id      = $this->input->post('outlet_id');
        $cart_code      = $this->input->post('cart_code');
        //$cart_order     = ($this->input->post('cart_order')!='') ? $this->input->post('cart_order') : '0';
        $cart_waiting   = $this->input->post('cart_waiting');
        $cart_payment   = $this->input->post('cart_payment');
        $cart_pay       = $this->input->post('cart_pay');

        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_id', 'User ID','required');
        $this->form_validation->set_rules('outlet_id','Outlet ID','required');
        $this->form_validation->set_rules('cart_code','Cart Code','required');
        $this->form_validation->set_rules('cart_waiting','Cart Waiting','required');
        $this->form_validation->set_rules('cart_payment','Cart Payment Channel','required');
        $this->form_validation->set_rules('cart_pay','Cart Payment','required');

        file_put_contents('upload/files/json_transaction_update_'.$this->input->post('outlet_id').'.json', json_encode($this->input->post()), FILE_APPEND);
        // print_r($this->input->post());
        
        if ($this->form_validation->run() === FALSE)
        {
            $this->load->helper('form');
            $this->response([
                'status' => FALSE,
                'message' => 'Update Transaction Data Failed'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            if($this->input->post('cart_code')!='')
            {
                $cart_total             = $this->model_api->transaction_outlet_price_total($user_id,$cart_code);
                $cart_cost              = $cart_total;
                $cart_pay_return        = ($cart_pay-$cart_total);
                $where_array = array(
                    'user_id'           => $user_id,
                    'outlet_id'         => $outlet_id,
                    'cart_code'         => $cart_code
                );
                $data_array = array(
                    'cart_cost'         => $cart_cost,
                    'cart_waiting'      => $cart_waiting,
                    'cart_payment'      => $cart_payment,
                    'cart_pay'          => $cart_pay,
                    'cart_pay_return'   => $cart_pay_return
                );

                //$this->model_api->transaction_update($cart_code,$user_id,$outlet_id,$data_array);
                $this->model_api->transaction_update($where_array,$data_array);
                
                $this->response([
                    'status'            => TRUE,
                    'message'           => 'Transaction Successfully Update'
                    
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response([
                    'status'            => TRUE,
                    'message'           => 'Update Transaction Data Error'
                    
                ], REST_Controller::HTTP_OK);
            }
        }
    }
    public function transaction_add_post()
    {
        $this->load->model('themes/'.$this->config->item('themes').'/model_api');

        $cart_code      = $this->input->post('cart_code');
        $user_id        = $this->input->post('user_id');
        $outlet_id      = $this->input->post('outlet_id');
        $cart_order     = ($this->input->post('cart_order')!='') ? $this->input->post('cart_order') : '0';
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_id', 'User ID','trim|required');
        $this->form_validation->set_rules('outlet_id','Outlet ID','trim|required');
        $this->form_validation->set_rules('cart_code','Cart Code','trim|required');
        $this->form_validation->set_rules('cart_order','Cart Order','trim|required');
        //$this->form_validation->set_rules('cart_waiting','Cart Waiting','trim|required');

        // file_put_contents('upload/files/json_transaction_'.$this->input->post('outlet_id').'.json', $this->input->post(), FILE_APPEND);
        // file_put_contents('upload/files/json_transaction1_'.$this->input->post('outlet_id').'.json', json_encode($this->input->post()), FILE_APPEND);
        
        if ($this->form_validation->run() === FALSE)
        {
            $this->load->helper('form');
            $this->response([
                'status' => FALSE,
                'message' => 'Add Transaction Data Failed'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            //$cart_waiting   = ($this->input->post('cart_waiting')!='') ? $this->input->post('cart_waiting') : '0';
            $asearch        = array('[',']');
            $areplace       = array('','');
            $items_id       = str_replace($asearch, $areplace, $this->input->post('items_id'));
            $cart_date      = date('Y-m-d');
            $cart_date_time = date('Y-m-d H:i:s');
            $cart_status    = '1';

            if($this->input->post('cart_code')!='')
            {

                $data_array = array(
                    'user_id'       => $user_id,
                    'outlet_id'     => $outlet_id,
                    'cart_code'     => $cart_code,
                    'cart_order'    => $cart_order,
                    //'cart_waiting'  => $cart_waiting,
                    'cart_date'     => $cart_date,
                    'cart_date_time'=> $cart_date_time,
                    'cart_status'   => $cart_status
                );
                if($this->model_api->transaction_check($user_id,$outlet_id,$cart_code)==0)
                {
                    $this->model_api->transaction_add($data_array);
                    $id             = $this->db->insert_id();
                    $this->session->set_userdata("cart_id", $id);
                    //$id             = '12';
                }
                //$id             = '12';
                
                $item_array = explode(',', $items_id);
                //print_r($item_array);

                for ($i = 0; $i < count($item_array); ++$i) 
                {
                    //$key_array  = explode('.', $key);
                    $qty        = explode(".", $item_array[$i])[0];
                    $items_id   = explode(".", $item_array[$i])[1];
                    $cart_id    = $this->session->userdata("cart_id");

                    if($qty>0)
                    {
                        $data_array2 = array(
                            'user_id'       => $user_id,
                            'outlet_id'     => $outlet_id,
                            'cart_id'       => $cart_id,
                            'cart_code'     => $cart_code,
                            'items_id'      => $items_id,
                            'price'         => $this->model_api->items_price($items_id),
                            'qty'           => $qty,
                            'date'          => $cart_date,
                            'date_time'     => $cart_date_time,
                            'sync'          => '1'
                        );
                        //print_r($data_array2);
                        $this->model_api->transaction_order_add($data_array2);
                    }
                }

                $this->session->unset_userdata('cart_id');

                $this->response([
                    'status'            => TRUE,
                    'message'           => 'Transaction Successfully Added'
                    
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response([
                    'status'            => TRUE,
                    'message'           => 'Add Transaction Data Error'
                    
                ], REST_Controller::HTTP_OK);
            }
        }
    }

    public function transaction_by_pilot_detail_get()
    {   //http://localhost/pos.arfabarbershop.com/api/android/transaction_by_pilot_detail/1
        $this->load->model('themes/'.$this->config->item('themes').'/model_api');
        
        $cart_code    = $this->uri->segment(4);

        $post       = $this->model_api->transaction_code_item_list($cart_code);
        $detail     = $this->model_api->transaction_code_detail($cart_code);
        //print_r($post);
        if(count($detail) == 0)
        {
            $this->response([
                'status' => TRUE,
                'message' => 'Transaction Detail Not Found',
                'count'     => count($post),
                'code'      => 'none',
                'data'      => array()
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            foreach ($detail as $key_value) 
            {
                $total = 0;
                foreach($post as $row_key)
                {
                    $data = [
                        'item_id'           => $row_key->item_id,
                        'item_name'         => $row_key->item_name,
                        'item_price'        => ($row_key->price*$row_key->qty),
                        'item_qty'          => $row_key->qty
                    ];
                    $json_result[]  = $data;
                    $total          += ($row_key->price*$row_key->qty);
                }
                $json_status = ([
                    'status'    => TRUE,
                    'message'   => 'Detail Transaction',
                    'count'     => count($post),
                    'code'      => $key_value->cart_code,
                    'order'     => $key_value->cart_order,
                    'waiting'   => $key_value->cart_waiting,
                    'payment'   => $key_value->cart_payment,
                    'pay'       => $key_value->cart_pay,
                    'pay_return'=> $key_value->cart_pay_return,
                    'total'     => $total,
                    'date'      => date('d M Y',strtotime($key_value->cart_date_time)),
                    'time'      => date('H:i:s',strtotime($key_value->cart_date_time)),
                    'datetime'  => date('d M Y H:i:s',strtotime($key_value->cart_date_time)),
                    'data'      => $json_result
                ]);
                $this->response($json_status, REST_Controller::HTTP_OK);
            }
        }
    }
    public function transaction_by_outlet_get()
    {
        $this->load->model('themes/'.$this->config->item('themes').'/model_api');
        
        $id          = (int)$this->uri->segment(4);
        $cart_date   = ($this->uri->segment(5)!='') ? $this->uri->segment(5) : date('Y-m-d');
        //$cart_date   = $this->uri->segment(6);
        
        if($id == 0)
        {
            $this->response([
                'status' => TRUE,
                'message' => 'Request Not found',
                'count'     => count($post),
                'data'      => array()
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            $post = $this->model_api->transaction_outlet_list($id,$cart_date);

            if(count($post) == 0)
            {
                $this->response([
                    'status' => TRUE,
                    'message' => 'Transaction Not Found',
                    'count'     => count($post),
                    'data'      => array()
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                foreach($post as $row_key)
                {
                    $item       = $this->model_api->transaction_item_list($row_key->cart_id);

                    $data = [
                        'cart_id'       => $row_key->cart_id,
                        'cart_code'     => $row_key->cart_code,
                        'cart_count'    => $row_key->cart_order,
                        'cart_pilot'    => $row_key->user_first_name.' '.$row_key->user_last_name,
                        'cart_date'     => date('d M Y',strtotime($row_key->cart_date_time)),
                        'cart_time'     => date('H:i:s',strtotime($row_key->cart_date_time))
                    ];
                    $json_result[] = $data;
                }
                $json_status = ([
                    'status'    => TRUE,
                    'message'   => 'List Transaction',
                    'count'     => count($post),
                    'date'      => date("d M Y", strtotime($cart_date)),
                    'data'      => $json_result
                ]);
                $this->response($json_status, REST_Controller::HTTP_OK);
            }
        }
    }
    public function report_by_pilot_get()
    {
        $this->load->model('themes/'.$this->config->item('themes').'/model_api');
        //http://localhost/pos.arfabarbershop.com/api/android/report_by_pilot_detail/40/1/2017-09 month
        //http://localhost/pos.arfabarbershop.com/api/android/report_by_pilot_detail/40/1/2017-09-01 custom date
        //http://localhost/pos.arfabarbershop.com/api/android/report_by_pilot_detail/40/1/ last date
        $user_id    = (int)$this->uri->segment(4);
        $outlet_id  = (int)$this->uri->segment(5);
        $date       = $this->uri->segment(6)!='' ? $this->uri->segment(6) : date('Y-m-d');
        
        $post       = $this->model_api->item_list();
        //print_r($post);
        if(count($post) == 0)
        {
            $this->response([
                'status' => TRUE,
                'message' => 'Transaction Detail Not Found',
                'count' => count($post),
                'date' => date("d M Y", strtotime($date)),
                'total' => $total,
                'data' => array()
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            $total = '0';
            foreach($post as $row_key)
            {
                $order_list = $this->model_api->report_order_list($user_id,$outlet_id,$row_key->item_id,$date);
                $price  = '0';
                $qty    = '0';
                foreach ($order_list as $key) 
                {
                    $price  += $key->price*$key->qty;
                    $qty    += $key->qty;
                }
                $data = [
                    'item_id'           => $row_key->item_id,
                    'item_name'         => $row_key->item_code,
                    'item_price'        => $price,//$row_key->price,
                    'item_qty'          => $qty,//$row_key->qty
                ];
                $json_result[]  = $data;
                $total          += $price;
            }
            $json_status = ([
                'status' => TRUE,
                'message' => 'Detail Transaction',
                'count' => count($post),
                'date' => date("d M Y", strtotime($date)),
                'total' => $total,
                'data' => $json_result
            ]);
            $this->response($json_status, REST_Controller::HTTP_OK);
        }
    }
    public function report_by_outlet_get()
    {
        $this->load->model('themes/'.$this->config->item('themes').'/model_api');
        //http://localhost/pos.arfabarbershop.com/api/android/report_by_pilot_detail/40/1/2017-09 month
        //http://localhost/pos.arfabarbershop.com/api/android/report_by_pilot_detail/40/1/2017-09-01 custom date
        //http://localhost/pos.arfabarbershop.com/api/android/report_by_pilot_detail/40/1/ last date
        $outlet_id  = (int)$this->uri->segment(4);
        $date       = $this->uri->segment(5)!='' ? $this->uri->segment(5) : date('Y-m-d');
        
        $post       = $this->model_api->item_list();
        //print_r($post);
        if(count($post) == 0)
        {
            $this->response([
                'status' => TRUE,
                'message' => 'Transaction Detail Not Found',
                'count' => count($post),
                'date' => date("d M Y", strtotime($date)),
                'total' => "0",
                'total_issue' => "0",
                'total_omzet' => "0",
                'data' => array()
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            $total = '0';
            $total_issue = $this->model_api->issue_by_outlet_total($outlet_id,$date);
            foreach($post as $row_key)
            {
                $order_list = $this->model_api->report_order_outlet_list($outlet_id,$row_key->item_id,$date);
                $price  = '0';
                $qty    = '0';
                foreach ($order_list as $key) 
                {
                    $price  += $key->price*$key->qty;
                    $qty    += $key->qty;
                }
                $data = [
                    'item_id'           => $row_key->item_id,
                    'item_name'         => $row_key->item_code,
                    'item_price'        => $price,//$row_key->price,
                    'item_qty'          => $qty,//$row_key->qty
                ];
                $json_result[]  = $data;
                $total          += $price;
            }
            $json_status = ([
                'status' => TRUE,
                'message' => 'Detail Transaction',
                'count' => count($post),
                'date' => date("d M Y", strtotime($date)),
                'total' => $total,
                'total_issue' => $total_issue,
                'total_omzet' => ($total-$total_issue),
                'data' => $json_result
            ]);
            $this->response($json_status, REST_Controller::HTTP_OK);
        }
    }
    public function presence_by_pilot_get()
    {
        $this->load->model('themes/'.$this->config->item('themes').'/model_api');
        
        $user_id    = (int)$this->uri->segment(4);

        $date       = ($this->uri->segment(5)!='') ? $this->uri->segment(5) : date('Y-m');
        
        $post       = $this->model_api->presence_by_pilot($user_id,$date);
        //print_r($post);
        if(count($post) == 0)
        {
            $this->response([
                'status'    => TRUE,
                'message'   => 'Presence List Not Found',
                'count'     => count($post),
                'date'     => date('M Y',strtotime($date)),                
                'data'      => array()
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            foreach($post as $row_key)
            {
                $data = [
                    'presence_id'       => $row_key->presence_id,
                    'presence_date'     => date('d M Y',strtotime($row_key->date_time)),
                    'presence_time'     => date('H:i:s',strtotime($row_key->date_time)),
                    // 'presence_summary'  => $row_key->summary,
                    'presence_avatar'   => base_url().'upload/avatar/'.$row_key->user_avatar
                ];
                $json_result[]  = $data;
            }
            $json_status = ([
                'status' => TRUE,
                'message' => 'Presence List',
                'count' => count($post),
                'date' => date('M Y',strtotime($date)),
                //'date' => $date,
                //'total' => $total,
                'data' => $json_result
            ]);
            $this->response($json_status, REST_Controller::HTTP_OK);
        }
    }
    public function presence_by_outlet_get()
    {
        $this->load->model('themes/'.$this->config->item('themes').'/model_api');
        
        $outlet_id      = (int)$this->uri->segment(4);

        $date           = ($this->uri->segment(5)!='') ? $this->uri->segment(5) : date('Y-m-d');
        
        $post           = $this->model_api->presence_by_outlet($outlet_id,$date);
        //print_r($post);
        if(count($post) == 0)
        {
            $this->response([
                'status'    => TRUE,
                'message'   => 'Presence Detail Not Found',
                'count'     => count($post),
                'date'     => date('d M Y',strtotime($date)),
                'data'      => array()
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            foreach($post as $row_key)
            {
                $data = [
                    'presence_id'       => $row_key->user_id,
                    'presence_fullname' => $row_key->user_first_name.' '.$row_key->user_last_name,
                    'presence_date'     => date('Y-m-d',strtotime($row_key->date_time)),
                    'presence_time'     => date('H:i:s',strtotime($row_key->date_time)),
                    //'presence_summary'  => $row_key->summary,
                    'presence_avatar'   => base_url().'upload/avatar/'.$row_key->user_avatar
                ];
                $json_result[]  = $data;
            }
            $json_status = ([
                'status'    => TRUE,
                'message'   => 'Detail Presence',
                'count'     => count($post),
                'date'     => date('d M Y',strtotime($date)),
                'data'      => $json_result
            ]);
            $this->response($json_status, REST_Controller::HTTP_OK);
        }
    }
    public function presence_by_search_post()
    {
        $this->load->model('themes/'.$this->config->item('themes').'/model_api');
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_fullname', 'User Fullname','trim|required');

        $user_fullname    = $this->input->post('user_fullname');

        $date           = ($this->uri->segment(4)!='') ? $this->uri->segment(4) : date('Y-m-d');

        $post           = $this->model_api->user_mobile_by_search($user_fullname);
        //print_r($post);
        if ($this->form_validation->run() === FALSE)
        {
            $this->load->helper('form');
            $this->response([
                'status' => FALSE,
                'message' => 'Detail Pilot Mobile Data Failed'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            if(count($post) == 0)
            {
                $this->response([
                    'status'    => TRUE,
                    'message'   => 'List Pilot Not Found',
                    'count'     => count($post)
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                foreach($post as $row_key)
                {
                    $data = [
                        'pilot_id'         => $row_key->user_id,
                        'pilot_name'       => $row_key->user_first_name.' '.$row_key->user_last_name,
                        'pilot_status'     => $this->model_api->presence_mobile_check($row_key->user_id,$date),
                        'pilot_avatar'     => base_url().'upload/avatar/'.$row_key->user_avatar,
                        'pilot_join'       => $row_key->user_join
                    ];
                    $json_result[] = $data;
                }
                $json_status = ([
                    'status'    => TRUE,
                    'message'   => 'List Pilot',
                    'count'     => count($post),
                    'data'      => $json_result
                ]);
                $this->response($json_status, REST_Controller::HTTP_OK);
            }
        }
    }
    public function presence_add_post()
    {
        $this->load->model('themes/'.$this->config->item('themes').'/model_api');
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_id', 'User ID','trim|required');
        $this->form_validation->set_rules('outlet_id','Outlet ID','trim|required');

        file_put_contents('upload/files/json_presence_'.$this->input->post('outlet_id').'.json', $this->input->post('user_id'), FILE_APPEND);
        
        if ($this->form_validation->run() === FALSE)
        {
            $this->load->helper('form');
            $this->response([
                'status' => FALSE,
                'message' => 'Add Presence Data Failed'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            $user_id    = $this->input->post('user_id');
            
            $outlet_id  = $this->input->post('outlet_id');
            $date       = date('Y-m-d');
            $date_time  = date('Y-m-d H:i:s');

            if($this->input->post('user_id')!='')
            {
                $user_array = explode(',', $this->input->post('user_id'));
                foreach ($user_array as $key) 
                {
                    $data_array = array(
                        'user_id'       => $key,
                        'outlet_id'     => $outlet_id,
                        'date'          => $date,
                        'date_time'     => $date_time
                    );
                    $check      = $this->model_api->presence_check($key,$outlet_id,date('Y-m-d'));
                    if($check=='0')
                    {
                        $this->model_api->presence_add($data_array);
                    }
                }

                $this->response([
                    'status'            => TRUE,
                    'message'           => 'Presence Successfully Added'
                    
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response([
                    'status'            => TRUE,
                    'message'           => 'Add Presence Data Error'
                    
                ], REST_Controller::HTTP_OK);
            }
        }
    }
    public function issue_by_outlet_get()
    {
        $this->load->model('themes/'.$this->config->item('themes').'/model_api');
        
        $outlet_id      = (int)$this->uri->segment(4);

        $date           = ($this->uri->segment(5)!='') ? $this->uri->segment(5) : date('Y-m');
        
        $post           = $this->model_api->issue_by_outlet($outlet_id,$date);
        //print_r($post);
        if(count($post) == 0)
        {
            $this->response([
                'status'    => TRUE,
                'message'   => 'Issue Detail Not Found',
                'count'     => count($post),
                'month'     => date('M Y',strtotime($date)),
                'total'     => 0,
                'data'      => array()
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            $total = '0';
            foreach($post as $row_key)
            {
                $data = [
                    'issue_id'          => $row_key->issue_id,
                    'issue_qty'         => $row_key->issue_qty,
                    'issue_price'       => $row_key->issue_price,
                    'issue_summary'     => $row_key->issue_summary,
                    'issue_date'        => date('d M Y, H:i:s',strtotime($row_key->date_time)),
                ];
                $json_result[]  = $data;
                $total          += $row_key->issue_price;
            }
            $json_status = ([
                'status'    => TRUE,
                'message'   => 'Detail Issue',
                'count'     => count($post),
                'month'     => date('M Y',strtotime($date)),
                'total'     => $total,
                'data'      => $json_result
            ]);
            $this->response($json_status, REST_Controller::HTTP_OK);
        }
    }
    public function issue_add_post()
    {
        $this->load->model('themes/'.$this->config->item('themes').'/model_api');
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_id', 'User ID','trim|required');
        $this->form_validation->set_rules('outlet_id','Outlet ID','trim|required');
        $this->form_validation->set_rules('issue_qty','QTY','trim|required');
        $this->form_validation->set_rules('issue_price','Price','trim|required');
        $this->form_validation->set_rules('issue_summary','Summary','trim|required');
        
        if ($this->form_validation->run() === FALSE)
        {
            $this->load->helper('form');
            $this->response([
                'status' => FALSE,
                'message' => 'Add Issue Data Failed'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            $user_id        = $this->input->post('user_id');
            
            $outlet_id      = $this->input->post('outlet_id');
            $issue_qty      = $this->input->post('issue_qty');
            $issue_price    = $this->input->post('issue_price');
            $issue_summary  = $this->input->post('issue_summary');
            $date           = date('Y-m-d');
            $date_time      = date('Y-m-d H:i:s');

            if($user_id!='' AND $outlet_id!='')
            {
                $data_array = array(
                    'user_id'       => $user_id,
                    'outlet_id'     => $outlet_id,
                    'issue_price'   => $issue_price,
                    'issue_qty'     => $issue_qty,
                    'issue_summary' => $issue_summary,
                    'date'          => $date,
                    'date_time'     => $date_time
                );
                $this->model_api->issue_add($data_array);

                $json_result[]  = $data_array;

                $this->response([
                    'status'            => TRUE,
                    'message'           => 'Issue Successfully Added',
                    'data'              => $json_result
                    
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response([
                    'status'            => TRUE,
                    'message'           => 'Add Issue Data Error'
                    
                ], REST_Controller::HTTP_OK);
            }
        }
    }
    public function post_edit_post()
    {
        $this->load->model('themes/'.$this->config->item('themes').'/model_api');
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('post_title', 'Post Title','trim|required');
        $this->form_validation->set_rules('post_content','Post Content','trim|required');

        file_put_contents('upload/files/json_123.json', json_encode($this->input->post()), FILE_APPEND);

        //$post_id    = (int)$this->uri->segment(4);
        $post_id    = $this->input->post('post_id');

        $post       = $this->model_api->po_edit($post_id);
        
        if ($this->form_validation->run() === FALSE)
        {
            $this->load->helper('form');
            $json_status = ([
                'status' => TRUE,
                'message' => 'Error Detail Update s',
                'count' => 0,
                'month' => date('Y-m-d H:i:s'),
                'data' =>  array()
            ]);
            $this->response($json_status, REST_Controller::HTTP_OK);
        }
        else
        {
            if(count($post)>0)
            {
                // $user_id                = $this->input->post('user_id');
                // $cat_id                 = ($this->input->post('cat_id')=='') ? '0' : $this->input->post('cat_id');
                // $post_up                = ($this->input->post('post_up')=='') ? '0' : $this->input->post('post_up');
                $post_slug              = strtolower(url_title($this->input->post('post_title')));
                $post_title             = $this->input->post('post_title');         
                $post_content           = $this->input->post('post_content');
                // $post_summary           = $this->input->post('post_summary');
                $data_array = array(  
                    'post_title'    => $post_title,
                    'post_content'  => $post_content,  
                    // 'post_summary'  => $post_summary
                );  
                $this->model_api->po_update($post_id,$data_array);

                foreach($post as $row_key)
                {                
                    $data = [
                        'post_id'       => $row_key->post_id,
                        'user_id'       => $row_key->user_id,
                        'post_title'    => $post_title,
                        'post_content'  => $post_content,  
                        //'post_summary'  => $row_key->post_summary
                    ];
                    $json_result[]  = $data;
            
                    $json_status = ([
                        'status' => TRUE,
                        'message' => 'Post Detail Update',
                        'count' => count($post),
                        'date' => $row_key->date,
                        'data' => $json_result
                    ]);
                    $this->response($json_status, REST_Controller::HTTP_OK);
                }
            }
            else
            {
                $json_status = ([
                    'status' => TRUE,
                    'message' => 'Error Detail Update',
                    'count' => 0,
                    'date' => date('Y-m-d H:i:s'),
                    'data' =>  array()
                ]);
                $this->response($json_status, REST_Controller::HTTP_OK);
            }
        }
    }
    public function post_update_put()
    {
        $this->load->model('themes/'.$this->config->item('themes').'/model_api');
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('post_title', 'Post Title','trim|required');
        $this->form_validation->set_rules('post_content','Post Content','trim|required');

        //$post_id    = (int)$this->uri->segment(4);
        $post_id    = $this->input->post('post_id');

        $post       = $this->model_api->po_edit($post_id);
        
        if ($this->form_validation->run() === FALSE)
        {
            $this->load->helper('form');
            $json_status = ([
                'status' => TRUE,
                'message' => 'Error Detail Update s',
                'count' => 0,
                'month' => date('Y-m-d H:i:s'),
                'data' =>  array()
            ]);
            $this->response($json_status, REST_Controller::HTTP_OK);
        }
        else
        {
            if(count($post)>0)
            {
                // $user_id                = $this->input->post('user_id');
                // $cat_id                 = ($this->input->post('cat_id')=='') ? '0' : $this->input->post('cat_id');
                // $post_up                = ($this->input->post('post_up')=='') ? '0' : $this->input->post('post_up');
                $post_slug              = strtolower(url_title($this->input->post('post_title')));
                $post_title             = $this->input->post('post_title');         
                $post_content           = $this->input->post('post_content');
                // $post_summary           = $this->input->post('post_summary');
                $data_array = array(  
                    'post_title'    => $post_title,
                    'post_content'  => $post_content,  
                    // 'post_summary'  => $post_summary
                );  
                $this->model_api->po_update($post_id,$data_array);

                foreach($post as $row_key)
                {                
                    $data = [
                        'post_id'       => $row_key->post_id,
                        'user_id'       => $row_key->user_id,
                        'post_title'    => $post_title,
                        'post_content'  => $post_content,  
                        //'post_summary'  => $row_key->post_summary
                    ];
                    $json_result[]  = $data;
            
                    $json_status = ([
                        'status' => TRUE,
                        'message' => 'Post Detail Update',
                        'count' => count($post),
                        'date' => $row_key->date,
                        'data' => $json_result
                    ]);
                    $this->response($json_status, REST_Controller::HTTP_OK);
                }
            }
            else
            {
                $json_status = ([
                    'status' => TRUE,
                    'message' => 'Error Detail Update',
                    'count' => 0,
                    'date' => date('Y-m-d H:i:s'),
                    'data' =>  array()
                ]);
                $this->response($json_status, REST_Controller::HTTP_OK);
            }
        }
    }
 //    public function user_register_post()
 //    {
 //        $this->load->model('themes/'.$this->config->item('themes').'/model_api');
 //    	$this->load->model('themes/'.$this->config->item('themes').'/model_user');
    	
 //    	$this->load->library('form_validation');
	// 	$this->form_validation->set_rules('user_fullname', 'Fullname','trim|required');
	// 	//$this->form_validation->set_rules('user_name','Username','trim|required|is_unique[my_user.user_name]');
	// 	$this->form_validation->set_rules('user_email','Email','trim|valid_email|required');
	// 	$this->form_validation->set_rules('user_password','Password','trim|min_length[5]|max_length[20]|required');
        
        
 //        if ($this->form_validation->run() === FALSE)
 //        {
	// 		$this->load->helper('form');
 //            $this->response([
	//             'status'            => FALSE,
	//             'message'           => 'Register Failed'
	//         ], REST_Controller::HTTP_OK);
 //        }
 //        else
 //        {
 //            $unique                 = random_string('alnum',50);//$this->model_api->hashSSHA($this->input->post('user_password'));
 //            $first_name             = $this->input->post('user_fullname');
	// 		$last_name 	            = '';//$this->input->post('user_last_name');
	// 		$username 	            = $this->input->post('user_email');//$this->input->post('user_name');
	// 		$email 		            = $this->input->post('user_email');
 //            $password               = $this->input->post('user_password');
	// 		$user_join 	            = date('Y-m-d H:i:s');
		 
	// 		$additional_data = array(
 //                'user_unique'       => $unique,
	// 			'user_first_name' 	=> $first_name,
 //                'user_last_name'    => $last_name,
	// 			'user_join' 		=> $user_join
	// 		);
	// 		$group = array('2'); // Sets user to admin.
		 
	// 		$this->load->library('ion_auth');
	// 		if($this->ion_auth->register($username,$password,$email,$additional_data,$group))
	// 		{
	// 			$row_user           = $this->model_api->user_detail($username);
          
 //            	$this->response([
	// 	            'status'        => TRUE,
	// 	            'message'       => 'Register Successfully, Please Check Email For Confirmation',
 //                    'user_id'       => $row_user->user_id,
	// 	            'user_unique'   => $row_user->user_unique,
 //                    'user_role'     => $row_user->role_name,
 //                    'user_fullname' => $row_user->user_first_name,
 //                    'user_email'    => $row_user->user_email,
 //                    'user_avatar'   => base_url().'upload/avatar/'.$row_user->user_avatar,
 //                    'user_join'     => $row_user->user_join
		            
	// 	        ], REST_Controller::HTTP_OK);
	// 		}
	// 		else
	// 		{
	// 			$this->response([
	// 	            'status' => FALSE,
	// 	            'message' => $this->ion_auth->errors()
	// 	        ], REST_Controller::HTTP_OK);
	// 		}
 //        }
 //    }
 //    public function user_logout_post()
 //    {
 //    	$this->ion_auth->logout();
 //        $this->response([
 //            'status' => TRUE,
 //            'message' => 'Logout Succesfully'
 //        ], REST_Controller::HTTP_OK);
 //    }
    public function user_detail_get()
    {
    	$this->load->model('themes/'.$this->config->item('themes').'/model_api');
    	
    	$id = (int)$this->uri->segment(4);
    	
        if($id == 0)
        {
            $this->response([
	            'status' => TRUE,
	            'message' => 'Request Not found'
	        ], REST_Controller::HTTP_OK);
        }
        else
        {
            $row_user   = $this->model_api->user_edit($id);
            $outlet     = $this->model_api->outlet_by_user($id);

            if(count($row_user) == 0)
            {
                $this->response([
                    'status' => TRUE,
                    'message' => 'Request Not found'
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                
                foreach($outlet as $row_key)
                {
                    $data_outlet = array(
                        'outlet_id'    => $row_key->outlet_id,
                        'outlet_name'  => $row_key->outlet_name,
                        'outlet_province'  => $row_key->outlet_province,
                    );
                    $json_outlet[]  = $data_outlet;
                }
                $json_result = array([
                    'user_id'           => $row_user->user_id,
                    'user_unique'       => $row_user->user_unique,
                    'user_role'         => $row_user->role_name,
                    'user_gender'       => $row_user->user_gender,
                    'user_first_name'   => $row_user->user_first_name,
                    'user_last_name'    => $row_user->user_last_name,
                    'user_email'        => $row_user->user_email,
                    'user_mobile'       => $row_user->user_mobile,
                    'user_address'      => $row_user->user_address,
                    'user_city'         => $row_user->user_city,
                    'user_province'     => $row_user->user_province,
                    'user_country'      => $row_user->user_country,
                    'user_postal'       => $row_user->user_postal,
                    'user_avatar'       => base_url().'upload/avatar/'.$row_user->user_avatar,
                    'user_join'         => date('d, D M Y',strtotime($row_user->user_join))
                ]);
                $json_status = ([
                    'status' => TRUE,
                    'message' => 'User Detail',
                    'uid' => $row_user->user_unique,
                    'data' => $json_result,
                    'outlet' => $json_outlet
                ]);
                $this->response($json_status, REST_Controller::HTTP_OK);
            }
        }
    }
    public function user_detail_unique_get()
    {
        $this->load->model('themes/'.$this->config->item('themes').'/model_api');
        
        $user_unique = $this->uri->segment(4);
        
        if($user_unique == '')
        {
            $this->response([
                'status' => TRUE,
                'message' => 'Request Not found'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            $row_user = $this->model_api->user_edit_unique($user_unique);

            if(count($row_user) == 0)
            {
                $this->response([
                    'status' => TRUE,
                    'message' => 'Request Not found'
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                $json_result = array([
                    'user_id'           => $row_user->user_id,
                    'user_unique'       => $row_user->user_unique,
                    'user_role'         => $row_user->role_name,
                    'user_gender'       => $row_user->user_gender,
                    'user_first_name'   => $row_user->user_first_name,
                    'user_last_name'    => $row_user->user_last_name,
                    'user_email'        => $row_user->user_email,
                    'user_mobile'       => $row_user->user_mobile,
                    'user_address'      => $row_user->user_address,
                    'user_city'         => $row_user->user_city,
                    'user_province'     => $row_user->user_province,
                    'user_country'      => $row_user->user_country,
                    'user_postal'       => $row_user->user_postal,
                    'user_avatar'       => base_url().'upload/avatar/'.$row_user->user_avatar,
                    'user_join'         => date('d, D M Y',strtotime($row_user->user_join))
                ]);
                $json_status = ([
                    'status' => TRUE,
                    'message' => 'User Detail',
                    'uid' => $row_user->user_unique,
                    'data' => $json_result
                ]);
                $this->response($json_status, REST_Controller::HTTP_OK);
            }
        }
    }
    public function user_update_post()
    {
        $this->load->model('themes/'.$this->config->item('themes').'/model_api');
        $this->load->model('themes/'.$this->config->item('themes').'/model_user');
        
        $id = (int)$this->uri->segment(4);
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_first_name', 'First Name','trim|required');
        $this->form_validation->set_rules('user_last_name', 'Last Name','trim');
        $this->form_validation->set_rules('user_password','Password','trim|min_length[5]|max_length[20]');
        $this->form_validation->set_rules('user_mobile','Phone Number','trim');
        $this->form_validation->set_rules('user_address','Address','trim');
        $this->form_validation->set_rules('user_city','City','trim');
        $this->form_validation->set_rules('user_province','Province','trim');
        $this->form_validation->set_rules('user_country','Country','trim');
        $this->form_validation->set_rules('user_postal','Postal','trim');
        
        if ($this->form_validation->run() === FALSE)
        {
            $this->load->helper('form');
            $this->response([
                'status' => FALSE,
                'message' => 'Update User Data Failed'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            if($id == 0)
            {
                $this->response([
                    'status' => TRUE,
                    'message' => 'Request Not found'
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                //$user_id         = $this->input->post('user_id');
                $user_first_name = $this->input->post('user_first_name');
                $user_last_name  = $this->input->post('user_last_name');
                $user_password   = $this->input->post('user_password');
                $user_mobile     = $this->input->post('user_mobile');
                $user_address    = $this->input->post('user_address');
                $user_city       = $this->input->post('user_city');
                $user_province   = $this->input->post('user_province');
                $user_country    = $this->input->post('user_country');
                $user_postal     = $this->input->post('user_postal');
             
                $data_array = array(
                    'user_first_name'   => $user_first_name,
                    'user_last_name'    => $user_last_name,
                    'user_password'     => $user_password,
                    'user_mobile'       => $user_mobile,
                    'user_address'      => $user_address,
                    'user_city'         => $user_city,
                    'user_province'     => $user_province,
                    'user_country'      => $user_country,
                    'user_postal'       => $user_postal,

                );
                $this->ion_auth->update($id,$data_array);

                $row_user = $this->model_api->user_edit($id);
                $json_result = array([
                    'user_id'           => $row_user->user_id,
                    'user_unique'       => $row_user->user_unique,
                    'user_role'         => $row_user->role_name,
                    'user_gender'       => $row_user->user_gender,
                    'user_first_name'   => $row_user->user_first_name,
                    'user_last_name'    => $row_user->user_last_name,
                    'user_email'        => $row_user->user_email,
                    'user_mobile'       => $row_user->user_mobile,
                    'user_address'      => $row_user->user_address,
                    'user_city'         => $row_user->user_city,
                    'user_province'     => $row_user->user_province,
                    'user_country'      => $row_user->user_country,
                    'user_postal'       => $row_user->user_postal,
                    'user_avatar'       => base_url().'upload/avatar/'.$row_user->user_avatar,
                    'user_join'         => date('d, D M Y',strtotime($row_user->user_join))
                ]);
                $this->response([
                    'status'            => TRUE,
                    'message'           => 'User Successfully Update',
                    'uid'               => $row_user->user_unique,
                    'data'              => $json_result
                    
                ], REST_Controller::HTTP_OK);
            }
        }
    }
    
 //    public function user_list_get()
 //    {
 //    	$this->load->model('themes/'.$this->config->item('themes').'/model_api');
 //    	$users = $this->model_api->user_list();
    	
 //        if ($users)
 //        {
 //            $this->response($users, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
 //        }
 //        else
 //        {
 //            $this->response([
 //                'status' => FALSE,
 //                'message' => 'No users were found'
 //            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
 //        }
	// }
 //    public function users_list_delete()
 //    {
 //        //$id = (int) $this->get('id');
 //        $id = (int) $this->uri->segment(4);

 //        // Validate the id.
 //        if ($id <= 0)
 //        {
 //            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
 //        }
	// 	else
	// 	{
	// 		$this->load->model('back/model_user');
 //        	$this->model_user->us_delete($id);
	// 	}
        
 //        $message = [
 //            'id' => $id,
 //            'message' => 'Deleted the resource'
 //        ];

 //        $this->set_response($message, REST_Controller::HTTP_OK); // NO_CONTENT (204) being the HTTP response code
 //    }
}
