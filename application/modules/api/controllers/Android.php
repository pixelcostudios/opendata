<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class android extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        // header('Content-type=application/json; charset=utf-8');
        // header('Access-Control-Allow-Origin: *');
        // header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
		$this->load->library('ion_auth');
        $this->load->helper(array('form', 'url', 'string'));
        $this->load->helper(array('path'));
        $this->load->model('model_android');
    }
 	public function index_get()
    {
    	$this->response([
            'status' => TRUE,
            'error' => FALSE,
            'message' => 'Request Not found'
        ], REST_Controller::HTTP_OK);
    }
    public function index_post()
    {
        $this->response([
            'status' => TRUE,
            'error' => FALSE,
            'message' => 'Request Post Not found'
        ], REST_Controller::HTTP_OK);
    }
    public function user_login_post()
    {
        $this->load->model('model_android');
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_email', 'User Email', 'trim|required');
        $this->form_validation->set_rules('user_password', 'Password', 'required');
        if ($this->form_validation->run() === FALSE)
        {
            $this->load->helper('form');
            $this->response([
                'status' => FALSE,
                'error' => TRUE,
                // 'message' => 'Login Failed',
                'message' => 'Login credentials are wrong. Please try again!'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            $remember = (bool) $this->input->post('remember');
            $identity = $this->input->post('user_email');
            $password = $this->input->post('user_password');
            if ($this->ion_auth->login($identity, $password, $remember))
            {
                $user_unique            = $this->model_android->char_random('25');
                $data_array = array(
                    'user_unique'       => $user_unique
                );
                $row_user   = $this->model_android->user_detail_email($identity);
                $this->model_android->user_update($row_user->user_id,$data_array);
               
                $json_result = array(
                    'user_id'           => $row_user->user_id,
                    'user_unique'       => $user_unique,
                    // 'user_role'         => $row_user->role_name,
                    // 'user_gender'       => $row_user->user_gender,
                    'user_first_name'   => $row_user->user_first_name,
                    'user_last_name'    => $row_user->user_last_name,
                    'user_email'        => $row_user->user_email,
                    'user_mobile'       => $row_user->user_mobile,
                    'user_address'      => $row_user->user_address,
                    'user_city'         => $row_user->user_city,
                    'user_province'     => $row_user->user_province,
                    'user_country'      => $row_user->user_country,
                    //'user_postal'       => $row_user->user_postal,
                    'user_avatar'       => base_url().'upload/avatar/'.$row_user->user_avatar,
                    'user_join'         => $row_user->user_join
                );
                $this->response([
                    'status'            => TRUE,
                    'error'             => FALSE,
                    'message'           => 'Login Successfully',  
                    'data'              => $json_result
                    
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'error' => FALSE,
                    'message' => $this->ion_auth->errors()
                ], REST_Controller::HTTP_OK);
            }
        }
    }
    public function post_activities_get()
    {
        $this->load->model('model_android');
        //$this->load->model('themes/'.$this->config->item('themes').'/model_post');
        
        $user_id = $this->uri->segment('4');
        $post = $this->model_android->po_type_user($user_id,'activities','DESC');
        if(count($post)>0)
        {
            foreach($post as $row)
            {
                $thumbnail             = $this->model_android->ip_value($row->post_id,'post','ASC','ipost');
                $thumbnail             = count($thumbnail>0) ? base_url().'upload/crop/250/250/'.$thumbnail : base_url().'upload/default.png';
                $data = [
                    'post_id'          => $row->post_id,
                    'post_title'       => $row->post_title,
                    'post_type'        => $row->post_type,
                    // 'post_slug'        => $row->post_slug,
                    'post_content'     => substr(strip_tags($row->post_content),0,95),
                    'category'         => '',
                    'date'             => $row->date,
                    'thumbnail'        => $thumbnail
                ];
                $result[] = $data;
            }
            $json_status = ([
                'status' => TRUE,
                'message' => 'Activities',
                // 'page'  => '1',
                'total'  => count($post),
                'data' => $result
            ]);
            $this->response($json_status, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response([
                'status' => FALSE,
                'message' => 'No Activities were found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }
    public function post_event_get()
    {
        $this->load->model('model_android');
        //$this->load->model('themes/'.$this->config->item('themes').'/model_post');
        
        $user_id = $this->uri->segment('4');
        $post = $this->model_android->po_type_user($user_id,'event','DESC');
        if(count($post)>0)
        {
            foreach($post as $row)
            {
                $thumbnail             = $this->model_android->ip_value($row->post_id,'post','ASC','ipost');
                $thumbnail             = count($thumbnail>0) ? base_url().'upload/crop/250/250/'.$thumbnail : base_url().'upload/default.png';
                $data = [
                    'post_id'          => $row->post_id,
                    'post_title'       => $row->post_title,
                    'post_type'        => $row->post_type,
                    // 'post_slug'        => $row->post_slug,
                    'post_content'     => substr(strip_tags($row->post_content),0,95),
                    'category'         => '',
                    'date'             => $row->date,
                    'thumbnail'        => $thumbnail
                ];
                $result[] = $data;
            }
            $json_status = ([
                'status' => TRUE,
                'message' => 'List Event',
                // 'page'  => '1',
                'total'  => count($post),
                'data' => $result
            ]);
            $this->response($json_status, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response([
                'status' => FALSE,
                'message' => 'No Event were found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }
    public function post_training_get()
    {
        $user_id = $this->uri->segment('4');
        $post = $this->model_android->po_type_user($user_id,'training','DESC');
        if(count($post)>0)
        {
            foreach($post as $row)
            {
                $thumbnail             = $this->model_android->ip_value($row->post_id,'post','ASC','ipost');
                $thumbnail             = count($thumbnail>0) ? base_url().'upload/crop/250/250/'.$thumbnail : base_url().'upload/default.png';
                $data = [
                    'post_id'          => $row->post_id,
                    'post_title'       => $row->post_title,
                    'post_type'        => $row->post_type,
                    // 'post_slug'        => $row->post_slug,
                    'post_content'     => substr(strip_tags($row->post_content),0,95),
                    'category'         => '',
                    'date'             => $row->date,
                    'thumbnail'        => $thumbnail
                ];
                $result[] = $data;
            }
            $json_status = ([
                'status' => TRUE,
                'message' => 'List Training',
                // 'page'  => '1',
                'total'  => count($post),
                'data' => $result
            ]);
            $this->response($json_status, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response([
                'status' => FALSE,
                'message' => 'No Training were found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }
    public function post_detail_get()
    {
        $post = $this->model_android->po_id($this->uri->segment(4));
        //print_r($post);
        if(count($post)>0)
        {
            foreach($post as $row)
            {
                $thumbnail_url             = $this->model_android->ip_value($row->post_id,'post','ASC','ipost');
                $thumbnail             = count($thumbnail_url>0) ? base_url().'upload/'.$thumbnail_url : base_url().'upload/default.png';
                $data = array([
                    'post_id'          => $row->post_id,
                    'post_title'       => $row->post_title,
                    // 'post_slug'        => $row->post_slug,
                    'post_content'     => $row->post_content,
                    'category'         => 'Category',//$row->post_content,
                    'date'             => $row->date,
                    'thumbnail'        => $thumbnail
                ]);
                $result = $data;
            }
            $json_status = ([
                'status' => TRUE,
                'message' => 'Post Detail',
                'data' => $result
            ]);
            $this->response($json_status, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response([
                'status' => FALSE,
                'message' => 'No Posts were found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }
    
    public function post_edit_post()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('post_title', 'Post Title','trim|required');
        $this->form_validation->set_rules('post_content','Post Content','trim|required');

        //file_put_contents('upload/files/json_123.json', json_encode($this->input->post()), FILE_APPEND);

        //$post_id    = (int)$this->uri->segment(4);
        $post_id    = $this->input->post('post_id');

        $post       = $this->model_api->po_edit($post_id);
        
        if ($this->form_validation->run() === FALSE)
        {
            $this->load->helper('form');
            $json_status = ([
                'status' => TRUE,
                'message' => 'Error Detail Update',
                'count' => 0,
                'month' => date('Y-m-d H:i:s'),
                'data' =>  array()
            ]);
            $this->response($json_status, REST_Controller::HTTP_OK);
        }
        else
        {
            if(count($post)>0)
            {
                // $user_id                = $this->input->post('user_id');
                // $cat_id                 = ($this->input->post('cat_id')=='') ? '0' : $this->input->post('cat_id');
                // $post_up                = ($this->input->post('post_up')=='') ? '0' : $this->input->post('post_up');
                $post_slug              = strtolower(url_title($this->input->post('post_title')));
                $post_title             = $this->input->post('post_title');         
                $post_content           = $this->input->post('post_content');
                // $post_summary           = $this->input->post('post_summary');
                $data_array = array(  
                    'post_title'    => $post_title,
                    'post_content'  => $post_content,  
                    // 'post_summary'  => $post_summary
                );  
                $this->model_api->po_update($post_id,$data_array);

                foreach($post as $row_key)
                {                
                    $data = [
                        'post_id'       => $row_key->post_id,
                        'user_id'       => $row_key->user_id,
                        'post_title'    => $post_title,
                        'post_content'  => $post_content,  
                        //'post_summary'  => $row_key->post_summary
                    ];
                    $json_result[]  = $data;
            
                    $json_status = ([
                        'status' => TRUE,
                        'message' => 'Post Detail Update',
                        'count' => count($post),
                        'date' => $row_key->date,
                        'data' => $json_result
                    ]);
                    $this->response($json_status, REST_Controller::HTTP_OK);
                }
            }
            else
            {
                $json_status = ([
                    'status' => TRUE,
                    'message' => 'Error Detail Update',
                    'count' => 0,
                    'date' => date('Y-m-d H:i:s'),
                    'data' =>  array()
                ]);
                $this->response($json_status, REST_Controller::HTTP_OK);
            }
        }
    }
    public function post_update_post()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('post_title', 'Post Title','trim|required');
        $this->form_validation->set_rules('post_content','Post Content','trim|required');
        $this->form_validation->set_rules('category','Category','trim|required');
        $this->form_validation->set_rules('date','Date','trim|required');

        //$post_id    = (int)$this->uri->segment(4);
        $post_id    = (int)$this->uri->segment(4);//$this->input->post('post_id');
        $user_id    = (int)$this->uri->segment(5);

        $post       = $this->model_android->po_edit($post_id);
        
        if ($this->form_validation->run() === FALSE)
        {
            $this->load->helper('form');
            $json_status = ([
                'status' => TRUE,
                'message' => 'Error Detail Update',
                'count' => 0,
                'month' => date('Y-m-d H:i:s'),
                'data' =>  array()
            ]);
            $this->response($json_status, REST_Controller::HTTP_OK);
        }
        else
        {
            if(count($post)>0)
            {  
                $post_slug              = strtolower(url_title($this->input->post('post_title')));
                $post_title             = $this->input->post('post_title');         
                $post_content           = $this->input->post('post_content');
                $category               = $this->input->post('category');
                $date                   = $this->input->post('date');
                // $post_summary           = $this->input->post('post_summary');
                $data_array = array(
                    'post_slug'     => $post_slug,
                    'post_title'    => $post_title,
                    'post_content'  => $post_content,
                    'cat_id'        => $category,
                    'date'          => $date
                    // 'post_summary'  => $post_summary
                );  
                $this->model_android->po_update($user_id,$post_id,$data_array);

                foreach($post as $row_key)
                {                
                    $data = [
                        'post_id'       => $row_key->post_id,
                        'user_id'       => $row_key->user_id,
                        'post_title'    => $post_title,
                        'post_content'  => $post_content,
                        'date'          => $row_key->date,  
                        //'post_summary'  => $row_key->post_summary
                    ];
                    $json_result[]  = $data;
            
                    $json_status = ([
                        'status' => TRUE,
                        'message' => 'Post Detail Update',
                        'count' => count($post),
                        'date' => $row_key->date,
                        'data' => $json_result
                    ]);
                    $this->response($json_status, REST_Controller::HTTP_OK);
                }
            }
            else
            {
                $json_status = ([
                    'status' => TRUE,
                    'message' => 'Error Detail Update',
                    'count' => 0,
                    'date' => date('Y-m-d H:i:s'),
                    'data' =>  array()
                ]);
                $this->response($json_status, REST_Controller::HTTP_OK);
            }
        }
    }
    public function post_add_post()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('post_type', 'Post Type','trim|required');
        $this->form_validation->set_rules('post_title', 'Post Title','trim|required');
        $this->form_validation->set_rules('post_content','Post Content','trim|required');
        $this->form_validation->set_rules('category','Category','trim|required');
        $this->form_validation->set_rules('date','Date','trim|required');

        $user_id    = (int)$this->uri->segment(4);

        // $post       = $this->model_android->po_add($data_array);
        
        if ($this->form_validation->run() === FALSE)
        {
            $this->load->helper('form');
            $json_status = ([
                'status' => TRUE,
                'message' => 'Error Post Add',
                'data' =>  array()
            ]);
            $this->response($json_status, REST_Controller::HTTP_OK);
        }
        else
        {
            $post_type              = $this->input->post('post_type'); 
            $post_slug              = strtolower(url_title($this->input->post('post_title')));
            $post_title             = $this->input->post('post_title');         
            $post_content           = $this->input->post('post_content');
            $category               = $this->input->post('category');
            $date                   = $this->input->post('date');
            // $post_summary           = $this->input->post('post_summary');
            $data_array = array(  
                'user_id'       => $user_id,
                'post_type'     => $post_type,
                'post_slug'     => $post_slug,
                'post_title'    => $post_title,
                'post_content'  => $post_content,
                'cat_id'        => $category,
                'date'          => $date
                // 'post_summary'  => $post_summary
            ); 

            $this->model_android->po_add($data_array);
            $post_id 			 = $this->db->insert_id();

            $post                = $this->model_android->po_id($post_id);
            foreach($post as $row_key)
            {                
                $data = [
                    'post_id'       => $row_key->post_id,
                    'user_id'       => $row_key->user_id,
                    'post_title'    => $post_title,
                    'post_content'  => $post_content,
                    'date'          => $row_key->date,  
                    //'post_summary'  => $row_key->post_summary
                ];
                $json_result[]  = $data;
        
                $json_status = ([
                    'status' => TRUE,
                    'message' => 'Post Detail',
                    'count' => count($post),
                    'date' => $row_key->date,
                    'data' => $json_result
                ]);
                $this->response($json_status, REST_Controller::HTTP_OK);
            }


            // if(count($post)>0)
            // {
            //     $post_slug              = strtolower(url_title($this->input->post('post_title')));
            //     $post_title             = $this->input->post('post_title');         
            //     $post_content           = $this->input->post('post_content');
            //     $category               = $this->input->post('category');
            //     $date                   = $this->input->post('date');
            //     // $post_summary           = $this->input->post('post_summary');
            //     $data_array = array(  
            //         'post_title'    => $post_title,
            //         'post_content'  => $post_content,
            //         'date'          => $date
            //         // 'post_summary'  => $post_summary
            //     );  
            //     $this->model_android->po_update($user_id,$post_id,$data_array);

            //     foreach($post as $row_key)
            //     {                
            //         $data = [
            //             'post_id'       => $row_key->post_id,
            //             'user_id'       => $row_key->user_id,
            //             'post_title'    => $post_title,
            //             'post_content'  => $post_content,
            //             'date'          => $row_key->date,  
            //             //'post_summary'  => $row_key->post_summary
            //         ];
            //         $json_result[]  = $data;
            
            //         $json_status = ([
            //             'status' => TRUE,
            //             'message' => 'Post Detail Update',
            //             'count' => count($post),
            //             'date' => $row_key->date,
            //             'data' => $json_result
            //         ]);
            //         $this->response($json_status, REST_Controller::HTTP_OK);
            //     }
            // }
            // else
            // {
            //     $json_status = ([
            //         'status' => TRUE,
            //         'message' => 'Error Add',
            //         'count' => 0,
            //         'date' => date('Y-m-d H:i:s'),
            //         'data' =>  array()
            //     ]);
            //     $this->response($json_status, REST_Controller::HTTP_OK);
            // }
        }
    }

    public function category_institute_get()
    {
        $this->load->model('model_android');
        
        $post = $this->model_android->ca_list('institute','0','DESC');
        if(count($post)>0)
        {
            foreach($post as $row)
            {
                //$thumbnail             = $this->model_android->ip_value($row->post_id,'post','ASC','ipost');
                $data = [
                    'cat_id'          => $row->cat_id,
                    'cat_title'       => $row->cat_title,
                    //'thumbnail'        => $thumbnail
                ];
                $result[] = $data;
            }
            $json_status = ([
                'status' => TRUE,
                'message' => 'Category Institute',
                'total'  => count($post),
                'data' => $result
            ]);
            $this->response($json_status, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response([
                'status' => FALSE,
                'message' => 'No Category Institute were found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }
    public function category_type_get()
    {
        $cat_id = $this->uri->segment('4');

        if($cat_id!='')
        {
            $post = $this->model_android->ca_list('type',$cat_id,'ASC');
            if(count($post)>0)
            {
                foreach($post as $row)
                {
                    $data = [
                        'cat_id'          => $row->cat_id,
                        'cat_title'       => $row->cat_title
                    ];
                    $result_parent[] = $data;
                        
                    $post_1 = $this->model_android->ca_list('type',$row->cat_id,'ASC');
                    //$thumbnail             = $this->model_android->ip_value($row->post_id,'post','ASC','ipost');
                    if(count($post_1)>0)
                    {
                        foreach($post_1 as $row_1)
                        {
                            $data_1 = [
                                'cat_id'          => $row_1->cat_id,
                                'cat_title'       => '- '.$row_1->cat_title
                            ];
                            $result_parent_1[] = $data_1;

                            $post_2 = $this->model_android->ca_list('type',$row->cat_id,'ASC');
                            if(count($post_1)>0)
                            {
                                foreach($post_2 as $row_2)
                                {
                                    $data_2 = [
                                        'cat_id'          => $row_2->cat_id,
                                        'cat_title'       => '-- '.$row_2->cat_title
                                    ];
                                    $result_2[] = $data_2;
                                }
                                $result2 = array_merge($result_parent_1,$result_2);
                            }
                            else 
                            {
                                $result2[] = $data_1;
                            }
                        }
                        $resultnya = array_merge($result_parent,$result2);
                    }
                    else 
                    {
                        $resultnya[] = $data;
                    }
                    
                }
                $json_status = ([
                    'status' => TRUE,
                    'message' => 'Category Type',
                    'total'  => count($post),
                    'data' => $resultnya
                ]);
                $this->response($json_status, REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No Category Type were found'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        }
        else 
        {
            $post = $this->model_android->ca_list('type','0','ASC');
            if(count($post)>0)
            {
                foreach($post as $row)
                {
                    $data = [
                        'cat_id'          => $row->cat_id,
                        'cat_title'       => $row->cat_title
                    ];
                    $result_parent[] = $data;
                }
                $json_status = ([
                    'status' => TRUE,
                    'message' => 'Category Type Parent',
                    'total'  => count($post),
                    'data' => $result_parent
                ]);
                $this->response($json_status, REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No Category Type were found'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }
    public function user_register_post()
    {	
    	$this->load->library('form_validation');
        $this->form_validation->set_rules('user_fullname', 'Fullname','trim|required');
        $this->form_validation->set_rules('user_ktp', 'KTP','trim|required');
        $this->form_validation->set_rules('user_birthday', 'Birthday','trim|required');
		//$this->form_validation->set_rules('user_name','Username','trim|required|is_unique[my_user.user_name]');
		$this->form_validation->set_rules('user_email','Email','trim|valid_email|required');
		$this->form_validation->set_rules('user_password','Password','trim|min_length[5]|max_length[20]|required');
        
        
        if ($this->form_validation->run() === FALSE)
        {
			$this->load->helper('form');
            $this->response([
	            'status'            => FALSE,
	            'message'           => 'Register Failed'
	        ], REST_Controller::HTTP_OK);
        }
        else
        {
            $unique                 = random_string('alnum',50);//$this->model_api->hashSSHA($this->input->post('user_password'));
            $first_name             = $this->input->post('user_fullname');
			$username 	            = $this->input->post('user_email');//$this->input->post('user_name');
            $email 		            = $this->input->post('user_email');
            $phone 		            = $this->input->post('user_phone');
            $ktp 		            = $this->input->post('user_ktp');
            $birthday 		        = $this->input->post('user_birthday');
            $password               = $this->input->post('user_password');
            $address                = $this->input->post('user_address');
			$user_join 	            = date('Y-m-d H:i:s');
		 
			$additional_data = array(
                'user_unique'       => $unique,
                'user_ktp' 	        => $ktp,
                'user_phone' 	    => $phone,
                'user_first_name' 	=> $first_name,
                'user_birthday'     => $birthday,
                'user_address'      => $address,
				'user_join' 		=> $user_join
			);
			$group = array('3'); // Sets user to admin.
		 
            $this->load->library('ion_auth');
            $id = $this->ion_auth->register($username,$password,$email,$additional_data,$group);
			if($id)
			{
				$row_user           = $this->model_android->user_detail($username);
                $data = [
                    'user_id'       => $row_user->user_id,
		            'user_unique'   => $row_user->user_unique,
                    'user_role'     => $row_user->role_name,
                    'user_phone'    => $row_user->user_phone,
                    'user_fullname' => $row_user->user_first_name,
                    'user_address'  => $row_user->user_address,
                    'user_email'    => $row_user->user_email,
                    'user_avatar'   => base_url().'upload/avatar/'.$row_user->user_avatar,
                    'user_join'     => $row_user->user_join
                ];
            	$json_result = array([
		            'status'        => TRUE,
                    'message'       => 'Register Successfully',
                    'data'          => $data,
		        ], REST_Controller::HTTP_OK);
			}
			else
			{
				$this->response([
		            'status' => FALSE,
                    'message' => $this->ion_auth->errors(),
		        ], REST_Controller::HTTP_OK);
			}
        }
    }
 //    public function user_logout_post()
 //    {
 //    	$this->ion_auth->logout();
 //        $this->response([
 //            'status' => TRUE,
 //            'message' => 'Logout Succesfully'
 //        ], REST_Controller::HTTP_OK);
 //    }
    public function user_detail_get()
    {
    	$id = (int)$this->uri->segment('4');
    	
        if($id == 0)
        {
            $this->response([
	            'status' => TRUE,
	            'message' => 'Request Not found'
	        ], REST_Controller::HTTP_OK);
        }
        else
        {
            $user   = $this->model_android->user_edit($id);

            if(count($user) == 0)
            {
                $this->response([
                    'status' => TRUE,
                    'message' => 'Request Not found'
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                foreach ($user as $row_user) 
                {
                    $data = array([
                        'user_id'           => $row_user->user_id,
                        'user_unique'       => $row_user->user_unique,
                        'user_role'         => $row_user->role_name,
                        'user_fullname'     => $row_user->user_first_name.' '.$row_user->user_last_name,
                        'user_email'        => $row_user->user_email,
                        'user_mobile'        => $row_user->user_mobile,
                        'user_address'      => $row_user->user_address,
                        'user_avatar'       => base_url().'upload/avatar/'.$row_user->user_avatar,
                        'user_join'         => date('d, D M Y',strtotime($row_user->user_join))
                    ]);
                    $json_status = ([
                        'status' => TRUE,
                        'message' => 'User Detail',
                        'uid' => $row_user->user_unique,
                        'data' => $data
                    ]);
                    $this->response($json_status, REST_Controller::HTTP_OK);
                }
            }
        }
    }
    public function user_detail_unique_get()
    {
        $user_unique = $this->uri->segment(4);
        
        if($user_unique == '')
        {
            $this->response([
                'status' => TRUE,
                'message' => 'Request Not found'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            $row_user = $this->model_api->user_edit_unique($user_unique);

            if(count($row_user) == 0)
            {
                $this->response([
                    'status' => TRUE,
                    'message' => 'Request Not found'
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                $json_result = array([
                    'user_id'           => $row_user->user_id,
                    'user_unique'       => $row_user->user_unique,
                    'user_role'         => $row_user->role_name,
                    'user_gender'       => $row_user->user_gender,
                    'user_first_name'   => $row_user->user_first_name,
                    'user_last_name'    => $row_user->user_last_name,
                    'user_email'        => $row_user->user_email,
                    'user_mobile'       => $row_user->user_mobile,
                    'user_address'      => $row_user->user_address,
                    'user_city'         => $row_user->user_city,
                    'user_province'     => $row_user->user_province,
                    'user_country'      => $row_user->user_country,
                    'user_postal'       => $row_user->user_postal,
                    'user_avatar'       => base_url().'upload/avatar/'.$row_user->user_avatar,
                    'user_join'         => date('d, D M Y',strtotime($row_user->user_join))
                ]);
                $json_status = ([
                    'status' => TRUE,
                    'message' => 'User Detail',
                    'uid' => $row_user->user_unique,
                    'data' => $json_result
                ]);
                $this->response($json_status, REST_Controller::HTTP_OK);
            }
        }
    }
    public function user_update_post()
    {
        $id = (int)$this->uri->segment(4);
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_fullname', 'FUllname','trim|required');
        // $this->form_validation->set_rules('user_last_name', 'Last Name','trim');
        // $this->form_validation->set_rules('user_password','Password','trim|min_length[5]|max_length[20]');
        $this->form_validation->set_rules('user_mobile','Phone Number','trim');
        $this->form_validation->set_rules('user_address','Address','trim');
        // $this->form_validation->set_rules('user_city','City','trim');
        // $this->form_validation->set_rules('user_province','Province','trim');
        // $this->form_validation->set_rules('user_country','Country','trim');
        // $this->form_validation->set_rules('user_postal','Postal','trim');
        
        if ($this->form_validation->run() === FALSE)
        {
            $this->load->helper('form');
            $this->response([
                'status' => FALSE,
                'message' => 'Update User Data Failed'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            if($id == 0)
            {
                $this->response([
                    'status' => TRUE,
                    'message' => 'Request Not found'
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                //$user_id         = $this->input->post('user_id');
                $user_first_name = $this->input->post('user_fullname');
                $user_password   = $this->input->post('user_password');
                $user_mobile     = $this->input->post('user_mobile');
                $user_address    = $this->input->post('user_address');
                // $user_city       = $this->input->post('user_city');
                // $user_province   = $this->input->post('user_province');
                // $user_country    = $this->input->post('user_country');
                // $user_postal     = $this->input->post('user_postal');
                if($user_password=='')
                {
                    $data_array = array(
                        'user_first_name'   => $user_first_name,
                        'user_mobile'       => $user_mobile,
                        'user_address'      => $user_address
                    );
                }
                else {
                    $data_array = array(
                        'user_first_name'   => $user_first_name,
                        'user_password'     => $user_password,
                        'user_mobile'       => $user_mobile,
                        'user_address'      => $user_address
                    );
                }
                $this->ion_auth->update($id,$data_array);

                $user = $this->model_android->user_edit($id);
                
                foreach ($user as $row_user) 
                {
                    $json_result = array([
                        'user_id'           => $row_user->user_id,
                        'user_unique'       => $row_user->user_unique,
                        'user_role'         => $row_user->role_name,
                        'user_fullname'     => $row_user->user_first_name,
                        // 'user_last_name'    => $row_user->user_last_name,
                        'user_email'        => $row_user->user_email,
                        'user_mobile'       => $row_user->user_mobile,
                        'user_address'      => $row_user->user_address,
                        'user_avatar'       => base_url().'upload/avatar/'.$row_user->user_avatar,
                        'user_join'         => date('d, D M Y',strtotime($row_user->user_join))
                    ]);
                    $this->response([
                        'status'            => TRUE,
                        'message'           => 'User Successfully Update',
                        'uid'               => $row_user->user_unique,
                        'data'              => $json_result
                        
                    ], REST_Controller::HTTP_OK);
                }
            }
        }
    }
}