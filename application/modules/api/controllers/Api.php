<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */

class Api extends REST_Controller {

	function __construct()
    {
        // Construct the parent class
        // header('Content-type=application/json; charset=utf-8');
        // header('Access-Control-Allow-Origin: *');
        // header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
		$this->load->library('ion_auth');
        $this->load->helper(array('form', 'url', 'string'));
		$this->load->helper(array('path'));
    }
 	public function index_get()
    {
    	$this->response([
            'status' => TRUE,
            'error' => FALSE,
            'message' => 'Request Not found'
        ], REST_Controller::HTTP_OK);
    }
    public function index_post()
    {
        $this->response([
            'status' => TRUE,
            'error' => FALSE,
            'message' => 'Request Post Not found'
        ], REST_Controller::HTTP_OK);
    }
}
