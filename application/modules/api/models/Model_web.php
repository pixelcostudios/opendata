<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

class Model_web extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}
    function post_list($post_type,$post_up)
    {
        $this->db->select('*');
        $this->db->from("my_post");
        $this->db->where('post_type',$post_type);
        $this->db->where('post_up',$post_up);
        $query	= $this->db->get();
        return $query->result_array();
    }
	// function po_cat($post_type,$start,$limit)
	// {
	// 	$this->db->select('post_id, user_id, my_post.cat_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_key, post_desc, post_count, post_comment, date, status');
	// 	$this->db->from("my_post");
	// 	$this->db->where('my_post.post_type',$post_type);
 //        $this->db->where('my_post.post_up','0');
	// 	$this->db->limit($start,$limit);
	// 	$this->db->order_by('my_post.date','desc');
	// 	$query	= $this->db->get();
	// 	return $query->result();
	// }
	// function po_cat_count($post_type)
	// {
	// 	$this->db->select('post_id, user_id, my_post.cat_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_key, post_desc, post_count, post_comment, date, status');
	// 	$this->db->from("my_post");
	// 	$this->db->where('my_post.post_type',$post_type);
 //        $this->db->where('my_post.post_up','0');
	// 	$this->db->order_by('my_post.date','desc');
	// 	return $this->db->count_all_results();
	// }
	// function po_cat_id($post_type,$cat_id,$start,$limit)
	// {
	// 	$this->db->select('post_id, user_id, cat_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_key, post_desc, post_count, post_comment, date, status, meta_id, meta_type, meta_source, meta_dest, meta_title, meta_slug, meta_date');
	// 	$this->db->from("my_post");
	// 	$this->db->where('post_type',$post_type);
	// 	$this->db->where('my_meta.meta_dest',$cat_id);
	// 	$this->db->where('my_meta.meta_status','1');
	// 	$this->db->join('my_meta','my_post.post_id=my_meta.meta_source');
	// 	$this->db->limit($start,$limit);
	// 	$this->db->order_by('date','desc');
	// 	$query	= $this->db->get();
	// 	return $query->result();
	// }
	// function po_cat_id_count($post_type,$cat_id)
	// {
	// 	$this->db->select('post_id');
	// 	$this->db->from("my_post");
	// 	$this->db->where('post_type',$post_type);
	// 	$this->db->where('my_meta.meta_dest',$cat_id);
	// 	$this->db->where('my_meta.meta_status','1');
	// 	$this->db->join('my_meta','my_post.post_id=my_meta.meta_source');
	// 	$this->db->order_by('date','desc');
	// 	return $this->db->count_all_results();
	// }
 //    function po_post($post_type,$post_up,$start,$limit)
 //    {
 //        $this->db->select('post_id, my_post.user_id, my_post.cat_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_key, post_desc, post_comment, date, status, user_name, user_first_name as user_fullname');
 //        $this->db->from("my_post");
 //        $this->db->join('my_user', 'my_user.user_id = my_post.user_id', 'left');
 //        $this->db->where('my_post.post_type',$post_type);
 //        $this->db->where('my_post.post_up',$post_up);
 //        $this->db->limit($start,$limit);
 //        $this->db->order_by('my_post.date','desc');
 //        $query	= $this->db->get();
 //        return $query->result();
 //    }
 //    function po_post_count($post_type,$post_up)
 //    {
 //        $this->db->select('post_id');
 //        $this->db->from("my_post");
 //        $this->db->join('my_user', 'my_user.user_id = my_post.user_id', 'left');
 //        $this->db->where('my_post.post_type',$post_type);
 //        $this->db->where('my_post.post_up',$post_up);
 //        $this->db->order_by('my_post.date','desc');
 //        return $this->db->count_all_results();
 //    }
	// function po_search($post_type,$cat,$start,$limit)
	// {
	// 	$this->db->select('post_id, my_post.user_id, cat_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_key, post_desc, post_count, post_comment, date, status, user_name, user_first_name as user_fullname');
	// 	$this->db->from("my_post");
	// 	$this->db->group_start();
	// 	$this->db->like('post_slug',$cat);
	// 	$this->db->or_like('post_title',$cat);
	// 	$this->db->or_like('post_content',$cat);
	// 	$this->db->or_like('post_summary',$cat);
	// 	$this->db->group_end();
 //        $this->db->join('my_user', 'my_user.user_id = my_post.user_id', 'left');
	// 	$this->db->where('post_type',$post_type);
	// 	$this->db->limit($start,$limit);
	// 	$this->db->order_by('date','desc');
	// 	$query	= $this->db->get();
	// 	return $query->result();
	// }
	// function po_search_count($post_type,$cat)
	// {
	// 	$this->db->select('post_id, my_post.user_id, cat_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_key, post_desc, post_count, post_comment, date, status, user_first_name as user_fullname');
	// 	$this->db->from("my_post");
	// 	$this->db->group_start();
	// 	$this->db->like('post_slug',$cat);
	// 	$this->db->or_like('post_title',$cat);
	// 	$this->db->or_like('post_content',$cat);
	// 	$this->db->or_like('post_summary',$cat);
	// 	$this->db->group_end();
	// 	$this->db->join('my_user', 'my_user.user_id = my_post.user_id', 'left');
	// 	$this->db->where('post_type',$post_type);
	// 	$this->db->order_by('date','desc');
	// 	return $this->db->count_all_results();
	// }
	// function po_search_ajax($cat)
	// {
	// 	$this->db->select('post_id, user_id, cat_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_key, post_desc, post_comment, date, status');
	// 	$this->db->from("my_post");
	// 	$this->db->group_start();
	// 	$this->db->like('post_slug',$cat);
	// 	$this->db->or_like('post_title',$cat);
	// 	$this->db->or_like('post_content',$cat);
	// 	$this->db->or_like('post_summary',$cat);
	// 	$this->db->group_end();
	// 	$this->db->order_by('date','desc');
	// 	$query	= $this->db->get();
	// 	return $query->result();
	// }
	// function po_add($data_array)
	// {
	// 	$this->db->insert('my_post',$data_array);
	// 	return $this->db->insert_id();
	// }
	// function po_update($post_id,$data_array)
	// {
	// 	$this->db->where('post_id', $post_id);
	// 	$this->db->update('my_post',$data_array);
	// 	return true;
	// }
	// function po_update_status($post_id,$status)
	// {
	// 	$this->db->set('status', $status); 
	// 	$this->db->where('post_id', $post_id);
	// 	$this->db->update('my_post');
	// 	return true;
	// }
	// function po_delete($post_id)
	// {
	// 	$this->db->where('post_id',$post_id);
	// 	$this->db->delete('my_post');
	// 	return true;
	// }
	// public function record_count() 
	// {
 //        return $this->db->count_all("my_post");
 //    }
 //    //Cart
 //    function cart_cat()
	// {
	// 	$this->db->select('cart_id, my_cart.user_id, cart_code, cart_tracking, cart_unique, cart_service, cart_cost, cart_date, cart_status, cart_transaction, cart_transaction_code, cart_transaction_status, cart_transaction_date, cart_payment, cart_fraud, user_email');
	// 	$this->db->from("my_cart");
	// 	$this->db->join('my_user', 'my_user.user_id=my_cart.user_id');
	// 	$query	= $this->db->get();
	// 	return $query->result();
	// }
	// function cart_cat_count()
	// {
	// 	$this->db->select('cart_id');
	// 	$this->db->from("my_cart");
	// 	$this->db->join('my_user', 'my_user.user_id=my_cart.user_id');
	// 	return $this->db->count_all_results();
	// }
}