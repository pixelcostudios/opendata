<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

class Model_android extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}
	function po_type_user($user_id,$post_type,$order)
	{
		$this->db->select('post_id, user_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_key, post_desc, post_comment, date, status');
		$this->db->from("my_post");
		$this->db->where('user_id',$user_id);
		$this->db->where('post_type',$post_type);
		$this->db->where('status','1');
		$this->db->order_by('date',$order);
		$query	= $this->db->get();
		return $query->result();
	}


	//Category
	function ca_list($cat_type,$cat_up,$cat_order)
	{
		$this->db->select('cat_id, cat_type, cat_up, cat_code, cat_title, cat_slug, cat_summary, cat_key, cat_desc, cat_date, cat_status');
		$this->db->from("my_cat");
		$this->db->where('cat_type',$cat_type);
		$this->db->where('cat_up', $cat_up);
		$this->db->order_by('cat_code', $cat_order);
		$query	= $this->db->get();
		return $query->result();
	}

	//User
	function user_detail($user_name)
	{
		$this->db->select('my_user.user_id, user_unique, user_name, user_password, user_email, user_avatar, user_gender, user_first_name, user_last_name, user_phone, user_mobile, user_signature, user_birthday, user_address, user_city, user_province, user_country, user_postal, user_profession, user_company, user_join, user_active, role_name, role_description');
        $this->db->from("my_user");
        $this->db->join('my_user_group', 'my_user.user_id = my_user_group.user_id', 'left');
        $this->db->join('my_user_role', 'my_user_role.role_id = my_user_group.role_id', 'left');
        $this->db->where('my_user.user_name',$user_name);
        //$this->db->where('my_user.user_email',$user_email);
        $result	= $this->db->get();
        return $result->row();
	}
	function po_id($post_id)
	{
		$this->db->select('post_id, user_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_video, post_key, post_desc, post_comment, date, status, meta_id, meta_type, meta_source, meta_dest, meta_title, meta_slug, meta_date');
		$this->db->from("my_post");
		$this->db->where('my_post.status','1');
		// $this->db->where('my_meta.meta_status','1');
		$this->db->join('my_meta', 'my_meta.meta_source = my_post.post_id','left');
		$this->db->where('my_post.post_id',$post_id);
		$this->db->where('status','1');
		$query	= $this->db->get();
		return $query->result();
	}
	function ip_value($ipost_all,$ipost_type,$order,$value)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		$this->db->where('ipost_all',$ipost_all);
		$this->db->where('ipost_type',$ipost_type);
		$this->db->order_by('date',$order);
		$this->db->limit('1');
		$query	= $this->db->get();
		return $query->row($value);
	}
	function po_update($user_id,$post_id,$data_array)
	{
		$this->db->where('user_id', $user_id);
		$this->db->where('post_id', $post_id);
		$this->db->update('my_post',$data_array);
		return true;
	}
	function po_edit($post_id)
	{
		$this->db->select('*');
		$this->db->from("my_post");
		$this->db->where('post_id',$post_id);
		$query	= $this->db->get();
		return $query->result();
	}
	function po_add($data_array)
	{
		$this->db->insert('my_post',$data_array);
		return $this->db->insert_id();
	}
	//NIKK
	function nikk_update($user_id,$nikk_id,$data_array)
	{
		$this->db->where('user_id', $user_id);
		$this->db->where('nikk_id', $nikk_id);
		$this->db->update('my_nikk',$data_array);
		return true;
	}
	function nikk_add($data_array)
	{
		$this->db->insert('my_nikk',$data_array);
		return $this->db->insert_id();
	}



	function po_type_limit($post_type,$order,$limit)
	{
		$this->db->select('post_id, user_id, my_post.cat_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_video, post_key, post_desc, post_comment, date, status, meta_id, meta_type, meta_source, meta_dest, meta_title, meta_slug, meta_date');
		$this->db->from("my_post");
		$this->db->where('post_type',$post_type);
		$this->db->where('my_post.status','1');
		$this->db->where('my_meta.meta_status','1');
		$this->db->join('my_meta', 'my_post.post_id = my_meta.meta_source','left');
		$this->db->order_by('date',$order);
		$this->db->limit($limit);
		$query	= $this->db->get();
		return $query->result();
	}
	
    function user_detail_email($user_email)
	{
		$this->db->select('my_user.user_id, user_unique, user_name, user_password, user_email, user_avatar, user_gender, user_first_name, user_last_name, user_phone, user_mobile, user_signature, user_birthday, user_address, user_city, user_province, user_country, user_postal, user_profession, user_company, user_join, user_active, role_name, role_description');
        $this->db->from("my_user");
        $this->db->join('my_user_group', 'my_user.user_id = my_user_group.user_id', 'left');
        $this->db->join('my_user_role', 'my_user_role.role_id = my_user_group.role_id', 'left');
        $this->db->where('my_user.user_email',$user_email);
        //$this->db->where('my_user.user_email',$user_email);
        $result	= $this->db->get();
        return $result->row();
    }
    function user_edit($user_id)
	{
		$this->db->select('my_user.user_id, user_name, user_password, user_email, user_avatar, user_unique, user_gender, user_first_name, user_last_name, user_phone, user_mobile, user_signature, user_birthday, user_address, user_city, user_province, user_country, user_postal, user_profession, user_company, user_join, user_active, role_name, role_description');
        $this->db->from("my_user");
        $this->db->join('my_user_group', 'my_user.user_id = my_user_group.user_id', 'left');
        $this->db->join('my_user_role', 'my_user_role.role_id = my_user_group.role_id', 'left');
		$this->db->where('my_user.user_id',$user_id);
		$query	= $this->db->get();
		return $query->result();
	}
	function user_edit_unique($user_unique)
	{
		$this->db->select('my_user.user_id, user_name, user_password, user_email, user_avatar, user_unique, user_gender, user_first_name, user_last_name, user_phone, user_mobile, user_signature, user_birthday, user_address, user_city, user_province, user_country, user_postal, user_profession, user_company, user_join, user_active, role_name, role_description');
        $this->db->from("my_user");
        $this->db->join('my_user_group', 'my_user.user_id = my_user_group.user_id', 'left');
        $this->db->join('my_user_role', 'my_user_role.role_id = my_user_group.role_id', 'left');
		$this->db->where('my_user.user_unique',$user_unique);
		$result	= $this->db->get();
		return $result->row();
	}
	function user_update($user_id,$data_array)
	{
		$this->db->where('user_id', $user_id);
		$this->db->update('my_user',$data_array);
		return true;
	}
	function user_mobile_by_search($user_fullname)
	{
		$this->db->select('*');
		$this->db->from("my_user");
		$this->db->join("my_user_group","my_user_group.user_id=my_user.user_id");
		$this->db->where("my_user.user_active","1");
		$this->db->where("my_user_group.role_id","6");
		$this->db->group_start();
		$this->db->like("my_user.user_first_name",$user_fullname);
		$this->db->or_like("my_user.user_last_name",$user_fullname);
		//$this->db->like($like_array);
		$this->db->group_end();
		$query	= $this->db->get();
		return $query->result();
	}
	

	// function user_list()
	// {
	// 	$this->db->select('user_id, user_name, user_password, user_email, user_avatar, user_gender, user_first_name, user_last_name, user_join, user_activity');
	// 	$this->db->from("my_user");
	// 	$query	= $this->db->get();
	// 	return $query->result();
	// }
	
	// function po_post_type($post_type,$cat_id,$order)
	// {
	// 	$this->db->select('post_id, user_id, my_post.cat_id, post_type, post_slug, post_title, post_content, post_summary, post_price, post_comment, date, status');
	// 	$this->db->from('my_post');
	// 	$this->db->join('my_cat', 'my_post.cat_id = my_cat.cat_id', 'left');
	// 	$this->db->where('post_type',$post_type);
	// 	$this->db->where('status','1');
	// 	$this->db->order_by('date', $order);
	// 	//$this->db->limit($limit);
	// 	$query	= $this->db->get();
	// 	return $query->result();
	// }
	//Phone Book
	// function ph_cat($cat_id,$order_by)
	// {
 //        $this->db->select('*');
 //        $this->db->from("my_phone_book");
	// 	$this->db->where('my_phone_book.cat_id',$cat_id);
	// 	$this->db->order_by('phone_join',$order_by);
	// 	$query	= $this->db->get();
	// 	return $query->result();
	// }
	function login($user_name,$user_password) 
	{
		$this->db->select("setting_name, setting_value");
		$this->db->from("my_user");
		$this->db->where('user_name',$user_name, 'user_password',md5($user_password));
		$this->db->limit(1);
		//return $this->db->count_all_results();
		$query = $this -> db -> get();
 
	   if($query -> num_rows() == 1)
	   {
	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
	}
	function panel()
	{
		$this->db->select("setting_name, setting_value");
		$this->db->from("my_setting");
		$this->db->where('setting_name','panel');
		$query	= $this->db->get(); //$query	= $this->db->query("SELECT setting_name, setting_value from my_setting WHERE setting_name='panel'");
		return $this->db->count_all_results();
	}	
	function setting_expl($setting_name) 
	{
		$this->db->select("setting_name, setting_value");
		$this->db->from("my_setting");
		$this->db->where('setting_name',$setting_name);
		$query	= $this->db->get();
		$row	= $query->row();
		//$result	= unserialize(base64_decode($row->setting_value));
		$result	= $row->setting_value;
		return $result;
	}
	function hashSSHA($password) {

        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
        $hash = array("salt" => $salt, "encrypted" => $encrypted);
        return $hash;
    }
    function currency($n)
	{
		$n = (0+str_replace(",","",$n));
		return str_replace(",",".",number_format($n));
	}
	function char_random($length)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}
}