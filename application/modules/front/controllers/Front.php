<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//require APPPATH . "third_party/MX/Controller.php";
	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
        // $this->load->library('email');
        // $this->load->library('pdf');
        $this->load->helper(array('form'));
        $this->load->helper(array('url'));
		$this->load->helper(array('path'));
        $this->load->model("model_post");
		$this->load->model("model_setting");

		$this->load->model('siteman_model');
		$this->load->model('lembaga_model');
		$this->load->model('publik_model');
		$this->load->model('post_model');
		$this->load->model('apbd_model');
	}
	public function index()
	{
		// $data['welcome']                = $this->model_post->po_id('7');
		// $data['slideshow']              = $this->model_post->po_type_limit('slideshow','DESC','5');
		// $data['hotel']              	= $this->model_post->po_type_limit('hotel','DESC','5');
		// $data['about']                	= $this->model_post->po_id('1');
		$data['pageTitle'] = "Selamat Datang";
		$data['page'] = array(
			'title'=>'Selamat Datang',
			'desc' =>'Deskripsi',
			'image' =>'image'
				);
		$data['info'] = $this->siteman_model->configs();
		$data['about'] = $this->post_model->laman_load(1);
		// $data['posts_list'] = $this->publik_model->post_list();
		
		$data['posts_list'] = $this->model_post->po_post_limit('post','1','DESC','5');

		$varKode = '3402';
		$this->load->model('wilayah_model');
		if($varKode <> ''){
			$data['kodewilayah'] = $varKode;
			$data['wilayah'] = $this->wilayah_model->get_wilayah($varKode);
			$data['pageTitle'] = "Selamat Datang";
			$data['page'] = array(
				'title'=>'Pemerintah Daerah '.$data['wilayah'],
				'desc' =>'Open Data APBD Pemerintah Daerah '.$data['wilayah'],
				'image' => base_url('assets/img/logo.png'),
					);
			$data['info'] = $this->siteman_model->configs();
			$datawilayah = $this->publik_model->lembaga_list($varKode);

			// print_r($datawilayah);

			$data['org'] = $datawilayah['org'];
			$data['tahuns'] = $this->apbd_model->_apbd_tahun_by_lembaga('33');
			
			// $data['summary'] = $this->apbd_model->apbd_lembaga_home($datawilayah['id'],$data['tahuns']);
			$data['lembagaID'] = $varKode;
			
			$data['program'] = $this->publik_model->program_nangkis_by_wilayah($varKode);
			$data['kegiatan'] = $this->publik_model->kegiatan_nangkis_by_wilayah($varKode);
			$data['posts'] = $this->publik_model->post_by_wilayah($varKode);
			
			/*
			 * Muat data APBD 0
			 * 
			 * */
			$varRek = (@$_REQUEST['r']) ? $_REQUEST['r']:0;
			$varThn = (@$_REQUEST['y']) ? $_REQUEST['y']:0;
			
			$data['rekening'] = $this->apbd_model->nama_rekening($varRek,'33');
			
			if($varRek){
				$data['rekenin_title'] = "Detail Rekening <strong>".$data['rekening']."</strong> (dlm juta rupiah)";
				// $data['summary'] = $this->apbd_model->apbd_lembaga_by_rek($datawilayah['id'],$varRek,$data['tahuns']);
				$data['pie'] = true;
			}else{
				// echo "Tidak Ada rek";
				$data['pie'] = false;
				$data['rekenin_title'] = "Rangkuman APBD (dlm juta rupiah)";
				$data['summary'] = $this->apbd_model->apbd_lembaga_home('33',array('2017','2018'));
				// print_r($data['summary']);
			}
		}
    	$this->load->view('pubs/beranda',$data);
	}
	function nama($varKode=''){
		$data['pageTitle'] = "Selamat Datang";
		$data['info'] = array(
			"alamat_kantor"=>"Jl. Kaliurang Km. 5 Gg. Tejomoyo CT III/3 Yogyakarta  55281  Indonesia",
			"lat"=>98.0232,
			"lng"=>110.0232,
			"zoom"=>13,
			"telp"=>'0274 550 1283',
			"email"=>'perkumpulanidea@gmail.com',
			
				);
		$data['menu_atas'] = false;
		$data['menu_kiri'] = false;
		$data['posts'] = false;
    $this->load->view('pubs/publik_template',$data);
	}
}
