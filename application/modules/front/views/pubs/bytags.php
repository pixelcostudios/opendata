<?php 
$this->load->view('front/pubs/header');
?>
			<!-- section start -->
			<!-- ================ -->
			<section class="clearfix">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="blog section">
								<?php
								
								if($tags){
									
									echo "
									
									";
								}else{
									
									if($tag){
										echo "
										<div class=\"box box-primary\">
											<div class=\"box-header with-border\">
												<h3 class=\"box-title\">
												<span class=\"pull-left\">
												<i class=\"fa fa-tag\"></i> Identifikasi Anggaran dalam Isu <strong>".$tag['nama']."</strong>
												</span>

												<span class=\"pull-right\">
												<select class=\"form-control select_year\" id=\"select-cat\" onchange=\"window.location = jQuery('#select-cat option:selected').val();\">";
												?>
												<option value="<?php echo base_url().$this->uri->segment('1').'/'.$this->uri->segment('2').'/'.$this->uri->segment('3').'/'.$this->uri->segment('4');?>">Pilih Tahun</option>
												<?php 
												foreach (range('2017','2017') as $year) 
												{ ?>
													<option value="<?php echo base_url().$this->uri->segment('1').'/'.$this->uri->segment('2').'/'.$this->uri->segment('3').'/'.$this->uri->segment('4').'/'.$year;?>" <?php if($this->uri->segment('5')==$year){?>selected<?php }?>> <?php echo $year;?></option>
												<?php }
												
										echo "</select>
												</span>
												</h3>
											</div>
											<div class=\"box-body\">
											<!-- grafik --> 
												<div id=\"graph_container\" class=\"chart\"></div>
											<!-- /grafik -->";echo count($apbd).'sample';
											if(count($apbd) > 0){
												echo "
													<table class=\"table table-responsive table-stripped\">
													<thead><tr><th>#</th>
														<th>Mata Anggaran/Lembaga</th>
														<th>Tahun</th>
														<th>Kode Rekening</th>
														<th>Nominal</th>
													</tr></thead>
													<tbody>";
													$nomer = 1;
													$sumNominal = 0;
													foreach($apbd as $rs){
														
														echo "<tr><td class=\"angka\">".$nomer."</td>
															<td><a href=\"\">".$rs['uraian']." </a>
															<br />&#8213; <a href=\"".site_url('institusi/opd/'.$rs['lembaga_id'].'/'.fixNamaUrl($rs['lembaga_nama']))."\">".$rs['lembaga_nama']."</a> 
															&#8213; <a href=\"".site_url('apbd/pemda/'.$rs['kode_wilayah'].'/'.fixNamaUrl($rs['wnama']))."\">".$rs['wnama']."</a> 
															</td>
															<td>".$rs['tahun']."</td>
															<td>".$rs['nrekening']."</td>
															<td class=\"angka\">".number_format($rs['nominal'])."</td>
															
														</tr>";
														$sumNominal += $rs['nominal'];
														$nomer++;
													}
													echo "</tbody>
													<tfoot>
														<tr>
														<th></th>
														<th colspan=\"3\"></th>
														<th>".number_format($sumNominal)."</th>
														</tr>
													</tfoot>
													</table>
													
												";
											}else{
												echo "
												<div class=\"alert alert-warning\">
													<h3>Data Tidak Tersedia</h3>
													<p>Tidak terdapat mata anggaran dalam arsip APBD yang ditandai dengan isu ".$tag['nama']."</p>
												</div>
												
												";
											}
												//echo var_dump($tags_area['apbd']);
											echo "</div>
										</div>										
										
										";
										// echo var_dump($apbd);
									}
								}
								?>

							</div>
							
								
						</div>
					</div>
				</div>
			</section>
			<!-- section end -->
<!-- DataTables CSS -->
<link href="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/datatables/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<!-- Datatables-->
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/datatables/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>jszip/jszip.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/js/buttons.colVis.min.js"></script>

<!--Highchart.js-->
<script type="text/javascript" src="<?php echo base_url("themes/idea/assets/plugins/"); ?>highcharts/highcharts.js"></script>
<script type="text/javascript" src="<?php echo base_url("themes/idea/assets/plugins/"); ?>highcharts/highcharts-3d.js"></script>
<script type="text/javascript" src="<?php echo base_url("themes/idea/assets/plugins/"); ?>highcharts/modules/exporting.js"></script>

<!--Highchart.js Thems-->
<script type="text/javascript" src="<?php echo base_url("themes/idea/assets/plugins/"); ?>highcharts/themes/sand-signika.js"></script>

<script>

// Make monochrome colors and set them as default for all pies
Highcharts.getOptions().plotOptions.pie.colors = (function () {
    var colors = [],
        base = Highcharts.getOptions().colors[0],
        i;

    for (i = 0; i < 10; i += 1) {
        // Start out with a darkened base color (negative brighten), and end
        // up with a much brighter color
        colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
    }
    return colors;
}());
	
$(document).ready(function() {
	$('table.datatables').DataTable( {
		"language": {
						"url": "<?php echo base_url();?>themes/idea/assets/plugins/datatables/datatables_ID.js"
				},
		dom: 'Bfrtip',
		buttons: [
			'excelHtml5',
			'csvHtml5',
			{extend: 'pdfHtml5',
				orientation: 'landscape',
				pageSize: 'A4'
			},
			'print',
			{
				extend: 'colvis',
				columns: ':gt(2)'
			}
		]
	});
	
	
				$('#graph_container').highcharts({
				chart: {
						type: 'column',
						options3d: {
								enabled: true,
								alpha: 15,
								beta: 1,
								viewDistance: 25,
								depth: 40
						}
				},
				title: {
						text: "Identifikasi Anggaran dalam Isu <strong><?php echo $tag['nama'];?>"
				},
				yAxis: {
					min: 0, 
					title: {text: 'Nominal (dlm Juta Rupiah)'},
				},
				xAxis: {
						categories: [ <?php foreach($apbd as $rs){echo "'".$rs['uraian']."',";}?>]
				},
				tooltip:{
					crosshairs: [false, true],
				},
				plotOptions: {
						column: {
								depth: 40
						}
				},
				series: [
				{
						name: 'Anggaran',
						data: [<?php $j=0; foreach($apbd as $rs){ $j++; $comma=($j==count($apbd)) ? '' : ','; echo floor($rs['nominal']).$comma;}?>],
					}]
			});		
			
});	
</script>

<?php 
$this->load->view('front/pubs/footer');
