<?php 
$this->load->view('front/pubs/header');
$cur_url = current_url();
?>
			<!-- section start -->
			<!-- ================ -->
			<section class="clearfix">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class=" section"></div>
							<h3><a class="logo-font" href="<?php echo $cur_url; ?>"><?php echo $wilayah;?></a>
							<span class="text-default"></span></h3>
							<div class="separator-2"></div>
							<div class="box box-primary">
								<div class="box-header with-border">
									<h3 class="box-title">Ringkasan APBD <?php echo $wilayah;?></h3>
								</div>
								<div class="box-body">
									<?php 
			echo "
			<!-- grafik -->
				<div id=\"graph_container\" class=\"chart\" style=\"margin-bottom:2em;\"></div>
			<!-- /grafik -->
			
			<!-- tabular -->
			<table class=\"table table-bordered table-responsive datatables\">
			<thead><tr>
				<th>Kode Rekening</th>
				<th>Uraian</th>";

				$XAxis = "";
				$x=1;
				$xn=count($tahuns);
				
				foreach($tahuns as $tahun){
					$strKoma = ($x < $xn) ? ",":"";
					$XAxis .= "'".$tahun."' ".$strKoma;
					$x++;
					echo "<th>".$tahun."</th>";
				}
				echo "
			</tr></thead>
			<tbody>";
			
			$toGraph = array();
			$toPie = array();
			$nomer = 1;
			// print_r($summary);
			foreach($summary['data'] as $key=>$rs){
				// print_r($rs);
				echo "<tr>
				<td>".$rs['rekening']."</td>
				<td>".$rs['uraian']."</td>";
				$tg = 1;
				$nilai = "";
				foreach($tahuns as $tahun){
					$strKoma = ($tg < $xn)? ", ":"";

					// echo $summary['akun'][$tahun][$rs['nominal']];
					$rupiah = $rs['nominal'];
					echo "<td class=\"angka\">".number_format($rupiah,2)."</td>";
					$angka = ($rupiah > 0) ? ($rupiah / 1000000) : 0;

					$nilai .= number_format($angka,2,".","") . $strKoma;
					$tg++;
					
				}
				echo "
				</tr>";
				// $toGraph[$rs["akuntansi"]] = array("nama"=>$rs["uraian"], "nilai"=>$nilai);

				$data_pie[] = array("nama"=>$rs["uraian"], "nilai"=>$nilai);
				$nomer++;
			}
			// print_r($data_pie);
			echo "
			</tbody>
			</table>

			<!-- tabular -->
			<!-- pie -->";
			if($pie){
				foreach($tahuns as $thn){
					echo "
					<div class=\"box box-primary\">
						<div class=\"box-header with-border\">
							<h3 class=\"box-title\">Grafik Distribusi Komponen Mata Anggaran Tahun <strong>".$thn."</strong></h3>
						</div>
						<div class=\"box-body\">
							<div class=\"chart\" id=\"pie_container_".$thn."\"></div>
						</div>
					</div>
					";
				}
			}
			echo "
			<!-- /pie -->";

									?>
								</div>
							</div>
							<div class="separator-2"></div>
						<?php



						if(!$pie){
						?>							
													<div class="box box-primary">
								<div class="box-header with-border">
									<h3 class="box-title">Daftar Lembaga di <?php echo $wilayah;?></h3>
								</div>
								<div class="box-body text-center">
									<?php
									if(count($org) > 0){
										echo "
										<div class=\"row\">";
										foreach($org as $rs){
											echo "
											<div class=\"col-md-6 col-sm-12 col-xs-12\">
												<div class=\"box box-widget widget-user-2\">
													<!-- Add the bg color to the header using any of the bg-* classes -->
													<div class=\"widget-user-header bg-gray\">
														<a href=\"".site_url('institusi/opd/'.$rs['id'].'/'.fixNamaUrl($rs['nama']))."\">
														<div class=\"widget-user-image\">
															<img class=\"img-circle\" src=\"".$rs['logo']."\" alt=\"".$rs['nama']."\">
														</div>
														</a>
														<!-- /.widget-user-image -->
														<h3 class=\"widget-user-username\"><a href=\"".site_url('institusi/opd/'.$rs['id'].'/'.fixNamaUrl($rs['nama']))."\">".$rs['nama']."</a></h3>
														<h5 class=\"widget-user-desc\"><a href=\"".site_url('apbd/pemda/'.$rs['kodewilayah'].'/'.fixNamaUrl($rs['namawilayah']))."\">".$rs['namawilayah']."</a></h5>
													</div>";?>
													<?php /*
													<div class=\"box-footer no-padding\">
														<ul class=\"nav nav-pills\">";
															foreach($rs['apbd'] as $thn){
																echo "<li><a href=\"".site_url('institusi/opd/'.$rs["id"].'/'.fixNamaUrl($rs["nama"]).'/?thn='.$thn)."\">APBD ".$thn."</a></li>";
															}
															echo "
														</ul>
													</div><?php */?>
													<?php echo "
												</div>
											
											</div>
											";
										}
										echo "
										</div>";
									}
									?>
								</div>
							</div>
<?php
}
?>
						
								
						</div>
						
					</div>
				</div>
			</section>
			<!-- section end -->
<!-- DataTables CSS -->
<link href="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/datatables/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<!-- Datatables-->
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/datatables/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>jszip/jszip.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/js/buttons.colVis.min.js"></script>

<!--Highchart.js-->
<script type="text/javascript" src="<?php echo base_url("themes/idea/assets/plugins/"); ?>highcharts/highcharts.js"></script>
<script type="text/javascript" src="<?php echo base_url("themes/idea/assets/plugins/"); ?>highcharts/highcharts-3d.js"></script>
<script type="text/javascript" src="<?php echo base_url("themes/idea/assets/plugins/"); ?>highcharts/modules/exporting.js"></script>

<!--Highchart.js Thems-->
<script type="text/javascript" src="<?php echo base_url("themes/idea/assets/plugins/"); ?>highcharts/themes/sand-signika.js"></script>

<script>
	
$(document).ready(function() {
	$('table.datatables').DataTable( {
		"language": {
						"url": "<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/datatables_ID.js"
				},
		dom: 'Bfrtip',
		buttons: [
			'excelHtml5',
			'csvHtml5',
			{extend: 'pdfHtml5',
				orientation: 'landscape',
				pageSize: 'A4'
			},
			'print',
			{
				extend: 'colvis',
				columns: ':gt(2)'
			}
		]
	});
	
	
	<?php
	
	if($summary){
		?>
			$('#graph_container').highcharts({
				chart: {
						type: 'column',
						options3d: {
								enabled: true,
								alpha: 15,
								beta: 1,
								viewDistance: 25,
								depth: 40
						}
				},
				title: {
						text: 'Anggaran Pendapatan dan Belanja <?php echo  $rekening." - ".$wilayah;?>'
				},
				yAxis: {
					min: 0, 
					title: {text: 'Nominal (dlm Juta Rupiah)'},
				},
				xAxis: {
						categories: [<?php echo $XAxis; ?>]
				},
				tooltip:{
					crosshairs: [false, true],
				},
				plotOptions: {
						column: {
								depth: 40
						}
				},
				series: [
				<?php
				$tg = 1;
				$ntg = count($data_pie);
				foreach($data_pie as $item){
					$strComma = ($tg < $ntg)? ", ":"";
					echo "{
						name: '".$item["nama"]."',
						data: [".$item["nilai"]."],
					}".$strComma;
					$tg++;
				}
				?>]
			});		
		<?php

		if($pie){
			foreach($tahuns as $thn){
				?>
				Highcharts.chart('pie_container_<?php echo $thn;?>', {
						chart: {
								type: 'pie',
								options3d: {
										enabled: true,
										alpha: 45,
										beta: 0
								}
						},
						title: {
								text: 'Komponen Penyusun <?php echo $rekening ." ". $thn; ?>'
						},
						tooltip: {
								pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
						},
						plotOptions: {
								pie: {
										allowPointSelect: true,
										cursor: 'pointer',
										depth: 35,
										dataLabels: {
												enabled: true,
												format: '{point.name}'
										}
								}
						},
						series: [{
								type: 'pie',
								name: 'Browser share',
								data: [
									<?php 
									//echo var_dump($toPie[$thn]);
									$n = count($toPie[$thn]);
									$i=1;
									foreach($toPie[$thn] as $key=>$item){
										$strKoma = ($i < $n) ? ",":"";
										echo "['".$item['uraian']."',".$item['nominal']."]".$strKoma;
										$i++;
									}
									?>
								]
						}]
				});
				<?php
			}
		}
	}
	?>
	
});	
</script>			
<?php 
$this->load->view('front/pubs/footer');
