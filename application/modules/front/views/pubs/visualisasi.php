<?php 
$this->load->view('front/pubs/header');
$cur_url = current_url();
?>
			<!-- section start -->
			<!-- ================ -->
			<section class="clearfix">
				<div class="container">
					<div class="row">
						<?php 

				if(!$pie){
				?>							
							<div class="box box-primary">
								<div class="box-header with-border">
									<h3 class="box-title">Visualisasi Belanja & Pendapatan Dinas di <?php echo $wilayah;?></h3>
								</div>
								<div class="box-body text-center">
									<?php
									if(count($org) > 0){
										echo "
										<div class=\"row\">";
										foreach($org as $rs){
											echo "
											<div class=\"col-md-12 col-sm-12 col-xs-12\">
												<div class=\"box box-widget widget-user-2\">
													<!-- Add the bg color to the header using any of the bg-* classes -->
													<div class=\"widget-user-header bg-gray\">
														<a href=\"".base_url('institusi/opd/'.$rs['id'].'/'.fixNamaUrl($rs['nama']))."\">
														<div class=\"widget-user-imagew\">
															<img class=\"img-circle\" height=\"20\" src=\"".$rs['logo']."\" alt=\"".$rs['nama']."\">
														</div>
														</a>
														<!-- /.widget-user-image -->
														<h3 class=\"widget-user-username\"><a href=\"".base_url('institusi/opd/'.$rs['id'].'/'.fixNamaUrl($rs['nama']))."\">".$rs['nama']."</a></h3>
													</div>";?>
													<?php /*
													<div class=\"box-footer no-padding\">
														<ul class=\"nav nav-pills\">";
															foreach($rs['apbd'] as $thn){
																echo "<li><a href=\"".base_url('institusi/opd/'.$rs["id"].'/'.fixNamaUrl($rs["nama"]).'/?thn='.$thn)."\">APBD ".$thn."</a></li>";
															}
															echo "
														</ul>
													</div>
													*/ ?>
													<?php echo "
												</div>
											
											</div>
											";
										}
										echo "
										</div>";
									}
									?>
								</div>
							</div>
<?php
}
?>
						
								
						</div>
						
					</div>
				</div>
			</section>
			<!-- section end -->
<!-- DataTables CSS -->
<link href="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/datatables/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<!-- Datatables-->
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/datatables/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>jszip/jszip.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/js/buttons.colVis.min.js"></script>

<!--Highchart.js-->
<script type="text/javascript" src="<?php echo base_url("themes/idea/assets/plugins/"); ?>highcharts/highcharts.js"></script>
<script type="text/javascript" src="<?php echo base_url("themes/idea/assets/plugins/"); ?>highcharts/highcharts-3d.js"></script>
<script type="text/javascript" src="<?php echo base_url("themes/idea/assets/plugins/"); ?>highcharts/modules/exporting.js"></script>

<!--Highchart.js Thems-->
<script type="text/javascript" src="<?php echo base_url("themes/idea/assets/plugins/"); ?>highcharts/themes/sand-signika.js"></script>

<script>
	
$(document).ready(function() {
	$('table.datatables').DataTable( {
		"language": {
						"url": "<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/datatables_ID.js"
				},
		dom: 'Bfrtip',
		buttons: [
			'excelHtml5',
			'csvHtml5',
			{extend: 'pdfHtml5',
				orientation: 'landscape',
				pageSize: 'A4'
			},
			'print',
			{
				extend: 'colvis',
				columns: ':gt(2)'
			}
		]
	});
	
	
	<?php
	
	if($summary){
		?>
			$('#graph_container').highcharts({
				chart: {
						type: 'column',
						options3d: {
								enabled: true,
								alpha: 15,
								beta: 15,
								viewDistance: 25,
								depth: 40
						}
				},
				title: {
						text: 'Anggaran Pendapatan dan Belanja <?php echo  $rekening." - ".$wilayah;?>'
				},
				yAxis: {
					min: 0, 
					title: {text: 'Nominal (dlm Juta Rupiah)'},
				},
				xAxis: {
						categories: [<?php echo $XAxis; ?>]
				},
				tooltip:{
					crosshairs: [false, true],
				},
				plotOptions: {
						column: {
								depth: 40
						}
				},
				series: [
				<?php
				$tg = 1;
				$ntg = count($toGraph);
				foreach($toGraph as $key=>$item){
					$strComma = ($tg < $ntg)? ", ":"";
					echo "{
						name: '".$item["nama"]."',
						data: [".$item["nilai"]."],
					}".$strComma;
					$tg++;
				}
				?>]
			});		
		<?php

		if($pie){
			foreach($tahuns as $thn){
				?>
				Highcharts.chart('pie_container_<?php echo $thn;?>', {
						chart: {
								type: 'pie',
								options3d: {
										enabled: true,
										alpha: 45,
										beta: 0
								}
						},
						title: {
								text: 'Komponen Penyusun <?php echo $rekening ." ". $thn; ?>'
						},
						tooltip: {
								pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
						},
						plotOptions: {
								pie: {
										allowPointSelect: true,
										cursor: 'pointer',
										depth: 35,
										dataLabels: {
												enabled: true,
												format: '{point.name}'
										}
								}
						},
						series: [{
								type: 'pie',
								name: 'Browser share',
								data: [
									<?php 
									//echo var_dump($toPie[$thn]);
									$n = count($toPie[$thn]);
									$i=1;
									foreach($toPie[$thn] as $key=>$item){
										$strKoma = ($i < $n) ? ",":"";
										echo "['".$item['uraian']."',".$item['nominal']."]".$strKoma;
										$i++;
									}
									?>
								]
						}]
				});
				<?php
			}
		}
	}
	?>
	
});	
</script>			
<?php 
$this->load->view('front/pubs/footer');
