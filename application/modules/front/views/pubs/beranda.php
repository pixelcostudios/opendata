<?php 
$this->load->view('pubs/header');
?>
			<!-- section start -->
			<!-- ================ -->
			<section class="clearfix">
				<div class="container">
					
					<div class="row">
						
						
						
					</div>
					
			</div>
			<!-- section start -->
			<!-- ================ -->
			<section class="clearfix">
				<div class="container">
					<div class="row">
						<div class="col-md-8">

						<?php 
				echo "
				<!-- grafik -->
					<div id=\"graph_container\" class=\"chart\" style=\"margin-bottom:2em;\"></div>
				<!-- /grafik -->
				
				<!-- tabular -->
				<table class=\"table table-bordered table-responsive datatables\">
				<thead><tr>
				<th>Kode Rekening</th>
				<th>Uraian</th>";

				$XAxis = "";
				$x=1;
				$xn=count($tahuns);
				
				foreach($tahuns as $tahun){
					$strKoma = ($x < $xn) ? ",":"";
					$XAxis .= "'".$tahun."' ".$strKoma;
					$x++;
					echo "<th>".$tahun."</th>";
				}
				echo "
			</tr></thead>
			<tbody>";
			
			$toGraph = array();
			$toPie = array();
			$nomer = 1;
			// print_r($summary);
			foreach($summary['data'] as $key=>$rs){
				// print_r($rs);
				echo "<tr>
				<td>".$rs['rekening']."</td>
				<td>".$rs['uraian']."</td>";
				$tg = 1;
				$nilai = "";
				foreach($tahuns as $tahun){
					$strKoma = ($tg < $xn)? ", ":"";

					// echo $summary['akun'][$tahun][$rs['nominal']];
					$rupiah = $rs['nominal'];
					// if(array_key_exists($rs['nominal'],$summary['akun'][$tahun])){
					// 	$rupiah = $summary['akun'][$tahun][$rs['nominal']];
					// }else{
					// 	$rupiah = 0;
					// }
					$rest = $this->apbd_model->totals($tahun,'33',$rs['uraian']);
					// print_r($rest);
					echo "<td class=\"angka\">".number_format($rest['0']['nominal'],2)."</td>";
					//echo "<td class=\"angka\">".number_format($rupiah,2)."</td>";
					$angka = ($rupiah > 0) ? ($rupiah / 1000000) : 0;

					$nilai .= number_format($angka,2,".","") . $strKoma;
					$tg++;
					// print_r($tahun);
					// print_r($summary['akun'][$tahun]);
					// print_r($summary['akun']);
// 					if(array_key_exists($tahun, $summary['akun'])){
// 						if(array_key_exists($rs['nominal'],$summary['akun'][$tahun])){
// 							$rupiah = $summary['akun'][$tahun][$rs['nominal']];
// 						}else{
// 							$rupiah = 0;
// 						}
						
// //						$rupiah = $summary['akun'][$tahun][$rs['akuntansi']];
// 						echo "<td class=\"angka\">".number_format($rupiah,2)."</td>";
// 						$angka = ($rupiah > 0) ? ($rupiah / 1000000) : 0;
// 						$nilai .= number_format($angka,2,".","") . $strKoma;
// 						$tg++;

// 					}else{
// 						$rupiah = 0;
// 						echo "<td class=\"angka\">".number_format($rupiah,2)."</td>";
// 						$angka = ($rupiah > 0) ? ($rupiah / 1000000) : 0;
// 						$nilai .= number_format($angka,2,".","") . $strKoma;
// 						$tg++;
						
// 					}
					// $toPie[$tahun][$rs['akuntansi']] = array('akun'=>$rs['akuntansi'],'uraian'=>$rs['uraian'],'nominal'=>$angka);
				}
				echo "
				</tr>";
				// $toGraph[$rs["akuntansi"]] = array("nama"=>$rs["uraian"], "nilai"=>$nilai);

				$data_pie[] = array("nama"=>$rs["uraian"], "nilai"=>$nilai);
				$nomer++;
			}
			// print_r($data_pie);
			echo "
			</tbody>
			</table>

			<!-- tabular -->
			<!-- pie -->";
			if($pie){
				foreach($tahuns as $thn){
					echo "
					<div class=\"box box-primary\">
						<div class=\"box-header with-border\">
							<h3 class=\"box-title\">Grafik Distribusi Komponen Mata Anggaran Tahun <strong>".$thn."</strong></h3>
						</div>
						<div class=\"box-body\">
							<div class=\"chart\" id=\"pie_container_".$thn."\"></div>
						</div>
					</div>
					";
				}
			}
			echo "
			<!-- /pie -->";

									?>
						
							<div class=" section"></div>
							
							<h3><?php echo $pageTitle;?> <span class="text-default"></span></h3>
							<div class="separator-2"></div>
								<?php

							if(count($posts_list) > 0){
								echo "
								<div class=\"blog\">
								";
								foreach($posts_list as $item){ $images = $this->model_post->ip_list($item->post_id,'post','ASC');
									// $sampul = (strlen($item["post_cover"]) > 0)? base_url()."assets/uploads/".$item["post_cover"] : "";
									$teks = fixTag($item->post_content);
									if(strlen($teks)>310){
										$abstrak = substr($teks,0,strpos($teks," ",300));
									}else{
										$abstrak = $teks;
									}
									$post_url = base_url().$item->post_slug.'.html';
									
									echo "
									<div class=\"blog-item\">
										<div class=\"row\">
													
											<div class=\"col-xs-12 col-sm-12 blog-content\">
												<h2><a href=\"".$post_url."\">".$item->post_title."</a></h2>
												<div>".$abstrak."... <a href=\"".$post_url."\"><em>selengkapnya</em> <i class=\"fa fa-angle-double-right\"></i></a></div>
												
											</div>
										</div>    
									</div><!--/.blog-item-->
									";
									
								}
								echo "
								</div>";								
							}
							// print_r($posts_list);
								?>
						</div>
						<div class="col-md-4 bg-blue-sky">
						<div class="col-md-12">
							<div class=""  style="color: #444;">
								<?php 
								$desc = substr($about['post_content'], strpos($about['post_content'], "<p"), strpos($about['post_content'], "</p>")+4);
								?>
								<h2><a href="<?php echo base_url($about['post_slug']);?>.html" class="logo-font"><?php echo $about['post_title'];?></a></h2>
								<div><?php echo $desc;?>
								<a href="<?php echo base_url($about['post_slug']);?>.html">...selengkapnya</a></div><br>
							</div>
						</div>
							<div class=" bg-blue-sky widget">
								<aside id="wg_embed" class="widget ">
									<div class="title-section clearfix">
										<h4 class="lead-title"><i class="fa fa-paste"></i> Tempelkan di Web lain</h4>
										<div class="white-line uk-clearfix"></div>
									</div>
									<div>
										<p style="color:#444">Gunakan skrip berikut ini untuk memasang tautan dari situs web anda ke halaman ini:</p>
										<div  style="height:100px;width:100%;overflow:auto;background:#f5f5f5;color:" onclick="selectText('selectable')" id="selectable">
										<code>
											&lt;div style="border:solid 1px #ccc;border-radius:.5em;-webkit-box-shadow: 5px 6px 8px 0px #d8d8d8;box-shadow: 5px 6px 8px 0px #d8d8d8;background:#fff;color:#888;padding:1em;"&gt;&lt;a href="http://<?php echo $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; ?>" target="_blank"&gt;&lt;img src="<?php echo base_url('/themes/idea/assets/img/oda-l.png');?>" alt="Open Data APBD" style="width:100%;margin:0;padding:0;margin-bottom:1em;"/&gt;&lt;span style="margin:0;padding:0;font-size:1.6em;font-weight:600;color:#888;line-height:1em;"&gt;Open Data APB <?php echo $pageTitle; ?>&lt;/span&gt;&lt;/a&gt;&lt;/div&gt;
										</code>
										</div>
										<br />pratayang:

											<div style="border:solid 1px #ccc;border-radius:.5em;-webkit-box-shadow: 5px 6px 8px 0px #d8d8d8;box-shadow: 5px 6px 8px 0px #d8d8d8;background:#fff;color:#888;padding:1em;"><a href="http://<?php echo $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; ?>" target="_blank">
											<img src="<?php echo base_url('/themes/idea/assets/img/oda-l.png');?>" alt="Open Data APBD" style="width:100%;margin:0;padding:0;margin-bottom:1em;"/>
											<span style="margin:0;padding:0;font-weight:600;color:#888;line-height:1em;"><?php echo APP_TITLE;?></span></a></div>

											<script type="text/javascript">
													function selectText(containerid) {
															if (document.selection) {
																	var range = document.body.createTextRange();
																	range.moveToElementText(document.getElementById(containerid));
																	range.select();
															} else if (window.getSelection) {
																	var range = document.createRange();
																	range.selectNode(document.getElementById(containerid));
																	window.getSelection().addRange(range);
															}
													}
											</script>															
										
									</div>
								</aside>	
								
								
								
							</div>
							
							
						</div>
					</div>
				</div>
			</section>
			<!-- section end -->

			<!-- footer top start -->
			<!-- ================ -->
			<div class="footer-top" style="padding-bottom:0;margin-bottom:0;">

			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<footer id="footer" class="clearfix">
				<!-- .subfooter start -->
				<!-- ================ -->
				<div class="subfooter">
					<div class="container">
						<div class="subfooter-inner">
							<div class="row">
								<div class="col-md-12 text-center">
									<p class="txt-sm">
										<a target="_blank" href="<?php echo site_url("siteman"); ?>">Klik disini untuk masuk dan beraktivitas dalam ruang tertutup login </a>
										<br />&copy; 2016 Perkumpulan IDEA Yogyakarta. Hakcipta dilindungi oleh Undang-undang.
									</p>
								</div>
								<div class="col-md-12">
									<p class="txt-sm text-right">
											<ul class="social-links">
												<?php 
												if(array_key_exists("twitter",$info)){
													echo "<li class=\"twitter\"><a target=\"_blank\" href=\"".$info['twitter']."\"><i class=\"fa fa-twitter\"></i></a></li>";
												}
												if(array_key_exists("facebook",$info)){
													echo "<li class=\"facebook\"><a target=\"_blank\" href=\"".$info['facebook']."\"><i class=\"fa fa-facebook\"></i></a></li>";
												}
												if(array_key_exists("instagram",$info)){
													echo "<li class=\"instagram\"><a target=\"_blank\" href=\"".$info['instagram']."\"><i class=\"fa fa-instagram\"></i></a></li>";
												}
												?>
											</ul>
									</p>
									
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- .subfooter end -->

			</footer>
			<!-- footer end -->
		<!-- Scroll-up -->
		<link rel="stylesheet" href="<?php echo base_url('themes/idea/assets/plugins/scrolltotop/css/scrollToTop.min.css'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('themes/idea/assets/plugins/rs-plugin/css/settings.css'); ?>" media="screen" />

		<div class="scrollToTop circle"><i class="fa fa-angle-up"></i></div>

		<script src="<?php echo base_url("themes/idea/assets/plugins/bootstrap-submenu/");?>js/bootstrap-submenu.min.js" defer></script>
		<script src="<?php echo base_url('themes/idea/assets/js/autoNumeric-min.js'); ?>" defer></script>
		<script src="<?php echo base_url('themes/idea/assets/js/jquery.magnific-popup.js'); ?>" defer></script>
		<script src="<?php echo base_url('themes/idea/assets/js/jquery.counterup.min.js'); ?>" defer></script>
		<script src="<?php echo base_url('themes/idea/assets/js/masonry.pkgd.min.js'); ?>" defer></script>
		<script src="<?php echo base_url('themes/idea/assets/js/jquery.parallax.js'); ?>" defer></script>
		<script src="<?php echo base_url('themes/idea/assets/js/jquery.fitvids.js'); ?>" defer></script>
		<script src="<?php echo base_url('themes/idea/assets/plugins/scrolltotop/jquery-scrollToTop.min.js'); ?>" defer></script>
		<script src="<?php echo base_url('themes/idea/assets/js/scripts.js'); ?>" defer></script>

		<!-- DataTables CSS -->
<link href="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/datatables/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<!-- Datatables-->
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/datatables/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>jszip/jszip.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/js/buttons.colVis.min.js"></script>

		<!--Highchart.js-->
<script type="text/javascript" src="<?php echo base_url("themes/idea/assets/plugins/"); ?>highcharts/highcharts.js"></script>
<script type="text/javascript" src="<?php echo base_url("themes/idea/assets/plugins/"); ?>highcharts/highcharts-3d.js"></script>
<script type="text/javascript" src="<?php echo base_url("themes/idea/assets/plugins/"); ?>highcharts/modules/exporting.js"></script>

<!--Highchart.js Thems-->
<script type="text/javascript" src="<?php echo base_url("themes/idea/assets/plugins/"); ?>highcharts/themes/sand-signika.js"></script>

		<script>
	
$(document).ready(function() {
	$('table.datatables').DataTable( {
		"language": {
						"url": "<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/datatables_ID.js"
				},
		dom: 'Bfrtip',
		buttons: [
			'excelHtml5',
			'csvHtml5',
			{extend: 'pdfHtml5',
				orientation: 'landscape',
				pageSize: 'A4'
			},
			'print',
			{
				extend: 'colvis',
				columns: ':gt(2)'
			}
		]
	});
	
	
	<?php
	
	if($summary){
		?>
			$('#graph_container').highcharts({
				chart: {
						type: 'column',
						options3d: {
								enabled: true,
								alpha: 15,
								beta: 1,
								viewDistance: 25,
								depth: 40
						}
				},
				title: {
						text: 'Anggaran Pendapatan dan Belanja <?php echo  $rekening." - ".$wilayah;?>'
				},
				yAxis: {
					min: 0, 
					title: {text: 'Nominal (dlm Juta Rupiah)'},
				},
				xAxis: {
						categories: [<?php echo $XAxis; ?>]
				},
				tooltip:{
					crosshairs: [false, true],
				},
				plotOptions: {
						column: {
								depth: 40
						}
				},
				series: [
				<?php
				$tg = 1;
				$ntg = count($data_pie);
				foreach($data_pie as $item){
					$strComma = ($tg < $ntg)? ", ":"";
					echo "{
						name: '".$item["nama"]."',
						data: [".$item["nilai"]."],
					}".$strComma;
					$tg++;
				}
				?>]
			});		
		<?php

		if($pie){
			foreach($tahuns as $thn){
				?>
				Highcharts.chart('pie_container_<?php echo $thn;?>', {
						chart: {
								type: 'pie',
								options3d: {
										enabled: true,
										alpha: 45,
										beta: 0
								}
						},
						title: {
								text: 'Komponen Penyusun <?php echo $rekening ." ". $thn; ?>'
						},
						tooltip: {
								pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
						},
						plotOptions: {
								pie: {
										allowPointSelect: true,
										cursor: 'pointer',
										depth: 35,
										dataLabels: {
												enabled: true,
												format: '{point.name}'
										}
								}
						},
						series: [{
								type: 'pie',
								name: 'Browser share',
								data: [
									<?php 
									//echo var_dump($toPie[$thn]);
									$n = count($toPie[$thn]);
									$i=1;
									foreach($toPie[$thn] as $key=>$item){
										$strKoma = ($i < $n) ? ",":"";
										echo "['".$item['uraian']."',".$item['nominal']."]".$strKoma;
										$i++;
									}
									?>
								]
						}]
				});
				<?php
			}
		}
	}
	?>
	
});	
</script>
		<script>
			$(document).ready(function(){
				$('[data-submenu]').submenupicker();
				$('body').scrollToTop({
					skin: 'cycle'
				});
			});
		</script>

		<!--Facebook -->
		<!-- Load Facebook SDK for JavaScript -->
		<div id="fb-root"></div>
		<script>
			window.fbAsyncInit = function() {
				FB.init({
					appId      : '577542629096084',
					xfbml      : true,
					version    : 'v2.7'
				});
			};

			(function(d, s, id){
				 var js, fjs = d.getElementsByTagName(s)[0];
				 if (d.getElementById(id)) {return;}
				 js = d.createElement(s); js.id = id;
				 js.src = "//connect.facebook.net/id_ID/sdk.js";
				 fjs.parentNode.insertBefore(js, fjs);
			 }(document, 'script', 'facebook-jssdk'));
		</script>	
		<!--LinkedIn-->
		<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: in_ID</script>
		<!--Twitter-->
		<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

		<!--Google Analitic-->
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-35956217-12', 'auto');
			ga('send', 'pageview');

		</script>			
		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<script type="text/javascript">
			var suka = $("a.suka");
			if(suka){
				$("a.suka").click(function(){
					event.preventDefault();
					var t = $(this).find("strong");
					var idnya = $(this).attr("id");var urlnya = "<?php echo site_url("sapi/suka/")?>"+idnya;
					$.ajax({url: urlnya, success: function(result){
						t.html(result);
					}	
					});
				});
			}
			
		</script>
		
	</Body>
</html>			

<?php 
//$this->load->view('pubs/footer');
