<?php 
$this->load->view('front/pubs/header');
?>
			<!-- section start -->
			<!-- ================ -->
			<section class="clearfix">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class=" section">
<!-- Rangkuman -->								
<?php
// 								echo "
// 								<h2><a class=\"logo-font\" href=\"".site_url('institusi/opd/'.$lembaga['id'].'/'.fixNamaUrl($lembaga['nama']).'/')."\">".$lembaga['nama'] ."</a></h2>
// 								<h4 class=\"sublogo-font\"><a href=\"".site_url('apbd/pemda/'.$lembaga['kodewilayah'])."\">".$lembaga['namawilayah'] ."</a></h4>
// 								";
// echo var_dump($summary);

if($summary){
	echo "
	<div class=\"box box-default\">
		<div class=\"box-header with-border\"><h3 class=\"box-title\">".$rekenin_title."</h3></div>
		<div class=\"box-body\">
			<!-- grafik -->
				<div id=\"graph_container\" class=\"chart\"></div>
			<!-- /grafik -->
			
			<!-- tabular -->
			<table class=\"table table-bordered table-responsive datatables\">
			<thead><tr><th style=\"width:30px;\">#</th>
				<th>Kode Rekening</th>
				<th>Uraian</th>";

				// print_r($tahuns).'kampret';

				$XAxis = "";
				$x=1;
				$xn=count($tahuns);
				
				foreach($tahuns as $tahun){
					$strKoma = ($x < $xn) ? ",":"";
					$XAxis .= "'".$tahun."' ".$strKoma;
					$x++;
					echo "<th>".$tahun."</th>";
				}
				echo "
			</tr></thead>
			<tbody>";
			$toGraph = array();
			$toPie = array();
			$nomer = 1;
			
			foreach($summary['data'] as $key=>$rs){ //print_r($summary['akun']);
				
				echo "<tr><td class=\"angka\">".$nomer."</td>
				<td><a href=\"".site_url('institusi/opd/'.$lembaga['id'].'/'.fixNamaUrl($lembaga['nama']).'/?r='.$rs['rekening'])."\">".$rs['rekening']."</a></td>
				<td><a href=\"".site_url('institusi/opd/'.$lembaga['id'].'/'.fixNamaUrl($lembaga['nama']).'/?r='.$rs['rekening'])."\">".$rs['uraian']."</a></td>";
				$tg = 1;
				$nilai = "";
				foreach($tahuns as $tahun){
					$strKoma = ($tg < $xn)? ", ":"";

					// if(array_key_exists($tahun, $summary['akun'])){
					// 	if(array_key_exists($rs['rekening'],$summary['akun'][$tahun])){
					// 		$rupiah = $summary['akun'][$tahun][$rs['nominal']];
					// 	}else{
					// 		$rupiah = 0;
					// 	}

					// 	$angka = ($rupiah > 0) ? ($rupiah / 1000000) : 0;
					// 	echo "<td class=\"angka\"><a href=\"".site_url('institusi/opd/'.$lembaga['id'].'/'.fixNamaUrl($lembaga['nama']).'/?r='.$rs['rekening'].'&amp;y='.$tahun)."\">".number_format($angka,2)."</a></td>";
					// 	$nilai .= number_format($angka,2,".","") . $strKoma;
					// 	$tg++;

					// }else{
					// 	$rupiah = 0;
					// 	echo "<td class=\"angka\">".number_format($rupiah,2)."</td>";
					// 	$angka = ($rupiah > 0) ? ($rupiah / 1000000) : 0;
					// 	$nilai .= number_format($angka,2,".","") . $strKoma;
					// 	$tg++;
						
					// }
					$rupiah = $rs['nominal'];

						$angka = ($rupiah > 0) ? ($rupiah / 1000000) : 0;
						echo "<td class=\"angka\"><a href=\"".site_url('institusi/opd/'.$lembaga['id'].'/'.fixNamaUrl($lembaga['nama']).'/?r='.$rs['rekening'].'&amp;y='.$tahun)."\">".$rupiah."</a></td>";
						$nilai .= number_format($angka,2,".","") . $strKoma;
						$tg++;
					$toPie[$tahun][$rs['rekening']] = array('akun'=>$rs['rekening'],'uraian'=>$rs['uraian'],'nominal'=>$angka);
				}
				echo "
				</tr>";
				$toGraph[$rs["rekening"]] = array("nama"=>$rs["uraian"], "nilai"=>$rupiah);
				$nomer++;
			}
			echo "
			</tbody>
			</table>

			<!-- tabular -->
			
			<!-- pie -->";
			if($pie){
				foreach($tahuns as $thn){
					echo "
					<div class=\"box box-primary\">
						<div class=\"box-header with-border\">
							<h3 class=\"box-title\">Grafik Distribusi Komponen Mata Anggaran Tahun <strong>".$thn."</strong></h3>
						</div>
						<div class=\"box-body\">
							<div class=\"chart\" id=\"pie_container_".$thn."\"></div>
						</div>
					</div>
					";
				}
			}
			echo "
			<!-- /pie -->
			
			

		</div>
	</div>
	";	
}

if($programs){
	
}
?>
<!-- /Rangkuman -->								

								
							</div>

								
						</div>
						<!--<div class="col-md-4 bg-blue-sky">
							<?php 
								//$this->load->view('pubs/sidebar_institusi');
							?>
							
						</div>-->
					</div>
				</div>
			</section>
			<!-- section end -->
<!-- DataTables CSS -->
<link href="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/datatables/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<!-- Datatables-->
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/datatables/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>jszip/jszip.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/buttons/js/buttons.colVis.min.js"></script>

<!--Highchart.js-->
<script type="text/javascript" src="<?php echo base_url("themes/idea/assets/plugins/"); ?>highcharts/highcharts.js"></script>
<script type="text/javascript" src="<?php echo base_url("themes/idea/assets/plugins/"); ?>highcharts/highcharts-3d.js"></script>
<script type="text/javascript" src="<?php echo base_url("themes/idea/assets/plugins/"); ?>highcharts/modules/exporting.js"></script>

<!--Highchart.js Thems-->
<script type="text/javascript" src="<?php echo base_url("themes/idea/assets/plugins/"); ?>highcharts/themes/sand-signika.js"></script>

<script>

// Make monochrome colors and set them as default for all pies
Highcharts.getOptions().plotOptions.pie.colors = (function () {
    var colors = [],
        base = Highcharts.getOptions().colors[0],
        i;

    for (i = 0; i < 10; i += 1) {
        // Start out with a darkened base color (negative brighten), and end
        // up with a much brighter color
        colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
    }
    return colors;
}());
	
$(document).ready(function() {
	$('table.datatables').DataTable( {
		"language": {
						"url": "<?php echo base_url("themes/idea/assets/plugins/"); ?>datatables/datatables_ID.js"
				},
		dom: 'Bfrtip',
		buttons: [
			'excelHtml5',
			'csvHtml5',
			{extend: 'pdfHtml5',
				orientation: 'landscape',
				pageSize: 'A4'
			},
			'print',
			{
				extend: 'colvis',
				columns: ':gt(2)'
			}
		]
	});
	
	
	<?php
	
	if($summary){
		?>
			$('#graph_container').highcharts({
				chart: {
						type: 'column',
						options3d: {
								enabled: true,
								alpha: 15,
								beta: 15,
								viewDistance: 25,
								depth: 40
						}
				},
				title: {
						text: 'Anggaran Pendapatan dan Belanja <?php echo $rekening." - ".$lembaga["nama"];?>'
				},
				yAxis: {
					min: 0, 
					title: {text: 'Nominal (dlm Juta Rupiah)'},
				},
				xAxis: {
						categories: [<?php echo $XAxis; ?>]
				},
				tooltip:{
					crosshairs: [false, true],
				},
				plotOptions: {
						column: {
								depth: 40
						}
				},
				series: [
				<?php
				$tg = 1;
				$ntg = count($toGraph);
				foreach($toGraph as $key=>$item){
					$strComma = ($tg < $ntg)? ", ":"";
					echo "{
						name: '".$item["nama"]."',
						data: [".$item["nilai"]."],
					}".$strComma;
					$tg++;
				}
				?>]
			});		
		<?php
		if($pie){
			foreach($tahuns as $thn){
				?>
				Highcharts.chart('pie_container_<?php echo $thn;?>', {
						chart: {
								type: 'pie',
								options3d: {
										enabled: true,
										alpha: 45,
										beta: 0
								}
						},
						title: {
								text: 'Komponen Penyusun <?php echo $rekening ." ". $thn; ?>'
						},
						tooltip: {
								pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
						},
						plotOptions: {
								pie: {
										allowPointSelect: true,
										cursor: 'pointer',
										depth: 35,
										dataLabels: {
												enabled: true,
												format: '{point.name}'
										}
								}
						},
						series: [{
								type: 'pie',
								name: 'Browser share',
								data: [
									<?php 
									//echo var_dump($toPie[$thn]);
									$n = count($toPie[$thn]);
									$i=1;
									foreach($toPie[$thn] as $key=>$item){
										$strKoma = ($i < $n) ? ",":"";
										echo "['".$item['uraian']."',".$item['nominal']."]".$strKoma;
										$i++;
									}
									?>
								]
						}]
				});
				<?php
			}
		}
	}
	?>
	
});	
</script>
				
<?php 
$this->load->view('front/pubs/footer');
