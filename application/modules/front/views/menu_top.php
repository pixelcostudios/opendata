<!-- Header Section -->
<header id="main-header">
    <div class="inner-container container">
        <div class="l-sec col-xs-8 col-sm-6 col-md-3">
            <a href="#" id="t-logo">
                <span class="title">Sofia Boutique</span>
                <span class="desc">Luxury Hotel</span>
            </a>
        </div>
        <div class="r-sec col-xs-4 col-sm-6 col-md-9">
            <nav id="main-menu">
                <ul class="list-inline">
                    <li class="active"><a href="<?php echo base_url();?>">Home</a></li>
                    <li><a href="<?php echo base_url();?>about-us.html">About Us</a></li>
                    <li><a href="#">Rooms</a>
                        <ul>
                            <?php foreach ($this->model_post->m_post_type('hotel') as $row_key) {?>
                            <li><a href="<?php echo base_url().$row_key->post_slug;?>.html"><?php echo $row_key->post_title;?></a></li>
                            <?php }?>
                        </ul>
                    </li>
                    <li><a href="<?php echo base_url();?>category/explore-hotel.html">Explore Hotel</a></li>
                    <li>
                        <a href="<?php echo base_url();?>category/explore-area.html">Explore Area</a>
                    </li>
                    <li><a href="#">Gallery</a>
                        <ul>
                            <li><a href="<?php echo base_url();?>category/gallery-photo.html">Photo</a></li>
                            <li><a href="<?php echo base_url();?>category/gallery-video.html">Video</a></li>
                            <li><a href="<?php echo base_url();?>category/gallery-360.html">360</a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo base_url();?>contact-us.html">Contact Us</a></li>
                </ul>
            </nav>
            <div id="main-menu-handle" class="ravis-btn btn-type-2"><i class="fa fa-bars"></i><i class="fa fa-close"></i></div><!-- Mobile Menu handle -->
            <a href="#" id="header-book-bow" class="ravis-btn btn-type-2"><span>Book Bow</span> <i class="fa fa-calendar"></i></a>
        </div>
    </div>
    <div id="mobile-menu-container"></div>
</header>
<!-- End of Header Section -->