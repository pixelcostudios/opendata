<footer id="main-footer">
<div class="inner-container container">
    <div class="t-sec clearfix">
        <div class="widget-box col-sm-6 col-md-3">
            <a href="#" id="f-logo">
                <span class="title">Sofia Boutique Hotel</span>
                <span class="desc">Luxury Hotel</span>
            </a>
            <div class="widget-content text-widget">
                <?php foreach ($about as $row_post){echo $row_post->post_summary;}?>
            </div>
        </div>
        <div class="widget-box col-sm-6 col-md-3">
            <h4 class="title">Newsletter</h4>
            <div class="widget-content newsletter">
                <div class="desc">
                    Some description of how your newsletter works will be located in this section.
                </div>
                <form class="news-letter-form">
                    <input type="text" class="email" placeholder="Email">
                    <button type="submit" class="ravis-btn btn-type-2">Sign up Now</button>
                </form>
            </div>
        </div>
        <div class="widget-box col-sm-6 col-md-3">
            <h4 class="title">Latest Posts</h4>
            <div class="widget-content latest-posts">
                <ul>
                    <li class="clearfix">
                        <div class="img-container col-xs-4">
                            <a href="pages/blog-details.html">
                                <img src="<?php echo base_url();?>themes/<?php echo $this->config->item('themes');?>/assets/img/gallery/5.jpg" alt="Room Image">
                            </a>
                        </div>
                        <div class="desc-box col-xs-8">
                            <a href="pages/blog-details.html" class="title">Toronto High End Market</a>
                            <div class="desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque dicta exercitationem hic laudantium nemo quis quo
                                reprehenderit repudiandae ut vel?
                            </div>
                            <a href="pages/blog-details.html" class="read-more">Read More</a>

                        </div>
                    </li>
                    <li class="clearfix">
                        <div class="img-container col-xs-4">
                            <a href="pages/blog-details.html">
                                <img src="<?php echo base_url();?>themes/<?php echo $this->config->item('themes');?>/assets/img/gallery/6.jpg" alt="Room Image">
                            </a>
                        </div>
                        <div class="desc-box col-xs-8">
                            <a href="pages/blog-details.html" class="title">Drop Everything Now</a>
                            <div class="desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus consequuntur corporis dolorem esse fugiat in
                                magnam nostrum qui rerum, sapiente.
                            </div>
                            <a href="pages/blog-details.html" class="read-more">Read More</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="widget-box col-sm-6 col-md-3">
            <h4 class="title">Contact Us</h4>
            <div class="widget-content contact">
                <ul class="contact-info">
                    <li>
                        <i class="fa fa-home"></i>
                        Jl. Palagan Tentara Pelajar, Sleman D.I. Yogyakarta 55581
                    </li>
                    <li>
                        <i class="fa fa-phone"></i>
                        08124567890
                    </li>
                    <li>
                        <i class="fa fa-envelope"></i>
                        info@sofiayogya.com
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="b-sec clearfix">
        <div class="copy-right">
            With
            <i class="fa fa-heart"></i> by
            <a href="http://pixelcostudios.com" target="_blank">Pixel CO Studios</a> © 2018. All Rights Reserved.
        </div>
        <ul class="social-icons list-inline">
            <li>
                <a href="#">
                    <i class="fa fa-facebook"></i>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-twitter"></i>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-google-plus"></i>
                </a>
            </li>
        </ul>
    </div>
</div>
</footer>