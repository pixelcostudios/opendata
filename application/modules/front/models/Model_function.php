<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

class Model_function extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}
	function setting($setting_name)
    {
        $this->db->select('setting_name, setting_value');
        $this->db->from("my_setting");
        $this->db->where('setting_name',$setting_name);
        $result	= $this->db->get();
        return $result->row('setting_value');
    }
    function setting_expl($setting_name) 
	{
		$this->db->select("setting_name, setting_value");
		$this->db->from("my_setting");
		$this->db->where('setting_name',$setting_name);
		$query	= $this->db->get();
		$row	= $query->row();
		$result	= $row->setting_value;
		return $result;
	}
	public function iCrop_Browser($url, $thumb_width, $thumb_height)
	{
		/*$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_AUTOREFERER, true); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_SSLVERSION,3); 
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)") ;
		$data = curl_exec($ch);
		
		//Gleen Ferdinand
		
		curl_close($ch);*/
		$data = file_get_contents($url);
		
		$imgInfo = getimagesize($url);
		$image = imagecreateFROMstring($data);
		
		$width = imagesx($image);
		$height = imagesy($image);
		
		$original_aspect = $width / $height;
		$thumb_aspect = $thumb_width / $thumb_height;
		
		if ( $original_aspect >= $thumb_aspect )
		{
		// If image is wider than thumbnail (in aspect ratio sense)
		$new_height = $thumb_height;
		$new_width = $width / ($height / $thumb_height);
		}
		else
		{
		// If the thumbnail is wider than the image
		$new_width = $thumb_width;
		$new_height = $height / ($width / $thumb_width);
		}
		
		$thumb = imagecreatetruecolor( $thumb_width, $thumb_height );
		
		imagealphablending($thumb, false);
		imagesavealpha($thumb,true);
		$transparent = imagecolorallocatealpha($thumb, 255, 255, 255, 127);
		imagefilledrectangle($thumb, 0, 0, $thumb_width, $thumb_height, $transparent);
		
		// Resize and crop
		imagecopyresampled($thumb,
		$image,
		0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
		0 - ($new_height - $thumb_height) / 2, // Center the image vertically
		0, 0,
		$new_width, $new_height,
		$width, $height);
		
		switch ($imgInfo[2]) {
		  case 1: header('Content-Type: image/gif'); imagegif($thumb,NULL); break;
		  case 2: header('Content-Type: image/jpeg'); imagejpeg($thumb,NULL);  break;
		  case 3: header('Content-Type: image/png'); imagepng($thumb,NULL); break;
		  default:  trigger_error('Failed resize image!', E_USER_WARNING);  break;
		 }
		 
		return $thumb;
	}
	public function iResize_Browser($url, $thumb_width)
	{
		/*$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1); 
		$data = curl_exec($ch);
		
		curl_close($ch);*/
		$data = file_get_contents($url);
		
		$imgInfo = getimagesize($url);
		$image = imagecreateFROMstring($data);
		
		$width = imagesx($image);
		$height = imagesy($image);
		
		$thumb_height = ($thumb_width/$width) * $height;
		
		$thumb = imagecreatetruecolor( $thumb_width, $thumb_height );
		
		imagealphablending($thumb, false);
		imagesavealpha($thumb,true);
		$transparent = imagecolorallocatealpha($thumb, 255, 255, 255, 127);
		imagefilledrectangle($thumb, 0, 0, $thumb_width, $thumb_height, $transparent);
		
		imagecopyresampled($thumb, $image, 0, 0, 0, 0, $thumb_width, $thumb_height, $width, $height);
		
		switch ($imgInfo[2]) {
		  case 1: header('Content-Type: image/gif'); imagegif($thumb,NULL); break;
		  case 2: header('Content-Type: image/jpeg'); imagejpeg($thumb,NULL);  break;
		  case 3: header('Content-Type: image/png'); imagepng($thumb,NULL); break;
		  default:  trigger_error('Failed resize image!', E_USER_WARNING);  break;
		 }
		 
		return $thumb;
	}
	public function youtube_id($url)
	{
	     $pattern = '#^(?:https?://)?(?:www\.)?(?:m\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch\?v=|/watch\?.+&v=))([\w-]{11})(?:.+)?$#x';
	     preg_match($pattern, $url, $matches);
	     return (isset($matches[1])) ? $matches[1] : false;
	}
}