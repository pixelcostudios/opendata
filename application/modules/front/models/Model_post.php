<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

class Model_post extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	function po_slug($post_slug)
	{
		$this->db->select('post_id, user_id, cat_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_link, post_video, post_key, post_desc, post_count, post_comment, date, status, meta_id	meta_type, meta_source, meta_dest, meta_title, meta_slug, meta_date, meta_status');
		$this->db->from("my_post");
		$this->db->join('my_meta', 'my_post.post_id = my_meta.meta_source','left');
		$this->db->where('post_slug',$post_slug);
		$this->db->where('status','1');
		$this->db->limit('1');
		$query	= $this->db->get();
		return $query->result();
	}
	function po_post_limit($post_type,$cat_id,$order,$limit)
	{
		$this->db->select('post_id, user_id, my_post.cat_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_link, post_video, post_key, post_desc, post_count, post_comment, date, status, meta_id	meta_type, meta_source, meta_dest, meta_title, meta_slug, meta_date, meta_status');
		$this->db->from("my_post");
		$this->db->join('my_meta', 'my_post.post_id = my_meta.meta_source','left');
		$this->db->where('post_type',$post_type);
		$this->db->where('my_meta.meta_dest',$cat_id);
		$this->db->where('my_post.status','1');
		$this->db->join('my_cat', 'my_meta.meta_dest = my_cat.cat_id');
		$this->db->order_by('date',$order);
		$this->db->limit($limit);
		$query	= $this->db->get();
		return $query->result();
	}
	// function po_post_limit_offset($post_type,$cat_id,$order,$limit,$offset)
	// {
	// 	$this->db->select('post_id, user_id, my_post.cat_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_video, post_key, post_desc, post_comment, date, status, my_cat.cat_id, cat_title, cat_slug');
	// 	$this->db->from("my_post");
	// 	$this->db->where('post_type',$post_type);
	// 	$this->db->where('my_post.cat_id',$cat_id);
	// 	$this->db->where('my_post.status','1');
	// 	$this->db->join('my_cat', 'my_post.cat_id = my_cat.cat_id');
	// 	$this->db->order_by('date',$order);
	// 	$this->db->limit($limit,$offset);
	// 	$query	= $this->db->get();
	// 	return $query->result();
	// }
	// function po_cat_offset($post_type,$order,$cat_slug,$limit,$offset)
	// {
	// 	$this->db->select('post_id, my_post.user_id, my_post.cat_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_key, post_desc, post_comment, date, status, user_avatar');
	// 	$this->db->from("my_post");
	// 	$this->db->where('my_post.post_type',$post_type);
    //     $this->db->where('my_post.post_up','0');
    //     $this->db->where('my_cat.cat_slug',$cat_slug);
    //     $this->db->where('my_post.status','1');
    //     $this->db->join('my_cat', 'my_post.cat_id = my_cat.cat_id');
    //     $this->db->join('my_user','my_post.user_id=my_user.user_id');
	// 	$this->db->limit($limit);
	// 	$this->db->offset($offset);
	// 	$this->db->order_by('my_post.date',$order);
	// 	$query	= $this->db->get();
	// 	return $query->result();
	// }
	// function po_cat_offset_count($post_type,$cat_slug)
	// {
	// 	$this->db->select('post_id');
	// 	$this->db->from("my_post");
	// 	$this->db->where('my_post.post_type',$post_type);
	// 	$this->db->where('my_post.post_up','0');
	// 	$this->db->where('my_cat.cat_slug',$cat_slug);
	// 	$this->db->where('my_post.status','1');
	// 	$this->db->join('my_cat', 'my_post.cat_id = my_cat.cat_id');
	// 	$this->db->order_by('my_post.date','desc');
	// 	return $this->db->count_all_results();
	// }
	// function po_cat_slug($order,$cat_slug,$limit)
	// {
	// 	$this->db->select('post_id, my_post.user_id, my_post.cat_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_key, post_desc, post_comment, date, status, my_cat.cat_id, cat_title, cat_slug');
	// 	$this->db->from("my_post");
    //     $this->db->where('my_post.post_up','0');
    //     $this->db->where('my_cat.cat_slug',$cat_slug);
    //     $this->db->where('my_post.status','1');
    //     $this->db->join('my_cat', 'my_post.cat_id = my_cat.cat_id');
	// 	$this->db->limit($limit);
	// 	$this->db->order_by('my_post.date',$order);
	// 	$query	= $this->db->get();
	// 	return $query->result();
	// }
	// function po_cat_up_limit($post_type,$cat_up,$order,$limit)
	// {
	// 	$this->db->select('post_id, user_id, my_post.cat_id, post_type, post_slug, post_title, post_content, post_summary, post_comment, date, status');
	// 	$this->db->from('my_post');
	// 	$this->db->join('my_cat', 'my_post.cat_id = my_cat.cat_id', 'left');
	// 	$this->db->where('post_type',$post_type);
	// 	$this->db->where('cat_up',$cat_up);
	// 	$this->db->where('status','1');
	// 	$this->db->order_by('date', $order);
	// 	$this->db->limit($limit);
	// 	$query	= $this->db->get();
	// 	return $query->result();
	// }
	// function po_cat_type($post_type,$cat_slug,$start,$limit)
	// {
	// 	$this->db->select('post_id, user_id, my_post.cat_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_video, post_key, post_desc, post_comment, date, status, my_cat.cat_id, cat_title, cat_slug');
	// 	$this->db->from("my_post");
	// 	$this->db->where('my_post.post_type',$post_type);
    //     $this->db->where('my_post.post_up','0');
    //     $this->db->where('my_cat.cat_slug',$cat_slug);
    //     $this->db->where('my_post.status','1');
    //     $this->db->join('my_cat', 'my_post.cat_id = my_cat.cat_id');
	// 	$this->db->limit($start,$limit);
	// 	$this->db->order_by('my_post.date','desc');
	// 	$query	= $this->db->get();
	// 	return $query->result();
	// }
	// function po_cat_type_count($post_type,$cat_slug)
	// {
	// 	$this->db->select('post_id');
	// 	$this->db->from("my_post");
	// 	$this->db->where('my_post.post_type',$post_type);
	// 	$this->db->where('my_post.post_up','0');
	// 	$this->db->where('my_cat.cat_slug',$cat_slug);
	// 	$this->db->where('my_post.status','1');
	// 	$this->db->join('my_cat', 'my_post.cat_id = my_cat.cat_id');
	// 	$this->db->order_by('my_post.date','desc');
	// 	return $this->db->count_all_results();
	// }
	// function po_cat_id_count($cat_id)
	// {
	// 	$this->db->select('post_id');
	// 	$this->db->from("my_post");
	// 	$this->db->where('cat_id',$cat_id);
	// 	$this->db->where('status','1');
	// 	return $this->db->count_all_results();
	// }
	function po_id($post_id)
	{
		$this->db->select('post_id, user_id, cat_id, post_type, post_slug, post_title, post_content, post_summary, post_comment, date, status');
		$this->db->from('my_post');
		$this->db->where('post_id',$post_id);
		$this->db->where('status','1');
		$query	= $this->db->get();
		return $query->result();
	}
	// function po_type($post_type,$start,$limit)
	// {
	// 	$this->db->select('post_id, my_post.user_id, cat_id, post_type, post_slug, post_title, post_content, post_summary, post_comment, date, status, user_avatar');
	// 	$this->db->from('my_post');
	// 	$this->db->join('my_user','my_post.user_id=my_user.user_id','left');
	// 	$this->db->where('post_type',$post_type);
	// 	$this->db->where('status','1');
	// 	$this->db->limit($start,$limit);
	// 	$this->db->order_by('my_post.date','desc');
	// 	$this->db->order_by('date','DESC');
	// 	$query	= $this->db->get();
	// 	return $query->result();
	// }
	// function po_type_count($post_type)
	// {
	// 	$this->db->select('post_id, my_post.user_id, cat_id, post_type, post_slug, post_title, post_content, post_summary, post_comment, date, status, user_avatar');
	// 	$this->db->from('my_post');
	// 	$this->db->join('my_user','my_post.user_id=my_user.user_id','left');
	// 	$this->db->where('post_type',$post_type);
	// 	$this->db->where('status','1');
	// 	return $this->db->count_all_results();
	// }
	function po_type_limit($post_type,$order,$limit)
	{
		$this->db->select('post_id, user_id, cat_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_link, post_video, post_key, post_desc, post_count, post_comment, date, status, meta_id	meta_type, meta_source, meta_dest, meta_title, meta_slug, meta_date, meta_status');
		$this->db->from("my_post");
		$this->db->join('my_meta', 'my_post.post_id = my_meta.meta_source','left');
		$this->db->where('post_type',$post_type);
		$this->db->where('my_post.status','1');
		$this->db->order_by('date', $order);
		$this->db->limit($limit);
		$query	= $this->db->get();
		return $query->result();
	}
	// function po_type_offset($post_type,$order,$limit,$offset)
	// {
	// 	$this->db->select('post_id, my_post.user_id, cat_id, post_type, post_slug, post_title, post_content, post_summary, post_comment, date, status, user_avatar');
	// 	$this->db->from('my_post');
	// 	$this->db->join('my_user','my_post.user_id=my_user.user_id');
	// 	$this->db->where('post_type',$post_type);
	// 	$this->db->where('status','1');
	// 	$this->db->order_by('date',$order);
	// 	$this->db->limit($limit);
	// 	$this->db->offset($offset);
	// 	$query	= $this->db->get();
	// 	return $query->result();
	// }
	function po_cat_type($post_type,$meta_slug,$start,$limit)
	{
		$this->db->select('post_id, user_id, cat_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_link, post_video, post_key, post_desc, post_count, post_comment, date, status, meta_id	meta_type, meta_source, meta_dest, meta_title, meta_slug, meta_date, meta_status');
		$this->db->from("my_post");
		$this->db->join('my_meta', 'my_post.post_id = my_meta.meta_source','left');
		$this->db->where('post_type',$post_type);
		$this->db->where('my_meta.meta_slug',$meta_slug);
		$this->db->where('my_post.status','1');
		$this->db->limit($start,$limit);
		$this->db->order_by('my_post.date','desc');
		$query	= $this->db->get();
		return $query->result();
	}
	function po_cat_type_count($post_type,$meta_slug)
	{
		$this->db->select('post_id, user_id, cat_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_link, post_video, post_key, post_desc, post_count, post_comment, date, status, meta_id	meta_type, meta_source, meta_dest, meta_title, meta_slug, meta_date, meta_status');
		$this->db->from("my_post");
		$this->db->join('my_meta', 'my_post.post_id = my_meta.meta_source','left');
		$this->db->where('post_type',$post_type);
		$this->db->where('my_meta.meta_slug',$meta_slug);
		$this->db->where('my_post.status','1');
		return $this->db->count_all_results();
	}
	function po_search($post_type,$cat,$start,$limit)
	{
		$this->db->select('post_id, my_post.user_id, cat_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_key, post_desc, post_count, post_comment, date, status, user_name, user_first_name as user_fullname');
		$this->db->from("my_post");
		$this->db->group_start();
		$this->db->like('post_slug',$cat);
		$this->db->or_like('post_title',$cat);
		$this->db->or_like('post_content',$cat);
		$this->db->or_like('post_summary',$cat);
		$this->db->group_end();
        $this->db->join('my_user', 'my_user.user_id = my_post.user_id', 'left');
		$this->db->where('post_type',$post_type);
		$this->db->limit($start,$limit);
		$this->db->order_by('date','desc');
		$query	= $this->db->get();
		return $query->result();
	}
	function po_search_count($post_type,$cat)
	{
		$this->db->select('post_id, my_post.user_id, cat_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_key, post_desc, post_count, post_comment, date, status, user_first_name as user_fullname');
		$this->db->from("my_post");
		$this->db->group_start();
		$this->db->like('post_slug',$cat);
		$this->db->or_like('post_title',$cat);
		$this->db->or_like('post_content',$cat);
		$this->db->or_like('post_summary',$cat);
		$this->db->group_end();
		$this->db->join('my_user', 'my_user.user_id = my_post.user_id', 'left');
		$this->db->where('post_type',$post_type);
		$this->db->order_by('date','desc');
		return $this->db->count_all_results();
	}
	//Menu
	function m_post_type($post_type)
	{
		$this->db->select('post_slug, post_title');
		$this->db->from('my_post');
		$this->db->where('post_type',$post_type);
		$this->db->where('status','1');
		$this->db->order_by('date','DESC');
		$query	= $this->db->get();
		return $query->result();
	}
	//Count
	function po_update($post_id,$data_array)
	{
		$this->db->where('post_id', $post_id);
		$this->db->update('my_post',$data_array);
		return true;
	}
	function po_count($post_id)
	{
		$this->db->select('post_count');
		$this->db->from('my_post');
		$this->db->where('post_id',$post_id);
		$query	= $this->db->get();
		return $query->row('post_count');
	}
	/**
	* 
	* @param Ipost
	* 
	* @return
	*/
	function ip_list($ipost_all,$ipost_type,$order)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		$this->db->where('ipost_all',$ipost_all);
		$this->db->where('ipost_type',$ipost_type);
		$this->db->order_by('date',$order);
		$query	= $this->db->get();
		return $query->result();
	}
	function ip_list_limit($ipost_all,$ipost_type,$order,$limit)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		$this->db->where('ipost_all',$ipost_all);
		$this->db->where('ipost_type',$ipost_type);
		$this->db->order_by('date',$order);
		$this->db->limit($limit);
		$query	= $this->db->get();
		return $query->result();
	}
	function ip_value($ipost_all,$ipost_type,$order,$value)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		$this->db->where('ipost_all',$ipost_all);
		$this->db->where('ipost_type',$ipost_type);
		$this->db->order_by('date',$order);
		$this->db->limit('1');
		$query	= $this->db->get();
		return $query->row($value);
	}
	function ip_list_count($ipost_all,$ipost_type)
	{
		$this->db->select('ipost_all, ipost_type');
		$this->db->from("my_ipost");
		$this->db->where('ipost_all',$ipost_all);
		$this->db->where('ipost_type',$ipost_type);
		return $this->db->count_all_results();
	}
}