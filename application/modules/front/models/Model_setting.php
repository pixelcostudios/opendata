<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

class Model_setting extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	function setting($setting_name) 
	{
		$this->db->select("setting_name, setting_value");
		$this->db->from("my_setting");
		$this->db->where('setting_name',$setting_name);
		$query	= $this->db->get();
		return $query->row();
	}
	function panel()
	{
		$this->db->select("setting_name, setting_value");
		$this->db->from("my_setting");
		$this->db->where('setting_name','panel');
		$query	= $this->db->get();
		return $query->row('setting_value');
	}	
	function setting_expl($setting_name) 
	{
		$this->db->select("setting_name, setting_value");
		$this->db->from("my_setting");
		$this->db->where('setting_name',$setting_name);
		$query	= $this->db->get();
		return $query->row('setting_value');
	}
	function currency($n)
	{
		$n = (0+str_replace(",","",$n));
		return str_replace(",",".",number_format($n));
	}
	function secondsToTime($inputSeconds) 
	{
	    $secondsInAMinute = 60;
	    $secondsInAnHour  = 60 * $secondsInAMinute;
	    $secondsInADay    = 24 * $secondsInAnHour;

	    // extract days
	    $days = floor($inputSeconds / $secondsInADay);

	    // extract hours
	    $hourSeconds = $inputSeconds % $secondsInADay;
	    $hours = floor($hourSeconds / $secondsInAnHour);

	    // extract minutes
	    $minuteSeconds = $hourSeconds % $secondsInAnHour;
	    $minutes = floor($minuteSeconds / $secondsInAMinute);

	    // extract the remaining seconds
	    $remainingSeconds = $minuteSeconds % $secondsInAMinute;
	    $seconds = ceil($remainingSeconds);

	    // return the final array
	    $obj = array(
	        'd' => (int) $days,
	        'h' => (int) $hours,
	        'm' => (int) $minutes,
	        's' => (int) $seconds,
	    );
	    return $obj;
	}
	function encrypt_decrypt($action, $string) {
	    $output = false;

	    $encrypt_method = "AES-256-CBC";
	    $secret_key = 'dealjogja.com';
	    $secret_iv = 'pixelcostudios';

	    // hash
	    $key = hash('sha256', $secret_key);
	    
	    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
	    $iv = substr(hash('sha256', $secret_iv), 0, 16);

	    if( $action == 'encrypt' ) {
	        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
	        $output = base64_encode($output);
	    }
	    else if( $action == 'decrypt' ){
	        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
	    }
	    return $output;
	}
	function char_random($length)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}
}