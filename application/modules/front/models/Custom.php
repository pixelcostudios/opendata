<?php
/*
 * Apbd_model.php
 * 
 * Copyright 2017 Isnu Suntoro <isnusun@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Custom extends CI_Model {

	public function __construct(){
		parent::__construct();
	}
	function apbds_edit($id)
	{
		$this->db->select('*');
		$this->db->from("tweb_apbds");
		$this->db->where('id',$id);
		$result	= $this->db->get();
		return $result->row();
	}
    function apbds_add($data_array)
	{
		$this->db->insert('tweb_apbds',$data_array);
		return $this->db->insert_id();
	}
	function apbds_rekening_update($rekening,$data_array)
	{
		$this->db->where('rekening', $rekening);
		$this->db->update('tweb_apbds',$data_array);
		return true;
	}
	function apbds_update($id,$data_array)
	{
		$this->db->where('id', $id);
		$this->db->update('tweb_apbds',$data_array);
		return true;
	}
	function apbds_delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tweb_apbds');
		return true;
	}
	function apbds_check($rekening,$uraian,$tahun)
	{
		$this->db->select('*');
		$this->db->from("tweb_apbds");
		$this->db->where('rekening',$rekening);
		$this->db->where('uraian',$uraian);
		$this->db->where('tahun',$tahun);
		return $this->db->count_all_results();
	}
	function apbd_list($kodewilayah)
    {
        $this->db->select('*');
        $this->db->from("tweb_lembaga");
        $this->db->where('kodewilayah',$kodewilayah);
        $query	= $this->db->get();
        return $query->result();
    }
}
