<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}
/**
 * @property M_base_config $M_base_config
 * @property base_config $base_config
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property CI_Lang $lang
 * @property CI_URI $uri
 * @property CI_DB_query_builder|CI_DB_mysqli_driver $db
 * @property CI_Config $config
 * @property CI_Input $input
 * @property CI_User_agent $agent
 * @property CI_Email $email
 * @property Mahana_hierarchy $mahana_hierarchy
 * @property CI_Form_validation $form_validation
 * @property CI_Session session
 * @property CI_Parser parser
 * @property Front front
 * @property CI_Upload upload
 * @property MY_Pagination pagination
 */
class model_category extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}
	function ca_list($cat_type,$cat_up,$order) 
	{
		$this->db->select('cat_id, cat_type, cat_up, cat_title, cat_slug, cat_key, cat_desc');
		$this->db->from("my_cat");
		$this->db->where('cat_type',$cat_type);
		$this->db->where('cat_up',$cat_up);
		$this->db->order_by('cat_id', $order);
		$query	= $this->db->get();
		return $query->result();
	}
	function ca_slug($cat_value,$cat_slug) 
	{
		$this->db->select("cat_id, cat_type, cat_up, cat_title, cat_slug, cat_summary, cat_key, cat_desc");
		$this->db->from("my_cat");
		$this->db->where($cat_value,$cat_slug);
		$query	= $this->db->get();
		$result = $query->row();
		return $result;
	}
	function ca_slug_value($cat_value,$cat_slug) 
	{
		$this->db->select("cat_id, cat_type, cat_up, cat_title, cat_slug, cat_summary, cat_key, cat_desc");
		$this->db->from("my_cat");
		$this->db->where('cat_slug',$cat_slug);
		$query	= $this->db->get();
		$result = $query->row($cat_value);
		return $result;
	}
	function ca_id($cat_type,$cat_id)
	{
		$this->db->select('cat_id, cat_type, cat_up, cat_title, cat_slug, cat_summary, cat_key, cat_desc');
		$this->db->from("my_cat");
		$this->db->where('cat_type',$cat_type, 'cat_id',$cat_id);
		$result	= $this->db->get();
		return $result;
	}
	function ca_title($cat_slug)
	{
		$this->db->select('cat_id, cat_title, cat_slug');
		$this->db->from("my_cat");
		$this->db->where('cat_slug',$cat_slug);
		$result	= $this->db->get();
		return $result->row('cat_title');;
	}
	function ca_up_title($cat_id)
	{
		$this->db->select('cat_id, cat_title, cat_slug');
		$this->db->from("my_cat");
		$this->db->where('cat_id',$cat_id);
		$result	= $this->db->get();
		return $result->row('cat_title');;
	}
}