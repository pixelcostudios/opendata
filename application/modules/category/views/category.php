<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $row_cat->cat_title;?> | <?php echo $meta_title;;?></title>

	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,400italic,700italic%7cPlayfair+Display:400,700%7cGreat+Vibes'
		  rel='stylesheet' type='text/css'><!-- Attach Google fonts -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/<?php echo $this->config->item('themes');?>/assets/css/styles.css"><!-- Attach the main stylesheet file -->

</head>
<body>
	<div class="main-wrapper">
		<?php $this->load->view('front/menu_top');?>

		<!--Breadcrumb Section-->
		<section id="breadcrumb-section" data-bg-img="<?php echo base_url();?>themes/<?php echo $this->config->item('themes');?>/assets/img/breadcrumb-events.jpg">
			<div class="inner-container container">
				<div class="ravis-title">
					<div class="inner-box">
						<div class="title"><?php echo $row_cat->cat_title;;?></div>
						<div class="sub-title"><?php echo $row_cat->cat_summary;?> </div>
					</div>
				</div>
				<div class="breadcrumb">
					<ul class="list-inline">
						<li><a href="<?php echo base_url();?>">Home</a></li>
						<li class="current"><a href="<?php echo base_url();?>category/<?php echo $row_cat->cat_slug;?>.html"><?php echo $row_cat->cat_title;;?></a></li>
					</ul>
				</div>
			</div>
		</section>
		<!--End of Breadcrumb Section-->


		<!-- Past Events -->
		<section id="past-events">
			<div class="inner-container container">

				<div class="event-container">
					<ul class="event-main-box clearfix">
                        <?php foreach ($post_list as $row_post){ $images = $this->model_post->ip_list($row_post->post_id,'post','ASC');?>
						<li class="item col-xs-6 col-md-4">
							<figure>
								<a href="<?php echo base_url().$row_post->post_slug;?>.html" class="more-details">
									<?php foreach ($images as $row_images){?>
                                    <img src="<?php echo base_url();?>upload/crop/370/276/<?php echo $row_images->ipost;?>" alt="<?php echo $row_post->post_title;?>" width="100%" />
                                    <?php }?>
								</a>
								<figcaption>
									<a href="<?php echo base_url().$row_post->post_slug;?>.html">
										<span class="title-box">
											<span class="title"><?php echo $row_post->post_title;?></span>
										</span>
										<span class="desc">
											<?php echo substr(strip_tags($row_post->post_content),0,120);?>...
										</span>
									</a>
								</figcaption>
							</figure>
                        </li>
                        <?php }?>
					</ul>
				</div>

				<!-- Pagination -->
				<div class="pagination-box">
					<ul class="list-inline">
						<li class="active"><a href="#"><span>1</span></a></li>
						<li><a href="#"><span>2</span></a></li>
						<li><a href="#"><span>3</span></a></li>
					</ul>
				</div>
				<!-- End of Pagination -->

			</div>
		</section>
		<!-- End of Past Events -->


		<?php $this->load->view('front/footer');?>

	</div>

	<!-- JS Include Section -->
	<script type="text/javascript" src="<?php echo base_url();?>themes/<?php echo $this->config->item('themes');?>/assets/js/jquery-3.1.0.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>themes/<?php echo $this->config->item('themes');?>/assets/js/helper.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>themes/<?php echo $this->config->item('themes');?>/assets/js/imagesloaded.pkgd.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>themes/<?php echo $this->config->item('themes');?>/assets/js/isotope.pkgd.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>themes/<?php echo $this->config->item('themes');?>/assets/js/template.js"></script>
</body>
</html>