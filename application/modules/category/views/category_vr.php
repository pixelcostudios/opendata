<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Virtual Tour | <?php //echo $meta_title;;?></title>

	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,400italic,700italic%7cPlayfair+Display:400,700%7cGreat+Vibes'
		  rel='stylesheet' type='text/css'><!-- Attach Google fonts -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/<?php echo $this->config->item('themes');?>/assets/css/styles.css"><!-- Attach the main stylesheet file -->

</head>
<body>
	<div class="main-wrapper">
		<?php $this->load->view('front/menu_top');?>

		<!--Breadcrumb Section-->
		<section id="breadcrumb-section" data-bg-img="<?php echo base_url();?>themes/<?php echo $this->config->item('themes');?>/assets/img/breadcrumb-events.jpg">
			<div class="inner-container container">
				<div class="ravis-title">
					<div class="inner-box">
						<div class="title"><?php echo $row_cat->cat_title;;?></div>
						<div class="sub-title"><?php echo $row_cat->cat_summary;?> </div>
					</div>
				</div>
				<div class="breadcrumb">
					<ul class="list-inline">
						<li><a href="<?php echo base_url();?>">Home</a></li>
						<li class="current"><a href="<?php echo base_url();?>category/<?php echo $row_cat->cat_slug;?>.html"><?php echo $row_cat->cat_title;;?></a></li>
					</ul>
				</div>
			</div>
		</section>
		<!--End of Breadcrumb Section-->


		<!-- Past Events -->
		<section id="past-events">
			<div class="inner-container container">

				<iframe width="100%" height="600" src="https://pixelcostudios.github.io/sofiayogya.com/themes/sofia/vr/index.html" frameborder="0">Your browser doesn't support iFrames.</iframe>

			</div>
		</section>
		<!-- End of Past Events -->


		<?php $this->load->view('front/footer');?>

	</div>

	<!-- JS Include Section -->
	<script type="text/javascript" src="<?php echo base_url();?>themes/<?php echo $this->config->item('themes');?>/assets/js/jquery-3.1.0.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>themes/<?php echo $this->config->item('themes');?>/assets/js/helper.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>themes/<?php echo $this->config->item('themes');?>/assets/js/imagesloaded.pkgd.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>themes/<?php echo $this->config->item('themes');?>/assets/js/isotope.pkgd.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>themes/<?php echo $this->config->item('themes');?>/assets/js/template.js"></script>
</body>
</html>