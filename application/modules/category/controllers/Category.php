<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//require APPPATH . "third_party/MX/Controller.php";

	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
        $this->load->library('email');
        // $this->load->library('pdf');
        $this->load->helper(array('form'));
        $this->load->helper(array('url'));
		$this->load->helper(array('path'));
		$this->load->library("pagination");
		$this->load->model("front/model_post");
		$this->load->model("model_category");
		$this->load->model("front/model_setting");
	}
	public function index()
	{
		$data['meta_title'] 			= $this->model_setting->setting('meta_title');
		$data['about']                	= $this->model_post->po_id('1');
    	// $data['product_category']       = $this->model_category->ca_list('product','0','DESC');
    	// $data['blog_category']       	= $this->model_category->ca_list('post','6','DESC');
    	// $data['artikel']                = $this->model_post->po_cat_up_limit('post','6','DESC','3');
		// $data['artikel_more']           = $this->model_post->po_post_offset_limit('post','6','DESC','5','5');
		// $data['product']                = $this->model_post->po_type_limit('product','DESC','5');
		// $data['row_user'] 				= (!empty($_SESSION['user_id'])) ? $this->model_user->user_edit($_SESSION['user_id']) : '';
		if(isset($_POST['Pixel_Search']))
        {
            setcookie('status','search', time()+2);
            redirect(base_url().'search/'.$this->model_setting->encrypt_decrypt('encrypt', $this->input->post('search')).'.html');
        }
    
    	switch ($this->uri->segment(2)) 
    	{
    		case 'product':
			$data['total_pages']    	= $this->model_post->po_cat_all_count('product');
			$data['show']    			= '2';

			$post_type 					= 'product';

			$config = array();
            $config["base_url"]         = base_url().'category/'.$this->uri->segment(2).'/';
            $total_row                  = $this->model_post->po_cat_all_count($post_type);
            $config['suffix'] 			= '.html';
			$config['first_url']        = '1.html';
			$config["total_rows"]       = $total_row;
			$config['attributes'] = array('class' => 'page btn btn-prim-col');
			$config["per_page"]         = '8';
			$config['use_page_numbers'] = TRUE;
			$config['num_links']        = $total_row;
			$config['cur_tag_open']     = '&nbsp;<a class="current btn btn-sec-col">';
			$config['cur_tag_close']    = '</a>';
			$config['next_link']        = '<i class="arrow_carrot-right"></i>';
			$config['prev_link']        = '<i class="arrow_carrot-left"></i>';
			$config['last_link']        = '<i class="arrow_carrot-2right"></i>';
			$config['num_links'] 		= 5;

            $this->pagination->initialize($config);
            if($this->uri->segment(3))
            {
                $page = ($this->uri->segment(3)-1)*$config["per_page"] ;
            }
            else
            {
                $page = 0;
            }
            $data["post_list"]          = $this->model_post->po_cat_all($post_type,$config["per_page"], $page);

	        $str_links                  = $this->pagination->create_links();
	        $data["links"]              = explode('&nbsp;',$str_links );
    		
	        
			$this->load->view('category_product_all',$data);
			break;

    		default:
    			$data['row_cat']    	= $this->model_category->ca_slug('cat_slug',$this->uri->segment(2)) ;  			
	    		if(count($data['row_cat'])>0)
	    		{
	    			switch ($data['row_cat']->cat_type) 
		    		{
	    				default:
	    				switch ($data['row_cat']->cat_id) 
		    			{
							default:
							$post_type 					= 'post';

		    				$data['total_pages']    	= $this->model_post->po_cat_type_count($post_type,$this->uri->segment(2));
	    					$data['show']    			= '2';

		    				$config = array();
				            $config["base_url"]         = base_url().'category/'.$this->uri->segment(2).'/';
				            $total_row                  = $this->model_post->po_cat_type_count($post_type,$this->uri->segment(2));
				            $config['suffix'] 			= '.html';
				            $config['first_url']        = '1.html';
							$config["total_rows"]       = $total_row;
							$config['attributes'] = array('class' => 'page btn btn-prim-col');
				            $config["per_page"]         = '5';
				            $config['use_page_numbers'] = TRUE;
				            $config['num_links']        = $total_row;
				            $config['cur_tag_open']     = '&nbsp;<a class="current btn btn-sec-col">';
				            $config['cur_tag_close']    = '</a>';
				            $config['next_link']        = '<i class="arrow_carrot-right"></i>';
							$config['prev_link']        = '<i class="arrow_carrot-left"></i>';
							$config['last_link']        = '<i class="arrow_carrot-2right"></i>';
							$config['num_links'] 		= 5;
							

				            $this->pagination->initialize($config);
				            if($this->uri->segment(3))
				            {
				                $page = ($this->uri->segment(3)-1)*$config["per_page"] ;
				            }
				            else
				            {
				                $page = 0;
				            }
				            $data["post_list"]          = $this->model_post->po_cat_type($post_type,$this->uri->segment(2),$config["per_page"], $page);

					        $str_links                  = $this->pagination->create_links();
					        $data["links"]              = explode('&nbsp;',$str_links );
							//print_r($data["post_list"]);
		    				$this->load->view('category',$data);
		    				break;

		    				case '6':
		    				// $data['gallery_cat']            = $this->model_post->po_post('post','3','ASC');
		    				// $data['post_list']         = $this->model_post->po_cat_up('post',$data['row_cat']->cat_id,'DESC');
							$data['meta_title'] 			= $this->model_setting->setting('meta_title');
							$this->load->view('category_vr',$data);
		    				break;
	    				}
	    				break;

	    				case 'product':
	    				switch ($data['row_cat']->cat_id) 
		    			{
		    				default:
		    				$data['total_pages']    	= $this->model_post->po_cat_offset_count('product',$this->uri->segment(2));
	    					$data['show']    			= '6';

		    				$post_type 					= 'product'; //echo $data['total_pages'];

		    				$config = array();
				            $config["base_url"]         = base_url().'category/'.$this->uri->segment(2).'/';
				            $total_row                  = $this->model_post->po_cat_type_count($post_type,$this->uri->segment(2));
				            $config['suffix'] 			= '.html'; //echo $total_row;
				            $config['first_url']        = '1.html';
				            $config["total_rows"]       = $total_row;
				            $config['attributes'] = array('class' => 'page btn btn-prim-col');
				            $config["per_page"]         = '8';
				            $config['use_page_numbers'] = TRUE;
				            $config['num_links']        = $total_row;
				            $config['cur_tag_open']     = '&nbsp;<a class="current btn btn-sec-col">';
				            $config['cur_tag_close']    = '</a>';
				            $config['next_link']        = '<i class="arrow_carrot-right"></i>';
							$config['prev_link']        = '<i class="arrow_carrot-left"></i>';
							$config['last_link']        = '<i class="arrow_carrot-2right"></i>';
				            $config['num_links'] 		= 5;

				            $this->pagination->initialize($config);
				            if($this->uri->segment(3))
				            {
				                $page = ($this->uri->segment(3)-1)*$config["per_page"] ;
				            }
				            else
				            {
				                $page = 0;
				            }
				            $data["post_list"]          = $this->model_post->po_cat_type_offset($post_type,$this->uri->segment(2),$config["per_page"], $page);

					        $str_links                  = $this->pagination->create_links();
					        $data["links"]              = explode('&nbsp;',$str_links );
					        
		    				$this->load->view('category_product',$data);
		    				break;

		    				// case '3':
		    				// $data['gallery_cat']            = $this->model_post->po_post('post','3','ASC');
		    				// $data['post_list']         = $this->model_post->po_cat_up('post',$data['row_cat']->cat_id,'DESC');
		    				// $this->load->view('themes/'.$this->config->item('themes').'/category_gallery',$data);
		    				// break;
	    				}
	    				break;
		    		}
	    			break;
	    		}
	    		else
	    		{
	    			$this->load->view('category_404',$data);
	    		}
    	}
    }
}
