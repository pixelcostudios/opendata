<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}
/**
 * @property M_base_config $M_base_config
 * @property base_config $base_config
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property CI_Lang $lang
 * @property CI_URI $uri
 * @property CI_DB_query_builder|CI_DB_mysqli_driver $db
 * @property CI_Config $config
 * @property CI_Input $input
 * @property CI_User_agent $agent
 * @property CI_Email $email
 * @property Mahana_hierarchy $mahana_hierarchy
 * @property CI_Form_validation $form_validation
 * @property CI_Session session
 * @property CI_Parser parser
 * @property Front front
 * @property CI_Upload upload
 * @property MY_Pagination pagination
 */
class model_mail extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}
	function ma_list()
	{
		$this->db->select('mail_id, mail_account, mail_folder, mail_subject, mail_msgno, mail_emailDate, mail_fromName, mail_toAddress, mail_ccAddress, mail_body, mail_status');
		$this->db->from("my_mail");
		$this->db->order_by('mail_emailDate','desc');
		$query	= $this->db->get();
		return $query->result();
	}
	function ma_cat($start,$limit)
	{
		$this->db->select('mail_id, mail_account, mail_folder, mail_subject, mail_msgno, mail_emailDate, mail_fromName, mail_toAddress, mail_ccAddress, mail_body, mail_status');
		$this->db->from("my_mail");
		$this->db->limit($start,$limit);
		$this->db->order_by('mail_emailDate','desc');
		$query	= $this->db->get();
		return $query->result();
	}
	function ma_cat_count()
	{
		$this->db->select('mail_id');
		$this->db->from("my_mail");
		$this->db->order_by('mail_emailDate','desc');
		return $this->db->count_all_results();
	}
	function ma_search($cat,$start,$limit)
	{
		$this->db->select('*');
		$this->db->from("my_mail");
		$this->db->group_start();
		$this->db->like('mail_account',$cat);
		$this->db->or_like('mail_subject',$cat);
		$this->db->or_like('mail_fromName',$cat);
		$this->db->or_like('mail_toAddress',$cat);
		$this->db->or_like('mail_ccAddress',$cat);
		$this->db->or_like('mail_body',$cat);
		$this->db->group_end();
		$this->db->limit($start,$limit);
		$this->db->order_by('mail_emailDate','desc');
		$query	= $this->db->get();
		return $query->result();
	}
	function ma_search_count($cat)
	{
		$this->db->select('*');
		$this->db->from("my_mail");
		$this->db->group_start();
		$this->db->like('mail_account',$cat);
		$this->db->or_like('mail_subject',$cat);
		$this->db->or_like('mail_fromName',$cat);
		$this->db->or_like('mail_toAddress',$cat);
		$this->db->or_like('mail_ccAddress',$cat);
		$this->db->or_like('mail_body',$cat);
		$this->db->group_end();
		$this->db->order_by('mail_emailDate','desc');
		return $this->db->count_all_results();
	}
	function ma_edit($mail_id)
	{
		$this->db->select('*');
		$this->db->from("my_mail");
		$this->db->where('mail_id',$mail_id);
		$result	= $this->db->get();
		return $result->row();
	}
    function ma_add($data_array)
	{
		$this->db->insert('my_mail',$data_array);
		return $this->db->insert_id();
	}
	function ma_update($mail_id,$data_array)
	{
		$this->db->where('mail_id', $mail_id);
		$this->db->update('my_mail',$data_array);
		return true;
	}
	function ma_update_msg($mail_msgno,$data_array)
	{
		$this->db->where('mail_msgno', $mail_msgno);
		$this->db->update('my_mail',$data_array);
		return true;
	}
	function ma_update_status($mail_id,$mail_status)
	{
		$this->db->set('mail_status', $mail_status); 
		$this->db->where('mail_id', $mail_id);
		$this->db->update('my_mail');
		return true;
	}
	function ma_delete($mail_id)
	{
		$this->db->where('mail_id',$mail_id);
		$this->db->delete('my_mail');
		return true;
	}
	function ma_check($mail_account,$mail_msgno)
	{
		$this->db->select('mail_account,mail_msgno');
		$this->db->from("my_mail");
		$this->db->where("mail_account",$mail_account);
		$this->db->where("mail_msgno",$mail_msgno);
		return $this->db->count_all_results();
	}
	//Attach
	function at_add($data_array)
	{
		$this->db->insert('my_mail_attach',$data_array);
		return $this->db->insert_id();
	}
	function at_update($attach_id,$data_array)
	{
		$this->db->where('attach_id', $attach_id);
		$this->db->update('my_mail_attach',$data_array);
		return true;
	}
	function at_check($attach_account,$attach_msgno)
	{
		$this->db->select('attach_account,attach_msgno');
		$this->db->from("my_mail_attach");
		$this->db->where("attach_account",$attach_account);
		$this->db->where("attach_msgno",$attach_msgno);
		return $this->db->count_all_results();
	}
	function getTextBetweenTags($string, $tagname)
	{
		$pattern = "/<$tagname>(.*?)<\/$tagname>/";
		preg_match($pattern, $string, $matches);
		return $matches[1];
	}
}