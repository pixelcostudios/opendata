<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mail extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//require APPPATH . "third_party/MX/Controller.php";

	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
        $this->load->library('email');
        // $this->load->library('pdf');
        $this->load->helper(array('form'));
        $this->load->helper(array('url'));
		$this->load->helper(array('path'));
		$this->load->library("pagination");
		$this->load->model("model_mail");
	}
	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		else 
		{
			redirect(base_url().'panel/dashboard.html');
		}
	}
	public function item() 
	{
		$mail_list = $this->model_mail->ma_list();
		foreach ($mail_list as $key) 
		{
			echo strip_tags($key->mail_body);
		}
	}
	public function get()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		require_once APPPATH.'third_party/imap.php';

        //set search criteria
        $date = date('d-M-y', strtotime('1 day ago'));
        $term = 'ALL UNDELETED SINCE "'.$date.'"';

        //ignore array of emails
        $exclude = [];

		$email    = 'cs@beskom.co.id';
        $password = 'AbC!123456';
        $host     = 'mail.beskom.co.id';//your email host
		$port     = '143';
		
        // $email    = 'gleenok@gmail.com';
        // $password = 'PK7^jE61';
        // $host     = 'imap.gmail.com';//your email host
        // $port     = '993';
        $savePath = "upload/mail";
        $delete   = false;

        $imap = new Imap($email, $password, $host, $port, 'Inbox', $savePath, $delete);

        //get emails pass in the search term and exclude array
		$emails = $imap->emails($term, $exclude);
		//print_r($emails);
		
		foreach ($emails as $key) 
		{
			if($this->model_mail->ma_check($key['account'],$key['msgno'])>0)
			{
				$data_array = array(  
					'mail_account'      => $key['account'],  
					'mail_folder'     	=> $key['folder'],  
					'mail_subject'      => $key['subject'],
					'mail_emailDate'    => $key['emailDate'],
					'mail_fromName'  	=> $key['fromName'],  
					'mail_toAddress'  	=> $key['toAddress'],  
					'mail_ccAddress'    => ($key['ccAddress']!='') ? $key['ccAddress'] : '',  
					'mail_body'    		=> $key['body'],//($key['body']!='') ? strip_tags($key['body'], '<div>') : '',   
					'mail_status'      	=> '1'
				);  
				$this->model_mail->ma_update_msg($key['msgno'],$data_array);
			}
			else 
			{
				$data_array = array(  
					'mail_account'      => $key['account'],  
					'mail_folder'     	=> $key['folder'],  
					'mail_subject'      => $key['subject'],
					'mail_msgno'     	=> $key['msgno'],
					'mail_emailDate'    => $key['emailDate'],
					'mail_fromName'  	=> $key['fromName'],  
					'mail_toAddress'  	=> $key['toAddress'],  
					'mail_ccAddress'    => ($key['ccAddress']!='') ? $key['ccAddress'] : '',
					'mail_body'    		=> $key['body'],//($key['body']!='') ? strip_tags($key['body'], '<div>') : '',  
					'mail_status'      	=> '1'
				);  
				$this->model_mail->ma_add($data_array);
				$id 			= $this->db->insert_id();

				foreach ($key['attachments'] as $value) 
				{
					$data_attach_array = array(  
						'attach_account'    => $value['account'],  
						'attach_msgno'     	=> $value['msgno'],  
						'attach_file'      	=> $value['file'],
						'attach_fileName'  	=> $value['fileName']
					);  
					$this->model_mail->at_add($data_attach_array);
				}
			}
		}
    }
}
