<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Category Add Editor</title>

    <link href="<?php echo base_url()?>themes/default/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/js/plugins/multiselect/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>themes/default/js/plugins/datetime/bootstrap-datetimepicker.css" rel="stylesheet" />
    <!-- Toastr style -->
    <link href="<?php echo base_url()?>themes/default/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/custom.css" rel="stylesheet">

</head>

<body class="fixed-sidebar">

    <div id="wrapper">

    <?php $this->load->view('menu/nav');?>

        <div id="page-wrapper" class="gray-bg">
        <?php $this->load->view('menu/nav_top');?>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Category </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>panel/dashboard.html">Home</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url()?>panel/categories/type/<?php echo $this->uri->segment(4);?>/10/1.html">Category</a>
                        </li>
                        <li class="active">
                            <strong>Category Editor</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content">

            <div class="row">
            <form name="myform" method="post" enctype="multipart/form-data">
                <div class="col-lg-9 padding-none">
	                <div class="ibox float-e-margins">
	                    <div class="ibox-title">
	                        <h5 class="pull-left margin-right-20">Category Editor</h5>
	                        <a href="<?php echo base_url()?>panel/categories/add<?php if($this->uri->segment(4)!=''){echo '/'.$this->uri->segment(4);}?>.html" class="btn btn-default pull-left">Add New Category</a>
	                        <button type="submit" name="Pixel_Save" class="btn btn-primary pull-right">Save Category</button>
	                        
	                    </div>
	                    <div class="ibox-content">
	                    <div class="form-group">
							<label for="title">Title ID </label>
								<input type="text" class="form-control" id="title" name="cat_title" placeholder="Title ID" required="required">
						</div>
	                    <div class="form-group col-lg-4 col-sm-4 col-xs-12 padding-left-0 padding-xs-none multi_select clearfix">
                        <label for="category">Parent</label><br>
                            <select id="example-multiple-selected" multiple="multiple" class="form-control" name="cat_up">
                                <?php foreach($cat_list as $row_cat){?>
                                    <option value="<?php echo $row_cat->cat_id;?>" <?php //if($row_cat->cat_id==$row->cat_id){?><?php //}?>><?php echo $row_cat->cat_title;?></option>
                                    <?php $cat_list_sub=$this->model_categories->ca_list($row_cat->cat_type,$row_cat->cat_id); foreach($cat_list_sub as $row_cat_sub){?>
                                        <option value="<?php echo $row_cat_sub->cat_id;?>">|-- <?php echo $row_cat_sub->cat_title;?></option>
                                        <?php $cat_list_sub1=$this->model_categories->ca_list($row_cat_sub->cat_type,$row_cat_sub->cat_id); foreach($cat_list_sub1 as $row_cat_sub1){?>
                                            <option value="<?php echo $row_cat_sub1->cat_id;?>">|--|-- <?php echo $row_cat_sub1->cat_title;?></option>
                                            <?php $cat_list_sub2=$this->model_categories->ca_list($row_cat_sub1->cat_type,$row_cat_sub1->cat_id); foreach($cat_list_sub2 as $row_cat_sub2){?>
                                                <option value="<?php echo $row_cat_sub2->cat_id;?>">|--|-- <?php echo $row_cat_sub2->cat_title;?></option>
                                            <?php }?>
                                        <?php }?>
                                    <?php }?>
                                <?php }?>
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-sm-4 col-xs-12 padding-none padding-xs-none">
                            <label for="status">Status</label>
                            <select name="cat_status" class="form-control">
                            	<option value="1">Publish</option>
								<option value="0">Draft</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-sm-4 col-xs-12 padding-right-0 padding-xs-none">
                            <label for="date">Date</label>
                            <div class="input-group">
                                <input name="cat_date" class="form-control date-picker" type="text" data-date-format="yyyy-mm-dd hh:mm">
                                <span class="input-group-addon tooltip-demo">
                                    <i class="fa fa-calendar" data-toggle="tooltip" data-placement="top" title="Calendar"></i>
                                </span>
                            </div>
                        </div>
						<div class="form-group clear">
							<label for="summary">Content ID</label>
							<textarea id="content_id" name="cat_summary"></textarea>
						</div>
                        <div class="form-group" >
                            <label for="title">Summary ID</label>
                            <textarea class="form-control" id="summary_id" name="cat_desc" placeholder="SEO Desc ID"></textarea>
                        </div>
						<div class="form-group" >
							<label for="title">SEO Key ID</label>
							<input type="text" class="form-control" name="cat_key" placeholder="SEO Key ID">
						</div>
						
						<div class="clear"></div>
	                    </div>
	                </div>
	            </div>
	            <div class="col-lg-3 padding-right-0">
	            <div class="bar-title white-bg">
	            	<div class="tabs-container">
                        <div class="tabs-left">
                            <ul class="nav nav-tabs tooltip-demo">
                                <li class="active"><a data-toggle="tab" href="#tab_images" aria-expanded="true"> <i class="fa fa-picture-o" data-toggle="tooltip" data-placement="top" title="Images"></i></a></li>
                                <li class=""><a data-toggle="tab" href="#tab_icon" aria-expanded="false"><i class="fa fa-dot-circle-o" data-toggle="tooltip" data-placement="top" title="Icon"></i></a></li>
                                <li class=""><a data-toggle="tab" href="#tab_thumb" aria-expanded="false"><i class="fa fa fa-camera" data-toggle="tooltip" data-placement="top" title="Thumb"></i></a></li>
                                <li class=""><a data-toggle="tab" href="#tab_background" aria-expanded="false"><i class="fa fa-th-large" data-toggle="tooltip" data-placement="top" title="Background"></i></a></li>
                                <li class=""><a data-toggle="tab" href="#tab_pdf" aria-expanded="false"><i class="fa fa-bookmark" data-toggle="tooltip" data-placement="top" title="PDF"></i></a></li>
                                <li class=""><a data-toggle="tab" href="#tab_files" aria-expanded="false"><i class="fa fa-file" data-toggle="tooltip" data-placement="top" title="Files"></i></a></li>
                            </ul>
                            <div class="tab-content ">
                                <div id="tab_images" class="tab-pane active">
                                    <div class="panel-body">
                                        <div class="input-file"><input type="file" name="upload_photo[]" multiple="multiple" id="input_images"></div>
										
                                    </div>
                                </div>
                                <div id="tab_icon" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="input-file"><input type="file" name="upload_icon[]" multiple="multiple" id="input_icon"></div>
										
                                    </div>
                                </div>
                                <div id="tab_thumb" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="input-file"><input type="file" name="upload_thumb[]" multiple="multiple" id="input_thumb"></div>
										
                                    </div>
                                </div>
                                <div id="tab_background" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="input-file"><input type="file" name="upload_background[]" multiple="multiple" id="input_background"></div>
										
                                    </div>
                                </div>
                                <div id="tab_pdf" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="input-file"><input type="file" name="upload_pdf[]" multiple="multiple" id="input_pdf"></div>
										
                                    </div>
                                </div>
                                <div id="tab_files" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="input-file"><input type="file" name="upload_files[]" multiple="multiple" id="input_files"></div>
										
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    <!--End Tab-->
	            </div>
	            </div>
            </form>
            </div>


            </div>
        <div class="footer">
            <div class="pull-right">
                10GB of <strong>250GB</strong> Free.
            </div>
            <div>
                <strong>Copyright</strong> <?php echo $this->model_setting->setting('company');?> &copy; <?php echo date('Y');?>
            </div>
        </div>

        </div>
        </div>



    <!-- Mainly scripts -->
    <script src="<?php echo base_url()?>themes/default/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url()?>themes/default/js/inspinia.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/multiselect/dist/js/bootstrap-multiselect.js" type="text/javascript" ></script>

    <!--CK Editor-->
    <script src="<?php echo base_url()?>themes/default/js/plugins/ckeditor/ckeditor.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/ckeditor/summary_en.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/ckeditor/summary_id.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/ckeditor/ck.js"></script>

	<!--Files Scripts-->
	<script src="<?php echo base_url()?>themes/default/js/plugins/filestyle/filestyle.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/filestyle/aplication.js"></script>
	
	<!-- Check -->
    <script src="<?php echo base_url()?>themes/default/js/check.js"></script>

	<!--Bootstrap Date Picker-->
	<script src="<?php echo base_url()?>themes/default/js/plugins/datetime/bootstrap-datetimepicker.min.js"></script>
    <!-- Toastr script -->
    <script src="<?php echo base_url()?>themes/default/js/plugins/toastr/toastr.min.js"></script>
    <script>
        $(function () {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": true,
                "preventDuplicates": false,
                "positionClass": "toast-bottom-right",
                "onclick": null,
                "showDuration": "400",
                "hideDuration": "1000",
                "timeOut": "7000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            //toastr.success('Successfully Update Profile', 'Update Profile');
            <?php if(isset($_COOKIE['status'])){ echo $this->model_status->status($_COOKIE['status']);}?>
            <?php if(isset($_COOKIE['status_second'])){ echo $this->model_status->status($_COOKIE['status_second']);}?>
        });
    </script>
	<script>
	    $(".date-picker").datetimepicker({format: 'yyyy-mm-dd hh:ii:ss',autoclose: true,});
	</script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#example-multiple-selected').multiselect();
        });
    </script>
</body>

</html>
