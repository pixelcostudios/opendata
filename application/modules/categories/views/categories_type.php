<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Category</title>
<link href="<?php echo base_url()?>themes/default/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url()?>themes/default/css/animate.css" rel="stylesheet">
<!-- Toastr style -->
<link href="<?php echo base_url()?>themes/default/css/plugins/toastr/toastr.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>themes/default/css/style.css" rel="stylesheet">
<link href="<?php echo base_url()?>themes/default/css/custom.css" rel="stylesheet">

</head>

<body class="fixed-sidebar">
<div id="wrapper">
<?php $this->load->view('menu/nav');?>

<div id="page-wrapper" class="gray-bg">
<?php $this->load->view('menu/nav_top');?>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Category</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo base_url()?>panel/dashboard.html">Home</a>
                </li>
                <li>
                    <a href="<?php echo base_url()?>panel/categories/<?php echo $this->uri->segment(3);?>/<?php echo $this->uri->segment(4);?>/10/1.html">Category</a>
                </li>
                <li class="active">
                    <strong>Category</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <form method="post">
        <div class="col-lg-12 padding-none">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Category</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                    	<div class="col-sm-3">
                            <div class="input-group"><input type="text" placeholder="Search" name="search" class="input-sm form-control"> <span class="input-group-btn">
                                <button type="submit" name="Pixel_Search" class="btn btn-sm btn-primary"> Go!</button> </span></div>
                        </div>
                        <div class="col-sm-6 m-b-xs">
                            <div data-toggle="buttons" class="btn-group">
                                <label onclick="location.href='<?php echo base_url()?>panel/categories/add<?php if($this->uri->segment(3)=='type'){echo '/'.$this->uri->segment(4);}?>.html'" class="btn btn-sm btn-white"> <i class="fa fa-plus"></i> Add New </label>
                            </div>
                        </div>
                        <div class="col-sm-3 m-b-xs">
                        <?php if($this->uri->segment(6)!=''){?>
                        <select class="input-sm form-control input-s-sm inline float-right category-show" id="select-view" onchange="window.location = jQuery('#select-view option:selected').val();">
                            <option value="<?php echo base_url()?>panel/categories/<?php if($this->uri->segment(3)==''){echo 'manage';}else{echo $this->uri->segment(3);}?>/<?php echo $this->uri->segment(4);?>/10/<?php if($this->uri->segment(6)==''){echo '1';}else{echo $this->uri->segment(6);}?>.html" <?php if($this->uri->segment(5)=='10'){?>selected=""<?php }?>>10</option>
                            <option value="<?php echo base_url()?>panel/categories/<?php if($this->uri->segment(3)==''){echo 'manage';}else{echo $this->uri->segment(3);}?>/<?php echo $this->uri->segment(4);?>/25/<?php if($this->uri->segment(6)==''){echo '1';}else{echo $this->uri->segment(6);}?>.html" <?php if($this->uri->segment(5)=='25'){?>selected=""<?php }?>>25</option>
                            <option value="<?php echo base_url()?>panel/categories/<?php if($this->uri->segment(3)==''){echo 'manage';}else{echo $this->uri->segment(3);}?>/<?php echo $this->uri->segment(4);?>/50/<?php if($this->uri->segment(6)==''){echo '1';}else{echo $this->uri->segment(6);}?>.html" <?php if($this->uri->segment(5)=='50'){?>selected=""<?php }?>>50</option>
                            <option value="<?php echo base_url()?>panel/categories/<?php if($this->uri->segment(3)==''){echo 'manage';}else{echo $this->uri->segment(3);}?>/<?php echo $this->uri->segment(4);?>/100/<?php if($this->uri->segment(6)==''){echo '1';}else{echo $this->uri->segment(6);}?>.html" <?php if($this->uri->segment(5)=='100'){?>selected=""<?php }?>>100</option>
                        </select>
                        <?php }else{?>
                        <select class="input-sm form-control input-s-sm inline float-right category-show" id="select-view" onchange="window.location = jQuery('#select-view option:selected').val();">
                            <option value="<?php echo base_url()?>panel/categories/<?php if($this->uri->segment(3)==''){echo 'manage';}else{echo $this->uri->segment(3);}?>/10/<?php if($this->uri->segment(5)==''){echo '1';}else{echo $this->uri->segment(5);}?>.html" <?php if($this->uri->segment(4)=='10'){?>selected=""<?php }?>>10</option>
                            <option value="<?php echo base_url()?>panel/categories/<?php if($this->uri->segment(3)==''){echo 'manage';}else{echo $this->uri->segment(3);}?>/25/<?php if($this->uri->segment(5)==''){echo '1';}else{echo $this->uri->segment(5);}?>.html" <?php if($this->uri->segment(4)=='25'){?>selected=""<?php }?>>25</option>
                            <option value="<?php echo base_url()?>panel/categories/<?php if($this->uri->segment(3)==''){echo 'manage';}else{echo $this->uri->segment(3);}?>/50/<?php if($this->uri->segment(5)==''){echo '1';}else{echo $this->uri->segment(5);}?>.html" <?php if($this->uri->segment(4)=='50'){?>selected=""<?php }?>>50</option>
                            <option value="<?php echo base_url()?>panel/categories/<?php if($this->uri->segment(3)==''){echo 'manage';}else{echo $this->uri->segment(3);}?>/100/<?php if($this->uri->segment(5)==''){echo '1';}else{echo $this->uri->segment(5);}?>.html" <?php if($this->uri->segment(4)=='100'){?>selected=""<?php }?>>100</option>
                        </select>
                        <?php }?>
                        
                        </div>
                    </div>
                    <div class="table-responsive margin-top-10">
                        <table class="table table-striped">
                            <thead>
                            <tr>

                                <th width="1%">
                                	<label>
                                            <input type="checkbox" class="colored-green checkall">
                                            <span class="text"></span>
                                        </label>
                                </th>
                                <th width="1%">Thumb</th>
                                <th>Title </th>
                                <th width="1%">Type</th>
                                <th width="13%">Date</th>
                                <th width="5%">Status</th>
                                <th width="5%">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php for ($i = 0; $i < count($cat_list); ++$i) { $images = $this->model_ipost->ip_list_value($cat_list[$i]->cat_id,'cat_post','DESC');?>
                            <tr>
                                <td>
                                	<label>
                                        <input type="checkbox" name="post-check[]" value="<?php echo $cat_list[$i]->cat_id; ?>" class="colored-green">
                                        <span class="text"></span>
                                    </label>
                                </td>
                                <td><img src="<?php echo base_url()?>upload/<?php if($images!=''){echo $images;}else{echo 'default.png';}?>" width="20" height="20"></td>
                                <td><a href="<?php echo base_url()?>panel/categories/edit/<?php echo $cat_list[$i]->cat_id; ?>/<?php echo $cat_list[$i]->cat_type; ?>.html"><?php echo $cat_list[$i]->cat_title; ?></a>  &nbsp;&nbsp;<a href="<?php echo base_url()?>panel/categories/add/0/<?php echo $cat_list[$i]->cat_type; ?>/sub/<?php echo $cat_list[$i]->cat_id; ?>.html"><i class="fa fa-plus"></i></a> &nbsp;&nbsp;<a href="<?php echo base_url()?>categories/<?php echo $cat_list[$i]->cat_slug; ?>.html"><i class="fa fa-search"></i></a></td>
                                <td><?php echo $cat_list[$i]->cat_type; ?></td>
                                <td><?php echo $cat_list[$i]->cat_date; ?></td>
                                <td><?php if($cat_list[$i]->cat_status=='1'){ ?><span class="label label-primary">Publish</span><?php }else{?><span class="label label-warning">Draft</span><?php }?></td>
                                <td>
                                	<div class="btn-group">
                                    <a class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        Action <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu drop-right">
                                        <li><a href="<?php echo base_url()?>panel/categories/edit/<?php echo $cat_list[$i]->cat_id; ?>/<?php echo $cat_list[$i]->cat_type; ?>.html">Edit</a></li>
                                        <li><a href="<?php echo base_url()?>panel/categories/delete/<?php echo $cat_list[$i]->cat_id; ?>/<?php echo $cat_list[$i]->cat_type; ?>.html">Delete</a></li>
                                    </ul>
                        			</div>
                                </td>
                            </tr>
                                <?php foreach($this->model_categories->ca_list($cat_list[$i]->cat_type,$cat_list[$i]->cat_id,'DESC') as $row_sub){ $images_sub = $this->model_ipost->ip_list_value($row_sub->cat_id,'cat_post','DESC');?>
                                <tr>
                                    <td>
                                        <label>
                                            <input type="checkbox" name="post-check[]" value="<?php echo $row_sub->cat_id; ?>" class="colored-green">
                                            <span class="text"></span>
                                        </label>
                                    </td>
                                    <td><img src="<?php echo base_url()?>upload/<?php if($images_sub!=''){echo $images_sub;}else{echo 'default.png';}?>" width="20" height="20"></td>
                                    <td>|-- <a href="<?php echo base_url()?>panel/categories/edit/<?php echo $row_sub->cat_id; ?>/<?php echo $row_sub->cat_type; ?>.html"><?php echo $row_sub->cat_title; ?></a>  &nbsp;&nbsp;<a href="<?php echo base_url()?>panel/categories/add/0/<?php echo $row_sub->cat_type; ?>/sub/<?php echo $row_sub->cat_id; ?>.html"><i class="fa fa-plus"></i></a> &nbsp;&nbsp;<a href="<?php echo base_url()?>categories/<?php echo $row_sub->cat_slug; ?>.html"><i class="fa fa-search"></i></a></td>
                                    <td><?php echo $row_sub->cat_type; ?></td>
                                    <td><?php echo $row_sub->cat_date; ?></td>
                                    <td><?php if($row_sub->cat_status=='1'){ ?><span class="label label-primary">Publish</span><?php }else{?><span class="label label-warning">Draft</span><?php }?></td>
                                    <td>
                                        <div class="btn-group">
                                        <a class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            Action <i class="fa fa-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu drop-right">
                                            <li><a href="<?php echo base_url()?>panel/categories/edit/<?php echo $row_sub->cat_id; ?>/<?php echo $row_sub->cat_type; ?>.html">Edit</a></li>
                                            <li><a href="<?php echo base_url()?>panel/categories/delete/<?php echo $row_sub->cat_id; ?>/<?php echo $row_sub->cat_type; ?>.html">Delete</a></li>
                                        </ul>
                                        </div>
                                    </td>
                                </tr>
                                    <?php foreach($this->model_categories->ca_list($cat_list[$i]->cat_type,$row_sub->cat_id,'DESC') as $row_sub1){ $images_sub = $this->model_ipost->ip_list_value($row_sub1->cat_id,'cat_post','DESC');?>
                                        <tr>
                                            <td>
                                                <label>
                                                    <input type="checkbox" name="post-check[]" value="<?php echo $row_sub->cat_id; ?>" class="colored-green">
                                                    <span class="text"></span>
                                                </label>
                                            </td>
                                            <td><img src="<?php echo base_url()?>upload/<?php if($images!=''){echo $images;}else{echo 'default.png';}?>" width="20" height="20"></td>
                                            <td>|--|-- <a href="<?php echo base_url()?>panel/categories/edit/<?php echo $row_sub1->cat_id; ?>/<?php echo $row_sub1->cat_type; ?>.html"><?php echo $row_sub1->cat_title; ?></a>  &nbsp;&nbsp;<a href="<?php echo base_url()?>panel/categories/add/0/<?php echo $row_sub1->cat_type; ?>/sub/<?php echo $row_sub1->cat_id; ?>.html"><i class="fa fa-plus"></i></a> &nbsp;&nbsp;<a href="<?php echo base_url()?>categories/<?php echo $row_sub1->cat_slug; ?>.html"><i class="fa fa-search"></i></a></td>
                                            <td><?php echo $row_sub->cat_type; ?></td>
                                            <td><?php echo $row_sub1->cat_date; ?></td>
                                            <td><?php if($row_sub1->cat_status=='1'){ ?><span class="label label-primary">Publish</span><?php }else{?><span class="label label-warning">Draft</span><?php }?></td>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        Action <i class="fa fa-angle-down"></i>
                                                    </a>
                                                    <ul class="dropdown-menu drop-right">
                                                        <li><a href="<?php echo base_url()?>panel/categories/edit/<?php echo $row_sub1->cat_id; ?>/<?php echo $row_sub1->cat_type; ?>.html">Edit</a></li>
                                                        <li><a href="<?php echo base_url()?>panel/categories/delete/<?php echo $row_sub1->cat_id; ?>/<?php echo $row_sub1->cat_type; ?>.html">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                            <?php if(count($cat_list)>0){?>
                            <tfoot>
                            <tr>
                                <td colspan="9" class="footable-visible">
                                	<div class="pull-left margin-top-20">
                                	<div class="btn-group">
                                    <div class="btn-group dropup">
                                        <button type="button" class="btn btn-default btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                            <i class="fa fa-ellipsis-horizontal"></i> Action <i class="fa fa-angle-up"></i>
                                        </button>
                                        <ul class="dropdown-menu dropdown-archive">
                                            <li class="margin-bottom-5">
                                                <button type="button" data-toggle="modal" data-target="#modal_publish" class="btn btn-default btn-sm"><i class="fa fa-check"></i> Publish</button>
                                            </li>
                                            <li class="margin-bottom-5">
                                                <button type="button" data-toggle="modal" data-target="#modal_draft" class="btn btn-default btn-sm"><i class="fa fa-pencil-square-o"></i> UnPublish</button>
                                            </li>
                                            <li>
                                                <button type="button" data-toggle="modal" data-target="#modal_delete" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i> Delete</button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                	</div>
                                	<div class="pull-right">
                                        <ul class="pagination pull-right">

                                            <!-- Show pagination links -->
                                            <?php foreach ($links as $link) {
                                                echo "<li>". $link."</li>";
                                            } ?>
                                	</div>
                                </td>
                            </tr>
                            </tfoot>
                            <?php }?>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    <div class="modal fade" id="modal_publish">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Are you sure ?</h4>

                      </div>
                      <div class="modal-body">
                      <p>Do you want to change this Category to Publish.</p>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="post-publish" value="publish" class="btn btn-primary">Publish</button>
                      </div>
                </div>
              </div>
            </div>
            <div class="modal fade" id="modal_draft">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Are you sure ?</h4>

                      </div>
                      <div class="modal-body">
                      <p>Do you want to change this Category to Draft.</p>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="post-draft" value="draft" class="btn btn-warning">UnPublish</button>
                      </div>
                </div>
              </div>
            </div>
            <div class="modal fade" id="modal_delete">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Are you sure ?</h4>

                      </div>
                      <div class="modal-body">
                      <p>Do you want to delete this Category.</p>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="post-delete" value="delete" class="btn btn-danger">Delete</button>
                      </div>
                </div>
              </div>
            </div>
	</form>
    </div>
</div>
<div class="footer">
    <div class="pull-right">
        10GB of <strong>250GB</strong> Free.
    </div>
    <div>
        <strong>Copyright</strong> <?php //echo $this->model_setting->setting('company');?> &copy; <?php echo date('Y');?>
    </div>
</div>

</div>
</div>

<!-- Mainly scripts -->
<script src="<?php echo base_url()?>themes/default/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url()?>themes/default/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
<script src="<?php echo base_url()?>themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Peity -->
<script src="<?php echo base_url()?>themes/default/js/plugins/peity/jquery.peity.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url()?>themes/default/js/inspinia.js"></script>
<script src="<?php echo base_url()?>themes/default/js/plugins/pace/pace.min.js"></script>

<!-- Check -->
<script src="<?php echo base_url()?>themes/default/js/check.js"></script>

<!-- Peity -->
<script src="<?php echo base_url()?>themes/default/js/demo/peity-demo.js"></script>
<!-- Toastr script -->
<script src="<?php echo base_url()?>themes/default/js/plugins/toastr/toastr.min.js"></script>
<script>
    $(function () {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "progressBar": true,
            "preventDuplicates": false,
            "positionClass": "toast-bottom-right",
            "onclick": null,
            "showDuration": "400",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        //toastr.success('Successfully Update Profile', 'Update Profile');
        <?php if(isset($_COOKIE['status'])){ echo $this->model_status->status($_COOKIE['status']);}?>
        <?php if(isset($_COOKIE['status_second'])){ echo $this->model_status->status($_COOKIE['status_second']);}?>
    });
</script>
<script>
$(document).ready(function(){
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
});
</script>

</body>

</html>
