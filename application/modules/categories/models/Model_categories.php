<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}
/**
 * @property M_base_config $M_base_config
 * @property base_config $base_config
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property CI_Lang $lang
 * @property CI_URI $uri
 * @property CI_DB_query_builder|CI_DB_mysqli_driver $db
 * @property CI_Config $config
 * @property CI_Input $input
 * @property CI_User_agent $agent
 * @property CI_Email $email
 * @property Mahana_hierarchy $mahana_hierarchy
 * @property CI_Form_validation $form_validation
 * @property CI_Session session
 * @property CI_Parser parser
 * @property Front front
 * @property CI_Upload upload
 * @property MY_Pagination pagination
 */
class model_categories extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}
	function ca_list($cat_type,$cat_up,$cat_order)
	{
		$this->db->select('cat_id, cat_type, cat_up, cat_title, cat_slug, cat_summary, cat_key, cat_desc, cat_date, cat_status');
		$this->db->from("my_cat");
		$this->db->where('cat_type',$cat_type);
		$this->db->where('cat_up', $cat_up);
		$this->db->order_by('cat_title',$cat_order);
		$query	= $this->db->get();
		return $query->result();
	}
	function ca_list_up($cat_up)
	{
		$this->db->select('cat_id, cat_type, cat_up, cat_title, cat_slug, cat_summary, cat_key, cat_desc, cat_date, cat_status');
		$this->db->from("my_cat");
		$this->db->where('cat_up', $cat_up);
		$query	= $this->db->get();
		return $query->result();
	}
	function ca_title($cat_id)
	{
		$this->db->select('cat_id, cat_title, cat_slug');
		$this->db->from("my_cat");
		$this->db->where('cat_id',$cat_id);
		$result	= $this->db->get();
		return $result->row('cat_title');;
	}
	function ca_slug($cat_id)
	{
		$this->db->select('cat_id, cat_title, cat_slug');
		$this->db->from("my_cat");
		$this->db->where('cat_id',$cat_id);
		$result	= $this->db->get();
		return $result->row('cat_slug');;
	}
	function ca_edit($cat_id)
	{
		$this->db->select('cat_id, cat_type, cat_up, cat_title, cat_slug, cat_summary, cat_key, cat_desc, cat_date, cat_status');
		$this->db->from("my_cat");
		$this->db->where('cat_id',$cat_id);
		$result	= $this->db->get();
		return $result->row();
	}
	function ca_all($start,$limit)
	{
		$this->db->select('cat_id, cat_type, cat_up, cat_title, cat_slug, cat_summary, cat_key, cat_desc, cat_date, cat_status');
		$this->db->from("my_cat");
		$this->db->where('cat_up', '0');
		$this->db->limit($start,$limit);
		$this->db->order_by('cat_title','ASC');
		$query	= $this->db->get();
		return $query->result();
	}
	function ca_all_count()
	{
		$this->db->select('cat_id, cat_type, cat_up, cat_title, cat_slug, cat_summary, cat_key, cat_desc, cat_date, cat_status');
		$this->db->from("my_cat");
		$this->db->where('cat_up', '0');
		$this->db->order_by('cat_title','ASC');
		return $this->db->count_all_results();
	}
	function ca_cat($cat_type,$start,$limit)
	{
		$this->db->select('cat_id, cat_type, cat_up, cat_title, cat_slug, cat_summary, cat_key, cat_desc, cat_date, cat_status');
		$this->db->from("my_cat");
		$this->db->where('cat_type',$cat_type);
		$this->db->where('cat_up', '0');
		$this->db->limit($start,$limit);
		$this->db->order_by('cat_title','ASC');
		$query	= $this->db->get();
		return $query->result();
	}
	function ca_cat_count($cat_type)
	{
		$this->db->select('cat_id, cat_type, cat_up, cat_title, cat_slug, cat_summary, cat_key, cat_desc, cat_date, cat_status');
		$this->db->from("my_cat");
		$this->db->where('cat_type',$cat_type);
		$this->db->where('cat_up', '0');
		$this->db->order_by('cat_title','ASC');
		return $this->db->count_all_results();
	}
    function ca_cat_search($cat_type,$cat,$start,$limit)
    {
        $this->db->select('cat_id, cat_type, cat_up, cat_title, cat_slug, cat_summary, cat_key, cat_desc, cat_date, cat_status');
        $this->db->from("my_cat");
        $this->db->group_start();
        $this->db->like('cat_slug',$cat);
        $this->db->or_like('cat_title',$cat);
        $this->db->or_like('cat_summary', $cat);
        $this->db->or_like('cat_desc',$cat);
        $this->db->group_end();
        $this->db->where('cat_type',$cat_type);
        $this->db->where('cat_up', '0');
        $this->db->limit($start,$limit);
        $this->db->order_by('cat_title','ASC');
        $query	= $this->db->get();
        return $query->result();
    }
    function ca_cat_search_count($cat_type,$cat)
    {
        $this->db->select('cat_id, cat_type, cat_up, cat_title, cat_slug, cat_summary, cat_key, cat_desc, cat_date, cat_status');
        $this->db->from("my_cat");
        $this->db->group_start();
        $this->db->like('cat_slug',$cat);
        $this->db->or_like('cat_title',$cat);
        $this->db->or_like('cat_summary', $cat);
        $this->db->or_like('cat_desc',$cat);
        $this->db->group_end();
        $this->db->where('cat_type',$cat_type);
        $this->db->where('cat_up', '0');
        $this->db->order_by('cat_title','ASC');
        return $this->db->count_all_results();
    }
	function ca_add($data_array)
	{
		$this->db->insert('my_cat',$data_array);
		return $this->db->insert_id();
	}
	function ca_update($cat_id,$data_array)
	{
		$this->db->where('cat_id', $cat_id);
		$this->db->update('my_cat',$data_array);
		return true;
	}
	function ca_update_status($cat_id,$cat_status)
	{
		$this->db->set('cat_status', $cat_status); 
		$this->db->where('cat_id', $cat_id);
		$this->db->update('my_cat');
		return true;
	}
	function ca_delete($cat_id)
	{
		$this->db->where('cat_id',$cat_id);
		$this->db->delete('my_cat');
		return true;
	}
}