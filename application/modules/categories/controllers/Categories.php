<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//require APPPATH . "third_party/MX/Controller.php";

	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
        $this->load->library('email');
        // $this->load->library('pdf');
        $this->load->helper(array('form'));
        $this->load->helper(array('url'));
		$this->load->helper(array('path'));
		$this->load->library("pagination");
		$this->load->model("menu/model_nav");
		$this->load->model("post/model_ipost");
		$this->load->model("model_categories");
		$this->load->model("post/model_status");
		$this->load->model("setting/model_setting");
	}
	public function index()
	{
		if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
		}
		else 
		{
			redirect(base_url().'panel/dashboard.html');
		}
	}
	public function manage()
	{
		 if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['list_admin_count']       = $this->model_nav->us_list_type_count('admin');
        $data['list_mobile_count'] 	    = $this->model_nav->us_list_type_count('mobile');
		$data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);
		
		if(isset($_POST['Pixel_Search']))
        {
            setcookie('status','search', time()+2);
            redirect(base_url().'panel/categories/search/'.$this->model_setting->my_simple_crypt($this->input->post('search'),$action = 'e').'.html');
		}
		
		if($this->uri->segment(5))
		{
			$config = array();
			$config["base_url"]         = base_url().'panel/categories/manage/'.$this->uri->segment(4).'/';
			$total_row                  = $this->model_categories->ca_all_count();
			$config['suffix'] 			= '.html';
			$config['first_url']        = '1.html';
			$config["total_rows"]       = $total_row;
			$config["per_page"]         = ($this->uri->segment(4)) ? $this->uri->segment(4) : '10';
			$config['use_page_numbers'] = TRUE;
			$config['num_links']        = $total_row;
			$config['cur_tag_open']     = '&nbsp;<a class="active">';
			$config['cur_tag_close']    = '</a>';
			$config['next_link']        = 'Next';
			$config['prev_link']        = 'Prev';
			$config['num_links'] 		= 5;

			$this->pagination->initialize($config);
			if($this->uri->segment(5))
			{
				$page = ($this->uri->segment(5)-1)*$config["per_page"] ;
			}
			else
			{
				$page = 0;
			}
		}
		else 
		{
			$config = array();
			$config["base_url"]         = base_url().'panel/categories/manage/';
			$total_row                  = $this->model_categories->ca_all_count();
			$config['suffix'] 			= '.html';
			$config['first_url']        = '1.html';
			$config["total_rows"]       = $total_row;
			$config["per_page"]         = '10';
			$config['use_page_numbers'] = TRUE;
			$config['num_links']        = $total_row;
			$config['cur_tag_open']     = '&nbsp;<a class="active">';
			$config['cur_tag_close']    = '</a>';
			$config['next_link']        = 'Next';
			$config['prev_link']        = 'Prev';
			$config['num_links'] 		= 5;

			$this->pagination->initialize($config);
			if($this->uri->segment(4))
			{
				$page = ($this->uri->segment(4)-1)*$config["per_page"] ;
			}
			else
			{
				$page = 0;
			}
		}
		$data["cat_list"]           = $this->model_categories->ca_all($config["per_page"], $page);
		$str_links                  = $this->pagination->create_links();
		$data["links"]              = explode('&nbsp;',$str_links );

		if(isset($_POST['post-draft']) && $_POST['post-draft'] != "")
        {
            for ($i=0; $i<count($_POST['post-check']);$i++)
            {
                $cat_id=$_POST['post-check'][$i];

                $this->model_categories->ca_update_status($cat_id,'0');
            }
            setcookie('status','updated', time()+2);
            redirect(base_url(uri_string()).'.html');
        }
        elseif(isset($_POST['post-publish']) && $_POST['post-publish'] != "")
        {
            for ($i=0; $i<count($_POST['post-check']);$i++)
            {
                $cat_id=$_POST['post-check'][$i];

                $this->model_categories->ca_update_status($cat_id,'1');
            }
            setcookie('status','updated', time()+2);
            redirect(base_url(uri_string()).'.html');
        }
        elseif(isset($_POST['post-delete']) && $_POST['post-delete'] != "")
        {
            for ($i=0; $i<count($_POST['post-check']);$i++)
            {
                $cat_id=$_POST['post-check'][$i];

                $this->model_categories->ca_delete($cat_id);
            }
            setcookie('status','updated', time()+2);
            redirect(base_url(uri_string()).'.html');
        }
		$this->load->view('categories',$data);
	}

	public function edit()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		// $this->load->model("back/model_setting");
		// $this->load->model("back/model_nav");
		// $this->load->model("back/model_categories");
		// $this->load->model("back/model_ipost");
        // $this->load->model("back/model_status");
		/**
		* 
		* @var List Menu
		* 
		*/
        $data['list_post_count'] 	    = $this->model_nav->po_list_count('post','0');
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count'] 	    = $this->model_nav->us_list_count();
        $data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);
		
		/**
		* 
		* @var Data
		* 
		*/
		$data['row']				= $this->model_categories->ca_edit($this->uri->segment(4));
		
		$data['images']				= $this->model_ipost->ip_list($this->uri->segment(3),'cat','DESC');
		$data['images_count']		= $this->model_ipost->ip_list_count($this->uri->segment(3),'cat','DESC');
		$data['icon']				= $this->model_ipost->ip_list($this->uri->segment(3),'cat_icon','DESC');
		$data['icon_count']			= $this->model_ipost->ip_list_count($this->uri->segment(3),'cat_icon','DESC');
		$data['thumb']				= $this->model_ipost->ip_list($this->uri->segment(3),'cat_thumb','DESC');
		$data['thumb_count']		= $this->model_ipost->ip_list_count($this->uri->segment(3),'cat_thumb','DESC');
		$data['background']			= $this->model_ipost->ip_list($this->uri->segment(3),'cat_background','DESC');
		$data['background_count']	= $this->model_ipost->ip_list_count($this->uri->segment(3),'cat_background','DESC');
		$data['pdf']				= $this->model_ipost->ip_list($this->uri->segment(3),'cat_pdf','DESC');
		$data['pdf_count']			= $this->model_ipost->ip_list_count($this->uri->segment(3),'cat_pdf','DESC');
		$data['files']				= $this->model_ipost->ip_list($this->uri->segment(3),'cat_files','DESC');
		$data['files_count']		= $this->model_ipost->ip_list_count($this->uri->segment(3),'cat_files','DESC');
				
		/*categories List*/
		$cat_type					= ($this->uri->segment(5)=='') ? 'post' : $this->uri->segment(5);
		$data['cat_list'] 			= $this->model_categories->ca_list($cat_type,'0','DESC');
		
		if(isset($_POST['Pixel_Save']))
		{
			$id						= $this->uri->segment(4);
			$cat_up 				= ($this->input->post('cat_up')=='') ? '0' : $this->input->post('cat_up');
			$cat_title 				= $this->input->post('cat_title');
			$cat_slug 				= strtolower(url_title($this->input->post('cat_title')));
			$cat_summary 			= $this->input->post('cat_summary');
			$cat_key 				= $this->input->post('cat_key');
			$cat_desc 				= $this->input->post('cat_desc');
			$cat_date 				= ($this->input->post('cat_date')=='') ? date('Y-m-d H:i:s') : $this->input->post('cat_date');
			$cat_status 			= $this->input->post('cat_status');
			
			$data_array = array(  
				'cat_up' 		=> $cat_up,  
				'cat_title' 	=> $cat_title,  
				'cat_slug' 		=> $cat_slug,
				'cat_summary' 	=> $cat_summary,  
				'cat_key' 		=> $cat_key,  
				'cat_desc' 		=> $cat_desc,  
				'cat_date' 		=> $cat_date,  
				'cat_status' 	=> $cat_status 
			);  
			$this->model_categories->ca_update($this->uri->segment(4),$data_array);
			
			/**
			* @var Upload Files
			*/
			
			$this->model_ipost->ipost_upload('upload_photo','cat',$id,$_SESSION['user_id']);
			$this->model_ipost->ipost_upload('upload_icon','cat_icon',$id,$_SESSION['user_id']);
			$this->model_ipost->ipost_upload('upload_thumb','cat_thumb',$id,$_SESSION['user_id']);
			$this->model_ipost->ipost_upload('upload_background','cat_background',$id,$_SESSION['user_id']);
			$this->model_ipost->ipost_upload('upload_pdf','cat_pdf',$id,$_SESSION['user_id']);
			$this->model_ipost->ipost_upload('upload_files','cat_files',$id,$_SESSION['user_id']);
			
			if($this->uri->segment(5)=='sub')
			{
                setcookie('status','updated', time()+2);
				redirect(base_url().'panel/categories/edit/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5).'/'.$this->input->post('post_up').'.html');
			}
			else
			{
                setcookie('status','updated', time()+2);
				redirect(base_url(uri_string()).'.html');
			}
		}
		
		if(isset($_POST['Pixel_D_Images']))
		{
			if (isset($_POST['post-check'])){
			for ($i=0; $i<count($_POST['post-check']);$i++) 
				{	
					$i_name=explode(",",$_POST['post-check'][$i]);
				
					unlink("upload/".$i_name[1]);
					$this->model_ipost->ip_delete($i_name[0]);
				}
				setcookie('status','deleted_images', time()+2);
				redirect(base_url(uri_string()).'.html');
			}
			else
			{
				setcookie('status','noimages', time()+2);
				redirect(base_url(uri_string()).'.html');
			}
		}
		if(isset($_POST['Pixel_D_PDF']))
		{
			if (isset($_POST['pdf-check'])){
			for ($i=0; $i<count($_POST['pdf-check']);$i++) 
				{	
					$i_name=explode(",",$_POST['pdf-check'][$i]);
				
					unlink("./upload/files/".$i_name[1]);
					
					$this->model_ipost->ip_delete($i_name[0]);
				}
				setcookie('status','deleted_pdf', time()+2);
				redirect(base_url(uri_string()).'.html');
			}
			else
			{
				setcookie('status','noimages', time()+2);
				redirect(base_url(uri_string()).'.html');
			}
		}
		if(isset($_POST['Pixel_D_Files']))
		{
			if (isset($_POST['files-check'])){
			for ($i=0; $i<count($_POST['files-check']);$i++) 
				{	
					$i_name=explode(",",$_POST['files-check'][$i]);
				
					unlink("./upload/files/".$i_name[1]);
					$this->model_ipost->ip_delete($i_name[0]);
				}
				setcookie('status','deleted_files', time()+2);
				redirect(base_url(uri_string()).'.html');
			}
			else
			{
				setcookie('status','noimages', time()+2);
				redirect(base_url(uri_string()).'.html');
			}
		}
		$this->load->view('categories_edit', $data);
	}
	public function add()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		/**
		* 
		* @var List Menu
		* 
		*/
        $data['list_post_count'] 	    = $this->model_nav->po_list_count('post','0');
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count'] 	    = $this->model_nav->us_list_count();
        $data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);
				
		/*categories List*/
        // if($this->uri->segment(5)=='sub')
        // {
        //     $cat_type                   = ($this->uri->segment(4)=='') ? 'post' : $this->uri->segment(4);
        // }
        // else
        // {
		//     $cat_type					= ($this->uri->segment(3)=='') ? 'post' : $this->uri->segment(3);
		// }
		$cat_type                   = ($this->uri->segment(4)=='') ? 'post' : $this->uri->segment(4);
		
		$data['cat_list'] 			= $this->model_categories->ca_list($cat_type,'0','DESC');
		
		if(isset($_POST['Pixel_Save']))
		{
			$cat_up 				= ($this->input->post('cat_up')=='') ? '0' : $this->input->post('cat_up');
			$cat_title 				= $this->input->post('cat_title');
			$cat_slug 				= strtolower(url_title($this->input->post('cat_title')));
			$cat_summary 			= $this->input->post('cat_summary');
			$cat_key 				= $this->input->post('cat_key');
			$cat_desc 				= $this->input->post('cat_desc');
			$cat_date 				= ($this->input->post('cat_date')=='') ? date('Y-m-d H:i:s') : $this->input->post('cat_date');
			$cat_status 			= $this->input->post('cat_status');
			
			$data_array = array(  
				'cat_up' 		=> $cat_up,  
				'cat_type'  	=> $cat_type,
				'cat_title' 	=> $cat_title,
				'cat_slug' 		=> $cat_slug,
				'cat_summary' 	=> $cat_summary,  
				'cat_key' 		=> $cat_key,  
				'cat_desc' 		=> $cat_desc,  
				'cat_date' 		=> $cat_date,  
				'cat_status' 	=> $cat_status 
			);  
			$this->model_categories->ca_add($data_array);
			$id 			= $this->db->insert_id();
			
			/**
			* 
			* @var Upload Files
			* 
			*/

            $this->model_ipost->ipost_upload('upload_photo','cat',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_icon','cat_icon',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_thumb','cat_thumb',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_background','cat_background',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_pdf','cat_pdf',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_files','cat_files',$id,$_SESSION['user_id']);
			
			setcookie('status','updated', time()+4);
			redirect(base_url().'panel/categories/edit/'.$id.'/'.$cat_type.'.html');
		}
		$this->load->view('categories_add', $data);
	}
	public function delete()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		// $this->load->model("setting/model_setting");
		// $this->load->model("maenu/model_nav");	
		// $this->load->model("model_categories");
        // $this->load->model("model_status");
		
		$this->model_categories->ca_delete($this->uri->segment(3));
		
		redirect(base_url().'panel/categories/manage.html');
	}
	public function type()
	{
		 if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['list_admin_count']       = $this->model_nav->us_list_type_count('admin');
        $data['list_mobile_count'] 	    = $this->model_nav->us_list_type_count('mobile');
		$data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);
		
		$cat_type 					= $this->uri->segment(4);

		if(isset($_POST['Pixel_Search']))
        {
            setcookie('status','search', time()+2);
            redirect(base_url().'panel/categories/search/'.$this->uri->segment(4).'/'.$this->model_setting->my_simple_crypt($this->input->post('search'),$action = 'e').'.html');
        }
		
		if($this->uri->segment(6)!='')
		{
			$config = array();
			$config["base_url"]         = base_url().'panel/categories/type/'.$this->uri->segment(4).'/'.$this->uri->segment(5).'/';
			$total_row                  = $this->model_categories->ca_cat_count($cat_type);
			$config['suffix'] 			= '.html';
			$config['first_url']        = '1.html';
			$config["total_rows"]       = $total_row;
			$config["per_page"]         = ($this->uri->segment(5)) ? $this->uri->segment(5) : '10';
			$config['use_page_numbers'] = TRUE;
			$config['num_links']        = $total_row;
			$config['cur_tag_open']     = '&nbsp;<a class="active">';
			$config['cur_tag_close']    = '</a>';
			$config['next_link']        = 'Next';
			$config['prev_link']        = 'Prev';
			$config['num_links'] 		= 5; //print_r($total_row);

			$this->pagination->initialize($config);
			if($this->uri->segment(6))
			{
				$page = ($this->uri->segment(6)-1)*$config["per_page"] ;
			}
			else
			{
				$page = 0;
			}
		}
		else
		{
			$config = array();
			$config["base_url"]         = base_url().'panel/categories/type/'.$this->uri->segment(4).'/';
			$total_row                  = $this->model_categories->ca_cat_count($cat_type);
			$config['suffix'] 			= '.html';
			$config['first_url']        = '1.html';
			$config["total_rows"]       = $total_row;
			$config["per_page"]         = '10';
			$config['use_page_numbers'] = TRUE;
			$config['num_links']        = $total_row;
			$config['cur_tag_open']     = '&nbsp;<a class="active">';
			$config['cur_tag_close']    = '</a>';
			$config['next_link']        = 'Next';
			$config['prev_link']        = 'Prev';
			$config['num_links'] 		= 5; //print_r($total_row);

			$this->pagination->initialize($config);
			if($this->uri->segment(6))
			{
				$page = ($this->uri->segment(6)-1)*$config["per_page"] ;
			}
			else
			{
				$page = 0;
			}
		}
		$data["cat_list"]           = $this->model_categories->ca_cat($cat_type,$config["per_page"], $page);
		$str_links                  = $this->pagination->create_links();
		$data["links"]              = explode('&nbsp;',$str_links );
		//print_r($data["cat_list"]);
		if(isset($_POST['post-draft']) && $_POST['post-draft'] != "")
        {
            for ($i=0; $i<count($_POST['post-check']);$i++)
            {
                $cat_id=$_POST['post-check'][$i];

                $this->model_categories->ca_update_status($cat_id,'0');
            }
            setcookie('status','updated', time()+2);
            redirect(base_url(uri_string()).'.html');
        }
        elseif(isset($_POST['post-publish']) && $_POST['post-publish'] != "")
        {
            for ($i=0; $i<count($_POST['post-check']);$i++)
            {
                $cat_id=$_POST['post-check'][$i];

                $this->model_categories->ca_update_status($cat_id,'1');
            }
            setcookie('status','updated', time()+2);
            redirect(base_url(uri_string()).'.html');
        }
        elseif(isset($_POST['post-delete']) && $_POST['post-delete'] != "")
        {
            for ($i=0; $i<count($_POST['post-check']);$i++)
            {
                $cat_id=$_POST['post-check'][$i];

                $this->model_categories->ca_delete($cat_id);
            }
            setcookie('status','updated', time()+2);
            redirect(base_url(uri_string()).'.html');
        }
		$this->load->view('categories_type',$data);
	}
	public function search()
	{
		 if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['list_admin_count']       = $this->model_nav->us_list_type_count('admin');
        $data['list_mobile_count'] 	    = $this->model_nav->us_list_type_count('mobile');
		$data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);
		
		$cat_type 						= ($this->uri->segment(4)!='') ? $this->uri->segment(4) : 'post';
		$cat                        = $this->model_setting->my_simple_crypt($this->uri->segment(5),$action = 'd');

		if(isset($_POST['Pixel_Search']))
        {
            setcookie('status','search', time()+2);
            redirect(base_url().'panel/categories/search/'.$this->uri->segment(4).'/'.$this->model_setting->my_simple_crypt($this->input->post('search'),$action = 'e').'.html');
		}
		
		if($this->uri->segment(6))
		{
			$config = array();
			$config["base_url"]         = base_url().'panel/categories/search/'.$this->uri->segment(4).'/';
			$total_row                  = $this->model_categories->ca_cat_search_count($cat_type,$cat);
			$config['suffix'] 			= '.html';
			$config['first_url']        = '1.html';
			$config["total_rows"]       = $total_row;
			$config["per_page"]         = ($this->uri->segment(6)) ? $this->uri->segment(6) : '10';
			$config['use_page_numbers'] = TRUE;
			$config['num_links']        = $total_row;
			$config['cur_tag_open']     = '&nbsp;<a class="active">';
			$config['cur_tag_close']    = '</a>';
			$config['next_link']        = 'Next';
			$config['prev_link']        = 'Prev';
			$config['num_links'] 		= 5;

			$this->pagination->initialize($config);
			if($this->uri->segment(7))
			{
				$page = ($this->uri->segment(7)-1)*$config["per_page"] ;
			}
			else
			{
				$page = 0;
			}
		}
		else 
		{
			$config = array();
			$config["base_url"]         = base_url().'panel/categories/search/';
			$total_row                  = $this->model_categories->ca_cat_search_count($cat_type,$cat);
			$config['suffix'] 			= '.html';
			$config['first_url']        = '1.html';
			$config["total_rows"]       = $total_row;
			$config["per_page"]         = '10';
			$config['use_page_numbers'] = TRUE;
			$config['num_links']        = $total_row;
			$config['cur_tag_open']     = '&nbsp;<a class="active">';
			$config['cur_tag_close']    = '</a>';
			$config['next_link']        = 'Next';
			$config['prev_link']        = 'Prev';
			$config['num_links'] 		= 5;

			$this->pagination->initialize($config);
			if($this->uri->segment(6))
			{
				$page = ($this->uri->segment(6)-1)*$config["per_page"] ;
			}
			else
			{
				$page = 0;
			}
		}
		$data["cat_list"]           = $this->model_categories->ca_cat_search($cat_type,$cat,$config["per_page"], $page);
		$str_links                  = $this->pagination->create_links();
		$data["links"]              = explode('&nbsp;',$str_links );

		if(isset($_POST['post-draft']) && $_POST['post-draft'] != "")
        {
            for ($i=0; $i<count($_POST['post-check']);$i++)
            {
                $cat_id=$_POST['post-check'][$i];

                $this->model_categories->ca_update_status($cat_id,'0');
            }
            setcookie('status','updated', time()+2);
            redirect(base_url(uri_string()).'.html');
        }
        elseif(isset($_POST['post-publish']) && $_POST['post-publish'] != "")
        {
            for ($i=0; $i<count($_POST['post-check']);$i++)
            {
                $cat_id=$_POST['post-check'][$i];

                $this->model_categories->ca_update_status($cat_id,'1');
            }
            setcookie('status','updated', time()+2);
            redirect(base_url(uri_string()).'.html');
        }
        elseif(isset($_POST['post-delete']) && $_POST['post-delete'] != "")
        {
            for ($i=0; $i<count($_POST['post-check']);$i++)
            {
                $cat_id=$_POST['post-check'][$i];

                $this->model_categories->ca_delete($cat_id);
            }
            setcookie('status','updated', time()+2);
            redirect(base_url(uri_string()).'.html');
        }
		$this->load->view('categories',$data);
	}
}
