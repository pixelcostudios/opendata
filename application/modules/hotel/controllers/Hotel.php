<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hotel extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//require APPPATH . "third_party/MX/Controller.php";

	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
        $this->load->library('email');
        // $this->load->library('pdf');
        $this->load->helper(array('form'));
        $this->load->helper(array('url'));
		$this->load->helper(array('path'));
		$this->load->library("pagination");
		$this->load->model("menu/model_nav");
        $this->load->model("post/model_post");
        $this->load->model("post/model_ipost");
		$this->load->model("setting/model_setting");
        $this->load->model("model_meta");
        $this->load->model("member/model_member");
	}
	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		$data['list_post_count'] 	    = $this->model_nav->po_list_count('post','0');
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count'] 	    = $this->model_nav->us_list_count();
        $data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);
        $data['cat_list']               = $this->model_nav->ca_list('post','0');

        $post_type 						= 'hotel';//($this->uri->segment(3)) ? $this->uri->segment(3) : 'post';

        //pagination settings
        if(isset($_POST['Pixel_Search']))
        {
            setcookie('status','search', time()+2);
            redirect(base_url().'panel/hotel/search/'.$this->model_setting->my_simple_crypt($this->input->post('search'),$action = 'e').'.html');
        }

        if($this->uri->segment(3)=='')
        {
            $config = array();
            $config["base_url"]         = base_url().'panel/hotel/all/10/';
            $total_row                  = $this->model_post->po_cat_count($post_type);
            $config['suffix']           = '.html';
            $config['first_url']        = '1.html';
            $config["total_rows"]       = $total_row;
            $config["per_page"]         = '10';
            $config['use_page_numbers'] = TRUE;
            $config['num_links']        = $total_row;
            $config['cur_tag_open']     = '&nbsp;<a class="active">';
            $config['cur_tag_close']    = '</a>';
            $config['next_link']        = 'Next';
            $config['prev_link']        = 'Prev';
            $config['num_links']        = 5;

            $this->pagination->initialize($config);
            if($this->uri->segment(4))
            {
                $page = ($this->uri->segment(4)-1)*$config["per_page"] ;
            }
            else
            {
                $page = 0;
            }
            $data["post_list"]          = $this->model_post->po_cat($post_type,$config["per_page"], $page);
        }
        elseif($this->uri->segment(3)=='all')
        {
            $config = array();
            $config["base_url"]         = base_url().'panel/hotel/all/'.$this->uri->segment(4).'/';
            $total_row                  = $this->model_post->po_cat_count($post_type);
            $config['suffix']           = '.html';
            $config['first_url']        = '1.html';
            $config["total_rows"]       = $total_row;
            $config["per_page"]         = ($this->uri->segment(4)) ? $this->uri->segment(4) : '10';
            $config['use_page_numbers'] = TRUE;
            $config['num_links']        = $total_row;
            $config['cur_tag_open']     = '&nbsp;<a class="active">';
            $config['cur_tag_close']    = '</a>';
            $config['next_link']        = 'Next';
            $config['prev_link']        = 'Prev';
            $config['num_links']        = 5;

            $this->pagination->initialize($config);
            if($this->uri->segment(5))
            {
                $page = ($this->uri->segment(5)-1)*$config["per_page"] ;
            }
            else
            {
                $page = 0;
            }
            $data["post_list"]          = $this->model_post->po_cat($post_type,$config["per_page"], $page);
        }

        $str_links                  = $this->pagination->create_links();
        $data["links"]              = explode('&nbsp;',$str_links );

        $this->load->view('hotel',$data);

	}
    public function all()
    {
        if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        //echo $this->uri->segment(3);
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);
        $data['cat_list']               = $this->model_nav->ca_list('post','0');

        $post_type                      = 'hotel';

        //pagination settings
        if(isset($_POST['Pixel_Search']))
        {
            setcookie('status','search', time()+2);
            redirect(base_url().'panel/hotel/search/'.$this->model_setting->my_simple_crypt($this->input->post('search'),$action = 'e').'.html');
        }

        $config = array();
        $config["base_url"]         = base_url().'panel/hotel/all/'.$this->uri->segment(4).'/';
        $total_row                  = $this->model_post->po_cat_count($post_type);
        $config['suffix']           = '.html';
        $config['first_url']        = '1.html';
        $config["total_rows"]       = $total_row;
        $config["per_page"]         = ($this->uri->segment(4)) ? $this->uri->segment(4) : '10';
        $config['use_page_numbers'] = TRUE;
        $config['num_links']        = $total_row;
        $config['cur_tag_open']     = '&nbsp;<a class="active">';
        $config['cur_tag_close']    = '</a>';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['num_links']        = 5;

        $this->pagination->initialize($config);
        if($this->uri->segment(5))
        {
            $page = ($this->uri->segment(5)-1)*$config["per_page"] ;
        }
        else
        {
            $page = 0;
        }
        $data["post_list"]          = $this->model_post->po_cat($post_type,$config["per_page"], $page);

        $str_links                  = $this->pagination->create_links();
        $data["links"]              = explode('&nbsp;',$str_links );

        $this->load->view('hotel',$data);

    }
	function add()
	{
		if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        /**
        * 
        * @var List Menu
        * 
        */
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);
      
        /*User List*/
        $data['user_list']          = $this->model_member->us_list_type('admin');

        if(isset($_POST['Pixel_Save']))
        {
            $user_id                = $this->input->post('user_id');
            // $cat_id                 = ($this->input->post('cat_id')=='') ? '0' : $this->input->post('cat_id');
            $post_type             = 'hotel';
            $post_up                = ($this->input->post('post_up')=='') ? '0' : $this->input->post('post_up');
            $post_slug              = strtolower(url_title($this->input->post('post_title')));
            $post_title             = $this->input->post('post_title');         
            $post_content           = $this->input->post('post_content');
            $post_summary           = $this->input->post('post_summary');
            $post_link              = $this->input->post('post_link');
            $post_video             = $this->input->post('post_video');
            $post_key               = $this->input->post('post_key');
            $post_desc              = $this->input->post('post_desc');
            $post_comment           = $this->input->post('post_comment');
            $date                   = ($this->input->post('date')=='') ? date('Y-m-d H:i:s') : $this->input->post('date');
            $status                 = $this->input->post('status');
            
            $data_array = array(  
                'user_id'       => $user_id,  
                'post_type'     => $post_type,  
                'post_up'       => $post_up,
                'post_slug'     => $post_slug,
                'post_title'    => $post_title,
                'post_content'  => $post_content,  
                'post_summary'  => $post_summary,  
                'post_link'     => $post_link,  
                'post_video'    => $post_video,  
                'post_key'      => $post_key,  
                'post_desc'     => $post_desc,  
                'post_comment'  => $post_comment,  
                'date'          => $date,  
                'status'        => $status 
            );  
            $this->model_post->po_add($data_array);
			$id 			= $this->db->insert_id();
            
            /**
            * @var Upload Files
            */
            $this->model_ipost->ipost_upload('upload_photo','post',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_icon','post_icon',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_thumb','post_thumb',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_background','post_background',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_pdf','post_pdf',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_files','post_files',$id,$_SESSION['user_id']);
            
            if($this->uri->segment(5)=='sub')
            {
                setcookie('status','updated', time()+2);
                redirect(base_url().'panel/hotel/edit/'.$id.'.html');
            }
            else
            {
                setcookie('status','added', time()+2);
			    redirect(base_url().'panel/hotel/edit/'.$id.'.html');
            }
        }
        $this->load->view('hotel_add', $data);
	}
	function edit()
	{
		if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        /**
        * 
        * @var List Menu
        * 
        */
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);
        
        /**
        * 
        * @var Data
        * 
        */
        $post_id                    = $this->uri->segment(4);
        $data['row']                = $this->model_post->po_edit($post_id);
        
        $data['images']             = $this->model_ipost->ip_list($post_id,'post','DESC');
        $data['icon']               = $this->model_ipost->ip_list($post_id,'post_icon','DESC');
        $data['thumb']              = $this->model_ipost->ip_list($post_id,'post_thumb','DESC');
        $data['background']         = $this->model_ipost->ip_list($post_id,'post_background','DESC');
        $data['pdf']                = $this->model_ipost->ip_list($post_id,'post_pdf','DESC');
        $data['files']              = $this->model_ipost->ip_list($post_id,'post_files','DESC');
    
        /*User List*/
        $data['user_list']          = $this->model_member->us_list_type('admin');

        if(isset($_POST['Pixel_Save']))
        {
            $id                     = $this->uri->segment(4);
            $user_id                = $this->input->post('user_id');
            // $cat_id                 = ($this->input->post('cat_id')=='') ? '0' : $this->input->post('cat_id');
            $post_up                = ($this->input->post('post_up')=='') ? '0' : $this->input->post('post_up');
            $post_slug              = strtolower(url_title($this->input->post('post_title')));
            $post_title             = $this->input->post('post_title');         
            $post_content           = $this->input->post('post_content');
            $post_summary           = $this->input->post('post_summary');
            $post_link              = $this->input->post('post_link');
            $post_video             = $this->input->post('post_video');
            $post_key               = $this->input->post('post_key');
            $post_desc              = $this->input->post('post_desc');
            $post_comment           = $this->input->post('post_comment');
            $date                   = ($this->input->post('date')=='') ? date('Y-m-d H:i:s') : $this->input->post('date');
            $status                 = $this->input->post('status');
            
            $data_array = array(  
                'user_id'       => $user_id,  
                // 'cat_id'        => $cat_id,  
                'post_up'       => $post_up,
                'post_slug'     => $post_slug,
                'post_title'    => $post_title,
                'post_content'  => $post_content,  
                'post_summary'  => $post_summary,  
                'post_link'     => $post_link,  
                'post_video'    => $post_video,  
                'post_key'      => $post_key,  
                'post_desc'     => $post_desc,  
                'post_comment'  => $post_comment,  
                'date'          => $date,  
                'status'        => $status 
            );  
            $this->model_post->po_update($this->uri->segment(4),$data_array);

            /**
            * @var Upload Files
            */
            $this->model_ipost->ipost_upload('upload_photo','post',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_icon','post_icon',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_thumb','post_thumb',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_background','post_background',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_pdf','post_pdf',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_files','post_files',$id,$_SESSION['user_id']);
            
            if($this->uri->segment(5)=='sub')
            {
                setcookie('status','updated', time()+2);
                redirect(base_url().'panel/hotel/edit/'.$this->uri->segment(3).'.html');
            }
            else
            {
                setcookie('status','updated', time()+2);
                redirect(base_url(uri_string()).'.html');
            }
        }
        
        if(isset($_POST['Pixel_D_Images']))
        {
            if (isset($_POST['post-check'])){
            for ($i=0; $i<count($_POST['post-check']);$i++) 
                {   
                    $i_name=explode(",",$_POST['post-check'][$i]);
                
                    if (file_exists('upload/'.$i_name[1]))
			        {
                        unlink("upload/".$i_name[1]);
                    }
                    if (file_exists('upload/c_'.$i_name[1]))
			        {
                        unlink("upload/c_".$i_name[1]);
                    }
                    $this->model_ipost->ip_delete($i_name[0]);
                }
                setcookie('status','deleted_images', time()+2);
                redirect(base_url(uri_string()).'.html');
            }
            else
            {
                setcookie('status','noimages', time()+2);
                redirect(base_url(uri_string()).'.html');
            }
        }
        if(isset($_POST['Pixel_D_PDF']))
        {
            if (isset($_POST['pdf-check'])){
            for ($i=0; $i<count($_POST['pdf-check']);$i++) 
                {   
                    $i_name=explode(",",$_POST['pdf-check'][$i]);
                
                    if (file_exists('upload/files/'.$i_name[1]))
			        {
                        unlink("upload/files/".$i_name[1]);
                    }
                    $this->model_ipost->ip_delete($i_name[0]);
                }
                setcookie('status','deleted_pdf', time()+2);
                redirect(base_url(uri_string()).'.html');
            }
            else
            {
                setcookie('status','noimages', time()+2);
                redirect(base_url(uri_string()).'.html');
            }
        }
        if(isset($_POST['Pixel_D_Files']))
        {
            if (isset($_POST['files-check'])){
            for ($i=0; $i<count($_POST['files-check']);$i++) 
                {   
                    $i_name=explode(",",$_POST['files-check'][$i]);
                
                    if (file_exists('upload/files/'.$i_name[1]))
			        {
                        unlink("./upload/files/".$i_name[1]);
                    }
                    $this->model_ipost->ip_delete($i_name[0]);
                }
                setcookie('status','deleted_files', time()+2);
                redirect(base_url(uri_string()).'.html');
            }
            else
            {
                setcookie('status','noimages', time()+2);
                redirect(base_url(uri_string()).'.html');
            }
        }
        $this->load->view('hotel_edit', $data);
	}
    function search()
    {
        $data['list_pages']         = $this->model_nav->pg_list('pages','0');
        $data['row_user']           = $this->model_nav->us_profile($_SESSION['user_id']);

        $post_type                  = 'hotel';
        $cat                        = $this->model_setting->my_simple_crypt($this->uri->segment(4),$action = 'd');
        if(isset($_POST['Pixel_Search']))
        {
            setcookie('status','search', time()+2);
            redirect(base_url().'panel/hotel/search/'.$this->model_setting->my_simple_crypt($this->input->post('search'),$action = 'e').'.html');
        }

        $config = array();
        $config["base_url"]         = ($this->uri->segment(5)) ? base_url().'panel/hotel/search/'.$this->uri->segment(4).'/'.$this->uri->segment(5).'/' : base_url().'panel/post/search/'.$this->uri->segment(4).'/10/';
        $total_row                  = $this->model_post->po_search_count($post_type,$cat);
        $config['suffix']           = '.html';
        $config['first_url']        = '1.html';
        $config["total_rows"]       = $total_row;
        $config["per_page"]         = ($this->uri->segment(5)) ? $this->uri->segment(5) : '10';
        $config['use_page_numbers'] = TRUE;
        $config['num_links']        = $total_row;
        $config['cur_tag_open']     = '&nbsp;<a class="active">';
        $config['cur_tag_close']    = '</a>';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['num_links']        = 5;

        $this->pagination->initialize($config);
        if($this->uri->segment(6))
        {
            $page = ($this->uri->segment(6)-1)*$config["per_page"] ;
        }
        else
        {
            $page = 0;
        }
        $data["post_list"]          = $this->model_post->po_search($post_type,$cat,$config["per_page"], $page);

        $str_links                  = $this->pagination->create_links();
        $data["links"]              = explode('&nbsp;',$str_links );

        $this->load->view('hotel',$data);
    }
}
