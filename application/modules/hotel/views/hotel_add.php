<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Hotel Editor</title>

    <link href="<?php echo base_url()?>themes/default/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/js/plugins/multiselect/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>themes/default/js/plugins/datetime/bootstrap-datetimepicker.css" rel="stylesheet" />
	<!-- Toastr style -->
	<link href="<?php echo base_url()?>themes/default/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/custom.css" rel="stylesheet">

</head>

<body class="fixed-sidebar">

    <div id="wrapper">
    <?php $this->load->view('menu/nav');?>

        <div id="page-wrapper" class="gray-bg">
        <?php $this->load->view('menu/nav_top');?>
        
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Hotel Editor</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>panel/dashboard.html">Home</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url()?>panel/hotel.html">Hotel</a>
                        </li>
                        <li class="active">
                            <strong>Pages Editor</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content">

            <div class="row">
            <form name="myform" method="POST" action="" enctype="multipart/form-data" class="form" data-async data-target="#addLocation">
            <?php //if(count($row)=='0'){ echo $this->model_status->status_pages('500');}else{?>
                <div class="col-lg-9 padding-none">
	                <div class="ibox float-e-margins">
	                    <div class="ibox-title">
	                    	<h5 class="pull-left margin-right-20">Hotel Editor</h5>
	                    	<a href="<?php echo base_url()?>panel/hotel/add.html" class="btn btn-default pull-left">Add New Hotel</a>
	                        <button type="submit" name="Pixel_Save" class="btn btn-primary float-right">Save Hotel</button>
	                    </div>
	                    <div class="ibox-content">
	                    <div class="form-group">
							<label for="title">Title ID </label>
							<div class="input-group">
								<input type="text" class="form-control" id="title" name="post_title" placeholder="Title ID" required="required">
								<span class="input-group-addon tooltip-demo" title="Preview Link ID"><a target="_blank"><i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="Preview Link ID"></i></a></span>
							</div>
						</div>
                        <div class="form-group col-lg-6 col-sm-6 col-xs-12 padding-left-0 padding-xs-none">
                            <label for="status">Status</label>
                            <select name="status" class="form-control">
                            	<option value="1">Publish</option>
								<option value="0">Draft</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-6 col-sm-6 col-xs-12 padding-right-0 padding-xs-none">
                            <label for="date">Date</label>
                            <div class="input-group">
                                <input name="date" class="form-control date-picker" type="text" data-date-format="yyyy-mm-dd hh:mm">
                                <span class="input-group-addon tooltip-demo">
                                    <i class="fa fa-calendar" data-toggle="tooltip" data-placement="top" title="Calendar"></i>
                                </span>
                            </div>
                        </div>
                        
						<div class="clear"></div>
						<div class="form-group clear" style="display: none;">
							<label for="title">Website Link </label>
							<input type="text" class="form-control" name="post_link" placeholder="Website Link">
						</div>
						
						<div class="form-group clear" style="display: none;">
							<label for="title">Video Link </label>
							<input type="text" class="form-control" name="post_video" placeholder="Video Link">
						</div>
						
						<div class="form-group clear">
							<label for="content">Content ID</label>
                            <textarea id="content_id" name="post_content"></textarea>
						</div>
						
						<div class="form-group clear">
							<label for="summary">Summary ID</label>
							<textarea id="summary_id" name="post_summary"></textarea>
						</div>

						<div class="form-group" >
							<label for="title">SEO Key ID</label>
							<input type="text" class="form-control" name="post_key" placeholder="SEO Key ID">
						</div>
						<div class="form-group" >
							<label for="title">SEO Desc ID</label>
							<textarea type="text" class="form-control" name="post_desc" placeholder="SEO Desc ID"></textarea>
						</div>                                
			            <div class="form-group col-lg-6 col-sm-6 col-xs-12 padding-left-0 padding-xs-none">
			                <label for="Comment">Comment</label>
			                <select name="post_comment" class="form-control">
			                	<option value="1">Yes</option>
								<option value="0">No</option>
			                </select>
			            </div>
			            <div class="form-group col-lg-6 col-sm-6 col-xs-12 padding-right-0 padding-xs-none">
			                <label for="Username">Username</label>
			                <select name="user_id" class="form-control">
			                <?php foreach($user_list as $row_user){?>
							<option value="<?php echo $row_user->user_id;?>"><?php echo $row_user->user_name;?> | <?php echo $row_user->user_first_name;?> <?php echo $row_user->user_last_name;?></option>
							<?php }?>
			                </select>
			            </div>
						<div class="clear"></div>
	                    </div>
	                </div>
	            </div>
	            <div class="col-lg-3 padding-right-0">
	            <div class="bar-title white-bg">
	            	<div class="tabs-container">
                        <div class="tabs-left">
                            <ul class="nav nav-tabs tooltip-demo">
                                <li class="active"><a data-toggle="tab" href="#tab_images" aria-expanded="true"> <i class="fa fa-picture-o" data-toggle="tooltip" data-placement="top" title="Images"></i></a></li>
                                <li class=""><a data-toggle="tab" href="#tab_icon" aria-expanded="false"><i class="fa fa-dot-circle-o" data-toggle="tooltip" data-placement="top" title="Icon"></i></a></li>
                                <li class=""><a data-toggle="tab" href="#tab_thumb" aria-expanded="false"><i class="fa fa fa-camera" data-toggle="tooltip" data-placement="top" title="Thumb"></i></a></li>
                                <li class=""><a data-toggle="tab" href="#tab_background" aria-expanded="false"><i class="fa fa-th-large" data-toggle="tooltip" data-placement="top" title="Background"></i></a></li>
                                <li class=""><a data-toggle="tab" href="#tab_pdf" aria-expanded="false"><i class="fa fa-bookmark" data-toggle="tooltip" data-placement="top" title="PDF"></i></a></li>
                                <li class=""><a data-toggle="tab" href="#tab_files" aria-expanded="false"><i class="fa fa-file" data-toggle="tooltip" data-placement="top" title="Files"></i></a></li>
                            </ul>
                            <div class="tab-content ">
                                <div id="tab_images" class="tab-pane active">
                                    <div class="panel-body">
                                        <div class="input-file"><input type="file" name="upload_photo[]" multiple="multiple" id="input_images"></div>
                                    </div>
                                </div>
                                <div id="tab_icon" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="input-file"><input type="file" name="upload_icon[]" multiple="multiple" id="input_icon"></div>
                                    </div>
                                </div>
                                <div id="tab_thumb" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="input-file"><input type="file" name="upload_thumb[]" multiple="multiple" id="input_thumb"></div>
                                    </div>
                                </div>
                                <div id="tab_background" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="input-file"><input type="file" name="upload_background[]" multiple="multiple" id="input_background"></div>
                                    </div>
                                </div>
                                <div id="tab_pdf" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="input-file"><input type="file" name="upload_pdf[]" multiple="multiple" id="input_pdf"></div>
                                    </div>
                                </div>
                                <div id="tab_files" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="input-file"><input type="file" name="upload_files[]" multiple="multiple" id="input_files"></div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    <!--End Tab-->
	            </div>
	            </div>
            <?php //}?>
            </form>
            </div>


            </div>
        <div class="footer">
            <div class="pull-right">
                <strong><?php echo $this->model_nav->disk_size();?></strong> Free.
            </div>
            <div>
                <strong>Copyright</strong> <?php echo $this->model_setting->setting('company');?> &copy; <?php echo date('Y');?>
            </div>
        </div>

        </div>
        </div>

    <!-- Mainly scripts -->
    <script src="<?php echo base_url()?>themes/default/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url()?>themes/default/js/inspinia.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/pace/pace.min.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/multiselect/dist/js/bootstrap-multiselect.js" type="text/javascript" ></script>
    
    <!--CK Editor-->
    <script src="<?php echo base_url()?>themes/default/js/plugins/ckeditor/ckeditor.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/ckeditor/en.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/ckeditor/id.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/ckeditor/ck.js"></script>

	<!--Files Scripts-->
	<script src="<?php echo base_url()?>themes/default/js/plugins/filestyle/filestyle.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/filestyle/aplication.js"></script>
	<!-- <script src="<?php echo base_url()?>themes/default/js/snippet-javascript-console.min.js"></script> -->
	<!-- Check -->
    <script src="<?php echo base_url()?>themes/default/js/check.js"></script>

	<!--Bootstrap Date Picker-->
	<script src="<?php echo base_url()?>themes/default/js/plugins/datetime/bootstrap-datetimepicker.min.js"></script>
	<!-- Toastr script -->
	<script src="<?php echo base_url()?>themes/default/js/plugins/toastr/toastr.min.js"></script>
	<script>
		$(function () {
			toastr.options = {
				"closeButton": true,
				"debug": false,
				"progressBar": true,
				"preventDuplicates": false,
				"positionClass": "toast-bottom-right",
				"onclick": null,
				"showDuration": "400",
				"hideDuration": "1000",
				"timeOut": "7000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
			//toastr.success('Successfully Update Profile', 'Update Profile');
			<?php if(isset($_COOKIE['status'])){ echo $this->model_status->status($_COOKIE['status']);}?>
			<?php if(isset($_COOKIE['status_second'])){ echo $this->model_status->status($_COOKIE['status_second']);}?>
		});
	</script>
	<script type="text/javascript">
    $(document).ready(function() {
		$(".date-picker").datetimepicker({format: 'yyyy-mm-dd hh:ii:ss',autoclose: true,});
        $('#example-multiple-selected').multiselect();
    });
    </script>

</body>

</html>
