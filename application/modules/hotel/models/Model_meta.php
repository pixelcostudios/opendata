<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

class model_meta extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}
	function me_edit($meta_id)
	{
		$this->db->select('meta_id, meta_type, meta_source, meta_dest, meta_title, meta_slug, meta_date');
		$this->db->from("my_meta");
		$this->db->where('meta_id',$meta_id);
		$result	= $this->db->get();
		return $result->row();
	}
	function me_list($meta_type,$meta_source)
	{
		$this->db->select('meta_id, meta_type, meta_source, meta_dest, meta_title, meta_slug, meta_date');
		$this->db->from("my_meta");
		$this->db->where('meta_type',$meta_type);
		$this->db->where('meta_source',$meta_source);
		$this->db->order_by('meta_date','desc');
		$query	= $this->db->get();
		return $query->result();
	}
	function me_source_list($meta_type,$meta_source)
	{
		$this->db->select('meta_id, meta_type, meta_source, meta_dest, meta_title, meta_slug, meta_date');
		$this->db->from("my_meta");
		$this->db->where('meta_type',$meta_type);
		$this->db->where('meta_source',$meta_source);
		$this->db->where('meta_status','1');
		$this->db->order_by('meta_date','desc');
		$query	= $this->db->get();
		return $query->result();
	}
	function me_list_not_in($meta_type,$meta_source,$meta_dest)
	{
		$this->db->select('meta_id, meta_type, meta_source, meta_dest, meta_title, meta_slug, meta_date');
		$this->db->from("my_meta");
		$this->db->where('meta_type',$meta_type);
		$this->db->where('meta_source',$meta_source);
		$this->db->where_not_in('meta_dest',$meta_dest);
		$this->db->order_by('meta_date','desc');
		$query	= $this->db->get();
		return $query->result();
	}
	function me_cat($start,$limit)
	{
		$this->db->select('meta_id, meta_type, meta_source, meta_dest, meta_title, meta_slug, meta_date');
		$this->db->from("my_meta");
		$this->db->limit($start,$limit);
		$this->db->order_by('meta_date','desc');
		$query	= $this->db->get();
		return $query->result();
	}
	function me_cat_count()
	{
		$this->db->select('meta_id, meta_type, meta_source, meta_dest, meta_title, meta_slug, meta_date');
		$this->db->from("my_meta");
		$this->db->order_by('meta_date','desc');
		return $this->db->count_all_results();
	}
	function me_cat_type($meta_type,$start,$limit)
	{
		$this->db->select('meta_id, meta_type, meta_source, meta_dest, meta_title, meta_slug, meta_date');
		$this->db->from("my_meta");
		$this->db->where('my_meta.meta_type',$meta_type);
		$this->db->limit($start,$limit);
		$this->db->order_by('meta_date','desc');
		$query	= $this->db->get();
		return $query->result();
	}
	function me_cat_type_count($meta_type)
	{
		$this->db->select('meta_id, meta_type, meta_source, meta_dest, meta_title, meta_slug, meta_date');
		$this->db->from("my_meta");
		$this->db->where('my_meta.meta_type',$meta_type);
		$this->db->order_by('meta_date','desc');
		return $this->db->count_all_results();
	}
	function me_search($meta_type,$cat,$start,$limit)
	{
		$this->db->select('meta_id, meta_type, meta_source, meta_dest, meta_title, meta_slug, meta_date');
		$this->db->from("my_meta");
		$this->db->group_start();
		$this->db->like('meta_title',$cat);
		$this->db->or_like('meta_slug',$cat);
		$this->db->group_end();
		$this->db->where('meta_type',$meta_type);
		$this->db->limit($start,$limit);
		$this->db->order_by('date','desc');
		$query	= $this->db->get();
		return $query->result();
	}
	function me_search_count($meta_type,$cat)
	{
		$this->db->select('meta_id, meta_type, meta_source, meta_dest, meta_title, meta_slug, meta_date');
		$this->db->from("my_meta");
		$this->db->group_start();
		$this->db->like('meta_title',$cat);
		$this->db->or_like('meta_slug',$cat);
		$this->db->group_end();
		$this->db->where('meta_type',$meta_type);
		$this->db->order_by('date','desc');
		return $this->db->count_all_results();
	}
	function me_source_check($meta_type,$meta_source,$meta_dest)
	{
		$this->db->select('meta_type, meta_source');
		$this->db->from("my_meta");
		$this->db->where('meta_type',$meta_type);
		$this->db->where('meta_source',$meta_source);
		$this->db->where('meta_dest',$meta_dest);
		return $this->db->count_all_results();
	}
	function me_source_title_check($meta_type,$meta_source,$meta_title)
	{
		$this->db->select('meta_type, meta_source');
		$this->db->from("my_meta");
		$this->db->where('meta_type',$meta_type);
		$this->db->where('meta_source',$meta_source);
		$this->db->where('meta_title',$meta_title);
		$this->db->where('meta_status','1');
		return $this->db->count_all_results();
	}
	function me_add($data_array)
	{
		$this->db->insert('my_meta',$data_array);
		return $this->db->insert_id();
	}
	function me_update($meta_id,$data_array)
	{
		$this->db->where('meta_id', $meta_id);
		$this->db->update('my_meta',$data_array);
		return true;
	}
	function me_update_status($meta_type,$meta_source,$meta_dest,$meta_status)
	{
		$this->db->set('meta_status', $meta_status); 
		$this->db->where('meta_type', $meta_type);
		$this->db->where('meta_source', $meta_source);
		$this->db->where('meta_dest', $meta_dest);
		$this->db->update('my_meta');
		return true;
	}
	function me_delete($meta_id)
	{
		$this->db->where('meta_id',$meta_id);
		$this->db->delete('my_meta');
		return true;
	}
	function me_delete_source($meta_source)
	{
		$this->db->where('meta_source',$meta_source);
		$this->db->delete('my_meta');
		return true;
	}
	function me_delete_dest($meta_dest)
	{
		$this->db->where('meta_dest',$meta_dest);
		$this->db->delete('my_meta');
		return true;
	}
}