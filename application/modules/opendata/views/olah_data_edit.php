<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Olah Data Editor</title>

    <link href="<?php echo base_url()?>themes/default/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/js/plugins/multiselect/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>themes/default/css/plugins/select2/select2.min.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="<?php echo base_url()?>themes/default/css/plugins/select2/select2.min.css" rel="stylesheet">
	<link href="<?php echo base_url()?>themes/default/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/custom.css" rel="stylesheet">

</head>

<body class="fixed-sidebar">

    <div id="wrapper">
    <?php $this->load->view('menu/nav');?>

        <div id="page-wrapper" class="gray-bg">
        <?php $this->load->view('menu/nav_top');?>
        
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Olah Data Editor</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>panel/dashboard.html">Home</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url()?>panel/opendata/olah_apbd2/<?php echo $this->uri->segment('4')?>.html">Olah Data</a>
                        </li>
                        <li class="active">
                            <strong>Olah Data Editor</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content">

            <div class="row">
            <form name="myform" method="post" class="form">
            <?php //if(count($row)=='0'){ echo $this->model_status->status_pages('500');}else{?>
                <div class="col-lg-12 padding-none">
	                <div class="ibox float-e-margins">
	                    <div class="ibox-title">
	                    	<h5 class="pull-left margin-right-20">Olah Data Editor</h5>
	                    	<a href="#" data-toggle="modal" data-target="#myModal" class="btn btn-default pull-left">Tambah Data</a>
	                        <!--<button type="submit" name="Pixel_Save" class="btn btn-primary float-right">Simpan</button>-->
	                    </div>
	                    <div class="ibox-content">
	                    
                        
                        <div class="ibox-title">
	                    	<h5 class="pull-left margin-right-20">Olah Data Tagging : <?php echo $row->nama;?></h5>
	                    </div>
                        
                        <?php foreach (range(2017,2017) as $year) {?>
                        <div class="ibox-title">
	                    	<h5 class="pull-left margin-right-20">Tahun <?php echo $year;?></h5>
	                    </div>
                        
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Mata Anggaran/Lembaga</th>
                                <th>Kode Rekening</th>
                                <th>Nominal</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=0; foreach ($this->model_opendata->apbd2_list($year,$this->uri->segment('4')) as $key) { $i++;?>
                            <tr>
                                <td><?php echo $i;?></td>
                                <td><?php echo $key->uraian;?></td>
                                <td><?php echo $key->nrekening;?></td>
                                <td><?php echo $this->model_setting->currency($key->nominal);?></td>
                                <td>
                                    <div class="btn-group">
                                    <a class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        Action <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu drop-right">
                                        <li><a href="<?php echo base_url();?>panel/opendata/olah_apbd2_delete/<?php echo $key->tags_id;?>.html">Delete</a></li>
                                    </ul>
                                    </div>
                                </td>
                            </tr>
                            <?php }?>
                            </tbody>
                        </table>

                        <?php }?>
						
						<div class="clear"></div>
	                    </div>
	                </div>
	            </div>
	            
            <?php //}?>

            <div class="modal inmodal" id="myModal" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                <div class="modal-content animated bounceInRight">
                    
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <i class="fa fa-laptop modal-icon"></i>
                            <h4 class="modal-title">Tambah Data</h4>
                            <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>
                        </div>
                        <div class="modal-body">
                            
                            <div class="form-group">
                            <label class="font-normal">Mata Anggaran/Lembaga</label>
                            <select class="select2_demo_1 form-control" name="apbd_id">
                                <option>Pilih Data</option>
                                <?php foreach ($anggaran as $key_anggaran) {?>
                                <option value="<?php echo $key_anggaran->tweb_apbd2_id;?>"><?php echo $key_anggaran->tahun;?> | <?php echo $key_anggaran->uraian;?> | <?php echo $key_anggaran->nama;?></option>
                                <?php }?>
                            </select>
                            <div><br>
                            <div class="form-group">
                                <label>Tahun</label> 
                                <select class="select2_demo_1 form-control" name="year">
                                <?php foreach (range(2017,date('Y')) as $year) {?>
                                <option value="<?php echo $year;?>"><?php echo $year;?></option>
                                <?php }?>
                            </select>
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="submit" name="Pixel_Save" class="btn btn-primary">Save changes</button>
                        </div>
                      
                    </div>
                </div>
            </div>
            </form>


            


            </div>


            </div>
        <div class="footer">
            <div class="pull-right">
                <strong><?php echo $this->model_nav->disk_size();?></strong> Free.
            </div>
            <div>
                <strong>Copyright</strong> <?php echo $this->model_setting->setting('company');?> &copy; <?php echo date('Y');?>
            </div>
        </div>

        </div>
        </div>

    <!-- Mainly scripts -->
    <script src="<?php echo base_url()?>themes/default/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url()?>themes/default/js/inspinia.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/pace/pace.min.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/select2/select2.full.min.js"></script>

	<!-- Check -->
    <script src="<?php echo base_url()?>themes/default/js/check.js"></script>

	<!--Bootstrap Date Picker-->
	<script src="<?php echo base_url()?>themes/default/js/plugins/datetime/bootstrap-datetimepicker.min.js"></script>
	<!-- Toastr script -->
    <script src="<?php echo base_url()?>themes/default/js/plugins/toastr/toastr.min.js"></script>
    <!-- Select2 -->
    <script src="<?php echo base_url()?>themes/default/js/plugins/select2/select2.full.min.js"></script>
	<script type="text/javascript">
    $(document).ready(function() {
		$(".date-picker").datetimepicker({format: 'yyyy-mm-dd hh:ii:ss',autoclose: true,});
    });
    </script>
    <script>
        $(document).ready(function(){
         $(".select2_demo_1").select2();
            $(".select2_demo_2").select2();
            $(".select2_demo_3").select2({
                placeholder: "Select a state",
                allowClear: true
            });
            });
		$(function () {
			toastr.options = {
				"closeButton": true,
				"debug": false,
				"progressBar": true,
				"preventDuplicates": false,
				"positionClass": "toast-bottom-right",
				"onclick": null,
				"showDuration": "400",
				"hideDuration": "1000",
				"timeOut": "7000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
			//toastr.success('Successfully Update Profile', 'Update Profile');
			<?php if(isset($_COOKIE['status'])){ echo $this->model_status->status($_COOKIE['status']);}?>
			<?php if(isset($_COOKIE['status_second'])){ echo $this->model_status->status($_COOKIE['status_second']);}?>
		});
	</script>
	

</body>

</html>
