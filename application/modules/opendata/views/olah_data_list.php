<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>Olah Data List</title>

<link href="<?php echo base_url()?>themes/default/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
<!-- <link href="<?php echo base_url()?>themes/default/css/plugins/iCheck/custom.css" rel="stylesheet"> -->
<link href="<?php echo base_url()?>themes/default/css/animate.css" rel="stylesheet">
<link href="<?php echo base_url()?>themes/default/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>themes/default/js/plugins/datetime/bootstrap-datetimepicker.css" rel="stylesheet" />
<link href="<?php echo base_url()?>themes/default/js/plugins/datetime/bootstrap-datepicker3.min.css" rel="stylesheet" />
<!-- Toastr style -->
<link href="<?php echo base_url()?>themes/default/css/plugins/toastr/toastr.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>themes/default/css/style.css" rel="stylesheet">
<link href="<?php echo base_url()?>themes/default/css/custom.css" rel="stylesheet">

</head>

<body class="fixed-sidebar">
<div id="wrapper">
<?php $this->load->view('menu/nav');?>

    <div id="page-wrapper" class="gray-bg">
    <?php $this->load->view('menu/nav_top');?>
    
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Olah Data List</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo base_url()?>panel/dashboard.html">Home</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url()?>panel/opendata/olah_data_list.html">Olah Data List</a>
                    </li>
                    <li class="active">
                        <strong>Olah Data List</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
        	<form method="post">
            <div class="col-lg-12 padding-none">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 class="pull-left margin-right-20">Olah Data List</h5>
                        <a href="<?php echo base_url()?>panel/opendata/olah_data_list_add.html" class="btn btn-default pull-left">Tambah Tagging</a>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive margin-top-20">
                            <table id="example" class="table table-striped table-bordered table-hover dataTables-example" width="100%">
                                <thead>
                                <tr>
                                    <th width="1%">
                                        <label>
                                            <input type="checkbox" class="colored-green" id="example-select-all" >
                                            <span class="text"></span>
                                        </label>
                                    </th>
                                    <th width="5%">No</th>
                                    <th>Kode</th>
                                    <th width="5%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=0; foreach ($post_list as $key) { $i++;?>
                                <tr>
                                    <td>
                                        <label>
                                            <input type="checkbox" name="post-check[]" value="<?php echo $key->id; ?>" class="colored-green">
                                            <span class="text"></span>
                                        </label>
                                    </td>
                                    <td><?php echo $i;?></td>
                                    <td><a href="<?php echo base_url()?>panel/opendata/olah_data_list_edit/<?php echo $key->id;?>.html"><?php echo $key->nama; ?></a> | <a href="<?php echo base_url()?>panel/opendata/olah_apbd2/<?php echo $key->id;?>.html"><i class="fa fa-pencil"></i> Olah Data</a></td>
                                    <td>
                                        <div class="btn-group">
                                        <a class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            Action <i class="fa fa-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu drop-right">
                                            <li><a href="<?php echo base_url()?>panel/opendata/olah_data_list_edit/<?php echo $key->id; ?>.html">Edit</a></li>
                                            <li><a data-toggle="modal" href="<?php echo base_url();?>panel/ajax/lembaga_delete/<?php echo $key->id; ?>" data-target="#modal_deleted">Delete</a></li>
                                        </ul>
                                        </div>
                                    </td>
                                </tr>
                                <?php }?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>
                                        <!-- <label>
                                            <input type="checkbox" class="colored-green" id="example-select-all"> 
                                            <span class="text"></span>
                                        </label> -->
                                    </th>
                                    <th>No</th>
                                    <th>Kode</th>
                                    <th>Action</th>
                                </tr>
                                <tr>
                                    <th colspan="6">
                                        <div class="pull-left">
                                            <div class="btn-group">
                                            <div class="btn-group dropup">
                                                <button type="button" class="btn btn-default btn-custom button-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                                    <i class="fa fa-ellipsis-horizontal"></i> Action <i class="fa fa-angle-up"></i>
                                                </button>
                                            <ul class="dropdown-menu dropdown-archive">
                                               <!--  <li class="margin-bottom-5">
                                                    <button type="button" data-toggle="modal" data-target="#modal_publish" class="btn btn-default btn-sm"><i class="fa fa-check"></i> Publish</button>
                                                </li>
                                                <li class="margin-bottom-5">
                                                    <button type="button" data-toggle="modal" data-target="#modal_draft" class="btn btn-default btn-sm"><i class="fa fa-pencil-square-o"></i> UnPublish</button>
                                                </li>
                                                <li class="margin-bottom-5">
                                                    <button type="button" data-toggle="modal" data-target="#modal_user" class="btn btn-default btn-sm"><i class="fa fa-user"></i> Change Owner</button>
                                                </li> -->
                                                <li>
                                                    <button type="button" data-toggle="modal" data-target="#modal_delete" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i> Delete</button>
                                                </li>
                                            </ul>
                                            </div>
                                        </div>
                                            </div>
                                    </th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal fade" id="modal_publish">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Are you sure ?</h4>

                      </div>
                      <div class="modal-body">
                      <p>Do you want to change this Lembaga to Publish.</p>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="post-publish" value="publish" class="btn btn-primary">Publish</button>
                      </div>
                </div>
              </div>
            </div>
            <div class="modal fade" id="modal_draft">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Are you sure ?</h4>

                      </div>
                      <div class="modal-body">
                      <p>Do you want to change this Lembaga to Draft.</p>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="post-draft" value="draft" class="btn btn-warning">UnPublish</button>
                      </div>
                </div>
              </div>
            </div>
            <div class="modal fade" id="modal_delete">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Are you sure ?</h4>

                      </div>
                      <div class="modal-body">
                      <p>Do you want to delete this Lembaga.</p>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="post-delete" value="delete" class="btn btn-danger">Delete</button>
                      </div>
                </div>
              </div>
            </div>
			</form>

            <form method="post">
                <div class="modal fade" id="modal_export">
                  <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Download Report Target</h4>
                          </div>
                          <div class="modal-body">
                            <div class="form-group col-lg-12 col-sm-12 col-xs-12 padding-left-0 padding-right-0 padding-xs-none clearfix">
                            <select class="outlet_list form-control col-sm-12" name="p_outlet_id" data-validation="required">
                                <option value="">Pilih Gerai</option>
                                <option value="all">Semua Gerai</option>
                                <?php foreach($outlet_list as $row_outlet){?>
                                <option value="<?php echo $row_outlet->outlet_id;?>"><?php echo $row_outlet->outlet_name;?></option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="form-group text-align-left">
                        <label for="merk">Tahun</label>
                        <div class="input-group" >
                            <input name="p_year" class="form-control date-picker-year" type="text" data-date-format="yyyy" id="date_start" data-validation="date" data-validation-format="yyyy" placeholder="Pilih Tahun">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                        </div>
                          <!-- <div class="input-file"><input type="file" name="upload_files[]" multiple="multiple" id="input_images"></div> -->
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="post-export" value="publish" class="btn btn-primary">Download</button>
                          </div>
                    </div>
                  </div>
                </div>
            </form>

            <form method="post" enctype="multipart/form-data">
                <div class="modal fade" id="modal_import">
                  <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Import Target</h4>
                          </div>
                          <div class="modal-body">
                            <div class="form-group text-align-left">
                                <label for="merk">Tahun</label>
                                <div class="input-group" >
                                    <input name="p_year" class="form-control date-picker-year" type="text" data-date-format="yyyy" id="date_start" data-validation="date" data-validation-format="yyyy" placeholder="Pilih Tahun">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group text-align-left">
                                <label for="merk">Upload Excel (<a href="../upload/template/target_import.xlsx">Download Format</a>)</label>
                                <div class="input-file"><input type="file" name="upload_files[]" multiple="multiple" data-validation="required" id="input_images"></div>
                            </div>
                          <!-- <div class="input-file"><input type="file" name="upload_files[]" multiple="multiple" id="input_images"></div> -->
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="post-import" value="publish" class="btn btn-primary">Import</button>
                          </div>
                    </div>
                  </div>
                </div>
            </form>

            <form method="post">
                <div class="modal fade" id="modal_edit">
                  <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Edit Target Semua Gerai</h4>
                          </div>
                          <div class="modal-body">
                        <div class="form-group text-align-left">
                        <label for="merk">Bulan</label>
                        <div class="input-group" >
                            <input name="p_month" class="form-control date-picker-month" type="text" data-date-format="yyyy-mm" data-validation="date" data-validation-format="yyyy-mm" placeholder="Pilih Bulan">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                        </div>
                          <!-- <div class="input-file"><input type="file" name="upload_files[]" multiple="multiple" id="input_images"></div> -->
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="post-edit" value="publish" class="btn btn-primary">Lihat</button>
                          </div>
                    </div>
                  </div>
                </div>
            </form>
        </div>
    </div>
    <div class="footer">
        <div class="pull-right">
            <strong><?php echo $this->model_nav->disk_size();?></strong> Free.
        </div>
        <div>
            <strong>Copyright</strong> <?php echo $this->model_setting->setting('company');?> &copy; <?php echo date('Y');?>
        </div>
    </div>

    </div>
</div>

<!-- Mainly scripts -->
<script src="<?php echo base_url()?>themes/default/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url()?>themes/default/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
<script src="<?php echo base_url()?>themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Peity -->
<!-- <script src="<?php echo base_url()?>themes/default/js/plugins/peity/jquery.peity.min.js"></script>
 -->
<!-- Custom and plugin javascript -->
<script src="<?php echo base_url()?>themes/default/js/inspinia.js"></script>
<!-- <script src="<?php echo base_url()?>themes/default/js/plugins/pace/pace.min.js"></script> -->
<script src="<?php echo base_url()?>themes/default/js/plugins/select2/select2.full.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/validation/jquery.form-validator.min.js"></script>

<!-- Check -->
<script src="<?php echo base_url()?>themes/default/js/check.js"></script>

<script src="<?php echo base_url()?>themes/default/js/plugins/datetime/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url()?>themes/default/js/plugins/datetime/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url()?>themes/default/js/plugins/dataTables/datatables.min.js"></script>

<script src="<?php echo base_url()?>themes/default/js/plugins/filestyle/filestyle.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/filestyle/aplication.js"></script>

<!-- Peity -->
<!-- <script src="<?php echo base_url()?>themes/default/js/demo/peity-demo.js"></script> -->
<!-- Toastr script -->
<script src="<?php echo base_url()?>themes/default/js/plugins/toastr/toastr.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){
// $('#checkall').change(function(){
//     var cells = table.cells( ).nodes();
//     $( cells ).find(':checkbox').prop('checked', $(this).is(':checked'));
// });
// $('#checkall').on('click', function(){
//       // Get all rows with search applied
//       var rows = oTable.rows({ 'search': 'applied' }).nodes();
//       // Check/uncheck checkboxes for all rows in the table
//       $('input[type="checkbox"]', rows).prop('checked', this.checked);
//    });

var table = $('.dataTables-example').DataTable({
        pageLength: 10,
        responsive: true,
        // scrollY:        "300px",
        scrollX:        true,
        scrollCollapse: true,
        fixedColumns:   {
            leftColumns: 4
        },
        // "aoColumnDefs": [
        //   { "sTitle": "My column title", "aTargets": [ 0 ] }
        // ],
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            { extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Report Presensi'},
            {extend: 'pdf', title: 'Report Presensi'},

            {extend: 'print',
             customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
            }
            }
        ]
    });

   // Handle click on "Select all" control
   $('#example-select-all').on('click', function(){
      // Check/uncheck all checkboxes in the table
      var rows = table.rows({ 'search': 'applied' }).nodes();
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
   });

   // Handle click on checkbox to set state of "Select all" control
   $('#example tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#example-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            // Set visual state of "Select all" control 
            // as 'indeterminate'
            el.indeterminate = true;
         }
      }
   });
});
</script>
<script>
    $(document).ready(function()
    {
            $(".outlet_list").select2();
            $(".date-picker").datetimepicker({minView: 2,format: 'yyyy-mm-dd',autoclose: true,});
            $(".date-picker-month").datepicker({
                startView: 1,
                minViewMode: 1,
                autoclose: true
            });
            $(".date-picker-year").datepicker({
                format: "yyyy",
                startView: 2,
                minViewMode: 2,
                autoclose: true
            });
            $.validate({
                modules: 'location, date, security, file'
            });
        });
    $(function () {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "progressBar": true,
            "preventDuplicates": false,
            "positionClass": "toast-bottom-right",
            "onclick": null,
            "showDuration": "400",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        //toastr.success('Successfully Update Profile', 'Update Profile');
        <?php if(isset($_COOKIE['status'])){ echo $this->model_status->status($_COOKIE['status']);}?>
        <?php if(isset($_COOKIE['status_second'])){ echo $this->model_status->status($_COOKIE['status_second']);}?>
    });
</script>
<script>
        // $(document).ready(function(){
        //     $('.i-checks').iCheck({
        //         checkboxClass: 'icheckbox_square-green',
        //         radioClass: 'iradio_square-green',
        //     });
        // });
    </script>

</body>

</html>
