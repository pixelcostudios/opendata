<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Data Lembaga Editor</title>

    <link href="<?php echo base_url()?>themes/default/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/plugins/select2/select2.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/js/plugins/datetime/bootstrap-datetimepicker.css" rel="stylesheet" />
	<!-- Toastr style -->
	<link href="<?php echo base_url()?>themes/default/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/custom.css" rel="stylesheet">

</head>

<body class="fixed-sidebar">

    <div id="wrapper">
    <?php $this->load->view('menu/nav');?>

        <div id="page-wrapper" class="gray-bg">
        <?php $this->load->view('menu/nav_top');?>
        
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Data Lembaga Editor</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>panel/dashboard.html">Home</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url()?>panel/opendata/lembaga.html">Data Lembaga</a>
                        </li>
                        <li class="active">
                            <strong>Data Lembaga Editor</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content">

            <div class="row">
            <form name="myform" method="POST" action="" enctype="multipart/form-data" class="form" data-async data-target="#addLocation">
            <?php //if(count($row)=='0'){ echo $this->model_status->status_pages('500');}else{?>
                <div class="col-lg-9 padding-none">
	                <div class="ibox float-e-margins">
	                    <div class="ibox-title">
	                    	<h5 class="pull-left margin-right-20">Data Lembaga Editor</h5>
	                    	<a href="<?php echo base_url()?>panel/lembaga/lembaga_add.html" class="btn btn-default pull-left">Tambah Lembaga</a>
	                        <button type="submit" name="Pixel_Save" class="btn btn-primary float-right">Simpan</button>
	                    </div>
	                    <div class="ibox-content">
	                    <div class="form-group">
							<label for="title">Nama Lembaga </label>
							<input type="text" class="form-control" id="title" name="nama" placeholder="Nama Lembaga" required="required">
						</div>
                        <div class="form-group">
							<label for="title">Kode Lembaga </label>
							<input type="text" class="form-control" id="title" name="kode_org" placeholder="Kode Lembaga" required="required">
						</div>
                        <div class="form-group col-lg-6 col-sm-6 col-xs-12 padding-left-0 padding-xs-none">
                            <label for="status">Provinsi</label>
                            <select name="provinsi" id="provinsi" class="form-control select_province">
                                <option value="">Pilih Provinsi *</option>
                                <?php foreach ($provinsi as $key) {?>
                            	<option value="<?php echo $key->id;?>"><?php echo ucwords(strtolower($key->nama));?></option>
								<?php }?>
                            </select>
                        </div>
                        <div class="form-group col-lg-6 col-sm-6 col-xs-12 padding-right-0 padding-xs-none">
                            <label for="status">Kabupaten / Kota</label>
                            <select name="kabupaten" id="kabupaten" class="form-control select_kabupaten">
                            	<option value="">Pilih Kabupaten *</option>
                                <?php foreach ($this->model_opendata->city_list($row->provinsi) as $key) {?>
                            	<option value="<?php echo $key->id;?>"><?php echo ucwords(strtolower($key->nama));?></option>
								<?php }?>
                            </select>
                        </div>
                        <div class="form-group col-sm-6 padding-left-0">
							<label for="title">Email Resmi </label>
							<input type="text" class="form-control" id="title" name="email" placeholder="Email Resmi">
						</div>
                        <div class="form-group col-sm-6 padding-right-0">
							<label for="title">Website </label>
							<input type="text" class="form-control" id="title" name="url" placeholder="Website">
						</div>
                        <div class="form-group col-sm-6 padding-left-0">
							<label for="title">Telp</label>
							<input type="text" class="form-control" id="title" name="telp" placeholder="Telp">
						</div>
                        <div class="form-group col-sm-6 padding-right-0">
							<label for="title">No Faximili </label>
							<input type="text" class="form-control" id="title" name="fax" placeholder="No Faximili">
						</div>

                        <div class="form-group">
							<label for="title">Alamat </label>
							<input type="text" class="form-control" id="title" name="alamat" placeholder="Alamat" required="required">
						</div>
						
						<div class="form-group clear">
							<label for="content">Profil</label>
                            <textarea id="content_id" name="ndesc"></textarea>
						</div>
						
						<div class="clear"></div>
	                    </div>
	                </div>
	            </div>
	            <div class="col-lg-3 padding-right-0">
	            <div class="bar-title white-bg">
	            	<div class="tabs-container">
                        <div class="tabs-left">
                            <ul class="nav nav-tabs tooltip-demo">
                                <li class="active"><a data-toggle="tab" href="#tab_images" aria-expanded="true"> <i class="fa fa-picture-o" data-toggle="tooltip" data-placement="top" title="Images"></i></a></li>
                            </ul>
                            <div class="tab-content ">
                                <div id="tab_images" class="tab-pane active">
                                    <div class="panel-body">
                                        <div class="input-file"><input type="file" name="upload_photo[]" multiple="multiple" id="input_images"></div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    <!--End Tab-->
	            </div>
	            </div>
            <?php //}?>
            </form>
            </div>


            </div>
        <div class="footer">
            <div class="pull-right">
                <strong><?php echo $this->model_nav->disk_size();?></strong> Free.
            </div>
            <div>
                <strong>Copyright</strong> <?php echo $this->model_setting->setting('company');?> &copy; <?php echo date('Y');?>
            </div>
        </div>

        </div>
        </div>

    <!-- Mainly scripts -->
    <script src="<?php echo base_url()?>themes/default/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url()?>themes/default/js/inspinia.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/pace/pace.min.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/select2/select2.full.min.js"></script>
    
    <!--CK Editor-->
    <script src="<?php echo base_url()?>themes/default/js/plugins/ckeditor/ckeditor.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/ckeditor/en.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/ckeditor/id.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/ckeditor/ck.js"></script>

	<!--Files Scripts-->
	<script src="<?php echo base_url()?>themes/default/js/plugins/filestyle/filestyle.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/filestyle/aplication.js"></script>
	<!-- <script src="<?php echo base_url()?>themes/default/js/snippet-javascript-console.min.js"></script> -->
	<!-- Check -->
    <script src="<?php echo base_url()?>themes/default/js/check.js"></script>

	<!--Bootstrap Date Picker-->
	<script src="<?php echo base_url()?>themes/default/js/plugins/datetime/bootstrap-datetimepicker.min.js"></script>
	<!-- Toastr script -->
	<script src="<?php echo base_url()?>themes/default/js/plugins/toastr/toastr.min.js"></script>
	<script type="text/javascript">
    $(document).ready(function() {
		$(".date-picker").datetimepicker({format: 'yyyy-mm-dd hh:ii:ss',autoclose: true,});
        $(".select_province").select2({
            placeholder: "Pilih Provinsi",
            allowClear: false
        });
        // $(".select_kabupaten").select2({
        //     placeholder: "Pilih Kabupaten",
        //     allowClear: false
        // });
    });
    if (location.hostname === "localhost" || location.hostname === "127.0.0.1")
    {
    var urlArray  = window.location.pathname.split( '/' );
    var url_Base  = document.location.origin+'/'+urlArray[1]+'/';
    var urlPost   = urlArray[2];
    }
    else
    {
    var urlArray  = window.location.pathname.split( '/' );
    var url_Base  = document.location.origin+'/';
    var urlPost   = urlArray[1];
    }
    // alert(url_Base);
    $(function(){
        $.ajaxSetup({
            // contentType: 'application/json; charset=utf-8',
            type:"POST",
            url: url_Base + "ajax/location.html",
            // dataType: 'jsonp',
            // async = true,
            cache: false,
        });
        // console.log(data);
        $("#provinsi").change(function()
        {
            var value=$(this).val();
            if(value>0){
            $.ajax({
            data:{modul:'kabupaten',id:value},
                success: function(respond){
                    $("#kabupaten").html(respond);
                    // $("#kecamatan").html('<option value="0">Pilih Kecamatan *</option>');
                    // console.log("sample"+respond);
                }
            })
            
        }
        else
        {
            $.ajax({
            data:{modul:'kabupaten',id:value},
                success: function(respond){
                    $("#kabupaten").html('<option value="0">Pilih Kabupaten *</option>');
                    // $("#kecamatan").html('<option value="0">Pilih Kecamatan *</option>');
                    // console.log("gfsgds"+respond);
                }
            })
        }
        });
        
        // $("#kabupaten").change(function(){
        //     var value=$(this).val();
        //     if(value>0){
        //         $.ajax({
        //             data:{modul:'select_kecamatan',id:value},
        //                 success: function(respond){
        //                 $("#kecamatan").html(respond);
        //             }
        //         })
        //     }
        // })
    });
    </script>
    <script>
		$(function () {
			toastr.options = {
				"closeButton": true,
				"debug": false,
				"progressBar": true,
				"preventDuplicates": false,
				"positionClass": "toast-bottom-right",
				"onclick": null,
				"showDuration": "400",
				"hideDuration": "1000",
				"timeOut": "7000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
			//toastr.success('Successfully Update Profile', 'Update Profile');
			<?php if(isset($_COOKIE['status'])){ echo $this->model_status->status($_COOKIE['status']);}?>
			<?php if(isset($_COOKIE['status_second'])){ echo $this->model_status->status($_COOKIE['status_second']);}?>
		});
	</script>
	

</body>

</html>
