<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Data Lembaga Editor</title>

    <link href="<?php echo base_url()?>themes/default/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/js/plugins/multiselect/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>themes/default/css/plugins/select2/select2.min.css" rel="stylesheet">

	<!-- Toastr style -->
	<link href="<?php echo base_url()?>themes/default/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/custom.css" rel="stylesheet">

</head>

<body class="fixed-sidebar">

    <div id="wrapper">
    <?php $this->load->view('menu/nav');?>

        <div id="page-wrapper" class="gray-bg">
        <?php $this->load->view('menu/nav_top');?>
        
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Data Lembaga Editor</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>panel/dashboard.html">Home</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url()?>panel/opendata/lembaga.html">Data Lembaga</a>
                        </li>
                        <li class="active">
                            <strong>Data Lembaga Editor</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content">

            <div class="row">
            <form name="myform" method="POST" action="" enctype="multipart/form-data" class="form" data-async data-target="#addLocation">
            <?php //if(count($row)=='0'){ echo $this->model_status->status_pages('500');}else{?>
                <div class="col-lg-12 padding-none">
	                <div class="ibox float-e-margins">
	                    <div class="">
							<div class="nav-tabs-custom">
							<div class="ibox-title">
								<ul class="nav nav-tabs pull-right">
									<?php
									if(count($tahuns) > 0){
										
										if(count($tahuns) >= 3){
											for($i=0; $i<2; $i++){
												$strA = ($tahuns[$i] == $tahun)  ? "class=\"active\"":"";
												echo "<li ".$strA."><a href=\"".base_url('panel/opendata/olah_apbd/'.$org['id'].'/'.$tahuns[$i])."\">".$tahuns[$i]."</a></li>";
											}
											
											echo "
												<li class=\"dropdown\">
													<a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
														Tahun sblmnya <span class=\"caret\"></span>
													</a>
													<ul class=\"dropdown-menu\">";
													for ($i=2;$i < count($tahuns); $i++){
														echo "<li><a href=\"".base_url('panel/opendata/olah_apbd/'.$org['id'].'/'.$tahuns[$i])."\">".$tahuns[$i]."</a></li>";
													}
													
													echo "
													</ul>
												</li>
											";
											
										}else{
											foreach($tahuns as $thn){
												$strA = ($thn == $tahun)  ? "class=\"active\"":"";
												echo "<li ".$strA."><a href=\"".base_url('panel/opendata/olah_apbd/'.$org['id'].'/'.$thn)."\">".$thn."</a></li>";
											}
										}
									}
									
									?>
									<li class="pull-left header"><a href=""><i class="fa fa-th"></i> <?php echo $org['nama'] ." - ".$org['namawilayah'] ." Tahun <strong class=\"text-blue\">".$tahun;?></strong></a></li>
								</ul>
								 </div>

								<div class="tab-content">
									<div class="tab-pane active" id="tab_<?php echo $tahun; ?>">
										<?php
										$r = explode(".",$varRek);

										echo "
										<div>
											<ol class=\"breadcrumb\">
												<li><a href=\"".$url."/?\"><i class=\"fa fa-home\"></i></a></li>";
												if(count($r) > 1){
													$rn = "";
													foreach($r as $ra){
														$rn .= $ra .".";
														if(strlen($ra) >= 3){
															echo "<li><a href=\"".$url."/?r=".$rn."\">".$rn."</a></li>";
														}
													}
												}
												echo "
												
											</ol>
										</div>";
										
										if($apbd){
											
											echo "
											<table class=\"table table-bordered\">
											<thead><tr><th style=\"width:30px;\">#</th>
												<th>Kode Rekening</th>
												<th>Uraian</th>
												<th>Nominal</th>
												<th style=\"width:50%;min-width:75px\">Tags</th>
											</tr></thead>
											<tbody>";
											$nomer = 1;
											foreach($apbd as $key=>$rs){
												echo "<tr><td class=\"angka\">".$nomer."</td>
												<td><a href=\"".$url."/?r=".$rs['akuntansi']."\">".$rs['akuntansi']."</a></td>
												<td>".$rs['uraian']."</td>
												<td class=\"angka\">".number_format($rs['nominal'],2)."</td>
												<td>
													<form action=\"\" method=\"POST\">

														<div class=\"input-group\">
															<select name=\"tags\" multiple=\"multiple\" class=\"form-control select-tags\">";
																foreach($list_tags as $key=>$tag){
																	echo "<option value=\"".$key."\">".$tag."</option>";
																}
																echo "
															</select>
															<div class=\"input-group-btn\">
																<button type=\"button\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
															</div><!-- /btn-group -->
														</div><!-- /input-group -->
													</form>
												</td>
												
												<td></td>
												</tr>";
												$nomer++;
											}
											echo "
											</tbody>
											</table>";
										}else{
											echo "
											<div class=\"callout callout-danger\">
												<h4>Data TIDAK BISA DITAMPILKAN</h4>
												<p>Maaf, data yang anda inginkan tidak bisa ditampilkan disini, karena ketidaktersediaan data tersebut.</p>
											</div>
											";
										}

										?>
									</div>
								</div><!--/.tabs-content-->
							</div><!--/.nav-tabs-custom-->
							
						</div>
						<div class="clear"></div>
	                    </div>
	                </div>
	            </div>
            <?php //}?>
            </form>
            </div>


            </div>
        <div class="footer">
            <div class="pull-right">
                <strong><?php echo $this->model_nav->disk_size();?></strong> Free.
            </div>
            <div>
                <strong>Copyright</strong> <?php echo $this->model_setting->setting('company');?> &copy; <?php echo date('Y');?>
            </div>
        </div>

        </div>
        </div>

    <!-- Mainly scripts -->
    <script src="<?php echo base_url()?>themes/default/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url()?>themes/default/js/inspinia.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/pace/pace.min.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/select2/select2.full.min.js"></script>

    <!--CK Editor-->
    <script src="<?php echo base_url()?>themes/default/js/plugins/ckeditor/ckeditor.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/ckeditor/en.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/ckeditor/id.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/ckeditor/ck.js"></script>

	<!--Files Scripts-->
	<script src="<?php echo base_url()?>themes/default/js/plugins/filestyle/filestyle.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/filestyle/aplication.js"></script>
	<!-- <script src="<?php echo base_url()?>themes/default/js/snippet-javascript-console.min.js"></script> -->
	<!-- Check -->
    <script src="<?php echo base_url()?>themes/default/js/check.js"></script>

	<!--Bootstrap Date Picker-->
	<script src="<?php echo base_url()?>themes/default/js/plugins/datetime/bootstrap-datetimepicker.min.js"></script>
	<!-- Toastr script -->
	<script src="<?php echo base_url()?>themes/default/js/plugins/toastr/toastr.min.js"></script>
	<script type="text/javascript">
    $(document).ready(function() {
		$(".date-picker").datetimepicker({format: 'yyyy-mm-dd hh:ii:ss',autoclose: true,});
        $(".select_province").select2({
            placeholder: "Pilih Provinsi",
            allowClear: false
        });
		$(".select-tags").select2();
        // $(".select_kabupaten").select2({
        //     placeholder: "Pilih Kabupaten",
        //     allowClear: false
        // });
    });
    if (location.hostname === "localhost" || location.hostname === "127.0.0.1")
    {
    var urlArray  = window.location.pathname.split( '/' );
    var url_Base  = document.location.origin+'/'+urlArray[1]+'/';
    var urlPost   = urlArray[2];
    }
    else
    {
    var urlArray  = window.location.pathname.split( '/' );
    var url_Base  = document.location.origin+'/';
    var urlPost   = urlArray[1];
    }
    // alert(url_Base);
    $(function(){
        $.ajaxSetup({
            // contentType: 'application/json; charset=utf-8',
            type:"POST",
            url: url_Base + "ajax/location.html",
            // dataType: 'jsonp',
            // async = true,
            cache: false,
        });
        // console.log(data);
        $("#provinsi").change(function()
        {
            var value=$(this).val();
            if(value>0){
            $.ajax({
            data:{modul:'kabupaten',id:value},
                success: function(respond){
                    $("#kabupaten").html(respond);
                    // $("#kecamatan").html('<option value="0">Pilih Kecamatan *</option>');
                    // console.log("sample"+respond);
                }
            })
            
        }
        else
        {
            $.ajax({
            data:{modul:'kabupaten',id:value},
                success: function(respond){
                    $("#kabupaten").html('<option value="0">Pilih Kabupaten *</option>');
                    // $("#kecamatan").html('<option value="0">Pilih Kecamatan *</option>');
                    // console.log("gfsgds"+respond);
                }
            })
        }
        });
        
        // $("#kabupaten").change(function(){
        //     var value=$(this).val();
        //     if(value>0){
        //         $.ajax({
        //             data:{modul:'select_kecamatan',id:value},
        //                 success: function(respond){
        //                 $("#kecamatan").html(respond);
        //             }
        //         })
        //     }
        // })
    });
    </script>
    <script>
		$(function () {
			toastr.options = {
				"closeButton": true,
				"debug": false,
				"progressBar": true,
				"preventDuplicates": false,
				"positionClass": "toast-bottom-right",
				"onclick": null,
				"showDuration": "400",
				"hideDuration": "1000",
				"timeOut": "7000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
			//toastr.success('Successfully Update Profile', 'Update Profile');
			<?php if(isset($_COOKIE['status'])){ echo $this->model_status->status($_COOKIE['status']);}?>
			<?php if(isset($_COOKIE['status_second'])){ echo $this->model_status->status($_COOKIE['status_second']);}?>
		});
	</script>
	

</body>

</html>
