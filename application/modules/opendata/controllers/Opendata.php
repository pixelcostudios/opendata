<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(FCPATH . '/vendor/autoload.php');
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\IReader;
use PhpOffice\PhpSpreadsheet\Reader\Xls;

class Opendata extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//require APPPATH . "third_party/MX/Controller.php";

	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
        $this->load->library('email');
        // $this->load->library('pdf');
        $this->load->helper(array('form'));
        $this->load->helper(array('url'));
		$this->load->helper(array('path'));
		$this->load->library("pagination");
		$this->load->model("menu/model_nav");
		$this->load->model("post/model_post");
		$this->load->model("setting/model_setting");
		$this->load->model("post/model_ipost");
        $this->load->model("member/model_member");
        $this->load->model("model_opendata");
        $this->load->model("front/apbd_model");
         $this->load->model("front/lembaga_model");
	}
	public function index()
	{
        if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
		}
		else 
		{
			redirect(base_url().'panel/dashboard.html');
		}
	}
    public function lembaga()
    {
        if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        //echo $this->uri->segment(3);
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);
        $data['cat_list']               = $this->model_nav->ca_list('post','0');

        $data["post_list"]              = $this->model_opendata->lem_list('3402');
        
        if(isset($_POST['post-delete']) && $_POST['post-delete'] != "")
		{
            for ($i=0; $i<count($_POST['post-check']);$i++)
            {
                $post_id=$_POST['post-check'][$i];

                $this->model_opendata->lem_delete($post_id);
            }
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/opendata/lembaga.html');
        }
        $this->load->view('lembaga',$data);

    }
	function lembaga_add()
	{
		if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        /**
        * 
        * @var List Menu
        * 
        */
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);
      
        $data['provinsi']           = $this->model_opendata->prov_list();

        if(isset($_POST['Pixel_Save']))
        {
            $ndesc              = $this->input->post('ndesc');
            $nama               = $this->input->post('nama');         
            $alamat             = $this->input->post('alamat');
            $provinsi           = $this->input->post('provinsi');
            $kabupaten          = $this->input->post('kabupaten');
            $kode_org           = $this->input->post('kode_org');
            $url                = $this->input->post('url');
            $email              = $this->input->post('email');
            $telp               = $this->input->post('telp');
            $fax                = $this->input->post('fax');

            $data_array = array(  
                'ndesc'         => $ndesc,   
                'nama'          => $nama,
                'alamat'        => $alamat,
                'provinsi'      => $provinsi,
                'kabupaten'     => $kabupaten,  
                'kode_org'      => $kode_org,  
                'url'           => $url,  
                'email'         => $email,  
                'telp'          => $telp,  
                'fax'           => $fax
            );  
            $this->model_opendata->lem_add($data_array);
			$id 			= $this->db->insert_id();
            
            /**
            * @var Upload Files
            */
            // if(isset($_FILES['upload_photo']['tmp_name']))
		    // {
            //     $this->load->library('upload',[
            //         'upload_path'	=> FCPATH.'upload/',
            //         'allowed_types'	=> 'gif|jpg|png',
            //         'max_size'		=> 10000,
            //         'encrypt_name'	=> TRUE
            //     ]);
            //     if(!$this->upload->do_upload('upload_photo'))
            //     {
            //         //throw new Exception($this->upload->display_errors('',''),REST_Controller::HTTP_EXPECTATION_FAILED);
            //     }
            //     else 
            //     {
            //         $data 	    = $this->upload->data();
            //         $foto       = $data["file_name"];
            //     }
            // }
            // $data_array = array(  
            //     'foto'          => $foto
            // );  
            $this->model_opendata->lem_update($id,$data_array);

            setcookie('status','added', time()+2);
            redirect(base_url().'panel/opendata/lembaga_edit/'.$id.'.html');
        
        }
        $this->load->view('lembaga_add', $data);
	}
	function lembaga_edit()
	{
		if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        /**
        * 
        * @var List Menu
        * 
        */
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);
        
        /**
        * 
        * @var Data
        * 
        */
        $post_id                    = $this->uri->segment(4);
        $data['row']                = $this->model_opendata->lem_edit($post_id);

        $data['provinsi']           = $this->model_opendata->prov_list();

        if(isset($_POST['Pixel_Save']))
        {
            /**
            * @var Upload Files
            */
            if(isset($_FILES['upload_photo']['tmp_name']))
		    {
                $this->load->library('upload',[
                    'upload_path'	=> FCPATH.'upload/',
                    'allowed_types'	=> 'gif|jpg|png',
                    'max_size'		=> 10000,
                    'encrypt_name'	=> TRUE
                ]);
                if(!$this->upload->do_upload('upload_photo'))
                {
                    //throw new Exception($this->upload->display_errors('',''),REST_Controller::HTTP_EXPECTATION_FAILED);
                    $foto       = '';
                }
                else 
                {
                    $data 	    = $this->upload->data();
                    $foto       = $data["file_name"];
                }
            }

            $id                 = $this->uri->segment(4);
            $ndesc              = $this->input->post('ndesc');
            $nama               = $this->input->post('nama');         
            $alamat             = $this->input->post('alamat');
            $provinsi           = $this->input->post('provinsi');
            $kabupaten          = $this->input->post('kabupaten');
            $kode_org           = $this->input->post('kode_org');
            $url                = $this->input->post('url');
            $email              = $this->input->post('email');
            $telp               = $this->input->post('telp');
            $fax                = $this->input->post('fax');

            $data_array = array(  
                'ndesc'         => $ndesc,   
                'foto'          => $foto,
                'nama'          => $nama,
                'alamat'        => $alamat,
                'provinsi'      => $provinsi,
                'kabupaten'     => $kabupaten,  
                'kode_org'      => $kode_org,  
                'url'           => $url,  
                'email'         => $email,  
                'telp'          => $telp,  
                'fax'           => $fax
            );  
            $this->model_opendata->lem_update($this->uri->segment(4),$data_array);

            // $this->model_ipost->ipost_upload('upload_photo','post',$id,$_SESSION['user_id']);
            setcookie('status','updated', time()+2);
            redirect(base_url().'panel/opendata/lembaga_edit/'.$this->uri->segment(4).'.html');
        }
        
        if(isset($_POST['Pixel_D_Images']))
        {
            if (isset($_POST['post-check'])){
            for ($i=0; $i<count($_POST['post-check']);$i++) 
                {   
                    $i_name=explode(",",$_POST['post-check'][$i]);
                
                    if (file_exists('upload/'.$i_name[1]))
			        {
                        unlink("upload/".$i_name[1]);
                    }
                    if (file_exists('upload/c_'.$i_name[1]))
			        {
                        unlink("upload/c_".$i_name[1]);
                    }
                    $this->model_ipost->ip_delete($i_name[0]);
                }
                setcookie('status','deleted_images', time()+2);
                redirect(base_url(uri_string()).'.html');
            }
            else
            {
                setcookie('status','noimages', time()+2);
                redirect(base_url(uri_string()).'.html');
            }
        }
        $this->load->view('lembaga_edit', $data);
	}
    function search()
    {
        $data['list_post_count'] 	    = $this->model_nav->po_list_count('post','0');
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count'] 	    = $this->model_nav->us_list_count();
        $data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);
        $data['cat_list']               = $this->model_nav->ca_list('post','0');

        $post_type                  = 'pages';
        $cat                        = $this->model_setting->my_simple_crypt($this->uri->segment(4),$action = 'd');
        if(isset($_POST['Pixel_Search']))
        {
            setcookie('status','search', time()+2);
            redirect(base_url().'panel/pages/search/'.$this->model_setting->my_simple_crypt($this->input->post('search'),$action = 'e').'.html');
        }

        $config = array();
        $config["base_url"]         = ($this->uri->segment(5)) ? base_url().'panel/pages/search/'.$this->uri->segment(4).'/'.$this->uri->segment(5).'/' : base_url().'panel/post/search/'.$this->uri->segment(4).'/10/';
        $total_row                  = $this->model_post->po_search_count($post_type,$cat);
        $config['suffix']           = '.html';
        $config['first_url']        = '1.html';
        $config["total_rows"]       = $total_row;
        $config["per_page"]         = ($this->uri->segment(5)) ? $this->uri->segment(5) : '10';
        $config['use_page_numbers'] = TRUE;
        $config['num_links']        = $total_row;
        $config['cur_tag_open']     = '&nbsp;<a class="active">';
        $config['cur_tag_close']    = '</a>';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['num_links']        = 5;

        $this->pagination->initialize($config);
        if($this->uri->segment(6))
        {
            $page = ($this->uri->segment(6)-1)*$config["per_page"] ;
        }
        else
        {
            $page = 0;
        }
        $data["post_list"]          = $this->model_post->po_search($post_type,$cat,$config["per_page"], $page);

        $str_links                  = $this->pagination->create_links();
        $data["links"]              = explode('&nbsp;',$str_links );

        if(isset($_POST['post-draft']) && $_POST['post-draft'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$post_id=$_POST['post-check'][$i];
				
				$this->model_post->po_update_status($post_id,'0');
			}
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/pages.html');
		}
        elseif(isset($_POST['post-publish']) && $_POST['post-publish'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$post_id=$_POST['post-check'][$i];
				
				$this->model_post->po_update_status($post_id,'1');
			}
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/pages.html');
		}
		elseif(isset($_POST['post-delete']) && $_POST['post-delete'] != "")
		{
            for ($i=0; $i<count($_POST['post-check']);$i++)
            {
                $post_id=$_POST['post-check'][$i];

                foreach($this->model_ipost->ip_list($post_id,'post','DESC') as $row_sub)
                {
                    @unlink('./upload/'.$row_sub->ipost);
                    $this->model_ipost->ip_delete_type($post_id,'post');
                }
                foreach($this->model_ipost->ip_list($post_id,'post_icon','DESC') as $row_sub)
                {
                    @unlink('./upload/'.$row_sub->ipost);
                }
                foreach($this->model_ipost->ip_list($post_id,'post_thumb','DESC') as $row_sub)
                {
                    @unlink('./upload/'.$row_sub->ipost);
                }
                foreach($this->model_ipost->ip_list($post_id,'post_background','DESC') as $row_sub)
                {
                    @unlink('./upload/'.$row_sub->ipost);
                }
                foreach($this->model_ipost->ip_list($post_id,'post_pdf','DESC') as $row_sub)
                {
                    @unlink('./upload/files/'.$row_sub->ipost);
                }
                foreach($this->model_ipost->ip_list($post_id,'post_files','DESC') as $row_sub)
                {
                    @unlink('./upload/files/'.$row_sub->ipost);
                }
                $this->model_ipost->ip_delete_post($post_id);
                $this->model_post->po_delete($post_id);
            }
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/pages.html');
        }
        elseif(isset($_POST['post-delete-ajax']) && $_POST['post-delete-ajax'] != "")
		{
            $post_id=$_POST['post_id'];

            foreach($this->model_ipost->ip_list($post_id,'post','DESC') as $row_sub)
            {
                @unlink('./upload/'.$row_sub->ipost);
                $this->model_ipost->ip_delete_type($post_id,'post');
            }
            foreach($this->model_ipost->ip_list($post_id,'post_icon','DESC') as $row_sub)
            {
                @unlink('./upload/'.$row_sub->ipost);
            }
            foreach($this->model_ipost->ip_list($post_id,'post_thumb','DESC') as $row_sub)
            {
                @unlink('./upload/'.$row_sub->ipost);
            }
            foreach($this->model_ipost->ip_list($post_id,'post_background','DESC') as $row_sub)
            {
                @unlink('./upload/'.$row_sub->ipost);
            }
            foreach($this->model_ipost->ip_list($post_id,'post_pdf','DESC') as $row_sub)
            {
                @unlink('./upload/files/'.$row_sub->ipost);
            }
            foreach($this->model_ipost->ip_list($post_id,'post_files','DESC') as $row_sub)
            {
                @unlink('./upload/files/'.$row_sub->ipost);
            }
            $this->model_ipost->ip_delete_post($post_id);
            $this->model_post->po_delete($post_id);

            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/pages.html');
        }
        $this->load->view('pages',$data);
    }
    public function rekening()
    {
        if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        //echo $this->uri->segment(3);
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);
        $data['cat_list']               = $this->model_nav->ca_list('post','0');

        $data["post_list"]              = $this->model_opendata->lem_list('3402');
        
        if(isset($_POST['post-delete']) && $_POST['post-delete'] != "")
		{
            for ($i=0; $i<count($_POST['post-check']);$i++)
            {
                $post_id=$_POST['post-check'][$i];

                $this->model_opendata->lem_delete($post_id);
            }
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/opendata/lembaga.html');
        }
        $this->load->view('rekening',$data);

    }
    function olah_data() 
    {
        if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        /**
        * 
        * @var List Menu
        * 
        */
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);

        $data['list_lembaga']           = $this->model_opendata->lem_list('3402');
        
        if(isset($_POST['Pixel_Data']))
        {
            //$lembaga        = explode(',',$this->input->post('lembaga_id'));
            $lembaga_id     = $this->input->post('lembaga_id');
            // $lembaga_kode   = $lembaga[1];
            $year = $this->input->post('year');

            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/opendata/olah_apbd/'.$lembaga_id.'/'.$year.'.html');
        }
        $this->load->view('olah_data', $data);
    }
    function olah_data_list() 
    {
        if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        /**
        * 
        * @var List Menu
        * 
        */
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);

        $data['post_list']           = $this->model_opendata->tags_list();
        
        if(isset($_POST['post-delete']) && $_POST['post-delete'] != "")
		{
            for ($i=0; $i<count($_POST['post-check']);$i++)
            {
                $post_id=$_POST['post-check'][$i];

                $this->model_opendata->tags_delete($post_id);
            }
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/opendata/olah_data_list.html');
        }
        $this->load->view('olah_data_list', $data);
    }
    function olah_data_list_add()
	{
		if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        /**
        * 
        * @var List Menu
        * 
        */
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);
        
        /**
        * 
        * @var Data
        * 
        */
        $post_id                    = $this->uri->segment(4);
        $data['row']                = $this->model_opendata->tags_edit($post_id);

        $data['provinsi']           = $this->model_opendata->prov_list();

        if(isset($_POST['Pixel_Save']))
        {
            $ndesc              = $this->input->post('ndesc');
            $nama               = $this->input->post('nama'); 

            $data_array = array(  
                'ndesc'         => $ndesc,   
                'nama'          => $nama
            );  
            $this->model_opendata->tags_add($data_array);
            $id 			= $this->db->insert_id();

            setcookie('status','updated', time()+2);
            redirect(base_url().'panel/opendata/olah_data_list_edit/'.$id.'.html');
        }
        
        $this->load->view('olah_data_list_add', $data);
	}
    function olah_data_list_edit()
	{
		if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        /**
        * 
        * @var List Menu
        * 
        */
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);
        
        /**
        * 
        * @var Data
        * 
        */
        $post_id                    = $this->uri->segment(4);
        $data['row']                = $this->model_opendata->tags_edit($post_id);

        $data['provinsi']           = $this->model_opendata->prov_list();

        if(isset($_POST['Pixel_Save']))
        {
            $id                 = $this->uri->segment(4);
            $ndesc              = $this->input->post('ndesc');
            $nama               = $this->input->post('nama'); 

            $data_array = array(  
                'ndesc'         => $ndesc,   
                'nama'          => $nama
            );  
            $this->model_opendata->tags_update($this->uri->segment(4),$data_array);

            setcookie('status','updated', time()+2);
            redirect(base_url().'panel/opendata/olah_data_list_edit/'.$this->uri->segment(4).'.html');
        }
        
        $this->load->view('olah_data_list_edit', $data);
    }
    // function olah_data_list_add()
	// {
	// 	if (!$this->ion_auth->logged_in())
    //     {
    //         redirect(base_url().'panel');
    //     }
    //     /**
    //     * 
    //     * @var List Menu
    //     * 
    //     */
    //     $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
    //     $data['list_pages']             = $this->model_nav->pg_list('pages','0');
    //     $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
    //     $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
    //     $data['list_member_role']       = $this->model_nav->us_list_role();
    //     $data['list_member_count']      = $this->model_nav->us_list_count();
    //     $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);
        
    //     /**
    //     * 
    //     * @var Data
    //     * 
    //     */
    //     $post_id                    = $this->uri->segment(4);
    //     $data['row']                = $this->model_opendata->tags_edit($post_id);

    //     $data['provinsi']           = $this->model_opendata->prov_list();

    //     if(isset($_POST['Pixel_Save']))
    //     {
    //         $id                 = $this->uri->segment(4);
    //         $ndesc              = $this->input->post('ndesc');
    //         $nama               = $this->input->post('nama'); 

    //         $data_array = array(  
    //             'ndesc'         => $ndesc,   
    //             'nama'          => $nama
    //         );  
    //         $this->model_opendata->tags_update($this->uri->segment(4),$data_array);

    //         setcookie('status','updated', time()+2);
    //         redirect(base_url().'panel/opendata/olah_data_list_edit/'.$this->uri->segment(4).'.html');
    //     }
        
    //     $this->load->view('olah_data_list_add', $data);
	// }
    function upload_data()
	{
		if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        /**
        * 
        * @var List Menu
        * 
        */
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);

        $data['list_lembaga']           = $this->model_opendata->lem_list('3402');
    
        if(isset($_POST['Pixel_Save']))
        {
            //$lembaga        = explode(',',$this->input->post('lembaga_id'));
            $lembaga_id     = $this->input->post('lembaga_id');
            // $lembaga_kode   = $lembaga[1];
            $year = $this->input->post('year');

            if(isset($_FILES['upload_files']['tmp_name']))
		    {
                $this->load->library('upload',[
                    'upload_path'	=> FCPATH.'upload/files/',
                    'allowed_types'	=> 'csv|xls|xlsx',
                    'max_size'		=> 10000,
                    'encrypt_name'	=> TRUE
                ]);
                if(!$this->upload->do_upload('upload_files'))
                {
                    //throw new Exception($this->upload->display_errors('',''),REST_Controller::HTTP_EXPECTATION_FAILED);
                }
                else 
                {
                    $data 	    = $this->upload->data();
                    $file       = $data["file_name"];
                }
            }

            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
            $spreadsheet = $reader->load("upload/files/".$file);
            //$spreadsheet = $reader->load("document/2016.Kab Bantul.Lampiran.csv");
            $sheetData = $spreadsheet->getActiveSheet()->toArray();
            // print_r($sheetData);
            $i=0;
            foreach ($sheetData as $key) 
            {$i++;
                if($i>8)
                {
                    if($key[0]!='' AND $key[1]!='' AND $key[2])
                    {
                        //print_r($key);
                        // echo $key[0]; rekening
                        // echo '<br>';
                        // echo $key[1]; uraian
                        // echo '<br>';
                        // echo $key[2]; nominal
                        // echo '<br>';
                        // echo $key[3]; keterangan
                        $wilayah_kode 	= '3402';
                        $rekening		= str_replace(' ','',$key['0']);

                        if($this->model_opendata->apbds_check($rekening,$key['1'],$year)>0)
                        {
                            $data_array = array( 
                                'wilayah_kode'  => $wilayah_kode,
                                'tahun'      	=> $year,
                                'uraian'     	=> $key['1'],  
                                'nominal'      	=> $key['2'],
                                'keterangan'    => $key['3'],
                                'updated_at'	=> date('Y-m-d H:i:s')
                            );  
                            $this->model_opendata->apbds_rekening_update($rekening,$data_array);
                        }
                        else 
                        {
                            $data_array = array(  
                                'lembaga_id'      	=> $lembaga_id, 
                                // 'lembaga_kode'      => $lembaga_kode, 
                                'wilayah_kode'      => $wilayah_kode,   
                                'tahun'      		=> $year,  
                                'rekening'      	=> $rekening,  
                                'uraian'     		=> $key['1'],  
                                'nominal'      		=> $key['2'],
                                'keterangan'     	=> $key['3'],
                                'created_by'     	=> '1',
                                'created_at'		=> date('Y-m-d H:i:s')
                            );  
                            $this->model_opendata->apbds_add($data_array);
                            $id 			= $this->db->insert_id();
                        }
                    }
                }
            }
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/opendata/upload_data.html');
        }
        $this->load->view('upload_data',$data);
    }
    function excel()
	{
		if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        /**
        * 
        * @var List Menu
        * 
        */
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);

        $data['list_lembaga']           = $this->model_opendata->lem_list('3402');
        
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        $this->load->library('Excel');

        // $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
        // $spreadsheet = $reader->load("upload/files/2016.Kab-Bantul.Lampiran.csv");
        // //$spreadsheet = $reader->load("document/2016.Kab Bantul.Lampiran.csv");
        // $sheetData = $spreadsheet->getActiveSheet()->toArray();
        // print_r($sheetData);

        switch ($this->uri->segment('4')) 
        {
            case '33':
            $inputFileName = './document/lamp.1a.xls';

            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' .
                    $e->getMessage());
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            $i=0;
            // //lampiran
            for ($row = 22; $row <= $highestRow; $row++)
            {   $i++;
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                    null, true, false);
                    //$rowData['0']['1']
                echo '<pre>';
                
                if($rowData['0']['2']!='')
                {
                    $datas_array = array('Rekening'=>str_replace(' ','',$rowData['0']['2']),
                                    'Uraian'=>$rowData['0']['5'].$rowData['0']['6'].$rowData['0']['7'].$rowData['0']['8'].$rowData['0']['9'].$rowData['0']['10'].$rowData['0']['11'],
                                    'Jumlah'=>($rowData['0']['12']!='') ? $rowData['0']['12'] : 0,
                                    'Penjelasan'=>$rowData['0']['15'].$rowData['0']['16'].$rowData['0']['17']);
                        print_r($datas_array);
                        $lembaga_id    = '33';
                        $lembaga_kode  = '0';
                        $pemda_kode    = '0';
                        $wilayah_kode  = '3402';
                        $year          = '2017';
                        $data_array = array( 
                            'lembaga_id'    => $lembaga_id,  
                            'lembaga_kode'  => $lembaga_kode,
                            'pemda_kode'    => $pemda_kode,
                            'wilayah_kode'  => $wilayah_kode,
                            'tahun'      	=> $year,
                            'rekening'     	=> $datas_array['Rekening'], 
                            'uraian'     	=> $datas_array['Uraian'],  
                            'nominal'      	=> $datas_array['Jumlah'],
                            'keterangan'    => $datas_array['Penjelasan'],
                            'updated_at'	=> date('Y-m-d H:i:s')
                        );  
                        if($this->model_opendata->apbd2_check($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year)>0)
                        {
                            $this->model_opendata->apbd2_update($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year,$data_array);
                        }
                        else 
                        {
                            $this->model_opendata->apbd2_add($data_array);
                        }
                    }
                    echo '</pre>';
                }
                break;

            // case '29': // bapedda
            // $inputFileName = './document/bappedas.xls';

            // try {
            //     $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            //     $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            //     $objPHPExcel = $objReader->load($inputFileName);
            // } catch (Exception $e) {
            //     die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' .
            //         $e->getMessage());
            // }

            // $sheet = $objPHPExcel->getSheet(0);
            // $highestRow = $sheet->getHighestRow();
            // $highestColumn = $sheet->getHighestColumn();

            // $i=0;
            // // //bappedda
            // for ($row = 26; $row <= $highestRow; $row++)
            // {   $i++;
            //     $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
            //         null, true, false);
            //         //$rowData['0']['1']
            //     echo '<pre>';
               
            //     if($rowData['0']['3']!='')
            //     {
            //         $datas_array = array('Rekening'=>str_replace(' ','',$rowData['0']['3']),
            //                         'Uraian'=>$rowData['0']['8'].$rowData['0']['9'].$rowData['0']['10'].$rowData['0']['11'].$rowData['0']['12'].$rowData['0']['13'].$rowData['0']['14'].$rowData['0']['15'],
            //                         'Jumlah'=>($rowData['0']['16']!='') ? $rowData['0']['16'] : 0,
            //                         'Penjelasan'=>$rowData['0']['21'].$rowData['0']['22'].$rowData['0']['23']);
            //         print_r($datas_array);

            //         $lembaga_id    = '29';
            //         $lembaga_kode  = '1.06.01';
            //         $pemda_kode    = '4.03.01';
            //         $wilayah_kode  = '3402';
            //         $year          = '2017';
            //         $data_array = array( 
            //             'lembaga_id'    => $lembaga_id,  
            //             'lembaga_kode'  => $lembaga_kode,
            //             'pemda_kode'    => $pemda_kode,
            //             'wilayah_kode'  => $wilayah_kode,
            //             'tahun'      	=> $year,
            //             'rekening'     	=> $datas_array['Rekening'], 
            //             'uraian'     	=> $datas_array['Uraian'],  
            //             'nominal'      	=> $datas_array['Jumlah'],
            //             'keterangan'    => $datas_array['Penjelasan'],
            //             'updated_at'	=> date('Y-m-d H:i:s')
            //         );  
            //         if($this->model_opendata->apbd2_check($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year)>0)
            //         {
            //             $this->model_opendata->apbd2_update($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year,$data_array);
            //         }
            //         else 
            //         {
            //             $this->model_opendata->apbd2_add($data_array);
            //         }
            //     }
                
            //     echo '</pre>';
            // }
            //     break;

            // case '51': //dinkes
            // $inputFileName = './document/dinkes.xls';

            // try {
            //     $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            //     $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            //     $objPHPExcel = $objReader->load($inputFileName);
            // } catch (Exception $e) {
            //     die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' .
            //         $e->getMessage());
            // }

            // $sheet = $objPHPExcel->getSheet(0);
            // $highestRow = $sheet->getHighestRow();
            // $highestColumn = $sheet->getHighestColumn();

            // $i=0;
            // // //dinkes
            // for ($row = 27; $row <= $highestRow; $row++)
            // {   $i++;
            //     $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
            //         null, true, false);
            //         //$rowData['0']['1']
                
            //     echo '<pre>';
            //     if($rowData['0']['3']!='')
            //     {
            //         $datas_array = array('Rekening'=>str_replace(' ','',$rowData['0']['3']),
            //                         'Uraian'=>$rowData['0']['8'].$rowData['0']['9'].$rowData['0']['10'].$rowData['0']['11'].$rowData['0']['12'].$rowData['0']['13'],
            //                         'Jumlah'=>($rowData['0']['15']!='') ? $rowData['0']['15'] : 0,
            //                         'Dasar Hukum'=>$rowData['0']['18']);

            //         print_r($datas_array);

            //         // $lembaga_id    = '51';
            //         // $lembaga_kode  = '1.06.01';
            //         // $pemda_kode    = '1.02.01';
            //         // $wilayah_kode  = '3402';
            //         // $year          = '2017';
            //         // $data_array = array( 
            //         //     'lembaga_id'    => $lembaga_id,  
            //         //     'lembaga_kode'  => $lembaga_kode,
            //         //     'pemda_kode'    => $pemda_kode,
            //         //     'wilayah_kode'  => $wilayah_kode,
            //         //     'tahun'      	=> $year,
            //         //     'rekening'     	=> $datas_array['Rekening'], 
            //         //     'uraian'     	=> $datas_array['Uraian'],  
            //         //     'nominal'      	=> $datas_array['Jumlah'],
            //         //     'keterangan'    => $datas_array['Dasar Hukum'],
            //         //     'updated_at'	=> date('Y-m-d H:i:s')
            //         // );  
            //         // if($this->model_opendata->apbd2_check($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year)>0)
            //         // {
            //         //     $this->model_opendata->apbd2_update($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year,$data_array);
            //         // }
            //         // else 
            //         // {
            //         //     $this->model_opendata->apbd2_add($data_array);
            //         // }
            //     }
                
            //     echo '</pre>';
            // }
            //     break;

            case '111': //dinkes 2018
            $inputFileName = './document/lamp.xlsx';

            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' .
                    $e->getMessage());
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            $i=0;
            // //dinkes
            for ($row = 1; $row <= $highestRow; $row++)
            {   $i++;
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                    null, true, false);
                    //$rowData['0']['1']

                    // print_r($rowData);
                
                echo '<pre>';
                if($rowData['0']['1']!='')
                {
                    $datas_array = array('Rekening'=>str_replace(' ','',$rowData['0']['0']),
                                    'Uraian'=>$rowData['0']['1'].$rowData['0']['7'].$rowData['0']['8'].$rowData['0']['9'].$rowData['0']['10'].$rowData['0']['11'].$rowData['0']['12'].$rowData['0']['13'].$rowData['0']['14'].$rowData['0']['15'],
                                    'Jumlah'=>($rowData['0']['2']!='') ? str_replace('.','',$rowData['0']['2']) : 0,
                                    'Dasar Hukum'=>$rowData['0']['20'].$rowData['0']['21'].$rowData['0']['22'].$rowData['0']['23']);

                    print_r($datas_array);

                    $lembaga_id    = '33';
                    $lembaga_kode  = '1.02.01.01';
                    $pemda_kode    = '1.02.01';
                    $wilayah_kode  = '3402';
                    $year          = '2018';
                    $data_array = array( 
                        'lembaga_id'    => $lembaga_id,  
                        'lembaga_kode'  => $lembaga_kode,
                        'pemda_kode'    => $pemda_kode,
                        'wilayah_kode'  => $wilayah_kode,
                        'tahun'      	=> $year,
                        'rekening'     	=> $datas_array['Rekening'], 
                        'uraian'     	=> $datas_array['Uraian'],  
                        'nominal'      	=> $datas_array['Jumlah'],
                        'keterangan'    => $datas_array['Dasar Hukum'],
                        'updated_at'	=> date('Y-m-d H:i:s')
                    );  
                    if($this->model_opendata->apbd2_check($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year)>0)
                    {
                        $this->model_opendata->apbd2_update($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year,$data_array);
                    }
                    else 
                    {
                        $this->model_opendata->apbd2_add($data_array);
                    }
                }
                
                echo '</pre>';
            }
                break;


                case '46': //dinkes 2018
            $inputFileName = './document/2018/1. dinas pendidikan.xls';

            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' .
                    $e->getMessage());
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            $i=0;
            // //dinkes
            for ($row = 27; $row <= $highestRow; $row++)
            {   $i++;
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                    null, true, false);
                    //$rowData['0']['1']
                
                echo '<pre>';
                if($rowData['0']['3']!='')
                {
                    $datas_array = array('Rekening'=>str_replace(' ','',$rowData['0']['3']),
                                    'Uraian'=>$rowData['0']['6'].$rowData['0']['7'].$rowData['0']['8'].$rowData['0']['9'].$rowData['0']['10'].$rowData['0']['11'].$rowData['0']['12'].$rowData['0']['13'].$rowData['0']['14'].$rowData['0']['15'],
                                    'Jumlah'=>($rowData['0']['16']!='') ? $rowData['0']['16'] : 0,
                                    'Dasar Hukum'=>$rowData['0']['20'].$rowData['0']['21'].$rowData['0']['22'].$rowData['0']['23']);

                    print_r($datas_array);

                    $lembaga_id    = '46';
                    $lembaga_kode  = '1.01.01';
                    $pemda_kode    = '1.02.01';
                    $wilayah_kode  = '3402';
                    $year          = '2018';
                    $data_array = array( 
                        'lembaga_id'    => $lembaga_id,  
                        'lembaga_kode'  => $lembaga_kode,
                        'pemda_kode'    => $pemda_kode,
                        'wilayah_kode'  => $wilayah_kode,
                        'tahun'      	=> $year,
                        'rekening'     	=> $datas_array['Rekening'], 
                        'uraian'     	=> $datas_array['Uraian'],  
                        'nominal'      	=> $datas_array['Jumlah'],
                        'keterangan'    => $datas_array['Dasar Hukum'],
                        'updated_at'	=> date('Y-m-d H:i:s')
                    );  
                    if($this->model_opendata->apbd2_check($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year)>0)
                    {
                        $this->model_opendata->apbd2_update($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year,$data_array);
                    }
                    else 
                    {
                        $this->model_opendata->apbd2_add($data_array);
                    }
                }
                
                echo '</pre>';
            }
                break;

                case '51': //dinkes 2018
            $inputFileName = './document/2018/2. dinas kesehatan.xls';

            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' .
                    $e->getMessage());
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            $i=0;
            // //dinkes
            for ($row = 26; $row <= $highestRow; $row++)
            {   $i++;
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                    null, true, false);
                    //$rowData['0']['1']
                
                echo '<pre>';
                if($rowData['0']['3']!='')
                {
                    $datas_array = array('Rekening'=>str_replace(' ','',$rowData['0']['3']),
                                    'Uraian'=>$rowData['0']['6'].$rowData['0']['7'].$rowData['0']['8'].$rowData['0']['9'].$rowData['0']['10'].$rowData['0']['11'].$rowData['0']['12'].$rowData['0']['13'].$rowData['0']['14'].$rowData['0']['15'],
                                    'Jumlah'=>($rowData['0']['16']!='') ? $rowData['0']['16'] : 0,
                                    'Dasar Hukum'=>$rowData['0']['20'].$rowData['0']['21'].$rowData['0']['22'].$rowData['0']['23']);

                    print_r($datas_array);

                    $lembaga_id    = '51';
                    $lembaga_kode  = '1.02.01';
                    $pemda_kode    = '1.02.01';
                    $wilayah_kode  = '3402';
                    $year          = '2018';
                    $data_array = array( 
                        'lembaga_id'    => $lembaga_id,  
                        'lembaga_kode'  => $lembaga_kode,
                        'pemda_kode'    => $pemda_kode,
                        'wilayah_kode'  => $wilayah_kode,
                        'tahun'      	=> $year,
                        'rekening'     	=> $datas_array['Rekening'], 
                        'uraian'     	=> $datas_array['Uraian'],  
                        'nominal'      	=> $datas_array['Jumlah'],
                        'keterangan'    => $datas_array['Dasar Hukum'],
                        'updated_at'	=> date('Y-m-d H:i:s')
                    );  
                    if($this->model_opendata->apbd2_check($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year)>0)
                    {
                        $this->model_opendata->apbd2_update($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year,$data_array);
                    }
                    else 
                    {
                        $this->model_opendata->apbd2_add($data_array);
                    }
                }
                
                echo '</pre>';
            }
                break;

                case '58': //dinsos 2018
            $inputFileName = './document/2018/8. dinas sosial.xls';

            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' .
                    $e->getMessage());
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            $i=0;
            // //dinkes
            for ($row = 26; $row <= $highestRow; $row++)
            {   $i++;
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                    null, true, false);
                    //$rowData['0']['1']
                
                echo '<pre>';
                if($rowData['0']['3']!='')
                {
                    $datas_array = array('Rekening'=>str_replace(' ','',$rowData['0']['3']),
                                    'Uraian'=>$rowData['0']['6'].$rowData['0']['7'].$rowData['0']['8'].$rowData['0']['9'].$rowData['0']['10'].$rowData['0']['11'].$rowData['0']['12'].$rowData['0']['13'].$rowData['0']['14'].$rowData['0']['15'],
                                    'Jumlah'=>($rowData['0']['16']!='') ? $rowData['0']['16'] : 0,
                                    'Dasar Hukum'=>$rowData['0']['20'].$rowData['0']['21'].$rowData['0']['22'].$rowData['0']['23']);

                    print_r($datas_array);

                    $lembaga_id    = '58';
                    $lembaga_kode  = '1.13.01';
                    $pemda_kode    = '1.02.01';
                    $wilayah_kode  = '3402';
                    $year          = '2018';
                    $data_array = array( 
                        'lembaga_id'    => $lembaga_id,  
                        'lembaga_kode'  => $lembaga_kode,
                        'pemda_kode'    => $pemda_kode,
                        'wilayah_kode'  => $wilayah_kode,
                        'tahun'      	=> $year,
                        'rekening'     	=> $datas_array['Rekening'], 
                        'uraian'     	=> $datas_array['Uraian'],  
                        'nominal'      	=> $datas_array['Jumlah'],
                        'keterangan'    => $datas_array['Dasar Hukum'],
                        'updated_at'	=> date('Y-m-d H:i:s')
                    );  
                    if($this->model_opendata->apbd2_check($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year)>0)
                    {
                        $this->model_opendata->apbd2_update($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year,$data_array);
                    }
                    else 
                    {
                        $this->model_opendata->apbd2_add($data_array);
                    }
                }
                
                echo '</pre>';
            }
                break;

                case '79': //dinas tenaga kerja 2018
            $inputFileName = './document/2018/9. dinas tenaga kerja.xls';

            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' .
                    $e->getMessage());
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            $i=0;
            // //dinkes
            for ($row = 26; $row <= $highestRow; $row++)
            {   $i++;
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                    null, true, false);
                    //$rowData['0']['1']
                
                echo '<pre>';
                if($rowData['0']['3']!='')
                {
                    $datas_array = array('Rekening'=>str_replace(' ','',$rowData['0']['3']),
                                    'Uraian'=>$rowData['0']['6'].$rowData['0']['7'].$rowData['0']['8'].$rowData['0']['9'].$rowData['0']['10'].$rowData['0']['11'].$rowData['0']['12'].$rowData['0']['13'].$rowData['0']['14'].$rowData['0']['15'],
                                    'Jumlah'=>($rowData['0']['16']!='') ? $rowData['0']['16'] : 0,
                                    'Dasar Hukum'=>$rowData['0']['20'].$rowData['0']['21'].$rowData['0']['22'].$rowData['0']['23']);

                    print_r($datas_array);

                    $lembaga_id    = '79';
                    $lembaga_kode  = '1.14.01';
                    $pemda_kode    = '1.02.01';
                    $wilayah_kode  = '3402';
                    $year          = '2018';
                    $data_array = array( 
                        'lembaga_id'    => $lembaga_id,  
                        'lembaga_kode'  => $lembaga_kode,
                        'pemda_kode'    => $pemda_kode,
                        'wilayah_kode'  => $wilayah_kode,
                        'tahun'      	=> $year,
                        'rekening'     	=> $datas_array['Rekening'], 
                        'uraian'     	=> $datas_array['Uraian'],  
                        'nominal'      	=> $datas_array['Jumlah'],
                        'keterangan'    => $datas_array['Dasar Hukum'],
                        'updated_at'	=> date('Y-m-d H:i:s')
                    );  
                    if($this->model_opendata->apbd2_check($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year)>0)
                    {
                        $this->model_opendata->apbd2_update($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year,$data_array);
                    }
                    else 
                    {
                        $this->model_opendata->apbd2_add($data_array);
                    }
                }
                
                echo '</pre>';
            }
                break;

                case '56': //dinas tenaga kerja 2018
            $inputFileName = './document/2018/16. dinas koperasi.xls';

            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' .
                    $e->getMessage());
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            $i=0;
            // //dinkes
            for ($row = 26; $row <= $highestRow; $row++)
            {   $i++;
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                    null, true, false);
                    //$rowData['0']['1']
                
                echo '<pre>';
                if($rowData['0']['3']!='')
                {
                    $datas_array = array('Rekening'=>str_replace(' ','',$rowData['0']['3']),
                                    'Uraian'=>$rowData['0']['6'].$rowData['0']['7'].$rowData['0']['8'].$rowData['0']['9'].$rowData['0']['10'].$rowData['0']['11'].$rowData['0']['12'].$rowData['0']['13'].$rowData['0']['14'].$rowData['0']['15'],
                                    'Jumlah'=>($rowData['0']['16']!='') ? $rowData['0']['16'] : 0,
                                    'Dasar Hukum'=>$rowData['0']['20'].$rowData['0']['21'].$rowData['0']['22'].$rowData['0']['23']);

                    print_r($datas_array);

                    $lembaga_id    = '56';
                    $lembaga_kode  = '1.15.01';
                    $pemda_kode    = '1.02.01';
                    $wilayah_kode  = '3402';
                    $year          = '2018';
                    $data_array = array( 
                        'lembaga_id'    => $lembaga_id,  
                        'lembaga_kode'  => $lembaga_kode,
                        'pemda_kode'    => $pemda_kode,
                        'wilayah_kode'  => $wilayah_kode,
                        'tahun'      	=> $year,
                        'rekening'     	=> $datas_array['Rekening'], 
                        'uraian'     	=> $datas_array['Uraian'],  
                        'nominal'      	=> $datas_array['Jumlah'],
                        'keterangan'    => $datas_array['Dasar Hukum'],
                        'updated_at'	=> date('Y-m-d H:i:s')
                    );  
                    if($this->model_opendata->apbd2_check($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year)>0)
                    {
                        $this->model_opendata->apbd2_update($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year,$data_array);
                    }
                    else 
                    {
                        $this->model_opendata->apbd2_add($data_array);
                    }
                }
                
                echo '</pre>';
            }
                break;

                case '52': //Dinas Kebudayaan 2018
            $inputFileName = './document/2018/18. dinas kebudayaan.xls';

            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' .
                    $e->getMessage());
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            $i=0;
            // //dinkes
            for ($row = 26; $row <= $highestRow; $row++)
            {   $i++;
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                    null, true, false);
                    //$rowData['0']['1']
                
                echo '<pre>';
                if($rowData['0']['3']!='')
                {
                    $datas_array = array('Rekening'=>str_replace(' ','',$rowData['0']['3']),
                                    'Uraian'=>$rowData['0']['6'].$rowData['0']['7'].$rowData['0']['8'].$rowData['0']['9'].$rowData['0']['10'].$rowData['0']['11'].$rowData['0']['12'].$rowData['0']['13'].$rowData['0']['14'].$rowData['0']['15'],
                                    'Jumlah'=>($rowData['0']['16']!='') ? $rowData['0']['16'] : 0,
                                    'Dasar Hukum'=>$rowData['0']['20'].$rowData['0']['21'].$rowData['0']['22'].$rowData['0']['23']);

                    print_r($datas_array);

                    $lembaga_id    = '52';
                    $lembaga_kode  = '1.16.01';
                    $pemda_kode    = '1.02.01';
                    $wilayah_kode  = '3402';
                    $year          = '2018';
                    $data_array = array( 
                        'lembaga_id'    => $lembaga_id,  
                        'lembaga_kode'  => $lembaga_kode,
                        'pemda_kode'    => $pemda_kode,
                        'wilayah_kode'  => $wilayah_kode,
                        'tahun'      	=> $year,
                        'rekening'     	=> $datas_array['Rekening'], 
                        'uraian'     	=> $datas_array['Uraian'],  
                        'nominal'      	=> $datas_array['Jumlah'],
                        'keterangan'    => $datas_array['Dasar Hukum'],
                        'updated_at'	=> date('Y-m-d H:i:s')
                    );  
                    if($this->model_opendata->apbd2_check($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year)>0)
                    {
                        $this->model_opendata->apbd2_update($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year,$data_array);
                    }
                    else 
                    {
                        $this->model_opendata->apbd2_add($data_array);
                    }
                }
                
                echo '</pre>';
            }
                break;

                case '224': //Dinas Pariwisata 2018
            $inputFileName = './document/2018/20. dinas pariwisata.xls';

            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' .
                    $e->getMessage());
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            $i=0;
            // //dinkes
            for ($row = 26; $row <= $highestRow; $row++)
            {   $i++;
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                    null, true, false);
                    //$rowData['0']['1']
                
                echo '<pre>';
                if($rowData['0']['3']!='')
                {
                    $datas_array = array('Rekening'=>str_replace(' ','',$rowData['0']['3']),
                                    'Uraian'=>$rowData['0']['6'].$rowData['0']['7'].$rowData['0']['8'].$rowData['0']['9'].$rowData['0']['10'].$rowData['0']['11'].$rowData['0']['12'].$rowData['0']['13'].$rowData['0']['14'].$rowData['0']['15'],
                                    'Jumlah'=>($rowData['0']['16']!='') ? $rowData['0']['16'] : 0,
                                    'Dasar Hukum'=>$rowData['0']['20'].$rowData['0']['21'].$rowData['0']['22'].$rowData['0']['23']);

                    print_r($datas_array);

                    $lembaga_id    = '224';
                    $lembaga_kode  = '1.16.01';
                    $pemda_kode    = '1.02.01';
                    $wilayah_kode  = '3402';
                    $year          = '2018';
                    $data_array = array( 
                        'lembaga_id'    => $lembaga_id,  
                        'lembaga_kode'  => $lembaga_kode,
                        'pemda_kode'    => $pemda_kode,
                        'wilayah_kode'  => $wilayah_kode,
                        'tahun'      	=> $year,
                        'rekening'     	=> $datas_array['Rekening'], 
                        'uraian'     	=> $datas_array['Uraian'],  
                        'nominal'      	=> $datas_array['Jumlah'],
                        'keterangan'    => $datas_array['Dasar Hukum'],
                        'updated_at'	=> date('Y-m-d H:i:s')
                    );  
                    if($this->model_opendata->apbd2_check($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year)>0)
                    {
                        $this->model_opendata->apbd2_update($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year,$data_array);
                    }
                    else 
                    {
                        $this->model_opendata->apbd2_add($data_array);
                    }
                }
                
                echo '</pre>';
            }
                break;

            //     case '56': //Dinas Perindustrian Perdagangan dan Koperasi 2018
            // $inputFileName = './document/2018/22. dinas perdagangan.xls';

            // try {
            //     $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            //     $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            //     $objPHPExcel = $objReader->load($inputFileName);
            // } catch (Exception $e) {
            //     die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' .
            //         $e->getMessage());
            // }

            // $sheet = $objPHPExcel->getSheet(0);
            // $highestRow = $sheet->getHighestRow();
            // $highestColumn = $sheet->getHighestColumn();

            // $i=0;
            // // //dinkes
            // for ($row = 26; $row <= $highestRow; $row++)
            // {   $i++;
            //     $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
            //         null, true, false);
            //         //$rowData['0']['1']
                
            //     echo '<pre>';
            //     if($rowData['0']['3']!='')
            //     {
            //         $datas_array = array('Rekening'=>str_replace(' ','',$rowData['0']['3']),
            //                         'Uraian'=>$rowData['0']['6'].$rowData['0']['7'].$rowData['0']['8'].$rowData['0']['9'].$rowData['0']['10'].$rowData['0']['11'].$rowData['0']['12'].$rowData['0']['13'].$rowData['0']['14'].$rowData['0']['15'],
            //                         'Jumlah'=>($rowData['0']['16']!='') ? $rowData['0']['16'] : 0,
            //                         'Dasar Hukum'=>$rowData['0']['20'].$rowData['0']['21'].$rowData['0']['22'].$rowData['0']['23']);

            //         print_r($datas_array);

            //         $lembaga_id    = '56';
            //         $lembaga_kode  = '1.15.01';
            //         $pemda_kode    = '1.02.01';
            //         $wilayah_kode  = '3402';
            //         $year          = '2018';
            //         $data_array = array( 
            //             'lembaga_id'    => $lembaga_id,  
            //             'lembaga_kode'  => $lembaga_kode,
            //             'pemda_kode'    => $pemda_kode,
            //             'wilayah_kode'  => $wilayah_kode,
            //             'tahun'      	=> $year,
            //             'rekening'     	=> $datas_array['Rekening'], 
            //             'uraian'     	=> $datas_array['Uraian'],  
            //             'nominal'      	=> $datas_array['Jumlah'],
            //             'keterangan'    => $datas_array['Dasar Hukum'],
            //             'updated_at'	=> date('Y-m-d H:i:s')
            //         );  
            //         if($this->model_opendata->apbd2_check($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year)>0)
            //         {
            //             $this->model_opendata->apbd2_update($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year,$data_array);
            //         }
            //         else 
            //         {
            //             $this->model_opendata->apbd2_add($data_array);
            //         }
            //     }
                
            //     echo '</pre>';
            // }
            //     break;

                case '56': //Dinas Perindustrian Perdagangan dan Koperasi 2018
            $inputFileName = './document/2018/22. dinas perdagangan.xls';

            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' .
                    $e->getMessage());
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            $i=0;
            // //dinkes
            for ($row = 26; $row <= $highestRow; $row++)
            {   $i++;
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                    null, true, false);
                    //$rowData['0']['1']
                
                echo '<pre>';
                if($rowData['0']['3']!='')
                {
                    $datas_array = array('Rekening'=>str_replace(' ','',$rowData['0']['3']),
                                    'Uraian'=>$rowData['0']['6'].$rowData['0']['7'].$rowData['0']['8'].$rowData['0']['9'].$rowData['0']['10'].$rowData['0']['11'].$rowData['0']['12'].$rowData['0']['13'].$rowData['0']['14'].$rowData['0']['15'],
                                    'Jumlah'=>($rowData['0']['16']!='') ? $rowData['0']['16'] : 0,
                                    'Dasar Hukum'=>$rowData['0']['20'].$rowData['0']['21'].$rowData['0']['22'].$rowData['0']['23']);

                    print_r($datas_array);

                    $lembaga_id    = '56';
                    $lembaga_kode  = '1.15.01';
                    $pemda_kode    = '1.02.01';
                    $wilayah_kode  = '3402';
                    $year          = '2018';
                    $data_array = array( 
                        'lembaga_id'    => $lembaga_id,  
                        'lembaga_kode'  => $lembaga_kode,
                        'pemda_kode'    => $pemda_kode,
                        'wilayah_kode'  => $wilayah_kode,
                        'tahun'      	=> $year,
                        'rekening'     	=> $datas_array['Rekening'], 
                        'uraian'     	=> $datas_array['Uraian'],  
                        'nominal'      	=> $datas_array['Jumlah'],
                        'keterangan'    => $datas_array['Dasar Hukum'],
                        'updated_at'	=> date('Y-m-d H:i:s')
                    );  
                    // if($this->model_opendata->apbd2_check($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year)>0)
                    // {
                    //     $this->model_opendata->apbd2_update($lembaga_id,$datas_array['Rekening'],$wilayah_kode,$year,$data_array);
                    // }
                    // else 
                    // {
                    //     $this->model_opendata->apbd2_add($data_array);
                    // }
                }
                
                echo '</pre>';
            }
                break;
            // default:

            // break;
        }
    }
    function olah_apbd2()
	{
		if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        /**
        * 
        * @var List Menu
        * 
        */
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);

        $data['list_lembaga']           = $this->model_opendata->lem_list('3402');
    
		$data['user'] = $this->session->userdata;
		$data["pageTitle"] = "Olah Data ".APP_TITLE;
		$data["boxTitle"] = "Pilih Data APBD";
        $data['hasil'] = false;
        
        $post_id                    = $this->uri->segment(4);
        $data['row']                = $this->model_opendata->tags_edit($post_id);

        $data['anggaran']           = $this->model_opendata->apbd2_anggaran_list();

        if(isset($_POST['Pixel_Save']))
        {
            $data['row_apbd']                = $this->model_opendata->apbds2_edit($this->input->post('apbd_id'));

            $tag_id             = $post_id;
            $kode_wilayah       = $data['row_apbd']->wilayah_kode;
            $lembaga_id         = $data['row_apbd']->id;
            $apbd_id            = $this->input->post('apbd_id');
            $tahun              = $this->input->post('year'); 

            $data_array = array(  
                'tag_id'         => $tag_id,   
                'kode_wilayah'   => $kode_wilayah,
                'lembaga_id'     => $lembaga_id,
                'apbd_id'        => $apbd_id,
                'tahun'          => $tahun,
            );  
            $this->model_opendata->tags_web_add($data_array);
            // $id 			= $this->db->insert_id();
            // print_r($data_array);
            setcookie('status','updated', time()+2);
            redirect(base_url().'panel/opendata/olah_apbd2/'.$post_id.'.html');   
            // redirect(base_url().'panel/opendata/olah_data_list.html');             
        }
        $this->load->view('olah_data_edit', $data);
    }
    function olah_apbd2_delete()
	{
		if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        
        $post_id                    = $this->uri->segment(4);
        $this->model_opendata->tags_web_delete($post_id);

        setcookie('status','updated', time()+2);
        redirect(base_url().'panel/opendata/olah_data_list.html');   
    }
    function olah_apbd()
	{
		if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        /**
        * 
        * @var List Menu
        * 
        */
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);

        $data['list_lembaga']           = $this->model_opendata->lem_list('3402');
    
		$data['user'] = $this->session->userdata;
		$data["pageTitle"] = "Olah Data ".APP_TITLE;
		$data["boxTitle"] = "Pilih Data APBD";
        $data['hasil'] = false;
        
        $varID = $this->uri->segment('4');
        $varThn= $this->uri->segment('5');

		$data['form_action'] = site_url('olahdata/apbd_go');
		
		if($varID > 0){
			
                $data['tahuns'] = $this->apbd_model->_apbd_tahun_by_lembaga($varID);
                if(($varThn == 0)){
                    $varThn = $data['tahuns'][0];
                }
                $data['tahun'] = $varThn;
                $data['org']		= $this->lembaga_model->lembaga_load($varID);
                $data['url']		= current_url();
                
                $data['varRek'] = (@$_REQUEST['r']) ? $_REQUEST['r']:0;
                
                $data['apbd'] = $this->apbd_model->apbd_by_rekening($varID,$varThn,$data['varRek']);
                $data["boxTitle"] = "Olah Data APBD ".$data['org']['nama']." - ".$data['org']['namawilayah'];
                
                $data['list_tags'] = $this->apbd_model->tags_load(0,true);
                $this->load->view('olahdata_lembaga',$data);	
        }
    }
}
