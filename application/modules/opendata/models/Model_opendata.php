<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

class model_opendata extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}
	function lem_edit($id)
	{
		$this->db->select('*');
		$this->db->from("tweb_lembaga");
		$this->db->where('id',$id);
		$result	= $this->db->get();
		return $result->row();
	}
	function lem_list($kodewilayah)
	{
		$this->db->select('*, tweb_lembaga.id as lembaga_id, tweb_lembaga.nama as nama_lembaga, tweb_wilayah.nama as nama_wilayah');
		$this->db->from("tweb_lembaga");
		$this->db->where('kodewilayah',$kodewilayah);
		$this->db->join('tweb_wilayah','tweb_lembaga.kodewilayah=tweb_wilayah.kode','left');
		$this->db->order_by('kode_org','ASC');
		$query	= $this->db->get();
		return $query->result();
	}
	function lem_update($id,$data_array)
	{
		$this->db->where('id', $id);
		$this->db->update('tweb_lembaga',$data_array);
		return true;
	}
	function lem_add($data_array)
	{
		$this->db->insert('tweb_lembaga',$data_array);
		return $this->db->insert_id();
	}
	function lem_delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tweb_lembaga');
		return true;
	}
	function tags_list()
	{
		$this->db->select('*');
		$this->db->from("tweb_tags");
		$this->db->order_by('created_at','DESC');
		$query	= $this->db->get();
		return $query->result();
	}
	function tags_edit($id)
	{
		$this->db->select('*');
		$this->db->from("tweb_tags");
		$this->db->where('id',$id);
		$result	= $this->db->get();
		return $result->row();
	}
	function tags_update($id,$data_array)
	{
		$this->db->where('id', $id);
		$this->db->update('tweb_tags',$data_array);
		return true;
	}
	function tags_add($data_array)
	{
		$this->db->insert('tweb_tags',$data_array);
		return $this->db->insert_id();
	}
	function tags_delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tweb_tags');
		return true;
	}
	function prov_list()
	{
		$this->db->select('*');
		$this->db->from("tweb_wilayah");
		$this->db->where('parent','0');
		$this->db->order_by('nama','ASC');
		$query	= $this->db->get();
		return $query->result();
	}
	function ajax_city_list($parent)
	{
		// $this->db->select('*');
		// $this->db->from("tweb_wilayah");
		// $this->db->where('parent',$parent);
		// $this->db->order_by('nama','ASC');
		// $query	= $this->db->get();
		// return $query->result();

		$kabupaten="<option value='0'>Pilih Kabupaten *</option>";

		$this->db->order_by('nama','ASC');
		$kab= $this->db->get_where('tweb_wilayah',array('parent'=>$parent));

		foreach ($kab->result_array() as $data )
		{
			$kabupaten.= "<option value='$data[id]'>".ucwords(strtolower($data['nama']))."</option>";
		}
		return $kabupaten;
	}
	function city_list($parent)
	{
		$this->db->select('*');
		$this->db->from("tweb_wilayah");
		$this->db->where('parent',$parent);
		$this->db->order_by('nama','ASC');
		$query	= $this->db->get();
		return $query->result();
	}
	//APBDS
	function apbds_edit($id)
	{
		$this->db->select('*');
		$this->db->from("tweb_apbds");
		$this->db->where('id',$id);
		$result	= $this->db->get();
		return $result->row();
	}
    function apbds_add($data_array)
	{
		$this->db->insert('tweb_apbds',$data_array);
		return $this->db->insert_id();
	}
	function apbds_rekening_update($rekening,$data_array)
	{
		$this->db->where('rekening', $rekening);
		$this->db->update('tweb_apbds',$data_array);
		return true;
	}
	function apbds_update($id,$data_array)
	{
		$this->db->where('id', $id);
		$this->db->update('tweb_apbds',$data_array);
		return true;
	}
	function apbds_delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tweb_apbds');
		return true;
	}
	function apbds_check($rekening,$uraian,$tahun)
	{
		$this->db->select('*');
		$this->db->from("tweb_apbds");
		$this->db->where('rekening',$rekening);
		$this->db->where('uraian',$uraian);
		$this->db->where('tahun',$tahun);
		return $this->db->count_all_results();
	}
	function apbd_list($kodewilayah)
    {
        $this->db->select('*');
        $this->db->from("tweb_lembaga");
        $this->db->where('kodewilayah',$kodewilayah);
        $query	= $this->db->get();
        return $query->result();
    }

	//APBDS2
	function apbd2_update($lembaga_id,$rekening,$wilayah_kode,$tahun,$data_array)
	{
		$this->db->where('lembaga_id',$lembaga_id);
		$this->db->where('rekening',$rekening);
		$this->db->where('wilayah_kode',$wilayah_kode);
		$this->db->where('tahun',$tahun);
		$this->db->update('tweb_apbd2',$data_array);
		return true;
	}
	function apbds2_edit($id)
	{
		$this->db->select('*');
		$this->db->from("tweb_apbd2");
		$this->db->where('id',$id);
		$result	= $this->db->get();
		return $result->row();
	}
	function apbd2_add($data_array)
	{
		$this->db->insert('tweb_apbd2',$data_array);
		return $this->db->insert_id();
	}
	function apbd2_check($lembaga_id,$rekening,$wilayah_kode,$tahun)
	{
		$this->db->select('lembaga_id, rekening, wilayah_kode, tahun');
		$this->db->from("tweb_apbd2");
		$this->db->where('lembaga_id',$lembaga_id);
		$this->db->where('rekening',$rekening);
		$this->db->where('wilayah_kode',$wilayah_kode);
		$this->db->where('tahun',$tahun);
		return $this->db->count_all_results();
	}
	function apbd2_list($tahun,$tag_id)
	{
		$this->db->select('*, tweb_apbd2.rekening as nrekening,tweb_apbd_tags.id as tags_id');
		$this->db->from("tweb_apbd2");
		$this->db->join('tweb_apbd_tags','tweb_apbd2.id=tweb_apbd_tags.apbd_id');
		$this->db->where('tweb_apbd_tags.tahun',$tahun);
		$this->db->where('tweb_apbd_tags.tag_id',$tag_id);
		$query	= $this->db->get();
        return $query->result();
	}
	function apbd2_anggaran_list()
	{
		$this->db->select('*, tweb_apbd2.id as tweb_apbd2_id');
		$this->db->from("tweb_apbd2");//(LENGTH(akuntansi) = 4)
		// $this->db->where(LENGTH(akuntansi) = 4)));
		$this->db->join('tweb_lembaga','tweb_apbd2.lembaga_id=tweb_lembaga.id');
		// $this->db->where('LENGTH(akuntansi) <', 4, FALSE);
		$query	= $this->db->get();
        return $query->result();
	}
	
	function me_cat($start,$limit)
	{
		$this->db->select('meta_id, meta_type, meta_source, meta_dest, meta_title, meta_slug, meta_date');
		$this->db->from("my_meta");
		$this->db->limit($start,$limit);
		$this->db->order_by('meta_date','desc');
		$query	= $this->db->get();
		return $query->result();
	}
	function me_cat_count()
	{
		$this->db->select('meta_id, meta_type, meta_source, meta_dest, meta_title, meta_slug, meta_date');
		$this->db->from("my_meta");
		$this->db->order_by('meta_date','desc');
		return $this->db->count_all_results();
	}
	function me_cat_type($meta_type,$start,$limit)
	{
		$this->db->select('meta_id, meta_type, meta_source, meta_dest, meta_title, meta_slug, meta_date');
		$this->db->from("my_meta");
		$this->db->where('my_meta.meta_type',$meta_type);
		$this->db->limit($start,$limit);
		$this->db->order_by('meta_date','desc');
		$query	= $this->db->get();
		return $query->result();
	}
	function me_cat_type_count($meta_type)
	{
		$this->db->select('meta_id, meta_type, meta_source, meta_dest, meta_title, meta_slug, meta_date');
		$this->db->from("my_meta");
		$this->db->where('my_meta.meta_type',$meta_type);
		$this->db->order_by('meta_date','desc');
		return $this->db->count_all_results();
	}
	function me_search($meta_type,$cat,$start,$limit)
	{
		$this->db->select('meta_id, meta_type, meta_source, meta_dest, meta_title, meta_slug, meta_date');
		$this->db->from("my_meta");
		$this->db->group_start();
		$this->db->like('meta_title',$cat);
		$this->db->or_like('meta_slug',$cat);
		$this->db->group_end();
		$this->db->where('meta_type',$meta_type);
		$this->db->limit($start,$limit);
		$this->db->order_by('date','desc');
		$query	= $this->db->get();
		return $query->result();
	}
	function me_search_count($meta_type,$cat)
	{
		$this->db->select('meta_id, meta_type, meta_source, meta_dest, meta_title, meta_slug, meta_date');
		$this->db->from("my_meta");
		$this->db->group_start();
		$this->db->like('meta_title',$cat);
		$this->db->or_like('meta_slug',$cat);
		$this->db->group_end();
		$this->db->where('meta_type',$meta_type);
		$this->db->order_by('date','desc');
		return $this->db->count_all_results();
	}
	function me_source_check($meta_type,$meta_source,$meta_dest)
	{
		$this->db->select('meta_type, meta_source');
		$this->db->from("my_meta");
		$this->db->where('meta_type',$meta_type);
		$this->db->where('meta_source',$meta_source);
		$this->db->where('meta_dest',$meta_dest);
		return $this->db->count_all_results();
	}
	function me_source_title_check($meta_type,$meta_source,$meta_title)
	{
		$this->db->select('meta_type, meta_source');
		$this->db->from("my_meta");
		$this->db->where('meta_type',$meta_type);
		$this->db->where('meta_source',$meta_source);
		$this->db->where('meta_title',$meta_title);
		$this->db->where('meta_status','1');
		return $this->db->count_all_results();
	}
	function me_add($data_array)
	{
		$this->db->insert('my_meta',$data_array);
		return $this->db->insert_id();
	}
	function me_update($meta_id,$data_array)
	{
		$this->db->where('meta_id', $meta_id);
		$this->db->update('my_meta',$data_array);
		return true;
	}
	function me_update_status($meta_type,$meta_source,$meta_dest,$meta_status)
	{
		$this->db->set('meta_status', $meta_status); 
		$this->db->where('meta_type', $meta_type);
		$this->db->where('meta_source', $meta_source);
		$this->db->where('meta_dest', $meta_dest);
		$this->db->update('my_meta');
		return true;
	}
	function me_delete($meta_id)
	{
		$this->db->where('meta_id',$meta_id);
		$this->db->delete('my_meta');
		return true;
	}
	function me_delete_source($meta_source)
	{
		$this->db->where('meta_source',$meta_source);
		$this->db->delete('my_meta');
		return true;
	}
	function me_delete_dest($meta_dest)
	{
		$this->db->where('meta_dest',$meta_dest);
		$this->db->delete('my_meta');
		return true;
	}
	//Tags
	function tags_web_add($data_array)
	{
		$this->db->insert('tweb_apbd_tags',$data_array);
		return $this->db->insert_id();
	}
	function tags_web_update($id,$data_array)
	{
		$this->db->where('id', $id);
		$this->db->update('tweb_apbd_tags',$data_array);
		return true;
	}
	function tags_web_delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tweb_apbd_tags');
		return true;
	}
}