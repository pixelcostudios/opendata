<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//require APPPATH . "third_party/MX/Controller.php";

	function __construct()
	{
		parent::__construct();
		// $this->load->library('ion_auth');
        $this->load->library('email');
        // $this->load->library('pdf');
        $this->load->helper(array('form'));
        $this->load->helper(array('url'));
		$this->load->helper(array('path'));
		$this->load->library("pagination");
		$this->load->model("menu/model_nav");
		$this->load->model("post/model_post");
		$this->load->model("post/model_ipost");
		$this->load->model("categories/model_categories");
		$this->load->model("opendata/model_opendata");
	}
	public function index()
	{
		echo 'Not Found';//echo $this->uri->segment(1);
		//$this->load->view('categories');
	}
	public function post_delete() 
	{
		$html = '<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Are you sure to Delete ?</h4>

				</div>
				<div class="modal-body">
				<p>Do you want to delete this Post.</p>
				</div>
				<div class="modal-footer">
				<input type="hidden" name="post_id" value="'.$this->uri->segment(4).'">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" name="post-delete-ajax" value="ajax" class="btn btn-danger">Delete</button>
				</div>
		</div>';
		echo $html;
	}
	public function pages_delete() 
	{
		$html = '<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Are you sure to Delete ?</h4>

				</div>
				<div class="modal-body">
				<p>Do you want to delete this Post.</p>
				</div>
				<div class="modal-footer">
				<input type="hidden" name="post_id" value="'.$this->uri->segment(4).'">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" name="post-delete-ajax" value="ajax" class="btn btn-danger">Delete</button>
				</div>
		</div>';
		echo $html;
	}
	public function slideshow_delete() 
	{
		$html = '<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Are you sure to Delete ?</h4>

				</div>
				<div class="modal-body">
				<p>Do you want to delete this Post.</p>
				</div>
				<div class="modal-footer">
				<input type="hidden" name="post_id" value="'.$this->uri->segment(4).'">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" name="post-delete-ajax" value="ajax" class="btn btn-danger">Delete</button>
				</div>
		</div>';
		echo $html;
	}
	public function hotel_delete() 
	{
		$html = '<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Are you sure to Delete ?</h4>

				</div>
				<div class="modal-body">
				<p>Do you want to delete this Post.</p>
				</div>
				<div class="modal-footer">
				<input type="hidden" name="post_id" value="'.$this->uri->segment(4).'">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" name="post-delete-ajax" value="ajax" class="btn btn-danger">Delete</button>
				</div>
		</div>';
		echo $html;
	}
	public function location()
	{
		$modul    = $this->input->post('modul');
        $id       = $this->input->post('id');
        //$subdistrict=$this->input->post('subdistrict');

        switch ($modul) 
        {
            case 'kabupaten':
            echo $this->model_opendata->ajax_city_list($id);
            break;

            case 'kecamatan':
            echo $this->model_opendata->ajax_city_list($id);
            break;
        }
	}
}
