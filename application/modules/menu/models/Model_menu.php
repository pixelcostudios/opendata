<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

class model_menu extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}
	function me_list()
	{
		$this->db->select('id, type, position, label, link, parent, sort');
		$this->db->from("my_menu");
		$this->db->order_by('sort','ASC');
		$query	= $this->db->get();
		return $query->result();
	}
	function me_list_position($position)
	{
		$this->db->select('id, type, position, label, link, parent, sort');
		$this->db->from("my_menu");
		$this->db->where("position",$position);
		$this->db->order_by('sort','ASC');
		$query	= $this->db->get();
		return $query->result();
	}
	function me_parent_count($parent)
	{
		$this->db->select('id, label, link, parent, sort');
		$this->db->from("my_menu");
		$this->db->where('parent',$parent);
		return $this->db->count_all_results();
	}
	function me_add($data_array)
	{
		$this->db->insert('my_menu',$data_array);
		return $this->db->insert_id();
	}
	function me_update($id,$data_array)
	{
		$this->db->where('id', $id);
		$this->db->update('my_menu',$data_array);
		return true;
	}
	// function me_update_status($id,$status)
	// {
	// 	$this->db->set('show_menu', $status); 
	// 	$this->db->where('id', $id);
	// 	$this->db->update('my_menu');
	// 	return true;
	// }
	function me_delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('my_menu');
		return true;
	}
	function me_delete_parent($parent)
	{
		$this->db->where('parent',$parent);
		$this->db->delete('my_menu');
		return true;
	}
	function me_ca_list_in($cat_type,$cat_id)
	{
		$this->db->select('cat_id, cat_type, cat_up, cat_title, cat_slug, cat_summary, cat_key, cat_desc, cat_date, cat_status');
		$this->db->from("my_cat");
		$this->db->where('cat_type',$cat_type);
		$this->db->where_in('cat_id', $cat_id);
		$query	= $this->db->get();
		return $query->result();
	}
	function me_pg_list_in($post_type,$post_id)
	{
		$this->db->select('post_id, user_id, cat_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_key, post_desc, post_comment, date, status');
		$this->db->from("my_post");
		$this->db->where('post_type',$post_type);
		$this->db->where_in('post_id',$post_id);
		$query	= $this->db->get();
		return $query->result();
	}
	function parseJsonArray($jsonArray, $parent = 0) 
	{
		$return = array();
		foreach ($jsonArray as $subArray) 
		{
			$returnSubSubArray = array();
			if (isset($subArray->children)) 
			{
				$returnSubSubArray = $this->model_menu->parseJsonArray($subArray->children, $subArray->id);
			}

			$return[] 	= array('id' => $subArray->id, 'parent' => $parent);
			$return 	= array_merge($return, $returnSubSubArray);
		}
		return $return;
	}
	function get_menu($items,$class = 'dd-list') 
	{
	    $html = "<ol class=\"".$class."\" id=\"menu-id\">";

	    foreach($items as $key=>$value) {
	        $html.= '<li class="dd-item dd3-item" data-id="'.$value['id'].'" >
	                    <div class="dd-handle dd3-handle">Drag</div>
	                    <div class="dd3-content"><span id="label_show'.$value['id'].'">'.$value['label'].' | '.$value['type'].'</span> 
	                        <span class="span-right"><span id="link_show'.$value['id'].'">'.$value['link'].'</span> &nbsp;&nbsp; 
	                            <a class="edit-button" id="'.$value['id'].'" position="'.$value['position'].'" label="'.$value['label'].'" link="'.$value['link'].'" ><i class="fa fa-pencil"></i></a>
	                            <a class="del-button" id="'.$value['id'].'"><i class="fa fa-trash"></i></a></span> 
	                    </div>';
	        if(array_key_exists('child',$value)) {
	            $html .= $this->model_menu->get_menu($value['child'],'child');
	        }
	            $html .= "</li>";
	    }
	    $html .= "</ol>";

	    return $html;
	}
}