<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

class model_nav extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}
	/**
	* 
	* @param Post
	* 
	* @return
	*/
	function po_list_count($post_type)
	{
		$this->db->select('post_id, user_id, cat_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_key, post_desc, post_comment, date, status');
		$this->db->from("my_post");
		$this->db->where('post_type',$post_type);
		return $this->db->count_all_results();
	}
	/**
	* 
	* @param Category
	* 
	* @return
	*/
	function ca_list($cat_type,$cat_up)
	{
		$this->db->select('cat_id, cat_type, cat_up, cat_title, cat_slug, cat_summary, cat_key, cat_desc, cat_date, cat_status');
		$this->db->from("my_cat");
		$this->db->where('cat_type',$cat_type);
        $this->db->where('cat_up',$cat_up);
		$this->db->where('cat_status','1');
		$query	= $this->db->get();
		return $query->result();
    }
    function ca_type_list()
	{
		$this->db->select('cat_id, cat_type, cat_up, cat_title, cat_slug, cat_summary, cat_key, cat_desc, cat_date, cat_status');
		$this->db->from("my_cat");
        $this->db->where('cat_status','1');
        $this->db->group_by('cat_type');
		$query	= $this->db->get();
		return $query->result();
	}
	/**
	* 
	* @param Pages
	* 
	* @return
	*/
	function pg_list($post_type,$post_up)
	{
		$this->db->select('post_id, user_id, cat_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_key, post_desc, post_comment, date, status');
		$this->db->from("my_post");
		$this->db->where('post_type',$post_type);
		$this->db->where('post_up',$post_up);
		$query	= $this->db->get();
		return $query->result();
	}
	function pg_list_count($post_type,$post_up)
	{
		$this->db->select('post_id, user_id, cat_id, post_type, post_up, post_slug, post_title, post_content, post_summary, post_key, post_desc, post_comment, date, status');
		$this->db->from("my_post");
		$this->db->where('post_type',$post_type);
		$this->db->where('post_up',$post_up);
		return $this->db->count_all_results();
	}
	/**
	* 
	* @param Banner
	* 
	* @return
	*/
	function ba_count($ipost_all,$ipost_type)
	{
		$this->db->select('ipost_all, ipost_type');
		$this->db->from("my_ipost");
		$this->db->where('ipost_all',$ipost_all);
		$this->db->where('ipost_type',$ipost_type);
		return $this->db->count_all_results();
	}
	/**
	* 
	* @param Member
	* 
	* @return
	*/
	function us_list_role()
	{
		$this->db->select('role_id, role_name, role_setting, role_description');
		$this->db->from("my_user_role");
		$query	= $this->db->get();
		return $query->result();
	}
	function us_list_count()
	{
		$this->db->select('user_id, user_role, user_active');
		$this->db->from("my_user");
		return $this->db->count_all_results();
	}
    function us_list_type_count($role_name)
    {
        $this->db->select('my_user.user_id');
        $this->db->from("my_user");
        $this->db->join('my_user_group', 'my_user.user_id = my_user_group.user_id');
        $this->db->join('my_user_role', 'my_user_role.role_id = my_user_group.role_id');
        $this->db->where('my_user_role.role_name',$role_name);
        return $this->db->count_all_results();
    }
	function us_profile($user_id)
	{
		$this->db->select('user_id, user_name, user_password, user_email, user_avatar, user_gender, user_first_name, user_last_name, user_profession, user_join, user_activity');
		$this->db->from("my_user");
		$this->db->where('user_id',$user_id);
		$result	= $this->db->get();
		return $result->row();
	}
    /**
     *
     * @param Comment
     *
     * @return
     */
    function co_list($c_type,$limit)
    {
        $this->db->select('c_id, user_id, post_id, c_type, c_division, c_link, c_title, c_name, c_email, c_content, c_reply, date, date_reply, status');
        $this->db->from("my_cmt");
        $this->db->where('c_type',$c_type);
        $this->db->where('status','0');
        $this->db->limit($limit);
        $this->db->order_by('date','desc');
        $query	= $this->db->get();
        return $query->result();
    }
    function co_type_count($c_type)
    {
        $this->db->select('c_id');
        $this->db->from("my_cmt");
        $this->db->where('c_type',$c_type);
        $this->db->where('status','0');
        return $this->db->count_all_results();
    }
    /**
     *
     * @param Contact
     *
     * @return
     */
    function co_division($setting_type)
    {
        $this->db->select('setting_name, setting_type, setting_value, setting_explain');
        $this->db->from("my_setting");
        $this->db->where('setting_type',$setting_type);
        $this->db->order_by('setting_name','ASC');
        $query	= $this->db->get();
        return $query->result();
    }
    /**
     *
     * @param Setting
     *
     * @return
     */
     function st_list($setting_type)
    {
        $this->db->select('setting_name, setting_type, setting_value, setting_explain, setting_status');
        $this->db->from("my_setting");
        $this->db->where('setting_type',$setting_type);
        $this->db->order_by('setting_name','ASC');
        $query	= $this->db->get();
        return $query->result();
    }
    /**
     *
     * @param Custom
     *
     * @return
     */
    function disk_size()
    {
        $bytes      = disk_free_space(".");
        $si_prefix  = array( 'B', 'KB', 'MB', 'GB', 'TB', 'EB', 'ZB', 'YB' );
        $base       = 1024;
        $class      = min((int)log($bytes , $base) , count($si_prefix) - 1);
        //echo $bytes;
        $result     = sprintf('%1.2f' , $bytes / pow($base,$class)) . ' ' . $si_prefix[$class] . '';
        return $result;
    }
    function time_elapsed($datetime, $full = false)
    {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
}