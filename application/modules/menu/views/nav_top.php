<div class="row border-bottom">
<nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
<div class="navbar-header">
    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
    <form role="search" class="navbar-form-custom" action="search_results.html">
        <div class="form-group">
            <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
        </div>
    </form>
</div>
    <ul class="nav navbar-top-links navbar-right">
        <li>
            <span class="m-r-sm text-muted welcome-message">Welcome to Pixel Interface v6</span>
        </li>
        <?php /* //if($this->model_setting->setting_value('dashboard_contact','setting_status')=='1'){?>
        <li class="dropdown">
            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                <i class="fa fa-envelope"></i>  <span class="label label-warning"><?php echo $this->model_nav->co_type_count('comment');?></span>
            </a>
            <ul class="dropdown-menu dropdown-messages">
                <?php $comment_list = $this->model_nav->co_list('comment','3'); foreach($comment_list as $row_comment){?>
                <li>
                    <div class="dropdown-messages-box">
                        <a href="<?php echo base_url()?>panel/comment_edit/<?php echo $row_comment->c_id;?>.html" class="pull-left">
                            <img alt="image" src="<?php echo base_url()?>themes/default/img/comment-64x64.png">
                        </a>
                        <div>
                            <small class="pull-right"><?php echo $this->model_nav->time_elapsed($row_comment->date, $full = false);?></small>
                            <strong><?php echo $row_comment->c_name?></strong> with <strong><?php echo $row_comment->c_title;?></strong>. <br>
                            <small class="text-muted"><?php echo $row_comment->date;?></small>
                        </div>
                    </div>
                </li>
                <li class="divider"></li>
                <?php }?>
                <li>
                    <div class="text-center link-block">
                        <a href="<?php echo base_url()?>panel/comment.html">
                            <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                        </a>
                    </div>
                </li>
            </ul>
        </li>
        <?php //}?>
        <?php //if($this->model_setting->setting_value('dashboard_alert','setting_status')=='1'){?>
        <li class="dropdown">
            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>
            </a>
            <ul class="dropdown-menu dropdown-alerts">
                <li>
                    <a href="mailbox.html">
                        <div>
                            <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                            <span class="pull-right text-muted small">4 minutes ago</span>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="profile.html">
                        <div>
                            <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                            <span class="pull-right text-muted small">12 minutes ago</span>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="grid_options.html">
                        <div>
                            <i class="fa fa-upload fa-fw"></i> Server Rebooted
                            <span class="pull-right text-muted small">4 minutes ago</span>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <div class="text-center link-block">
                        <a href="notifications.html">
                            <strong>See All Alerts</strong>
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </li>
            </ul>
        </li>
		<?php //}*/?>
		<li><a href="<?php echo base_url()?>" target="_blank"><i class="fa fa-external-link"></i> Visit Site</a></li>
        <li>
            <a href="<?php echo base_url()?>panel/member/logout.html">
                <i class="fa fa-sign-out"></i> Log out
            </a>
        </li>
        <!--<li>
            <a class="right-sidebar-toggle">
                <i class="fa fa-tasks"></i>
            </a>
        </li>-->
    </ul>

</nav>
</div>