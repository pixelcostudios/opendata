<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                        <img alt="image" class="img-circle" src="<?php echo base_url()?>upload/avatar/<?php echo $row_user->user_avatar?>" width="80" />
                         </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo $row_user->user_gender?>. <?php echo $row_user->user_first_name?> <?php echo $row_user->user_last_name?></strong>
                         </span> <span class="text-muted text-xs block"><?php echo $row_user->user_profession?> <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="<?php echo base_url()?>panel/member/profile.html">Profile</a></li>
                        <!--<li><a href="<?php echo base_url()?>panel/contact.html">Contacts</a></li>-->
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url()?>panel/member/logout.html">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    PX+
                </div>
            </li>
            <li <?php if($this->uri->segment(2)=='dashboard'){echo 'class="active"';}?>><a href="<?php echo base_url()?>panel/dashboard.html"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a></li>
            <?php if($this->ion_auth->in_group('admin')){?>
            <?php //if($this->model_setting->setting_value('post_menu','setting_status')=='1'){?>
            <li <?php if($this->uri->segment(2)=='post' OR $this->uri->segment(4)=='post'){echo 'class="active"';}?>>
                <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">Artikel</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="<?php echo base_url()?>panel/post/manage.html">Kelola Artikel<span class="pull-right label label-primary"><?php //echo $list_post_count;?></span></a> </li>
                    <li><a href="<?php echo base_url()?>panel/categories/type/post/10/1.html">Kategori</a></li>
                    <?php $category_list = $this->model_nav->ca_list('post','0'); foreach($category_list as $row_cat){?>
                        <?php $cat_sub_list = $this->model_nav->ca_list($row_cat->cat_type,$row_cat->cat_id); if(count($cat_sub_list)>0){?>
                        <li>
                            <a href="<?php echo base_url()?>panel/post/manage/<?php echo $row_cat->cat_id;?>/10/1.html"><?php echo $row_cat->cat_title;?><span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <?php foreach($cat_sub_list as $row_cat_sub){?>
                                    <?php $cat_sub_list1 = $this->model_nav->ca_list($row_cat_sub->cat_type,$row_cat_sub->cat_id); if(count($cat_sub_list1)>0){?>
                                        <li>
                                        <a href="<?php echo base_url()?>panel/post/manage/<?php echo $row_cat_sub->cat_id;?>/10/1.html"><?php echo $row_cat_sub->cat_title;?><span class="fa arrow"></span></a>
                                            <ul class="nav nav-fourth-level">
                                            <?php foreach($cat_sub_list1 as $row_cat_sub1){?>
                                                <li><a href="<?php echo base_url()?>panel/post/<?php echo $row_cat_sub1->cat_type;?>/<?php echo $row_cat_sub1->cat_id;?>/10/1.html"><?php echo $row_cat_sub1->cat_title;?></a></li>
                                            <?php }?>
                                            </ul>
                                        </li>
                                    <?php }else{?>
                                    <li><a href="<?php echo base_url()?>panel/post/manage/<?php echo $row_cat_sub->cat_id;?>/10/1.html"><?php echo $row_cat_sub->cat_title;?></a></li>
                                    <?php }?>
                                <?php }?>
                            </ul>
                        </li>
                        <?php }else{?>
                        <li><a href="<?php echo base_url()?>panel/post/manage/<?php echo $row_cat->cat_id;?>/10/1.html"><?php echo $row_cat->cat_title;?> <!--<span class="pull-right label label-primary">3</span>--></a></li>
                        <?php }?>
                    <?php }?>
                </ul>
            </li>
            <?php //}?>
            <?php //if($this->model_setting->setting_value('pages_menu','setting_status')=='1'){?>
            <li <?php if($this->uri->segment(2)=='pages'){echo 'class="active"';}?>>
                <a href="#"><i class="fa fa-book"></i> <span class="nav-label">Halaman</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="<?php echo base_url()?>panel/pages.html">Kelola Halaman <span class="pull-right label label-primary"><?php echo $list_pages_count;?></span></a></li>
                    <?php foreach($list_pages as $row_m_pages){?>
                    <li><a href="<?php echo base_url()?>panel/pages/edit/<?php echo $row_m_pages->post_id;?>.html"><?php echo $row_m_pages->post_title;?></a></li>
                    <?php }?>
                </ul>
            </li>
            <?php //}?>
            <li <?php if($this->uri->segment(2)=='member'){echo 'class="active"';}?>>
                <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Member</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="<?php echo base_url()?>panel/member/manage.html">Manage Member <span class="pull-right label label-primary"><?php //echo $list_member_count;?></span></a></li>
                    <li><a href="<?php echo base_url()?>panel/member/role.html">Member Role</a></li>
                    <li <?php if($this->uri->segment(3)=='type'){echo 'class="active"';}?>>
                        <a href="#">Member Group<span class="fa arrow"></span></a>
                        <ul class="nav nav-fourth-level">
                            <?php foreach($list_member_role as $row_group){?>
		                    <li><a href="<?php echo base_url()?>panel/member/type/<?php echo $row_group->role_name;?>/10/1.html"><?php echo $row_group->role_description;?><!--<span class="pull-right label label-primary">3</span>--></a></li>
		                    <?php }?>
                        </ul>
                    </li>
                </ul>
            </li>
            <?php //}?>
            <?php //if($this->model_setting->setting_value('setting_menu','setting_status')=='1'){?>
            <li <?php if($this->uri->segment(2)=='setting'){echo 'class="active"';}?>>
                <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">Pengaturan</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                	<?php $setting_list = $this->model_nav->st_list('menu_setting'); foreach($setting_list as $row_set){ if($row_set->setting_status=='1' AND $row_set->setting_name!='setting_menu'){?>
                    <li>
		                <a href="<?php echo base_url()?>panel/setting/<?php echo str_replace('setting_','',$row_set->setting_name);?>.html">
		                    <span class="menu-text"><?php echo $row_set->setting_value;?></span>
		                </a>
		            </li>
		            <?php }}?>
                </ul>
            </li>
            <?php //}?>
            <li <?php if($this->uri->segment(2)=='opendata'){echo 'class="active"';}?>>
                <a href="#"><i class="fa fa-map-marker"></i> <span class="nav-label">Open Data</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="<?php echo base_url()?>panel/opendata/lembaga.html">Daftar Lembaga </a></li>
                    <!--<li><a href="<?php echo base_url()?>panel/opendata/rekening.html">Data Rekening </a></li>-->
                    <li><a href="<?php echo base_url()?>panel/opendata/olah_data_list.html">Olah Data </a></li>
                    <li><a href="<?php echo base_url()?>panel/opendata/upload_data.html">Upload Data </a></li>
                </ul>
            </li>
            <?php }elseif($this->ion_auth->in_group('level-1')){?>
            <li <?php if($this->uri->segment(2)=='opendata'){echo 'class="active"';}?>>
                <a href="#"><i class="fa fa-map-marker"></i> <span class="nav-label">Open Data</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="<?php echo base_url()?>panel/opendata/lembaga.html">Daftar Lembaga </a></li>
                    <li><a href="<?php echo base_url()?>panel/opendata/rekening.html">Data Rekening </a></li>
                    <li><a href="<?php echo base_url()?>panel/opendata/olah_data_list.html">Olah Data </a></li>
                    <li><a href="<?php echo base_url()?>panel/opendata/upload_data.html">Upload Data </a></li>
                </ul>
            </li>
            <?php }?>
        </ul>
    </div>
</nav>