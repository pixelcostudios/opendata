<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forgot extends MX_Controller {
	function __construct()
    {
        parent::__construct();
		$this->load->library('ion_auth');
        $this->load->helper(array('form', 'url', 'string'));
        $this->load->helper(array('path'));
        $this->load->model("member/model_member");
        $this->load->model("setting/model_setting");
    }
	public function index()
	{
		$this->load->library("mail_template");

        $this->session->set_flashdata('auth_status', 'Enter your email address and your password will be reset and emailed to you.');

        if(isset($_POST['Pixel_Forgot']))
		{
            $this->form_validation->set_rules('email', 'Email Address', 'required');
            if ($this->form_validation->run() == false) 
            {
                 $this->session->set_flashdata('auth_message', 'Error Forgot Password, Please try Again.');
                 redirect(base_url().'panel/forgot.html','refresh');
            }
            else 
            {
                $row_user                         = $this->model_user->us_edit_username($this->input->post('email')); 

                $subject                          = $this->model_setting->setting('c1_company').' Forgot Password';
                $from                             = $this->model_setting->setting('c1_email_1');
                $user_fullname                    = $row_user->user_first_name.' '.$row_user->user_last_name;
                $user_email                       = $row_user->user_email;
                $user_forgotten_password_code     = $this->model_setting->char_random('20');
                $themes                           = $this->config->item('themes');

                $data_array = array(
                    'user_forgotten_password_code'=> $user_forgotten_password_code
                );
                $this->model_user->us_update($row_user->user_id, $data_array);
                
                $data_array = array(
                  'subject'                       => $subject,
                  'user_fullname'                 => $user_fullname,
                  'user_email'                    => $user_email,
                  'user_forgotten_password_code'  => $user_forgotten_password_code,
                  'themes'                        => $this->config->item('themes'),
                  'facebook'                      => $this->model_setting->setting('facebook'),
                  'google_plus'                   => $this->model_setting->setting('google_plus'),
                  'twitter'                       => $this->model_setting->setting('twitter'),
                  'instagram'                     => $this->model_setting->setting('instagram'),
                  'powered'                       => $this->model_setting->setting('c1_company'),
                  'company'                       => $this->model_setting->setting('c1_company')
                );

                $body   = $this->mail_template->mail_forgot($data_array);

                $result = $this->email
                ->from($from,"Leona Skin Care")
                ->reply_to($from,"Leona Skin Care")
                ->to($user_email)        
                ->subject($subject)
                ->message($body)
                ->send();
                
                $this->session->set_flashdata('auth_message', 'Your Request Successfully, Please Check Your Email.');
                redirect(base_url().'panel/forgot.html','refresh');
            }
		}
		$this->load->view('forgot');
	}
}
