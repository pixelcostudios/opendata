<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Institusi extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('form'));
        $this->load->helper(array('url'));
		$this->load->helper(array('path'));

		$this->load->model('front/siteman_model');
		$this->load->model('front/lembaga_model');
		$this->load->model('front/publik_model');
		$this->load->model('front/post_model');
		$this->load->model('front/apbd_model');
		$this->load->model('front/wilayah_model');
	}

  function opd($varID = '',$varUrl=''){
		
		$this->load->model('wilayah_model');
		
		if($varID  > 0){
			$data['lembaga'] = $this->publik_model->lembaga_load($varID);

			if($data['lembaga']){
				// $data['tahuns'] = $this->apbd_model->_apbd_tahun_by_lembaga($varID);
				$data['tahuns'] = $this->apbd_model->_apbd_tahun_by_lembaga('33');
				
				$varRek = (@$_REQUEST['r']) ? $_REQUEST['r']:0;
				$varThn = (@$_REQUEST['y']) ? $_REQUEST['y']:0;

				$data['tahun'] = $varThn;

				
				$data['org'] = $this->publik_model->lembaga_list($data['lembaga']['kodewilayah']);
				
				$data['wilayah'] = $this->wilayah_model->get_wilayah($data['lembaga']['kodewilayah']);
				$data['pageTitle'] = $data['lembaga']['nama'] ." - ". $data['lembaga']['namawilayah'];
				$data['page'] = array(
					'title'=>'APBD '. $data['lembaga']['nama'] ." - ". $data['lembaga']['namawilayah'] ." - ". $varThn,
					'desc' =>'Deskripsi',
					'image' => base_url('assets/img/logo.png'),
						);
				$data['info'] = $this->siteman_model->configs();	
				$data['apbd'] = $this->apbd_model->apbd_load($varID,$varThn);
				$data['rekening'] = $this->apbd_model->nama_rekening($varRek,$varID);
				
				if($varRek){
					$data['rekenin_title'] = "Detail Rekening <strong>".$data['rekening']."</strong> (dlm juta rupiah)";
					$data['summary'] = $this->apbd_model->apbd_lembaga_by_rek($varID,$varRek,$data['tahuns']);
					$data['pie'] = true;
				}else{
					// echo "Tidak Ada rek";
					$data['pie'] = false;
					$data['rekenin_title'] = "Rangkuman APBD (dlm juta rupiah)";
					$data['summary'] = $this->apbd_model->apbd_lembaga_home($varID,$data['tahuns']);
					
					
				}

				// print_r($data['summary']);

				$data['programs'] = $this->publik_model->program_nangkis_by_org($varID,$varThn);
				$data['program'] = $this->publik_model->program_nangkis_by_org($varID,$varThn);
				$data['kegiatan'] = $this->publik_model->kegiatan_nangkis_by_org($varID,$varThn);
				$data['posts'] = $this->publik_model->post_by_org($varID);

				$this->load->view('front/pubs/opd',$data);

			}else{
				show_404();
			}
		}else{
			
		}
		
	}
	
}
