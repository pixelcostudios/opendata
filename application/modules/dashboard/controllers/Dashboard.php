<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {
	function __construct()
    {
        parent::__construct();
		$this->load->library('ion_auth');
        $this->load->helper(array('form', 'url', 'string'));
		$this->load->helper(array('path'));
    }
	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
        $this->load->model("menu/model_nav");
		// $this->load->model("back/model_pos");
		
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['list_admin_count']       = $this->model_nav->us_list_type_count('admin');
        $data['list_mobile_count'] 	    = $this->model_nav->us_list_type_count('mobile');
        $data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);
		
		$this->load->view('dashboard',$data);
    }
    public function logout() 
	{
		$this->ion_auth->logout();
		redirect(base_url().'panel');
    }
    public function mail() 
    {
        require_once APPPATH.'third_party/imap.php';

        //set search criteria
        $date = date('d-M-y', strtotime('1 day ago'));
        $term = 'ALL UNDELETED SINCE "'.$date.'"';

        //ignore array of emails
        $exclude = [];

        $email    = 'gleenok@gmail.com';
        $password = 'PK7^jE61';
        $host     = 'imap.gmail.com';//your email host
        $port     = '993';
        $savePath = "upload/mail";
        $delete   = false;

        $imap = new Imap($email, $password, $host, $port, 'Inbox', $savePath, $delete);

        //get emails pass in the search term and exclude array
        $emails = $imap->emails($term, $exclude);
        print_r($emails);
    }
}
