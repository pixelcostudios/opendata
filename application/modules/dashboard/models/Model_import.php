<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

class Model_import extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}
	//Kecamatan
	function kecamatan_add($data_array)
	{
		$this->db->insert('my_kecamatan',$data_array);
		return $this->db->insert_id();
	}
	function kecamatan_check($kecamatan)
	{
		$this->db->select('*');
		$this->db->from("my_kecamatan");
		$this->db->where("kecamatan",$kecamatan);
		return $this->db->count_all_results();
	}
	function kecamatan_update($kecamatan_id,$data_array)
	{
		$this->db->where('kecamatan_id', $kecamatan_id);
		$this->db->update('my_kecamatan',$data_array);
		return true;
	}
	//Desa
	function desa_add($data_array)
	{
		$this->db->insert('my_desa',$data_array);
		return $this->db->insert_id();
	}
	function desa_check($kecamatan_id,$desa)
	{
		$this->db->select('*');
		$this->db->from("my_desa");
		$this->db->where("kecamatan_id",$kecamatan_id);
		$this->db->where("desa",$desa);
		return $this->db->count_all_results();
	}
	function desa_update($kecamatan_id,$desa,$data_array)
	{
		$this->db->where('kecamatan_id', $kecamatan_id);
		$this->db->where('desa', $desa);
		$this->db->update('my_desa',$data_array);
		return true;
	}
	//Dukuh
	function dukuh_add($data_array)
	{
		$this->db->insert('my_dukuh',$data_array);
		return $this->db->insert_id();
	}
	function dukuh_check($desa_id,$dukuh)
	{
		$this->db->select('*');
		$this->db->from("my_dukuh");
		$this->db->where("desa_id",$desa_id);
		$this->db->where("dukuh",$dukuh);
		return $this->db->count_all_results();
	}
	function dukuh_update($desa_id,$dukuh,$data_array)
	{
		$this->db->where('desa_id', $desa_id);
		$this->db->where('dukuh', $dukuh);
		$this->db->update('my_dukuh',$data_array);
		return true;
	}
}