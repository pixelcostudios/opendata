<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Menu</title>
<link href="<?php echo base_url()?>themes/default/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url()?>themes/default/css/animate.css" rel="stylesheet">
<link href="<?php echo base_url()?>themes/default/js/plugins/multiselect/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css"/>
<!-- Toastr style -->
<link href="<?php echo base_url()?>themes/default/css/plugins/toastr/toastr.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>themes/default/css/style.css" rel="stylesheet">
<link href="<?php echo base_url()?>themes/default/css/custom.css" rel="stylesheet">
<style type="text/css">
.inmodal .modal-body {
    height: 450px;
    padding-right: 10px;
}
.inmodal .modal-body .list-group{
    margin-right: 20px;
}
.multiselect-container.dropdown-menu{
    bottom: 100%;
    top: auto;
}
</style>
</head>

<body class="fixed-sidebar">
<div id="wrapper">
<?php $this->load->view('back/nav');?>

<div id="page-wrapper" class="gray-bg">
<?php $this->load->view('back/nav_top');?>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Menu</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo base_url()?>panel/dashboard.html">Home</a>
                </li>
                <li>
                    <a href="<?php echo base_url()?>panel/menu.html">Menu</a>
                </li>
                <li class="active">
                    <strong>All Menu <?php echo ucwords($this->uri->segment(3));?></strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <form method="post">
        <div class="col-lg-12 padding-none">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>All Menu <?php echo ucwords($this->uri->segment(3));?></h5>
                    <div class="ibox-tools">
                        <a href="<?php echo base_url()?>panel/menu.html" >
                            <i class="fa fa-refresh"></i>
                        </a>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <div class="col-sm-4">
                            <div id="load"></div>
                            <menu id="nestable-menu">
                            <div class="form-group">
                                <button type="button" class="btn btn-sm btn-white col-sm-6" data-action="expand-all">Expand All</button>
                                <button type="button" class="btn btn-sm btn-white col-sm-6" data-action="collapse-all">Collapse All</button>
                                <div class="clear"></div>
                            </div>
                            </menu>
                            <div class="form-group multi_select clearfix clear">
                                <label for="category">Menu Position</label><br>
                                <select id="position" class="form-control position-select">
                                <option value="0">No Selected</option>
                                <?php foreach($position_list as $row_cat){?>
                                <option value="<?php echo $row_cat->cat_id;?>"><?php echo $row_cat->cat_title;?></option>
                                <?php }?>
                                </select>
                            </div>
                            <div class="form-group multi_select clearfix clear">
                                <label for="category">Select Menu From Category</label><br>
                                <select id="category" multiple="" class="form-control category-select">
                                <?php /*foreach($cat_list as $row_cat){?>
                                <option value="<?php echo $row_cat->cat_id;?>"><?php echo $row_cat->cat_title;?></option>
                                    <?php $cat_list_sub = $this->model_category->ca_list($row_cat->cat_type,$row_cat->cat_id); foreach($cat_list_sub as $row_cat_sub){?>
 b                                        <?php $cat_list_sub1 = $this->model_category->ca_list($row_cat_sub->cat_type,$row_cat_sub->cat_id); foreach($cat_list_sub1 as $row_cat_sub1){?>
                                            <option value="<?php echo $row_cat_sub1->cat_id;?>" >|--|-- <?php echo $row_cat_sub1->cat_title;?></option>
                                        <?php }?>
                                    <?php }?>
                                <?php }*/?>
                                </select>
                            </div>
                            <div class="form-group multi_select clearfix">
                                <label for="pages">Select Menu From Pages</label><br>
                                <select id="pages" multiple="" class="form-control pages-select">
                                <?php /*foreach($list_pages as $row_pages){?>
                                <option value="<?php echo $row_pages->post_id;?>"><?php echo $row_pages->post_title;?></option>
                                <?php }*/?>
                                </select>
                            </div>
                            <div class="form-group multi_select clearfix">
                                <label for="post">Select Menu From Post</label><br>
                                <select id="post" multiple="" class="form-control post-select">
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="title">Custom Menu Title </label>
                                <input type="text" id="label" class="form-control" placeholder="Title">
                            </div>
                            <div class="form-group">
                                <label for="title">Custom Menu Link </label>
                                <input type="text" id="link" class="form-control" placeholder="Link">
                            </div>
                            
                            <button id="submit" class="btn btn-sm btn-white col-sm-6">Submit</button> 
                            <button id="reset" class="btn btn-sm btn-white col-sm-6">Reset</button>


                            <input type="hidden" id="id">
                            <br/><br />
                        </div>
                        <div class="col-sm-8">
                            <div class="row margin-top-5">
                            <div class="col-sm-3"><label class="margin-top-10" for="category">Menu Position</label></div>
                            <div class="form-group col-sm-9">
                                <select class="form-control" id="select-cat" onchange="window.location = jQuery('#select-cat option:selected').val();">
                                <option value="<?php echo base_url()?>panel/menu.html">No Selected</option>
                                <?php foreach($position_list as $row_cat){?>
                                <option value="<?php echo base_url()?>panel/menu/<?php echo $row_cat->cat_id;?>.html" <?php if($this->uri->segment(3)==$row_cat->cat_id){?>selected<?php }?>><?php echo $row_cat->cat_title;?></option>
                                <?php }?>
                                </select>
                            </div>
                            </div>

                            <div class="cf nestable-lists">

                            <div class="dd" id="nestable">

                            <?php
                             
                            $ref   = [];
                            $items = [];

                            foreach($list_menu as $data) {

                                $thisRef = &$ref[$data->id];

                                $thisRef['parent']  = $data->parent;
                                $thisRef['type']    = $data->type;
                                $thisRef['label']   = $data->label;
                                $thisRef['link']    = $data->link;
                                $thisRef['position']= $data->position;
                                $thisRef['id']      = $data->id;

                               if($data->parent == 0) {
                                    $items[$data->id] = &$thisRef;
                               } else {
                                    $ref[$data->parent]['child'][$data->id] = &$thisRef;
                               }

                            }
                             
                            print $this->model_menu->get_menu($items);

                            ?>
                           </div>
                                <div class="clear">
                                </div>
                        </div>
                        <input type="hidden" id="nestable-output">
                        <?php if(count($list_menu)>0){?>
                        <br>
                        <button id="save" class="btn btn-sm btn-primary">Save Menu</button>
                        <a href="<?php echo base_url()?>panel/menu.html" class="btn btn-sm btn-white">Reload Menu</a>
                        <?php }?>
                        </div>
                        

                        <div class="clear">
                    </div>

                </div>
            </div>
        </div>
	</form>
    </div>
</div>
<div class="footer">
    <div class="pull-right">
        <strong><?php echo $this->model_nav->disk_size();?></strong> Free.
    </div>
    <div>
        <strong>Copyright</strong> <?php echo $this->model_setting->setting('company');?> &copy; <?php echo date('Y');?>
    </div>
</div>

</div>
</div>

<!-- Mainly scripts -->
<script src="<?php echo base_url()?>themes/default/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url()?>themes/default/js/bootstrap.min.js"></script>
<!-- <script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.1/js/bootstrap.min.js"></script> -->
<script src="<?php echo base_url()?>themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
<script src="<?php echo base_url()?>themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<script src="<?php echo base_url()?>themes/default/js/plugins/simple-menu/jquery.nestable.js"></script>
<script>
$(document).ready(function()
{
    var updateOutput = function(e)
    {
        var list   = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };

    // activate Nestable for list 1
    $('#nestable').nestable({
        group: 1
    })
    .on('change', updateOutput);

    // output initial serialised data
    updateOutput($('#nestable').data('output', $('#nestable-output')));

    $('#nestable-menu').on('click', function(e)
    {
        var target = $(e.target),
            action = target.data('action');
        if (action === 'expand-all') {
            $('.dd').nestable('expandAll');
        }
        if (action === 'collapse-all') {
            $('.dd').nestable('collapseAll');
        }
    });
});
</script>

<script>
  $(document).ready(function(){
    $("#load").hide();
    $("#submit").click(function(){
       $("#load").show();
       $('#category :selected').each(function(i, sel){ 
          selecteded = $(sel).val(); 
       });
        var categoryID = "";
        $("#category :selected").each(function(i,obj)
        {
            categoryID=categoryID+$(obj).val()+",";
        });
        categoryID = categoryID.substring(0,categoryID.length - 1);
        
        var pagesID = "";
        $("#pages :selected").each(function(i,obj)
        {
            pagesID=pagesID+$(obj).val()+",";
        });
        pagesID = pagesID.substring(0,pagesID.length - 1);
        
        var postID = "";
        $("#post :selected").each(function(i,obj)
        {
            postID=postID+$(obj).val()+",";
        });
        postID = postID.substring(0,postID.length - 1);

        var positionID = "";
        $("#position :selected").each(function(i,obj)
        {
            positionID=positionID+$(obj).val()+",";
        });
        positionID = positionID.substring(0,positionID.length - 1);
        var dataString = { 
              label     : $("#label").val(),
              link      : $("#link").val(),
              category  : categoryID,
              pages     : pagesID,
              post      : postID,
              position  : positionID,
              id        : $("#id").val()              
            };

        $.ajax({
            type: "POST",
            url: "<?php if($this->uri->segment(3)!=''){echo '../';}?>menu_new",
            data: dataString,
            dataType: "json",
            cache : false,
            success: function(data)
            {
              if(data.type == 'add')
              {
                 $("#menu-id").append(data.menu);
              } 
              else if(data.type == 'edit')
              {
                 $('#label_show'+data.id).html(data.label);
                 $('#link_show'+data.id).html(data.link);
                 $('#post_show'+data.id).html(data.post);
                 $('#position_show'+data.id).html(data.position);
              }
              $('#label').val('');
              $('#link').val('');
              $('#category').serialize();
              $('#pages').serialize();
              $('#post').serialize();
              $('#position').serialize();
              $('#id').val('');              
              $("#load").hide();
            } ,error: function(xhr, status, error) {
              alert(error);
            },
        });
    });

    $('.dd').on('change', function() {
        $("#load").show();
     
          var dataString = { 
              data : $("#nestable-output").val(),
            };

        $.ajax({
            type: "POST",
            url: "<?php if($this->uri->segment(3)!=''){echo '../';}?>menu_update",
            data: dataString,
            cache : false,
            success: function(data){
              $("#load").hide();
            } ,error: function(xhr, status, error) {
              alert(error);
            },
        });
    });

    $("#save").click(function(){
         $("#load").show();
     
          var dataString = { 
              data : $("#nestable-output").val(),
            };

        $.ajax({
            type: "POST",
            url: "<?php if($this->uri->segment(3)!=''){echo '../';}?>menu_update",
            data: dataString,
            cache : false,
            success: function(data){
              $("#load").hide();
              alert('Data has been saved');
          
            } ,error: function(xhr, status, error) {
              alert(error);
            },
        });
    });

 
    $(document).on("click",".del-button",function() {
        var x = confirm('Delete this menu?');
        var id = $(this).attr('id');
        if(x){
            $("#load").show();
             $.ajax({
                type: "POST",
                url: "<?php if($this->uri->segment(3)!=''){echo '../';}?>menu_delete_parent",
                data: { id : id },
                cache : false,
                success: function(data){
                  $("#load").hide();
                  $("li[data-id='" + id +"']").remove();
                } ,error: function(xhr, status, error) {
                  alert(error);
                },
            });
        }
    });

    $(document).on("click",".edit-button",function() {
        var id          = $(this).attr('id');
        var label       = $(this).attr('label');
        var link        = $(this).attr('link');
        var position    = $(this).attr('position');
        $("#id").val(id);
        $("#label").val(label);
        $("#link").val(link);
        $("#position").val(position).change();
    });

    $(document).on("click","#reset",function() {
        $('#label').val('');
        $('#link').val('');
        $('#position').val('');
        $('#id').val('');        
    });

  });

</script>

<!-- Peity -->
<script src="<?php echo base_url()?>themes/default/js/plugins/peity/jquery.peity.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url()?>themes/default/js/inspinia.js"></script>
<script src="<?php echo base_url()?>themes/default/js/plugins/pace/pace.min.js"></script>

<!-- Check -->
<script src="<?php echo base_url()?>themes/default/js/check.js"></script>

<script src="<?php echo base_url()?>themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url()?>themes/default/js/plugins/multiselect/dist/js/bootstrap-multiselect.js" type="text/javascript" ></script>
<!-- Peity -->
<script src="<?php echo base_url()?>themes/default/js/demo/peity-demo.js"></script>
<!-- Toastr script -->
<script src="<?php echo base_url()?>themes/default/js/plugins/toastr/toastr.min.js"></script>
<script>
    $(function () {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "progressBar": true,
            "preventDuplicates": false,
            "positionClass": "toast-bottom-right",
            "onclick": null,
            "showDuration": "400",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        //toastr.success('Successfully Update Profile', 'Update Profile');
        <?php if(isset($_COOKIE['status'])){ echo $this->model_status->status($_COOKIE['status']);}?>
        <?php if(isset($_COOKIE['status_second'])){ echo $this->model_status->status($_COOKIE['status_second']);}?>
    });
</script>
<script>
$(document).ready(function(){
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
});
$(document).ready(function() 
{
    $('#example-multiple-selected').multiselect();
    //Category Multiple Select
    $('.category-select').multiselect({
        includeSelectAllOption: true,
        maxHeight: 400,
        dropUp: true,
        enableFiltering: true,
        filterBehavior: 'value'
    });
    $.ajax({
      type: 'GET',
      url: '<?php if($this->uri->segment(3)!=''){echo '../';}?>ajax/category',
      dataType: 'json',
      success: function(data) {
         $.each(data, function (i, item) {
             cat_id     = item.cat_id;
             cat_title  = item.cat_title;
             if($(".category-select option[value='"+cat_id+"']").length == 0){
                $('.category-select').append('<option value="'+cat_id+'">' + cat_title + '</option>');
             }
        }); 
        $('.category-select').multiselect('rebuild');
      },
      error: function() {
            alert('error loading items');
      }
     });
    $('.category-select').trigger( 'change' );
    //Pages Multiple Select
    $('.pages-select').multiselect({
        includeSelectAllOption: true,
        maxHeight: 400,
        dropUp: true,
        enableFiltering: true,
        filterBehavior: 'value'
    });
    $.ajax({
      type: 'GET',
      url: '<?php if($this->uri->segment(3)!=''){echo '../';}?>ajax/pages',
      dataType: 'json',
      success: function(data) {
         $.each(data, function (i, item) {
             post_id     = item.post_id;
             post_title  = item.post_title;
             if($(".pages-select option[value='"+post_id+"']").length == 0){
                $('.pages-select').append('<option value="'+post_id+'">' + post_title + '</option>');
             }
        }); 
        $('.pages-select').multiselect('rebuild');
      },
      error: function() {
            alert('error loading items');
      }
     });
    $('.pages-select').trigger( 'change' );
    //Post Multiple Select
    $('.post-select').multiselect({
        includeSelectAllOption: true,
        maxHeight: 400,
        dropUp: true,
        enableFiltering: true,
        filterBehavior: 'value'
    });
    $.ajax({
      type: 'GET',
      url: '<?php if($this->uri->segment(3)!=''){echo '../';}?>ajax/post',
      dataType: 'json',
      success: function(data) {
         $.each(data, function (i, item) {
             post_id     = item.post_id;
             post_title  = item.post_title;
             if($(".post-select option[value='"+post_id+"']").length == 0){
                $('.post-select').append('<option value="' + post_id + '">' + post_title + '</option>');
             }
        }); 
        $('.post-select').multiselect('rebuild');
      },
      error: function() {
            alert('error loading items');
      }
     });
    $('.post-select').trigger( 'change' );
});
</script>
</body>

</html>
