<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends MX_Controller {
	function __construct()
    {
        parent::__construct();
		$this->load->library('ion_auth');
        $this->load->helper(array('form', 'url', 'string'));
        $this->load->helper(array('path'));
        $this->load->model("member/model_member");
        $this->load->model("setting/model_setting");
    }
	public function index()
	{
		$this->load->library("mail_template");

		if ($this->ion_auth->logged_in())
		{
			redirect(base_url().'panel/dashboard.html');
		}
		$this->load->library('form_validation');
		$this->form_validation->set_rules('first_name', 'First name','trim|required');
        $this->form_validation->set_rules('last_name', 'Last name','trim');
		$this->form_validation->set_rules('username','Username','trim|required|is_unique[my_user.user_name]');
		$this->form_validation->set_rules('email','Email','trim|valid_email|required');
		$this->form_validation->set_rules('password','Password','trim|min_length[5]|max_length[20]|required');
		
		if ($this->form_validation->run() === FALSE)
        {
			$this->load->helper('form');
            $this->load->view('register');
        }
        else
        {
            $first_name                  = $this->input->post('first_name');
			$last_name 	                 = $this->input->post('last_name');
			$username 	                 = $this->input->post('username');
			$email 		                 = $this->input->post('email');
			$password 	                 = $this->input->post('password');
            $user_join                   = date('Y-m-d H:i:s');
            $user_activation_code        = $this->model_setting->char_random('20');
			$user_active 	             = '0';
		 
			$additional_data = array(
				'user_first_name' 	     => $first_name,
				'user_last_name' 	     => $last_name,
                'user_join'              => $user_join,
                'user_activation_code'   => $user_activation_code,
				'user_active'            => $user_active
			);
			$group = array('2'); // Sets user to admin.
		 
			if($this->ion_auth->register($username,$password,$email,$additional_data,$group))
			{   // $_SESSION['auth_message'] = 'The account has been created. Check your email for activation.'; // $this->session->mark_as_flash('auth_message');
                
                $subject                 = $this->model_setting->setting('c1_company').' Registration Activation';
                $from                    = $this->model_setting->setting('c1_email_1');
                $user_fullname           = $first_name.' '.$last_name;
                $user_email              = $email;
                $user_activation_code    = $user_activation_code;
                $themes                  = $this->config->item('themes');

                $data_array = array(
                  'subject'              => $subject,
                  'user_fullname'        => $user_fullname,
                  'user_email'           => $user_email,
                  'user_activation_code' => $user_activation_code,
                  'themes'               => $this->config->item('themes'),
                  'facebook'             => $this->model_setting->setting('facebook'),
                  'google_plus'          => $this->model_setting->setting('google_plus'),
                  'twitter'              => $this->model_setting->setting('twitter'),
                  'instagram'            => $this->model_setting->setting('instagram'),
                  'powered'              => $this->model_setting->setting('c1_company'),
                  'company'              => $this->model_setting->setting('c1_company')
                );

                $body = $this->mail_template->mail_activation($data_array);

                $result = $this->email
                ->from($from,$this->model_setting->setting('c1_company'))
                ->reply_to($from,$this->model_setting->setting('c1_company'))
                ->to($user_email)        
                ->subject($subject)
                ->message($body)
                ->send();

                $this->session->set_flashdata('auth_message', 'The account has been created. Check your email for activation.');
				redirect(base_url().'panel/');
			}
			else
			{    // $_SESSION['auth_message'] = $this->ion_auth->errors(); // $this->session->mark_as_flash('auth_message');
                $this->session->set_flashdata('auth_message',  $this->ion_auth->errors());
				redirect(base_url().'panel/register.html');
			}
        }
	}
}
