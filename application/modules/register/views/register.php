<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Register</title>

    <link href="<?php echo base_url()?>themes/default/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/style.css" rel="stylesheet">
    <style type="text/css">
        .margin-bottom-20{
            margin-bottom: 20px;
        }
        .btn-primary:hover{
            background: #369212;
            border: 1px solid #369212;
        }
        .btn-primary{
            background: #dc4202;
            border: 1px solid #dc4202;
        }
        .middle-box h1 {
            margin-top: 0;
        }
    </style>
</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen   animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name margin-bottom-20"><img src="<?php echo base_url()?>themes/default/img/logo.svg" width="150"></h1>

            </div>
            <h3>Register to <?php echo $this->model_setting->setting('c1_company');?></h3>
            <p>Create account to see it in action.</p>
            <form class="m-t" role="form" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="First Name" required="" name="first_name">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Last Name" name="last_name">
                </div>
                
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="User Name" required="" name="username">
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Email" required="" name="email">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" required="" name="password">
                </div>
                <div class="form-group">
                        <div class="checkbox i-checks"><label> <input type="checkbox"><i></i> Agree the terms and policy </label></div>
                </div>
                <button type="submit" name="Pixel_Register" class="btn btn-primary block full-width m-b">Register</button>

                <p class="text-muted text-center"><small>Already have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="<?php echo base_url()?>panel/">Login</a>
                <br>
                <br>
            </form>
            <!--<p class="m-t"> <small>Wep app framework base on Bootstrap 3 &copy; 2017</small> </p>-->
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="<?php echo base_url()?>themes/default/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url()?>themes/default/js/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
</body>

</html>