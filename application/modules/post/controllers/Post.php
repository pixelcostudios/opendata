<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//require APPPATH . "third_party/MX/Controller.php";

	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
        $this->load->library('email');
        // $this->load->library('pdf');
        $this->load->helper(array('form'));
        $this->load->helper(array('url'));
		$this->load->helper(array('path'));
		$this->load->library("pagination");
		$this->load->model("menu/model_nav");
		$this->load->model("model_post");
        $this->load->model("setting/model_setting");
        $this->load->model("categories/model_categories");
        $this->load->model("member/model_member");
		$this->load->model("model_ipost");
		$this->load->model("model_meta");
	}
	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
        else 
        {
            redirect(base_url().'panel/post/manage.html');
        }
    }
    public function manage()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		$data['list_post_count'] 	    = $this->model_nav->po_list_count('post','0');
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count'] 	    = $this->model_nav->us_list_count();
        $data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);
        $data['cat_list']               = $this->model_nav->ca_list('post','0');

        $post_type 						= 'post';//($this->uri->segment(3)) ? $this->uri->segment(3) : 'post';

        //pagination settings
        if(isset($_POST['Pixel_Search']))
        {
            setcookie('status','search', time()+2);
            redirect(base_url().'panel/post/search/'.$this->model_setting->my_simple_crypt($this->input->post('search'),$action = 'e').'.html');
        }


        $config = array();
        $config['suffix']           = '.html';
        $config['first_url']        = '1.html';
        $config['use_page_numbers'] = TRUE;
        $config['cur_tag_open']     = '&nbsp;<a class="active">';
        $config['cur_tag_close']    = '</a>';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['num_links']        = 5;

        if($this->uri->segment(6)!='')
        {
            $config["base_url"]         = base_url().'panel/post/manage/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
            $total_row                  = ($this->uri->segment(4)!=0) ? $this->model_post->po_cat_id_count($post_type,$this->uri->segment(4)) : $this->model_post->po_cat_count($post_type);
            $config["total_rows"]       = $total_row;
            $config['num_links']        = $total_row;
            $config["per_page"]         = ($this->uri->segment(5)) ? $this->uri->segment(5) : '10';

            $this->pagination->initialize($config);

            $page = ($this->uri->segment(6)-1)*$config["per_page"] ;
            $data["post_list"]          = ($this->uri->segment(4)!=0) ? $this->model_post->po_cat_id($post_type,$this->uri->segment(4),$config["per_page"], $page) : $this->model_post->po_cat($post_type,$config["per_page"], $page);
        }
        else 
        {
            $config["base_url"]         = base_url().'panel/post/manage/';
            $total_row                  = $this->model_post->po_cat_count($post_type);
            $config["total_rows"]       = $total_row;
            $config['num_links']        = $total_row;
            $config["per_page"]         = ($this->uri->segment(5)) ? $this->uri->segment(5) : '10';

            $this->pagination->initialize($config);

            $page = ($this->uri->segment(4)!='') ? ($this->uri->segment(4)-1)*$config["per_page"] : 0;
            $data["post_list"]          = $this->model_post->po_cat($post_type,$config["per_page"], $page);
        }

        $str_links                  = $this->pagination->create_links();
        $data["links"]              = explode('&nbsp;',$str_links );

        if(isset($_POST['post-draft']) && $_POST['post-draft'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$post_id=$_POST['post-check'][$i];
				
				$this->model_post->po_update_status($post_id,'0');
			}
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/post.html');
		}
        elseif(isset($_POST['post-publish']) && $_POST['post-publish'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$post_id=$_POST['post-check'][$i];
				
				$this->model_post->po_update_status($post_id,'1');
			}
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/post.html');
		}
		elseif(isset($_POST['post-delete']) && $_POST['post-delete'] != "")
		{
            for ($i=0; $i<count($_POST['post-check']);$i++)
            {
                $post_id=$_POST['post-check'][$i];

                foreach($this->model_ipost->ip_list($post_id,'post','DESC') as $row_sub)
                {
                    @unlink('./upload/'.$row_sub->ipost);
                    $this->model_ipost->ip_delete_type($post_id,'post');
                }
                foreach($this->model_ipost->ip_list($post_id,'post_icon','DESC') as $row_sub)
                {
                    @unlink('./upload/'.$row_sub->ipost);
                }
                foreach($this->model_ipost->ip_list($post_id,'post_thumb','DESC') as $row_sub)
                {
                    @unlink('./upload/'.$row_sub->ipost);
                }
                foreach($this->model_ipost->ip_list($post_id,'post_background','DESC') as $row_sub)
                {
                    @unlink('./upload/'.$row_sub->ipost);
                }
                foreach($this->model_ipost->ip_list($post_id,'post_pdf','DESC') as $row_sub)
                {
                    @unlink('./upload/files/'.$row_sub->ipost);
                }
                foreach($this->model_ipost->ip_list($post_id,'post_files','DESC') as $row_sub)
                {
                    @unlink('./upload/files/'.$row_sub->ipost);
                }
                $this->model_ipost->ip_delete_post($post_id);
                $this->model_post->po_delete($post_id);
            }
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/post.html');
        }
        elseif(isset($_POST['post-delete-ajax']) && $_POST['post-delete-ajax'] != "")
		{
            $post_id=$_POST['post_id'];

            foreach($this->model_ipost->ip_list($post_id,'post','DESC') as $row_sub)
            {
                @unlink('./upload/'.$row_sub->ipost);
                $this->model_ipost->ip_delete_type($post_id,'post');
            }
            foreach($this->model_ipost->ip_list($post_id,'post_icon','DESC') as $row_sub)
            {
                @unlink('./upload/'.$row_sub->ipost);
            }
            foreach($this->model_ipost->ip_list($post_id,'post_thumb','DESC') as $row_sub)
            {
                @unlink('./upload/'.$row_sub->ipost);
            }
            foreach($this->model_ipost->ip_list($post_id,'post_background','DESC') as $row_sub)
            {
                @unlink('./upload/'.$row_sub->ipost);
            }
            foreach($this->model_ipost->ip_list($post_id,'post_pdf','DESC') as $row_sub)
            {
                @unlink('./upload/files/'.$row_sub->ipost);
            }
            foreach($this->model_ipost->ip_list($post_id,'post_files','DESC') as $row_sub)
            {
                @unlink('./upload/files/'.$row_sub->ipost);
            }
            $this->model_ipost->ip_delete_post($post_id);
            $this->model_post->po_delete($post_id);

            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/post.html');
        }
        $this->load->view('post',$data);

	}
    public function all()
    {
        if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        //echo $this->uri->segment(3);
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);
        $data['cat_list']               = $this->model_nav->ca_list('post','0');

        $post_type                      = 'post';

        //pagination settings
        if(isset($_POST['Pixel_Search']))
        {
            setcookie('status','search', time()+2);
            redirect(base_url().'panel/post/search/'.$this->model_setting->my_simple_crypt($this->input->post('search'),$action = 'e').'.html');
        }

        $config = array();
        $config["base_url"]         = base_url().'panel/post/all/'.$this->uri->segment(4).'/';
        $total_row                  = $this->model_post->po_cat_count($post_type);
        $config['suffix']           = '.html';
        $config['first_url']        = '1.html';
        $config["total_rows"]       = $total_row;
        $config["per_page"]         = ($this->uri->segment(4)) ? $this->uri->segment(4) : '10';
        $config['use_page_numbers'] = TRUE;
        $config['num_links']        = $total_row;
        $config['cur_tag_open']     = '&nbsp;<a class="active">';
        $config['cur_tag_close']    = '</a>';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['num_links']        = 5;

        $this->pagination->initialize($config);
        if($this->uri->segment(5))
        {
            $page = ($this->uri->segment(5)-1)*$config["per_page"] ;
        }
        else
        {
            $page = 0;
        }
        $data["post_list"]          = $this->model_post->po_cat($post_type,$config["per_page"], $page);

        $str_links                  = $this->pagination->create_links();
        $data["links"]              = explode('&nbsp;',$str_links );

        if(isset($_POST['post-draft']) && $_POST['post-draft'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$post_id=$_POST['post-check'][$i];
				
				$this->model_post->po_update_status($post_id,'0');
			}
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/post.html');
		}
        elseif(isset($_POST['post-publish']) && $_POST['post-publish'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$post_id=$_POST['post-check'][$i];
				
				$this->model_post->po_update_status($post_id,'1');
			}
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/post.html');
		}
		elseif(isset($_POST['post-delete']) && $_POST['post-delete'] != "")
		{
            for ($i=0; $i<count($_POST['post-check']);$i++)
            {
                $post_id=$_POST['post-check'][$i];

                foreach($this->model_ipost->ip_list($post_id,'post','DESC') as $row_sub)
                {
                    @unlink('./upload/'.$row_sub->ipost);
                    $this->model_ipost->ip_delete_type($post_id,'post');
                }
                foreach($this->model_ipost->ip_list($post_id,'post_icon','DESC') as $row_sub)
                {
                    @unlink('./upload/'.$row_sub->ipost);
                }
                foreach($this->model_ipost->ip_list($post_id,'post_thumb','DESC') as $row_sub)
                {
                    @unlink('./upload/'.$row_sub->ipost);
                }
                foreach($this->model_ipost->ip_list($post_id,'post_background','DESC') as $row_sub)
                {
                    @unlink('./upload/'.$row_sub->ipost);
                }
                foreach($this->model_ipost->ip_list($post_id,'post_pdf','DESC') as $row_sub)
                {
                    @unlink('./upload/files/'.$row_sub->ipost);
                }
                foreach($this->model_ipost->ip_list($post_id,'post_files','DESC') as $row_sub)
                {
                    @unlink('./upload/files/'.$row_sub->ipost);
                }
                $this->model_ipost->ip_delete_post($post_id);
                $this->model_post->po_delete($post_id);
            }
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/post.html');
        }
        elseif(isset($_POST['post-delete-ajax']) && $_POST['post-delete-ajax'] != "")
		{
            $post_id=$_POST['post_id'];

            foreach($this->model_ipost->ip_list($post_id,'post','DESC') as $row_sub)
            {
                @unlink('./upload/'.$row_sub->ipost);
                $this->model_ipost->ip_delete_type($post_id,'post');
            }
            foreach($this->model_ipost->ip_list($post_id,'post_icon','DESC') as $row_sub)
            {
                @unlink('./upload/'.$row_sub->ipost);
            }
            foreach($this->model_ipost->ip_list($post_id,'post_thumb','DESC') as $row_sub)
            {
                @unlink('./upload/'.$row_sub->ipost);
            }
            foreach($this->model_ipost->ip_list($post_id,'post_background','DESC') as $row_sub)
            {
                @unlink('./upload/'.$row_sub->ipost);
            }
            foreach($this->model_ipost->ip_list($post_id,'post_pdf','DESC') as $row_sub)
            {
                @unlink('./upload/files/'.$row_sub->ipost);
            }
            foreach($this->model_ipost->ip_list($post_id,'post_files','DESC') as $row_sub)
            {
                @unlink('./upload/files/'.$row_sub->ipost);
            }
            $this->model_ipost->ip_delete_post($post_id);
            $this->model_post->po_delete($post_id);

            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/post.html');
        }
        $this->load->view('post',$data);

    }
	function add()
	{
		if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        $this->load->model("model_status");
        $this->load->model("model_meta");
        /**
        * 
        * @var List Menu
        * 
        */
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);
        
        /**
        * 
        * @var Data
        * 
        */
        $data['cat_list']           = $this->model_categories->ca_list('post','0','DESC');
        $data['cat_array']          = $this->model_meta->me_source_list('category',$this->uri->segment(4));
        
        if(count($data['cat_array'])>0)
        {
            foreach ($data['cat_array'] as $key_cat) 
            {
                $array_cat[] = $key_cat->meta_dest;
            }
        }
        else
        {
            $array_cat = array('0' => 0);
        }
        
        $data['array_cat'] = $array_cat;

        /*User List*/
        $data['user_list']          = $this->model_member->us_list_type('admin');

        if(isset($_POST['Pixel_Save']))
        {
            $user_id                = $this->input->post('user_id');
            // $cat_id                 = ($this->input->post('cat_id')=='') ? '0' : $this->input->post('cat_id');
            $post_type             = 'post';
            $post_up                = ($this->input->post('post_up')=='') ? '0' : $this->input->post('post_up');
            $post_slug              = strtolower(url_title($this->input->post('post_title')));
            $post_title             = $this->input->post('post_title');         
            $post_content           = $this->input->post('post_content');
            $post_summary           = $this->input->post('post_summary');
            $post_link              = $this->input->post('post_link');
            $post_video             = $this->input->post('post_video');
            $post_key               = $this->input->post('post_key');
            $post_desc              = $this->input->post('post_desc');
            $post_comment           = $this->input->post('post_comment');
            $date                   = ($this->input->post('date')=='') ? date('Y-m-d H:i:s') : $this->input->post('date');
            $status                 = $this->input->post('status');
            
            $data_array = array(  
                'user_id'       => $user_id,  
                'post_type'     => $post_type,  
                'post_up'       => $post_up,
                'post_slug'     => $post_slug,
                'post_title'    => $post_title,
                'post_content'  => $post_content,  
                'post_summary'  => $post_summary,  
                'post_link'     => $post_link,  
                'post_video'    => $post_video,  
                'post_key'      => $post_key,  
                'post_desc'     => $post_desc,  
                'post_comment'  => $post_comment,  
                'date'          => $date,  
                'status'        => $status 
            );  
            $this->model_post->po_add($data_array);
			$id 			= $this->db->insert_id();
            
            if($this->input->post('category')!='')
            {
                foreach ($this->input->post('category') as $category)
                {

                    if($this->model_meta->me_source_check('category',$id,$category)=='0')
                    {
                        $data_array = array(  
                            'meta_type'     => 'category',  
                            'meta_source'   => $id,  
                            'meta_dest'     => $category,
                            'meta_title'    => $this->model_categories->ca_title($category),
                            'meta_slug'     => $this->model_categories->ca_slug($category),
                            'meta_date'     => date('Y-m-d H:i:s'),
                            'meta_status'   => '1'
                        ); 
                        $this->model_meta->me_add($data_array);
                    }
                    else
                    {
                        $this->model_meta->me_update_status('category',$id,$category,'1'); 

                        $meta_list = $this->model_meta->me_list_not_in('category',$id,$this->input->post('category'));

                        foreach ($meta_list as $key) 
                        {
                            $this->model_meta->me_update_status('category',$id,$key->meta_dest,'0');
                        }
                    }
                }
            }

            /**
            * @var Upload Files
            */
            $this->model_ipost->ipost_upload('upload_photo','post',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_icon','post_icon',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_thumb','post_thumb',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_background','post_background',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_pdf','post_pdf',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_files','post_files',$id,$_SESSION['user_id']);
            
            if($this->uri->segment(5)=='sub')
            {
                setcookie('status','updated', time()+2);
                redirect(base_url().'panel/post/edit/'.$id.'.html');
            }
            else
            {
                setcookie('status','added', time()+2);
			    redirect(base_url().'panel/post/edit/'.$id.'.html');
            }
        }
        $this->load->view('post_add', $data);
	}
	function edit()
	{
		if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        /**
        * 
        * @var List Menu
        * 
        */
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);
        
        /**
        * 
        * @var Data
        * 
        */
        $post_id                    = $this->uri->segment(4);
        $data['row']                = $this->model_post->po_edit($post_id);
        
        $data['images']             = $this->model_ipost->ip_list($post_id,'post','DESC');
        $data['icon']               = $this->model_ipost->ip_list($post_id,'post_icon','DESC');
        $data['thumb']              = $this->model_ipost->ip_list($post_id,'post_thumb','DESC');
        $data['background']         = $this->model_ipost->ip_list($post_id,'post_background','DESC');
        $data['pdf']                = $this->model_ipost->ip_list($post_id,'post_pdf','DESC');
        $data['files']              = $this->model_ipost->ip_list($post_id,'post_files','DESC');
    
        $data['cat_list']           = $this->model_categories->ca_list('post','0','DESC');
        $data['cat_array']          = $this->model_meta->me_source_list('category',$this->uri->segment(4));
        
        if(count($data['cat_array'])>0)
        {
            foreach ($data['cat_array'] as $key_cat) 
            {
                $array_cat[] = $key_cat->meta_dest;
            }
        }
        else
        {
            $array_cat = array('0' => 0);
        }
        
        $data['array_cat'] = $array_cat;

        /*User List*/
        $data['user_list']          = $this->model_member->us_list_type('admin');

        if(isset($_POST['Pixel_Save']))
        {
            $id                     = $this->uri->segment(4);
            $user_id                = $this->input->post('user_id');
            // $cat_id                 = ($this->input->post('cat_id')=='') ? '0' : $this->input->post('cat_id');
            $post_up                = ($this->input->post('post_up')=='') ? '0' : $this->input->post('post_up');
            $post_slug              = strtolower(url_title($this->input->post('post_title')));
            $post_title             = $this->input->post('post_title');         
            $post_content           = $this->input->post('post_content');
            $post_summary           = $this->input->post('post_summary');
            $post_link              = $this->input->post('post_link');
            $post_video             = $this->input->post('post_video');
            $post_key               = $this->input->post('post_key');
            $post_desc              = $this->input->post('post_desc');
            $post_comment           = $this->input->post('post_comment');
            $date                   = ($this->input->post('date')=='') ? date('Y-m-d H:i:s') : $this->input->post('date');
            $status                 = $this->input->post('status');
            
            $data_array = array(  
                'user_id'       => $user_id,  
                // 'cat_id'        => $cat_id,  
                'post_up'       => $post_up,
                'post_slug'     => $post_slug,
                'post_title'    => $post_title,
                'post_content'  => $post_content,  
                'post_summary'  => $post_summary,  
                'post_link'     => $post_link,  
                'post_video'    => $post_video,  
                'post_key'      => $post_key,  
                'post_desc'     => $post_desc,  
                'post_comment'  => $post_comment,  
                'date'          => $date,  
                'status'        => $status 
            );  
            $this->model_post->po_update($this->uri->segment(4),$data_array);
            
            if($this->input->post('category')!='')
            {
                foreach ($this->input->post('category') as $category)
                {

                    if($this->model_meta->me_source_check('category',$id,$category)=='0')
                    {
                        $data_array = array(  
                            'meta_type'     => 'category',  
                            'meta_source'   => $id,  
                            'meta_dest'     => $category,
                            'meta_title'    => $this->model_categories->ca_title($category),
                            'meta_slug'     => $this->model_categories->ca_slug($category),
                            'meta_date'     => date('Y-m-d H:i:s'),
                            'meta_status'   => '1'
                        ); 
                        $this->model_meta->me_add($data_array);
                    }
                    else
                    {
                        $this->model_meta->me_update_status('category',$id,$category,'1'); 

                        $meta_list = $this->model_meta->me_list_not_in('category',$id,$this->input->post('category'));

                        foreach ($meta_list as $key) 
                        {
                            $this->model_meta->me_update_status('category',$id,$key->meta_dest,'0');
                        }
                    }
                }
            }

            /**
            * @var Upload Files
            */
            $this->model_ipost->ipost_upload('upload_photo','post',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_icon','post_icon',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_thumb','post_thumb',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_background','post_background',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_pdf','post_pdf',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_files','post_files',$id,$_SESSION['user_id']);
            
            if($this->uri->segment(5)=='sub')
            {
                setcookie('status','updated', time()+2);
                redirect(base_url().'panel/post/edit/'.$this->uri->segment(4).'.html');
            }
            else
            {
                setcookie('status','updated', time()+2);
                redirect(base_url(uri_string()).'.html');
            }
        }
        
        if(isset($_POST['Pixel_D_Images']))
        {
            if (isset($_POST['post-check'])){
            for ($i=0; $i<count($_POST['post-check']);$i++) 
                {   
                    $i_name=explode(",",$_POST['post-check'][$i]);
                
                    if (file_exists('upload/'.$i_name[1]))
			        {
                        unlink("upload/".$i_name[1]);
                    }
                    if (file_exists('upload/c_'.$i_name[1]))
			        {
                        unlink("upload/c_".$i_name[1]);
                    }
                    $this->model_ipost->ip_delete($i_name[0]);
                }
                setcookie('status','deleted_images', time()+2);
                redirect(base_url(uri_string()).'.html');
            }
            else
            {
                setcookie('status','noimages', time()+2);
                redirect(base_url(uri_string()).'.html');
            }
        }
        if(isset($_POST['Pixel_D_PDF']))
        {
            if (isset($_POST['pdf-check'])){
            for ($i=0; $i<count($_POST['pdf-check']);$i++) 
                {   
                    $i_name=explode(",",$_POST['pdf-check'][$i]);
                
                    if (file_exists('upload/files/'.$i_name[1]))
			        {
                        unlink("upload/files/".$i_name[1]);
                    }
                    $this->model_ipost->ip_delete($i_name[0]);
                }
                setcookie('status','deleted_pdf', time()+2);
                redirect(base_url(uri_string()).'.html');
            }
            else
            {
                setcookie('status','noimages', time()+2);
                redirect(base_url(uri_string()).'.html');
            }
        }
        if(isset($_POST['Pixel_D_Files']))
        {
            if (isset($_POST['files-check'])){
            for ($i=0; $i<count($_POST['files-check']);$i++) 
                {   
                    $i_name=explode(",",$_POST['files-check'][$i]);
                
                    if (file_exists('upload/files/'.$i_name[1]))
			        {
                        unlink("./upload/files/".$i_name[1]);
                    }
                    $this->model_ipost->ip_delete($i_name[0]);
                }
                setcookie('status','deleted_files', time()+2);
                redirect(base_url(uri_string()).'.html');
            }
            else
            {
                setcookie('status','noimages', time()+2);
                redirect(base_url(uri_string()).'.html');
            }
        }
        $this->load->view('post_edit', $data);
	}
    function search()
    {
        $data['list_pages']         = $this->model_nav->pg_list('pages','0');
        $data['row_user']           = $this->model_nav->us_profile($_SESSION['user_id']);

        $post_type                  = 'post';
        $cat                        = $this->model_setting->my_simple_crypt($this->uri->segment(4),$action = 'd');
        if(isset($_POST['Pixel_Search']))
        {
            setcookie('status','search', time()+2);
            redirect(base_url().'panel/post/search/'.$this->model_setting->my_simple_crypt($this->input->post('search'),$action = 'e').'.html');
        }

        $config = array();
        $config["base_url"]         = ($this->uri->segment(5)) ? base_url().'panel/post/search/'.$this->uri->segment(4).'/'.$this->uri->segment(5).'/' : base_url().'panel/post/search/'.$this->uri->segment(4).'/10/';
        $total_row                  = $this->model_post->po_search_count($post_type,$cat);
        $config['suffix']           = '.html';
        $config['first_url']        = '1.html';
        $config["total_rows"]       = $total_row;
        $config["per_page"]         = ($this->uri->segment(5)) ? $this->uri->segment(5) : '10';
        $config['use_page_numbers'] = TRUE;
        $config['num_links']        = $total_row;
        $config['cur_tag_open']     = '&nbsp;<a class="active">';
        $config['cur_tag_close']    = '</a>';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['num_links']        = 5;

        $this->pagination->initialize($config);
        if($this->uri->segment(6))
        {
            $page = ($this->uri->segment(6)-1)*$config["per_page"] ;
        }
        else
        {
            $page = 0;
        }
        $data["post_list"]          = $this->model_post->po_search($post_type,$cat,$config["per_page"], $page);

        $str_links                  = $this->pagination->create_links();
        $data["links"]              = explode('&nbsp;',$str_links );

        if(isset($_POST['post-draft']) && $_POST['post-draft'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$post_id=$_POST['post-check'][$i];
				
				$this->model_post->po_update_status($post_id,'0');
			}
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/post.html');
		}
        elseif(isset($_POST['post-publish']) && $_POST['post-publish'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$post_id=$_POST['post-check'][$i];
				
				$this->model_post->po_update_status($post_id,'1');
			}
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/post.html');
		}
		elseif(isset($_POST['post-delete']) && $_POST['post-delete'] != "")
		{
            for ($i=0; $i<count($_POST['post-check']);$i++)
            {
                $post_id=$_POST['post-check'][$i];

                foreach($this->model_ipost->ip_list($post_id,'post','DESC') as $row_sub)
                {
                    @unlink('./upload/'.$row_sub->ipost);
                    $this->model_ipost->ip_delete_type($post_id,'post');
                }
                foreach($this->model_ipost->ip_list($post_id,'post_icon','DESC') as $row_sub)
                {
                    @unlink('./upload/'.$row_sub->ipost);
                }
                foreach($this->model_ipost->ip_list($post_id,'post_thumb','DESC') as $row_sub)
                {
                    @unlink('./upload/'.$row_sub->ipost);
                }
                foreach($this->model_ipost->ip_list($post_id,'post_background','DESC') as $row_sub)
                {
                    @unlink('./upload/'.$row_sub->ipost);
                }
                foreach($this->model_ipost->ip_list($post_id,'post_pdf','DESC') as $row_sub)
                {
                    @unlink('./upload/files/'.$row_sub->ipost);
                }
                foreach($this->model_ipost->ip_list($post_id,'post_files','DESC') as $row_sub)
                {
                    @unlink('./upload/files/'.$row_sub->ipost);
                }
                $this->model_ipost->ip_delete_post($post_id);
                $this->model_post->po_delete($post_id);
            }
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/post.html');
        }
        elseif(isset($_POST['post-delete-ajax']) && $_POST['post-delete-ajax'] != "")
		{
            $post_id=$_POST['post_id'];

            foreach($this->model_ipost->ip_list($post_id,'post','DESC') as $row_sub)
            {
                @unlink('./upload/'.$row_sub->ipost);
                $this->model_ipost->ip_delete_type($post_id,'post');
            }
            foreach($this->model_ipost->ip_list($post_id,'post_icon','DESC') as $row_sub)
            {
                @unlink('./upload/'.$row_sub->ipost);
            }
            foreach($this->model_ipost->ip_list($post_id,'post_thumb','DESC') as $row_sub)
            {
                @unlink('./upload/'.$row_sub->ipost);
            }
            foreach($this->model_ipost->ip_list($post_id,'post_background','DESC') as $row_sub)
            {
                @unlink('./upload/'.$row_sub->ipost);
            }
            foreach($this->model_ipost->ip_list($post_id,'post_pdf','DESC') as $row_sub)
            {
                @unlink('./upload/files/'.$row_sub->ipost);
            }
            foreach($this->model_ipost->ip_list($post_id,'post_files','DESC') as $row_sub)
            {
                @unlink('./upload/files/'.$row_sub->ipost);
            }
            $this->model_ipost->ip_delete_post($post_id);
            $this->model_post->po_delete($post_id);

            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/post.html');
        }
        $this->load->view('post',$data);
    }
    public function delete()
	{
        $this->load->model("back/model_status");

		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		
		$this->model_post->po_delete($this->uri->segment(3));

        setcookie('status','deleted', time()+2);
		redirect(base_url().'panel/post.html');
	}
}
