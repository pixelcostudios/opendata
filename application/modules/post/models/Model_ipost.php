<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

class model_ipost extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();		
	}
	function ip_list($ipost_all,$ipost_type,$order)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		$this->db->where('ipost_all',$ipost_all);
		$this->db->where('ipost_type',$ipost_type);
		$this->db->order_by('date',$order);
		$query	= $this->db->get();
		return $query->result();
	}
	function ip_list_value($ipost_all,$ipost_type,$order)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		$this->db->where('ipost_all',$ipost_all);
		$this->db->where('ipost_type',$ipost_type);
		$this->db->order_by('date',$order);
		$result	= $this->db->get();
		return $result->row('ipost');
	}
	function ip_list_limit($ipost_all,$ipost_type,$order,$limit)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		$this->db->where('ipost_all',$ipost_all);
		$this->db->where('ipost_type',$ipost_type);
		$this->db->order_by('date',$order);
		$this->db->limit('limit',$limit);
		$query	= $this->db->get();
		return $query->result();
	}
	function ip_list_count($ipost_all,$ipost_type)
	{
		$this->db->select('ipost_all, ipost_type');
		$this->db->from("my_ipost");
		$this->db->where('ipost_all',$ipost_all);
		$this->db->where('ipost_type',$ipost_type);
		return $this->db->count_all_results();
	}
	function ip_list_count_all($ipost_type)
	{
		$this->db->select('ipost_all, ipost_type');
		$this->db->from("my_ipost");
		$this->db->where('ipost_type',$ipost_type);
		return $this->db->count_all_results();
	}
	function ip_post($ipost_type,$ipost_all,$order,$start,$limit)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
        $this->db->where('ipost_type',$ipost_type);
		$this->db->where('ipost_all',$ipost_all);
		$this->db->order_by('date',$order);
		$this->db->limit($start,$limit);
		$query	= $this->db->get();
		return $query->result();
	}
	function ip_post_count($ipost_type,$ipost_all)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
        $this->db->where('ipost_type',$ipost_type);
		$this->db->where('ipost_all',$ipost_all);
		return $this->db->count_all_results();
	}
	function ip_post_all($ipost_type,$order,$start,$limit)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		$this->db->where('ipost_type',$ipost_type);
		$this->db->order_by('date',$order);
		$this->db->limit($start,$limit);
		$query	= $this->db->get();
		return $query->result();
	}
	function ip_post_all_count($ipost_type)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		$this->db->where('ipost_type',$ipost_type);
		return $this->db->count_all_results();
	}
	function ip_search($ipost_type,$cat,$order,$start,$limit)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		$this->db->group_start();
		$this->db->like('ipost',$cat);
		$this->db->or_like('ipost_files',$cat);
		$this->db->or_like('ipost_link',$cat);
		$this->db->or_like('ipost_slug',$cat);
		$this->db->or_like('ipost_title',$cat);
		$this->db->or_like('ipost_content',$cat);
		$this->db->or_like('ipost_embed',$cat);
		$this->db->or_like('ipost_copyright',$cat);
		$this->db->group_end();
        $this->db->where('ipost_type',$ipost_type);
		$this->db->order_by('date',$order);
		$this->db->limit($start,$limit);
		$query	= $this->db->get();
		return $query->result();
	}
	function ip_search_count($ipost_type,$cat)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		$this->db->group_start();
		$this->db->like('ipost',$cat);
		$this->db->or_like('ipost_files',$cat);
		$this->db->or_like('ipost_link',$cat);
		$this->db->or_like('ipost_slug',$cat);
		$this->db->or_like('ipost_title',$cat);
		$this->db->or_like('ipost_content',$cat);
		$this->db->or_like('ipost_embed',$cat);
		$this->db->or_like('ipost_copyright',$cat);
		$this->db->group_end();
        $this->db->where('ipost_type',$ipost_type);
		return $this->db->count_all_results();
	}
	function ip_add($data_array)
	{
		$this->db->insert('my_ipost',$data_array);
		return true;
	}
	function ip_edit($ipost_id)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		$this->db->where('ipost_id',$ipost_id);
		$result	= $this->db->get();
		return $result->row();
	}
	function ip_edit_type($ipost_type)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		$this->db->where('ipost_type',$ipost_type);
		$result	= $this->db->get();
		return $result->row();
	}
	function ip_update($ipost_id,$data_array)
	{
		$this->db->where('ipost_id', $ipost_id);
		$this->db->update('my_ipost',$data_array);
		return true;
	}
	function ip_update_type($ipost_type,$data_array)
	{
		$this->db->where('ipost_type', $ipost_type);
		$this->db->update('my_ipost',$data_array);
		return true;
	}
	function ip_update_status($ipost_id,$status)
	{
		$this->db->set('status', $status); 
		$this->db->where('ipost_id', $ipost_id);
		$this->db->update('my_ipost');
		return true;
	}
	function ip_value($ipost_id)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		$this->db->where('ipost_id',$ipost_id);
		$result	= $this->db->get();
		return $result->row();
	}
	function ip_value_type($ipost_type)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		$this->db->where('ipost_type',$ipost_type);
		$result	= $this->db->get();
		return $result->row();
	}
	function ip_delete($ipost_id)
	{
		$this->db->where('ipost_id',$ipost_id);
		$this->db->delete('my_ipost');
		return true;
	}
	function ip_delete_post($ipost_all)
	{
		$this->db->where('ipost_all',$ipost_all);
		$this->db->delete('my_ipost');
		return true;
	}
	function ip_delete_type($ipost_all,$ipost_type)
	{
		$this->db->where('ipost_all',$ipost_all);
		$this->db->where('ipost_type',$ipost_type);
		$this->db->delete('my_ipost');
		return true;
	}
	function md_all($order,$start,$limit)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		$this->db->order_by('date',$order);
		$this->db->limit($start,$limit);
		$query	= $this->db->get();
		return $query->result();
	}
	function md_all_count()
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		return $this->db->count_all_results();
	}
	function md_group_type($order)
	{
		$this->db->select('ipost_type');
		$this->db->from("my_ipost");
		$this->db->where('ipost_type !=','');
		$this->db->group_by('ipost_type');
		$this->db->order_by('ipost_type',$order);
		$query	= $this->db->get();
		return $query->result();
	}
	function ext_name($filepath) 
    { 
        preg_match('/[^?]*/', $filepath, $matches); 
        $string = $matches[0]; 
        $pattern = preg_split('/\./', $string, -1, PREG_SPLIT_OFFSET_CAPTURE); 
        $lastdot = $pattern[count($pattern)-1][1]; 
        $filename = basename(substr($string, 0, $lastdot-1)); 
        return $filename; 
    }
    function ext_filename($filepath) 
    { 
        preg_match('/[^?]*/', $filepath, $matches); 
        $string = $matches[0]; 
      
        $pattern = preg_split('/\./', $string, -1, PREG_SPLIT_OFFSET_CAPTURE); 

        # check if there is any extension 
        if(count($pattern) == 1) 
        { 
            $ext = 'none'; 
            //exit; 
        } 
        elseif(count($pattern) > 1) 
        { 
            $filenamepart = $pattern[count($pattern)-1][0]; 
            preg_match('/[^?]*/', $filenamepart, $matches); 
            $ext = str_replace('com/','',$matches[0]); 
        } 
		return $ext;
    } 
	function strip_i_name($result)
	{
		$string = strtolower($result);
		$a=array('+','%2F%',' ','.jpg','.png','.gif','.bmp');
		$b=array(' ',' ','-','','','','');
		$string = str_replace($a, $b, $string);
		$slug = preg_replace('/[^a-z0-9-]/', '', $string);
		$c=array('----','---','--');
		$d=array('-','-','');
		$slug = str_replace($c,$d,$slug);
		return $slug;
		// $a=array('(',')','{','}','[',']','%',' ','--');
		// $b=array('','','','','','','','-','');
		// $result = str_replace($a,$b,strtolower(preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $result)));
		// return $result;
	}
	function strip_i_slug($result)
	{
		$string = strtolower($result);
		$a=array('+','%2F%',' ');
		$b=array(' ',' ','-');
		$string = str_replace($a, $b, $string);
		$slug = preg_replace('/[^a-z0-9-]/', '', $string);
		$c=array('----','---','--');
		$d=array('-','-','');
		$slug = str_replace($c,$d,$slug);
		return $slug;
	}
	function strip_i_title($result)
	{
		$a=array('+','%2F%','_','-','.jpg','.png','.gif','.bmp','    ','   ','  ','.');
		$b=array(' ',' ',' ',' ','','','','',' ',' ',' ',' ');
		$result=str_replace($a,$b,$result);
		$result=ucwords(preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $result));
		return $result;
	}
	function ipost_upload($upload_field,$ipost_type,$ipost_all,$user_id) 
    { 
        if(isset($_FILES[$upload_field]['tmp_name']))
		{
			$number_of_files = sizeof($_FILES[$upload_field]['tmp_name']);
		    $files = $_FILES[$upload_field];
		    $errors = array();
		 
		    for($i=0;$i<$number_of_files;$i++)
		    {
		      if($_FILES[$upload_field]['error'][$i] != 0) $errors[$i][] = 'Couldn\'t upload file '.$_FILES[$upload_field]['name'][$i];
		    }
		    if(sizeof($errors)==0)
		    {
				$this->load->library('upload');
				
				for ($i = 0; $i < $number_of_files; $i++) 
				{
					switch($ipost_type)
					{
						case 'cat':
						case 'post':
						$config['upload_path'] 				= FCPATH . './upload/';
						$config['allowed_types'] 			= 'gif|jpg|jpeg|png';
						break;
						
						case 'cat_icon':
						case 'post_icon':
						$config['upload_path'] 				= FCPATH . './upload/';
						$config['allowed_types'] 			= 'gif|jpg|jpeg|png';
						break;
						
						case 'cat_thumb':
						case 'post_thumb':
						$config['upload_path'] 				= FCPATH . './upload/';
						$config['allowed_types'] 			= 'gif|jpg|jpeg|png';
						break;
						
						case 'cat_background':
						case 'post_background':
						$config['upload_path'] 				= FCPATH . './upload/';
						$config['allowed_types'] 			= 'gif|jpg|jpeg|png';
						break;
						
						case 'cat_pdf':
						case 'post_pdf':
						$config['upload_path'] 				= FCPATH . './upload/files/';
						$config['allowed_types'] 			= 'pdf';
						break;
						
						case 'cat_files':
						case 'post_files':
						$config['upload_path'] 				= FCPATH . './upload/files/';
						$config['allowed_types'] 			= 'gif|jpg|jpeg|png|pdf|doc|docx|xls|xlsx|ppt|pptx|zip|rar';
						break;
					}
					
					$config['file_name'] 					= $this->model_ipost->strip_i_name($files['name'][$i]);
				
					$_FILES[$upload_field]['name'] 			= $files['name'][$i];
					$_FILES[$upload_field]['type'] 			= $files['type'][$i];
					$_FILES[$upload_field]['tmp_name'] 		= $files['tmp_name'][$i];
					$_FILES[$upload_field]['error'] 		= $files['error'][$i];
					$_FILES[$upload_field]['size'] 			= $files['size'][$i];
					
					//now we initialize the upload library
					$this->upload->initialize($config);
					if ($this->upload->do_upload($upload_field))
					{
						$data 								= $this->upload->data();
						
						switch($ipost_type)
						{
							case 'cat':
							case 'post':
							case 'cat_icon':
							case 'post_icon':
							case 'cat_thumb':
							case 'post_thumb':
							case 'cat_background':
							case 'post_background':
							$this->load->library('image_lib');
							$config = array(
						    'source_image'      => './upload/'.$data["file_name"],
						    'new_image'         => './upload/c_'.$data["file_name"],
						    'maintain_ratio'    => true,
						    'width'             => 150,
						    'height'            => 150
						    );
						    $this->image_lib->initialize($config);
						    $this->image_lib->resize();
						    $this->image_lib->clear();
							break;
						}
						
		            	$data_array 	= array(  
						'user_id' 		=> $user_id,  
						'ipost_all' 	=> $ipost_all,  
						'ipost_type' 	=> $ipost_type,
						'ipost' 		=> $data["file_name"],//$this->model_ipost->strip_i_name($config['file_name']),//$filename,  
						'ipost_slug' 	=> $this->model_ipost->strip_i_slug($this->model_ipost->ext_name($_FILES[$upload_field]['name'])),
						'ipost_title' 	=> $this->model_ipost->strip_i_title($this->model_ipost->ext_name($_FILES[$upload_field]['name'])), 
						'date' 			=> date('Y-m-d H:i:s'),  
						'status' 		=> '1'
						);
		                $this->model_ipost->ip_add($data_array);
                        setcookie('status_second','added_files', time()+2);
					}
					
					/*switch($ipost_type)
					{
						case 'cat':
						case 'post':
						$config['new_image'] = $config['upload_path'].$config['file_name'];
					    $config['width'] = 150;
					    $config['height'] = 150;

					    $this->image_lib->initialize($config); 

					    if ( ! $this->image_lib->resize())
					    {
					        // an error occured
					    }
						//$this->model_ipost->crop_images($config['upload_path'].$data["file_name"], $config['upload_path'].'c_'.$data["file_name"], '150', '150', $crop=1);
						break;
					}*/
				}
		    }
		}
        return true; 
    }
    function media_upload($upload_field,$ipost_type,$ipost_id) 
    { 
        if(isset($_FILES[$upload_field]['tmp_name']))
		{
			$number_of_files = sizeof($_FILES[$upload_field]['tmp_name']);
		    $files = $_FILES[$upload_field];
		    $errors = array();
		 
		    for($i=0;$i<$number_of_files;$i++)
		    {
		      if($_FILES[$upload_field]['error'][$i] != 0) $errors[$i][] = 'Couldn\'t upload file '.$_FILES[$upload_field]['name'][$i];
		    }
		    if(sizeof($errors)==0)
		    {
				$this->load->library('upload');
				
				for ($i = 0; $i < $number_of_files; $i++) 
				{
					switch($ipost_type)
					{
						case 'cat':
						case 'post':
						case 'cat_icon':
						case 'post_icon':
						case 'cat_thumb':
						case 'post_thumb':
						case 'cat_background':
						case 'post_background':
						$config['upload_path'] 				= FCPATH . './upload/';
						$config['allowed_types'] 			= 'gif|jpg|jpeg|png';
						$row_ipost	= $this->model_ipost->ip_value($ipost_id);
						unlink('./upload/'.$row_ipost->ipost);
						break;
						
						case 'banner':
						$config['upload_path'] 				= FCPATH . './upload/banner/';
						$config['allowed_types'] 			= 'gif|jpg|jpeg|png';
						$row_ipost	= $this->model_ipost->ip_value($ipost_id);
						unlink('./upload/banner/'.$row_ipost->ipost);
						break;
						
						case 'cat_pdf':
						case 'post_pdf':
						$config['upload_path'] 				= FCPATH . './upload/files/';
						$config['allowed_types'] 			= 'pdf';
						$row_ipost	= $this->model_ipost->ip_value($ipost_id);
						unlink('./upload/files/'.$row_ipost->ipost);
						break;
						
						case 'cat_files':
						case 'post_files':
						$config['upload_path'] 				= FCPATH . './upload/files/';
						$config['allowed_types'] 			= 'gif|jpg|jpeg|png|pdf|doc|docx|xls|xlsx|ppt|pptx|zip|rar';
						$row_ipost	= $this->model_ipost->ip_value($ipost_id);
						unlink('./upload/files/'.$row_ipost->ipost);
						break;
					}
					
					$config['file_name'] 				= $this->model_ipost->strip_i_name($files['name'][$i]);
				
					$_FILES[$upload_field]['name'] 	= $files['name'][$i];
					$_FILES[$upload_field]['type'] 	= $files['type'][$i];
					$_FILES[$upload_field]['tmp_name'] = $files['tmp_name'][$i];
					$_FILES[$upload_field]['error'] 	= $files['error'][$i];
					$_FILES[$upload_field]['size'] 	= $files['size'][$i];
					//now we initialize the upload library
					$this->upload->initialize($config);
					if ($this->upload->do_upload($upload_field))
					{
						$data 			= $this->upload->data();
		            	$data_array 	= array(
						'ipost' 		=> $data["file_name"]
						);
		                $this->model_ipost->ip_update($ipost_id,$data_array);
                        setcookie('status_second','added_files', time()+2);
					}
				}
		    }
		}
        return true; 
    }
    function ibanner_upload($upload_field,$ipost_type,$ipost_id) 
    { 
        if(isset($_FILES[$upload_field]['tmp_name']))
		{
			$number_of_files = sizeof($_FILES[$upload_field]['tmp_name']);
		    $files = $_FILES[$upload_field];
		    $errors = array();
		 
		    for($i=0;$i<$number_of_files;$i++)
		    {
		      if($_FILES[$upload_field]['error'][$i] != 0) $errors[$i][] = 'Couldn\'t upload file '.$_FILES[$upload_field]['name'][$i];
		    }
		    if(sizeof($errors)==0)
		    {
				$this->load->library('upload');
				
				for ($i = 0; $i < $number_of_files; $i++) 
				{
					switch($ipost_type)
					{
						case 'post':
						$config['upload_path'] 				= FCPATH . './upload/banner/';
						$config['allowed_types'] 			= 'gif|jpg|jpeg|png|svg';
						break;
						
						case 'swf':
						$config['upload_path'] 				= FCPATH . './upload/banner/';
						$config['allowed_types'] 			= 'swf';
						break;
					}
					
					$config['file_name'] 				= $this->model_ipost->strip_i_name($files['name'][$i]);
				
					$_FILES[$upload_field]['name'] 	= $files['name'][$i];
					$_FILES[$upload_field]['type'] 	= $files['type'][$i];
					$_FILES[$upload_field]['tmp_name'] = $files['tmp_name'][$i];
					$_FILES[$upload_field]['error'] 	= $files['error'][$i];
					$_FILES[$upload_field]['size'] 	= $files['size'][$i];
					//now we initialize the upload library
					
					$row_ipost	= $this->model_ipost->ip_value($ipost_id);
					
					if(file_exists('./upload/avatar/'.$row_ipost->ipost))
					{
						unlink('./upload/banner/'.$row_ipost->ipost);
					}
					
					$this->upload->initialize($config);
					if ($this->upload->do_upload($upload_field))
					{
						switch($ipost_type)
						{
							case 'post':
							$data 			= $this->upload->data();
			            	$data_array 	= array(  
							'ipost_id' 		=> $ipost_id, 
							'ipost_type' 	=> 'banner', 
							'ipost' 		=> $data["file_name"],//$this->model_ipost->strip_i_name($config['file_name']),//$filename,  
							// 'ipost_slug' 	=> $this->model_ipost->strip_i_slug($this->model_ipost->ext_name($_FILES[$upload_field]['name'])),
							// 'ipost_title' 	=> $this->model_ipost->strip_i_title($this->model_ipost->ext_name($_FILES[$upload_field]['name'])), 
							//'date' 			=> date('Y-m-d H:i:s')
							);
			                $this->model_ipost->ip_update($ipost_id,$data_array);
                            setcookie('status_second','added_images', time()+2);
							break;
							
							case 'swf':
							$data 			= $this->upload->data();
			            	$data_array 	= array(  
							'ipost_id' 		=> $ipost_id, 
							'ipost_type' 	=> 'banner',
							'ipost_files' 	=> $data["file_name"],//$this->model_ipost->strip_i_name($config['file_name']),//$filename,  
							// 'ipost_slug' 	=> $this->model_ipost->strip_i_slug($this->model_ipost->ext_name($_FILES[$upload_field]['name'])),
							// 'ipost_title' 	=> $this->model_ipost->strip_i_title($this->model_ipost->ext_name($_FILES[$upload_field]['name'])), 
							//'date' 			=> date('Y-m-d H:i:s')
							);
			                $this->model_ipost->ip_update($ipost_id,$data_array);
                            setcookie('status_second','added_files', time()+2);
							break;
						}
					}
				}
		    }
		}
        return true; 
    }
    function ipopup_upload($upload_field,$ipost_type) 
    { 
        if(isset($_FILES[$upload_field]['tmp_name']))
		{
			$number_of_files = sizeof($_FILES[$upload_field]['tmp_name']);
		    $files = $_FILES[$upload_field];
		    $errors = array();
		 
		    for($i=0;$i<$number_of_files;$i++)
		    {
		      if($_FILES[$upload_field]['error'][$i] != 0) $errors[$i][] = 'Couldn\'t upload file '.$_FILES[$upload_field]['name'][$i];
		    }
		    if(sizeof($errors)==0)
		    {
				$this->load->library('upload');
				
				for ($i = 0; $i < $number_of_files; $i++) 
				{
					switch($ipost_type)
					{
						case 'post':
						$config['upload_path'] 				= FCPATH . './upload/banner/';
						$config['allowed_types'] 			= 'gif|jpg|jpeg|png|svg';
						break;
						
						case 'swf':
						$config['upload_path'] 				= FCPATH . './upload/banner/';
						$config['allowed_types'] 			= 'swf';
						break;
					}
					
					$config['file_name'] 				= $this->model_ipost->strip_i_name($files['name'][$i]);
				
					$_FILES[$upload_field]['name'] 	= $files['name'][$i];
					$_FILES[$upload_field]['type'] 	= $files['type'][$i];
					$_FILES[$upload_field]['tmp_name'] = $files['tmp_name'][$i];
					$_FILES[$upload_field]['error'] 	= $files['error'][$i];
					$_FILES[$upload_field]['size'] 	= $files['size'][$i];
					//now we initialize the upload library
					
					$row_ipost	= $this->model_ipost->ip_value_type('popup');
					
					if(file_exists('./upload/avatar/'.$row_ipost->ipost))
					{
						unlink('./upload/banner/'.$row_ipost->ipost);
					}

					$this->upload->initialize($config);
					if ($this->upload->do_upload($upload_field))
					{
						switch($ipost_type)
						{
							case 'post':
							$data 			= $this->upload->data();
			            	$data_array 	= array( 
							'ipost' 		=> $data["file_name"],
							);
			                $this->model_ipost->ip_update_type('popup',$data_array);
                            setcookie('status_second','added_images', time()+2);
							break;
							
							case 'swf':
							$data 			= $this->upload->data();
			            	$data_array 	= array( 
							'ipost_files' 	=> $data["file_name"],
							);
			                $this->model_ipost->ip_update_type('popup',$data_array);
                            setcookie('status_second','added_files', time()+2);
							break;
						}
					}
				}
		    }
		}
        return true; 
    }
    function avatar_upload($upload_field,$user_id)
    {
        if(isset($_FILES[$upload_field]['tmp_name']))
        {
            $number_of_files = sizeof($_FILES[$upload_field]['tmp_name']);
            $files = $_FILES[$upload_field];
            $errors = array();

            for($i=0;$i<$number_of_files;$i++)
            {
                if($_FILES[$upload_field]['error'][$i] != 0) $errors[$i][] = 'Couldn\'t upload file '.$_FILES[$upload_field]['name'][$i];
            }
            if(sizeof($errors)==0)
            {
                $this->load->library('upload');

                for ($i = 0; $i < $number_of_files; $i++)
                {
                    $config['upload_path'] 				= FCPATH . './upload/avatar/';
                    $config['allowed_types'] 			= 'gif|jpg|jpeg|png';

                    $config['file_name'] 				= $this->model_ipost->strip_i_name($files['name'][$i]);

                    $_FILES[$upload_field]['name'] 	    = $files['name'][$i];
                    $_FILES[$upload_field]['type'] 	    = $files['type'][$i];
                    $_FILES[$upload_field]['tmp_name']  = $files['tmp_name'][$i];
                    $_FILES[$upload_field]['error'] 	= $files['error'][$i];
                    $_FILES[$upload_field]['size'] 	    = $files['size'][$i];
                    //now we initialize the upload library

                    $row_user	= $this->model_member->us_edit($user_id);

					if(file_exists('./upload/avatar/'.$row_user->user_avatar))
					{
						unlink('./upload/avatar/'.$row_user->user_avatar);
					}
                    
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload($upload_field))
                    {
                        $data 			= $this->upload->data();
                        $data_array 	= array(
                            'user_avatar' 	=> $data["file_name"]
                        );
                        $this->ion_auth->update($user_id,$data_array);
                        setcookie('status_second','updated_avatar', time()+2);
                    }
                }
            }
        }
        return true;
    }
    function crop_images($src, $dst, $width, $height, $crop=1)
	{
		if(!list($w, $h) = getimagesize($src)) return "Unsupported picture type!";	  
		$type = strtolower(substr(strrchr($src,"."),1));
		if($type == 'jpeg') $type = 'jpg';
		switch($type){
		case 'bmp': $img = imagecreatefromwbmp($src); break;
		case 'gif': $img = imagecreatefromgif($src); break;
		case 'jpg': $img = imagecreatefromjpeg($src); break;
		case 'png': $img = imagecreatefrompng($src); break;
		default : return "Unsupported picture type!"; }
		if($crop){// resize
		if($w < $width or $h < $height) return "Picture is too small!";
		$ratio = max($width/$w, $height/$h);
		$h = $height / $ratio;
		$x = ($w - $width / $ratio) / 2;
		$w = $width / $ratio; }
		else{
		if($w < $width and $h < $height) return "Picture is too small!";
		$ratio = min($width/$w, $height/$h);
		$width = $w * $ratio;
		$height = $h * $ratio;
		$x = 0; }	  
		$new = imagecreatetruecolor($width, $height);
		if($type == "gif" or $type == "png"){// preserve transparency
		imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
		imagealphablending($new, false);
		imagesavealpha($new, true);}	  
		imagecopyresampled($new, $img, 0, 0, $x, 0, $width, $height, $w, $h);	  
		switch($type){
		case 'bmp': imagewbmp($new, $dst); break;
		case 'gif': imagegif($new, $dst); break;
		case 'jpg': imagejpeg($new, $dst); break;
		case 'png': imagepng($new, $dst); break;
		}
		return true;
	}
	function resize_images($src, $dst, $width)
	{
		$type = strtolower(substr(strrchr($src,"."),1));
		if($type == 'jpeg') $type = 'jpg';
		switch($type){
		case 'bmp': $img = imagecreatefromwbmp($src); break;
		case 'gif': $img = imagecreatefromgif($src); break;
		case 'jpg': $img = imagecreatefromjpeg($src); break;
		case 'png': $img = imagecreatefrompng($src); break;
		default : return "Unsupported picture type!"; }
		list($w, $h) = getimagesize($src);
		$height = ($width/$w) * $h;
		$new = imagecreatetruecolor($width, $height);  
		if($type == "gif" or $type == "png"){// preserve transparency
		imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
		imagealphablending($new, false);
		imagesavealpha($new, true); }	  
		imagecopyresampled($new, $img, 0, 0, 0, 0, $width, $height, $w, $h);	  
		switch($type){
		case 'bmp': imagewbmp($new, $dst); break;
		case 'gif': imagegif($new, $dst); break;
		case 'jpg': imagejpeg($new, $dst); break;
		case 'png': imagepng($new, $dst); break;}
		return true;
	}
	function setMemoryForImage($filename) 
	{ 
	   $imageInfo    = getimagesize($filename); 
	   $memoryNeeded = round(($imageInfo[0] * $imageInfo[1] * $imageInfo['bits'] * $imageInfo['channels'] / 8 + Pow(2, 16)) * 1.65); 
	   
	   $memoryLimit = (int) ini_get('memory_limit')*1048576; 

	  if ((memory_get_usage() + $memoryNeeded) > $memoryLimit) 
	   { 
	     ini_set('memory_limit', ceil((memory_get_usage() + $memoryNeeded + $memoryLimit)/1048576).'M'); 
	     return (true); 
	   } 
	   else return(false); 
	} 
	function scanDirectories($rootDir, $allData=array()) 
	{
    // set filenames invisible if you want
	    $invisibleFileNames = array(".", "..", ".htaccess", ".htpasswd");
	    // run through content of root directory
	    $dirContent = scandir($rootDir);
	    foreach($dirContent as $key => $content) {
	        // filter all files not accessible
	        $path = $rootDir.'/'.$content;
	        if(!in_array($content, $invisibleFileNames)) {
	            // if content is file & readable, add to array
	            if(is_file($path) && is_readable($path)) {
	                // save file name with path
	                $allData[] = $path;
	            // if content is a directory and readable, add path and name
	            }elseif(is_dir($path) && is_readable($path)) {
	                // recursive callback to open new directory
	                $allData = $this->model_ipost->scanDirectories($path, $allData);
	            }
	        }
	    }
	    return $allData;
	}
	function dl_files($input,$output)
	{
		$ch = curl_init ($input);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
		$rawdata=curl_exec ($ch);
		curl_close ($ch);
		$fp = fopen($output,'w');
		fwrite($fp, $rawdata);
		fclose($fp);
		return;
	}	
}