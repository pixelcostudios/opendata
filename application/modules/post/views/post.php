<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Artikel</title>
    <link href="<?php echo base_url()?>themes/default/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
    <!-- <link href="<?php echo base_url()?>themes/default/css/plugins/iCheck/custom.css" rel="stylesheet"> -->
    <link href="<?php echo base_url()?>themes/default/css/animate.css" rel="stylesheet">
    <!-- Toastr style -->
    <link href="<?php echo base_url()?>themes/default/css/plugins/select2/select2.min.css" rel="stylesheet">

    <link href="<?php echo base_url()?>themes/default/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/custom.css" rel="stylesheet">

</head>

<body class="fixed-sidebar">
    <div id="wrapper">
    <?php $this->load->view('menu/nav');?>

        <div id="page-wrapper" class="gray-bg">
        <?php $this->load->view('menu/nav_top');?>
        
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Artikel</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>panel/dashboard.html">Home</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url()?>panel/post/manage.html">Artikel</a>
                        </li>
                        <li class="active">
                            <strong>All Artikel</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
            <form method="post">
                <div class="col-lg-12 padding-none">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>All Artikel</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config 1</a>
                                    </li>
                                    <li><a href="#">Config 2</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                            	<div class="col-sm-3 report_pilot">
                                    <div class="input-group"><input type="text" name="search" placeholder="Search" class="input-sm form-control" data-validation="required"> <span class="input-group-btn">
                                        <button type="submit" name="Pixel_Search" class="btn btn-sm btn-primary"> Go!</button> </span></div>
                                </div>
                                <div class="col-sm-6 m-b-xs">
                                    <div data-toggle="buttons" class="btn-group">
                                        <label onclick="location.href='<?php echo base_url()?>panel/post/add.html'" class="btn btn-sm btn-primary"> <i class="fa fa-plus"></i> Tambah Artikel </label>
                                    </div>
                                </div>
                                <div class="col-sm-2 m-b-xs">
                                <?php if($this->uri->segment(3)=='manage'){?>
                                <select class="category_list input-sm form-control input-s-sm inline float-left category" id="select-cat" onchange="window.location = jQuery('#select-cat option:selected').val();">
                                    <option value="<?php echo base_url()?>panel/post/manage.html">All Category</option>
                                    <?php foreach($cat_list as $row_cat){?>
									<option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo 'manage';}else{echo $this->uri->segment(3);}?>/<?php echo $row_cat->cat_id;?>/<?php if($this->uri->segment(5)==''){echo '10';}else{echo $this->uri->segment(5);}?>/<?php if($this->uri->segment(6)==''){echo '1';}else{echo $this->uri->segment(6);}?>.html" <?php if($row_cat->cat_id==$this->uri->segment(4)){?>selected=""<?php }?> ><?php echo $row_cat->cat_title;?></option>
                                        <?php $cat_list_sub = $this->model_nav->ca_list($row_cat->cat_type,$row_cat->cat_id); foreach($cat_list_sub as $row_cat_sub){?>
                                            <option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo 'manage';}else{echo $this->uri->segment(3);}?>/<?php echo $row_cat_sub->cat_id;?>/<?php if($this->uri->segment(5)==''){echo '10';}else{echo $this->uri->segment(5);}?>/<?php if($this->uri->segment(6)==''){echo '1';}else{echo $this->uri->segment(6);}?>.html" <?php if($row_cat_sub->cat_id==$this->uri->segment(4)){?>selected=""<?php }?> >|-- <?php echo $row_cat_sub->cat_title;?></option>
                                            <?php $cat_list_sub1 = $this->model_nav->ca_list($row_cat_sub->cat_type,$row_cat_sub->cat_id); foreach($cat_list_sub1 as $row_cat_sub1){?>
                                                <option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo 'manage';}else{echo $this->uri->segment(3);}?>/<?php echo $row_cat_sub1->cat_id;?>/<?php if($this->uri->segment(5)==''){echo '10';}else{echo $this->uri->segment(5);}?>/<?php if($this->uri->segment(6)==''){echo '1';}else{echo $this->uri->segment(6);}?>.html" <?php if($row_cat_sub1->cat_id==$this->uri->segment(4)){?>selected=""<?php }?> >|--|-- <?php echo $row_cat_sub1->cat_title;?></option>
                                            <?php }?>
                                        <?php }?>
                                    <?php }?>
                                </select>
                                <?php }?>
                                </div>
                                <div class="col-sm-1 m-b-xs">
                                <select class="category_list input-sm form-control input-s-sm inline float-right category-show" id="select-view" onchange="window.location = jQuery('#select-view option:selected').val();">
                                    <option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo 'manage';}else{echo $this->uri->segment(3);}?>.html">Filter</option>
                                    <?php if($this->uri->segment(6)){?>
                                    <option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo 'search';}else{echo $this->uri->segment(3);}?>/<?php echo $this->uri->segment(4);?>/10/<?php if($this->uri->segment(6)==''){echo '1';}else{echo $this->uri->segment(6);}?>.html" <?php if($this->uri->segment(5)=='10'){?>selected=""<?php }?>>10</option>
                                    <option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo 'search';}else{echo $this->uri->segment(3);}?>/<?php echo $this->uri->segment(4);?>/25/<?php if($this->uri->segment(6)==''){echo '1';}else{echo $this->uri->segment(6);}?>.html" <?php if($this->uri->segment(5)=='25'){?>selected=""<?php }?>>25</option>
                                    <option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo 'search';}else{echo $this->uri->segment(3);}?>/<?php echo $this->uri->segment(4);?>/50/<?php if($this->uri->segment(6)==''){echo '1';}else{echo $this->uri->segment(6);}?>.html" <?php if($this->uri->segment(5)=='50'){?>selected=""<?php }?>>50</option>
                                    <option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo 'search';}else{echo $this->uri->segment(3);}?>/<?php echo $this->uri->segment(4);?>/100/<?php if($this->uri->segment(6)==''){echo '1';}else{echo $this->uri->segment(6);}?>.html" <?php if($this->uri->segment(5)=='100'){?>selected=""<?php }?>>100</option>
                                    <?php }else{?>
                                    <option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo 'manage';}else{echo $this->uri->segment(3);}?>/0/10/<?php if($this->uri->segment(5)==''){echo '1';}else{echo $this->uri->segment(5);}?>.html" <?php if($this->uri->segment(4)=='10'){?>selected=""<?php }?>>10</option>
                                    <option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo 'manage';}else{echo $this->uri->segment(3);}?>/0/25/<?php if($this->uri->segment(5)==''){echo '1';}else{echo $this->uri->segment(5);}?>.html" <?php if($this->uri->segment(4)=='25'){?>selected=""<?php }?>>25</option>
                                    <option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo 'manage';}else{echo $this->uri->segment(3);}?>/0/50/<?php if($this->uri->segment(5)==''){echo '1';}else{echo $this->uri->segment(5);}?>.html" <?php if($this->uri->segment(4)=='50'){?>selected=""<?php }?>>50</option>
                                    <option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo 'manage';}else{echo $this->uri->segment(3);}?>/0/100/<?php if($this->uri->segment(5)==''){echo '1';}else{echo $this->uri->segment(5);}?>.html" <?php if($this->uri->segment(4)=='100'){?>selected=""<?php }?>>100</option>
                                    <?php }?>
                                </select>
                                </div>
                            </div>
                            <div class="table-responsive margin-top-10">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>

                                        <th width="1%">
                                        	<label>
	                                                <input type="checkbox" class="colored-green checkall">
	                                                <span class="text"></span>
	                                            </label>
                                        </th>
                                        <th width="5%">Thumb </th>
                                        <th>Title </th>
                                        <th>Category </th>
                                        <th width="13%">Date</th>
                                        <th width="5%">Status</th>
                                        <th width="5%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php for ($i = 0; $i < count($post_list); ++$i) { 
                                        $images = $this->model_ipost->ip_list_value($post_list[$i]->post_id,'post','DESC');
                                        $category_list = $this->model_meta->me_source_list('category',$post_list[$i]->post_id);?>
                                    <tr>
                                        <td>
                                        	<label>
                                                <input type="checkbox" name="post-check[]" value="<?php echo $post_list[$i]->post_id; ?>" class="colored-green">
                                                <span class="text"></span>
                                            </label>
                                        </td>
                                        <td><img src="<?php echo base_url()?>upload/<?php if($images!=''){echo 'crop/50/50/c_'.$images;}else{echo 'default.png';}?>" width="20" height="20"></td>
                                        <td><a href="<?php echo base_url()?>panel/post/edit/<?php echo $post_list[$i]->post_id; ?>.html"><?php echo $post_list[$i]->post_title; ?></a> &nbsp;&nbsp;<a target="_blank" href="<?php echo base_url().$post_list[$i]->post_slug; ?>.html"><i class="fa fa-search"></i></a></td>
                                        <td><?php foreach ($category_list as $key) { echo $key->meta_title.', ';}?></td>
                                        <td><?php echo $post_list[$i]->date; ?></td>
                                        <td><?php if($post_list[$i]->status=='1'){ ?><span class="label label-primary">Publish</span><?php }else{?><span class="label label-warning">Draft</span><?php }?></td>
                                        <td>
                                        	<div class="btn-group">
	                                        <a class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
	                                            Action <i class="fa fa-angle-down"></i>
	                                        </a>
	                                        <ul class="dropdown-menu drop-right">
	                                            <li><a href="<?php echo base_url()?>panel/post/edit/<?php echo $post_list[$i]->post_id; ?>.html">Edit</a></li>
	                                            <li><a data-toggle="modal" href="<?php echo base_url();?>panel/ajax/post_delete/<?php echo $post_list[$i]->post_id; ?>" data-target="#modal_deleted">Delete</a></li>
	                                        </ul>
                                			</div>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                                    <?php if(count($post_list)>0){?>
                                    <tfoot>
	                                <tr>
	                                    <td colspan="7" class="footable-visible">
	                                    	<div class="pull-left margin-top-20">
		                                    	<div class="btn-group">
			                                    <div class="btn-group dropup">
			                                        <button type="button" class="btn btn-default btn-custom button-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
			                                            <i class="fa fa-ellipsis-horizontal"></i> Action <i class="fa fa-angle-up"></i>
			                                        </button>
			                                    <ul class="dropdown-menu dropdown-archive">
    			                                    <li class="margin-bottom-5">
                                                        <button type="button" data-toggle="modal" data-target="#modal_publish" class="btn btn-default btn-sm"><i class="fa fa-check"></i> Publish</button>
                                                    </li>
                                                    <li class="margin-bottom-5">
                                                        <button type="button" data-toggle="modal" data-target="#modal_draft" class="btn btn-default btn-sm"><i class="fa fa-pencil-square-o"></i> UnPublish</button>
                                                    </li>
                                                    <!-- <li class="margin-bottom-5">
                                                        <button type="button" data-toggle="modal" data-target="#modal_user" class="btn btn-default btn-sm"><i class="fa fa-user"></i> Change Owner</button>
                                                    </li> -->
                                                    <li>
                                                        <button type="button" data-toggle="modal" data-target="#modal_delete" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i> Delete</button>
                                                    </li>
			                                    </ul>
			                                    </div>
			                                </div>
		                                    	</div>
	                                    	<div class="float-right">
                                                <ul class="pagination pull-right">

                                                    <!-- Show pagination links -->
                                                    <?php foreach ($links as $link) {
                                                        echo "<li>". $link."</li>";
                                                    } ?>
	                                    	</div>
	                                        
	                                    </td>
	                                </tr>
	                                </tfoot>
                                    <?php }?>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            <div class="modal fade" id="modal_publish">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Are you sure ?</h4>

                      </div>
                      <div class="modal-body">
                      <p>Do you want to change this Post to Publish.</p>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="post-publish" value="publish" class="btn btn-primary">Publish</button>
                      </div>
                </div>
              </div>
            </div>
            <div class="modal fade" id="modal_draft">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Are you sure ?</h4>

                      </div>
                      <div class="modal-body">
                      <p>Do you want to change this Post to Draft.</p>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="post-draft" value="draft" class="btn btn-warning">UnPublish</button>
                      </div>
                </div>
              </div>
            </div>
            <!-- <div class="modal fade" id="modal_user">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Are you sure ?</h4>

                      </div>
                      <div class="modal-body">
                      <p>Do you want to change this Post to this User.</p>
                      <select class="select2_demo_1 form-control" name="user_id">
                      <option value="0">Select User</option>
                        </select>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="post-user" value="draft" class="btn btn-success">Change</button>
                      </div>
                </div>
              </div>
            </div> -->
            <div class="modal fade" id="modal_delete">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Are you sure ?</h4>

                      </div>
                      <div class="modal-body">
                      <p>Do you want to delete this Post.</p>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="post-delete" value="delete" class="btn btn-danger">Delete</button>
                      </div>
                </div>
              </div>
            </div>

            <div class="modal fade" id="modal_deleted">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                </div>
              </div>
            </div>
			</form>
            </div>
        </div>
        <div class="footer">
            <div class="pull-right">
                <strong><?php echo $this->model_nav->disk_size();?></strong> Free.
            </div>
            <div>
                <strong>Copyright</strong> <?php echo $this->model_setting->setting('company');?> &copy; <?php echo date('Y');?>
            </div>
        </div>

        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="<?php echo base_url()?>themes/default/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Peity -->
    <!-- <script src="<?php echo base_url()?>themes/default/js/plugins/peity/jquery.peity.min.js"></script> -->

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url()?>themes/default/js/inspinia.js"></script>
    <!-- <script src="<?php echo base_url()?>themes/default/js/plugins/pace/pace.min.js"></script> -->
    <!--<script src="<?php echo base_url()?>themes/default/js/plugins/validation/jquery.form-validator.min.js"></script>-->

    <!-- Check -->
    <script src="<?php echo base_url()?>themes/default/js/check.js"></script>
    <!-- Select2 -->
    <script src="<?php echo base_url()?>themes/default/js/plugins/select2/select2.full.min.js"></script>

    <!-- Peity -->
    <!-- <script src="<?php echo base_url()?>themes/default/js/demo/peity-demo.js"></script> -->
    <!-- Toastr script -->
    <script src="<?php echo base_url()?>themes/default/js/plugins/toastr/toastr.min.js"></script>
    <script>
        $(document).ready(function()
        {
            $(".category_list").select2();
            // $(".date-picker").datetimepicker({minView: 2,format: 'yyyy-mm-dd',autoclose: true,});
            // $(".date-picker-month").datepicker({
            //     startView: 1,
            //     minViewMode: 1,
            //     autoclose: true
            // });
            // $.validate({
            //     modules: 'location, date, security, file'
            // });
        });
        $(function () {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": true,
                "preventDuplicates": false,
                "positionClass": "toast-bottom-right",
                "onclick": null,
                "showDuration": "400",
                "hideDuration": "1000",
                "timeOut": "7000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            //toastr.success('Successfully Update Profile', 'Update Profile');
            <?php if(isset($_COOKIE['status'])){ echo $this->model_status->status($_COOKIE['status']);}?>
            <?php if(isset($_COOKIE['status_second'])){ echo $this->model_status->status($_COOKIE['status_second']);}?>
        });
    </script>
    <script>
        // $(document).ready(function(){
        //     $.ajax({
        //     type: 'GET',
        //     url: '<?php echo base_url();?>panel/ajax/user',
        //     dataType: 'json',
        //     success: function(data) {
        //      $.each(data, function (i, item) {
        //          user_id        = item.user_id;
        //          user_fullname  = item.user_fullname;
        //          if($(".select2_demo_1 option[value='"+user_id+"']").length == 0){
        //             $('.select2_demo_1').append('<option value="'+user_id+'">' + user_fullname + '</option>');
        //          }
        //     }); 
        //     $('.select2_demo_1').select2();
        //     },
        //     error: function() {
        //         alert('error loading items');
        //     }
        //     });
        // });
        // $(document).ready(function(){
        //     $('.i-checks').iCheck({
        //         checkboxClass: 'icheckbox_square-green',
        //         radioClass: 'iradio_square-green',
        //     });
        // });
    </script>

</body>

</html>
