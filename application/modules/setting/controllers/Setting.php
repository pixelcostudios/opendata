<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//require APPPATH . "third_party/MX/Controller.php";

	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
        $this->load->library('email');
        $this->load->helper(array('form'));
        $this->load->helper(array('url'));
		$this->load->helper(array('path'));
		$this->load->library("pagination");
		$this->load->model("menu/model_nav");
		$this->load->model("model_setting");
		$this->load->model("model_meta");
		$this->load->model("model_status");
	}
	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
    }
    public function contact()
    {
        if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		$this->load->model("model_setting");
		$this->load->model("menu/model_nav");
        $this->load->model("model_status");
		
		$data['list_post_count'] 	    = $this->model_nav->po_list_count('post','0');
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count'] 	    = $this->model_nav->us_list_count();
        $data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);
		
		$data['company_1']			= $this->model_setting->setting_list('company_1');
		$data['company_2']			= $this->model_setting->setting_list('company_2');
		$data['company_3']			= $this->model_setting->setting_list('company_3');
		$data['company_4']			= $this->model_setting->setting_list('company_4');
		
		if(isset($_POST['Pixel_Save']))
		{
			$i=0; 
			foreach($data['company_1'] as $row_value)
			{	$i++; 
				if($row_value->setting_status=='1')
				{
					$result 	= ($this->input->post($row_value->setting_name) != '') ? $this->input->post($row_value->setting_name) : '';
					
					$data_array = array(
					'setting_value' 		=> $result 
					);  
					$this->model_setting->setting_update($row_value->setting_name,$data_array);
				}
			}
			foreach($data['company_2'] as $row_value)
			{	$i++; 
				if($row_value->setting_status=='1')
				{
					$result 	= ($this->input->post($row_value->setting_name) != '') ? $this->input->post($row_value->setting_name) : '';
					
					$data_array = array(
					'setting_value' 		=> $result 
					);  
					$this->model_setting->setting_update($row_value->setting_name,$data_array);
				}
			}
			foreach($data['company_3'] as $row_value)
			{	$i++; 
				if($row_value->setting_status=='1')
				{
					$result 	= ($this->input->post($row_value->setting_name) != '') ? $this->input->post($row_value->setting_name) : '';
					
					$data_array = array(
					'setting_value' 		=> $result 
					);  
					$this->model_setting->setting_update($row_value->setting_name,$data_array);
				}
			}
			foreach($data['company_4'] as $row_value)
			{	$i++; 
				if($row_value->setting_status=='1')
				{
					$result 	= ($this->input->post($row_value->setting_name) != '') ? $this->input->post($row_value->setting_name) : '';
					
					$data_array = array(
					'setting_value' 		=> $result 
					);  
					$this->model_setting->setting_update($row_value->setting_name,$data_array);
				}
			}
			setcookie('status','updated', time()+2);
            redirect(base_url(uri_string()).'.html');
		}
		$this->load->view('setting_contact', $data);
    }
    public function meta()
    {
        if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        $this->load->model("model_setting");
        $this->load->model("menu/model_nav");
        $this->load->model("model_status");
        
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);
        
        if(isset($_POST['Pixel_Save']))
        {
            $meta_title     = ($this->input->post('meta_title') != '') ? $this->input->post('meta_title') : '';
            $data_array = array(
            'setting_value'         => $meta_title
            );  
            $this->model_setting->setting_update('meta_title',$data_array);
            
            $meta_keyword   = ($this->input->post('meta_keyword') != '') ? $this->input->post('meta_keyword') : '';
            $data_array = array(
            'setting_value'         => $meta_keyword
            );  
            $this->model_setting->setting_update('meta_keyword',$data_array);
            
            $meta_desc      = ($this->input->post('meta_desc') != '') ? $this->input->post('meta_desc') : '';
            $data_array = array(
            'setting_value'         => $meta_desc 
            );  
            $this->model_setting->setting_update('meta_desc',$data_array);

            setcookie('status','updated', time()+2);
            redirect(base_url(uri_string()).'.html');
        }
        //$data['setting_meta']     = unserialize(base64_decode($this->model_setting->setting('meta')));
        $this->load->view('setting_meta', $data);
    }

    public function robots()
    {
        if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		$this->load->model("model_setting");
		$this->load->model("menu/model_nav");
        $this->load->model("model_status");
		
		$data['list_post_count'] 	    = $this->model_nav->po_list_count('post','0');
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count'] 	    = $this->model_nav->us_list_count();
        $data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);
		
		if(isset($_POST['Pixel_Save']))
		{
			$result 	= ($this->input->post('meta_robots') != '') ? $this->input->post('meta_robots') : '';
					
			$data_array = array(
			'setting_value' 		=> $result 
			);  
			$this->model_setting->setting_update('meta_robots',$data_array);

            setcookie('status','updated', time()+2);
            redirect(base_url(uri_string()).'.html');
		}
		//$data['setting_meta'] 	= unserialize(base64_decode($this->model_setting->setting('meta')));
		$this->load->view('setting_robots', $data);
    }
    public function social_network()
    {
        if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		$this->load->model("model_setting");
		$this->load->model("menu/model_nav");
        $this->load->model("model_status");
		
		$data['list_post_count'] 	    = $this->model_nav->po_list_count('post','0');
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count'] 	    = $this->model_nav->us_list_count();
        $data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);
		
		$data['social_network']		= $this->model_setting->setting_list('social');

		if(isset($_POST['Pixel_Save']))
		{
			$i=0; 
			foreach($data['social_network'] as $row_social)
			{	$i++; 
				if($row_social->setting_status=='1')
				{
					$result 	= ($this->input->post($row_social->setting_name) != '') ? $this->input->post($row_social->setting_name) : '';
					
					$data_array = array(
					'setting_value' 		=> $result 
					);  
					$this->model_setting->setting_update($row_social->setting_name,$data_array);
				}
			}
			
			setcookie('status','updated', time()+2);
            redirect(base_url(uri_string()).'.html');
		}
		$this->load->view('setting_social_network', $data);
    }

    public function verified()
    {
        if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		$this->load->model("model_setting");
		$this->load->model("menu/model_nav");
        $this->load->model("model_status");
		
		$data['list_post_count'] 	    = $this->model_nav->po_list_count('post','0');
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count'] 	    = $this->model_nav->us_list_count();
        $data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);
		
		if(isset($_POST['Pixel_Save']))
		{
			$result 	= ($this->input->post('meta_verified') != '') ? $this->input->post('meta_verified') : '';
					
			$data_array = array(
			'setting_value' 		=> $result 
			);  
			$this->model_setting->setting_update('meta_verified',$data_array);

            setcookie('status','updated', time()+2);
            redirect(base_url(uri_string()).'.html');
		}
		$this->load->view('setting_verified', $data);
    }
    public function visitor()
    {
        if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		$this->load->model("model_setting");
		$this->load->model("menu/model_nav");
        $this->load->model("model_status");
		
		$data['list_post_count'] 	    = $this->model_nav->po_list_count('post','0');
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count'] 	    = $this->model_nav->us_list_count();
        $data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);
		
		if(isset($_POST['Pixel_Save']))
		{
			$result 	= ($this->input->post('meta_visitor') != '') ? $this->input->post('meta_visitor') : '';
					
			$data_array = array(
			'setting_value' 		=> $result 
			);  
			$this->model_setting->setting_update('meta_visitor',$data_array);

            setcookie('status','updated', time()+2);
            redirect(base_url(uri_string()).'.html');
		}
		$this->load->view('setting_visitor', $data);
	}
	public function backup_db()
    {
        if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        $this->load->helper('directory');
        
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);
        
        $files_array                    = directory_map('./upload/files/db/', FALSE, TRUE);

        if(count($files_array)>0)
        {
            $config = array();
            $config["base_url"]         = base_url().'panel/setting/backup_db/';
            $total_row                  = count($files_array);
            $config['suffix']           = '.html';
            $config['first_url']        = '1.html';
            $config["total_rows"]       = $total_row;
            $config["per_page"]         = '10';
            $config['use_page_numbers'] = TRUE;
            $config['num_links']        = $total_row;
            $config['cur_tag_open']     = '&nbsp;<a class="active">';
            $config['cur_tag_close']    = '</a>';
            $config['next_link']        = 'Next';
            $config['prev_link']        = 'Prev';
            $config['num_links']        = 5;

            $this->pagination->initialize($config);
            if($this->uri->segment(3))
            {
                $page = ($this->uri->segment(3)-1)*$config["per_page"] ;
            }
            else
            {
                $page = 0;
            }
            //print_r($page);
            $data["post_list"]          = array_slice($files_array, $page, $config["per_page"]);
            $str_links                  = $this->pagination->create_links();
            $data["links"]              = explode('&nbsp;',$str_links );  
        }
        else
        {
            $data["post_list"]          = $files_array;
        }

        //echo count($data['post_list']);
        //print_r($post_list);

        if(isset($_POST['Pixel_Backup_DB']))
        {
            $this->model_setting->backup_tables();

            setcookie('status','updated', time()+2);
            redirect(base_url().'panel/setting/backup_db.html');
        }
        elseif(isset($_POST['post-delete']) && $_POST['post-delete'] != "")
        {
            for ($i=0; $i<count($_POST['post-check']);$i++)
            {
                $files_name=$_POST['post-check'][$i];

                @unlink('./upload/files/db/'.$files_name);
            }
            setcookie('status','deleted_files', time()+2);
            redirect(base_url().'panel/setting/backup_db.html');
        }
        $this->load->view('setting_backup_db', $data);
    }
}
