<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

class model_setting extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}
    function setting($setting_name)
    {
        $this->db->select('setting_name, setting_value');
        $this->db->from("my_setting");
        $this->db->where('setting_name',$setting_name);
        $result	= $this->db->get();
        return $result->row('setting_value');;
    }
    function setting_value($setting_name,$setting_value)
    {
        $this->db->select('setting_name, setting_value, setting_explain, setting_status');
        $this->db->from("my_setting");
        $this->db->where('setting_name',$setting_name);
        $result	= $this->db->get();
        return $result->row($setting_value);;
    }
	function setting_edit($setting_name)
	{
		$this->db->select('setting_name, setting_type, setting_value, setting_explain, setting_status');
		$this->db->from("my_setting");
		$this->db->where('setting_name',$setting_name);
		$result	= $this->db->get();
		return $result->row();
	}
	function setting_list($setting_type)
    {
        $this->db->select('setting_name, setting_type, setting_value, setting_explain, setting_status');
        $this->db->from("my_setting");
        $this->db->where('setting_type',$setting_type);
        //$this->db->order_by('setting_name','ASC');
        $query	= $this->db->get();
        return $query->result();
    }
	function setting_cat($start,$limit)
	{
        $this->db->select('setting_name, setting_type, setting_value, setting_explain, setting_status');
        $this->db->from("my_setting");
		$this->db->limit($start,$limit);
		$this->db->order_by('setting_name','ASC');
		$query	= $this->db->get();
		return $query->result();
	}
	function setting_cat_count()
	{
        $this->db->select('setting_name, setting_type, setting_value, setting_explain, setting_status');
        $this->db->from("my_setting");
        $this->db->order_by('setting_name','ASC');
		return $this->db->count_all_results();
	}
    function setting_type($setting_type,$start,$limit)
    {
        $this->db->select('setting_name, setting_type, setting_value, setting_explain, setting_status');
        $this->db->from("my_setting");
        $this->db->where('setting_type',$setting_type);
        $this->db->limit($start,$limit);
        $this->db->order_by('setting_name','ASC');
        $query	= $this->db->get();
        return $query->result();
    }
    function setting_type_count($setting_type)
    {
        $this->db->select('setting_type');
        $this->db->from("my_setting");
        $this->db->where('setting_type',$setting_type);
        return $this->db->count_all_results();
    }
    function setting_name_count($setting_name)
    {
        $this->db->select('setting_name');
        $this->db->from("my_setting");
        $this->db->where('setting_name',$setting_name);
        return $this->db->count_all_results();
    }
	function setting_search($cat,$start,$limit)
	{
        $this->db->select('setting_name, setting_type, setting_value, setting_explain, setting_status');
        $this->db->from("my_setting");
        $this->db->group_start();
		$this->db->like('setting_name',$cat);
        $this->db->or_like('setting_value',$cat);
        $this->db->or_like('setting_explain',$cat);
        $this->db->group_end();
		$this->db->limit($start,$limit);
		$this->db->order_by('setting_name','ASC');
		$query	= $this->db->get();
		return $query->result();
	}
	function setting_search_count($cat)
	{
        $this->db->select('setting_name, setting_type, setting_value, setting_explain, setting_status');
        $this->db->from("my_setting");
        $this->db->group_start();
        $this->db->like('setting_name',$cat);
        $this->db->or_like('setting_value',$cat);
        $this->db->or_like('setting_explain',$cat);
        $this->db->group_end();
        $this->db->order_by('setting_name','ASC');
		return $this->db->count_all_results();
	}
	function setting_menu($cat,$start,$limit)
	{
        $this->db->select('setting_name, setting_type, setting_value, setting_explain, setting_status');
        $this->db->from("my_setting");
        $this->db->group_start();
		$this->db->like('setting_type',$cat);
        $this->db->group_end();
		$this->db->limit($start,$limit);
		$this->db->order_by('setting_name','ASC');
		$query	= $this->db->get();
		return $query->result();
	}
	function setting_menu_count($cat)
	{
        $this->db->select('setting_name, setting_type, setting_value, setting_explain, setting_status');
        $this->db->from("my_setting");
        $this->db->group_start();
        $this->db->like('setting_type',$cat);
        $this->db->group_end();
        $this->db->order_by('setting_name','ASC');
		return $this->db->count_all_results();
	}
	function setting_group_type()
    {
        $this->db->select('DISTINCT(setting_type)');
        $this->db->from("my_setting");
        $this->db->order_by('setting_type','ASC');
        $query	= $this->db->get();
        return $query->result();
    }
	function setting_add($data_array)
	{
		$this->db->insert('my_setting',$data_array);
		return $this->db->insert_id();
	}
	function setting_update($setting_name,$data_array)
	{
		$this->db->where('setting_name', $setting_name);
		$this->db->update('my_setting',$data_array);
		return true;
	}
		function setting_update_status($setting_name,$setting_status)
	{
		$this->db->set('setting_status', $setting_status); 
		$this->db->where('setting_name', $setting_name);
		$this->db->update('my_setting');
		return true;
	}
	function setting_delete($setting_name)
	{
		$this->db->where('setting_name',$setting_name);
		$this->db->delete('my_setting');
		return true;
	}
	function setting_social()
	{
		$result = array('Amazon', 'Android', 'Apple', 'Badoo', 'Behance', 'Blogger', 'Deviantart', 'Dribbble', 'Dropbox', 'E-mail', 'Evernote', 'Facebook', 'Facebook Page', 'Feedburner', 'Flickr', 'Forrst', 'Foursquare', 'Github', 'Goodreads', 'Google Plus', 'Instagram', 'Instapaper', 'Ios', 'Joli Cloud', 'Last fm', 'Linkedin', 'Myspace', 'Picasa', 'Pinboard', 'Pinterest', 'Playstation', 'Posterous Spaces', 'Quora', 'Readability', 'Read_it_later', 'rss', 'Skitch', 'Skype', 'Spotify', 'Stumbleupon', 'Tumblr', 'Twitter', 'Tripadvisor', 'Vimeo', 'Vine', 'Windows', 'Wordpress', 'Xbox', 'Xing', 'Yahoo', 'Yelp', 'Youtube', 'Zoo Tool');
		return $result;
	}
	function currency($n)
	{
		$n = (0+str_replace(",","",$n));
		return str_replace(",",".",number_format($n));
	}
	function doc_images($source,$dest,$page,$width)
	{
		$ch = curl_init('https://docs.google.com/gview?url='.$source.'&a=bi&pagenumber='.$page.'&w='.$width.'&pli==1');
		$fp = fopen($dest, 'wb');
		curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_exec($ch);
		curl_close($ch);
		fclose($fp);
	}
	function char_random($length)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}
	function getDomain()
	{
		$CI =& get_instance();
		return preg_replace("/^[\w]{2,6}:\/\/([\w\d\.\-]+).*$/","$1", $CI->config->slash_item('base_url'));
	}
	function encode_url($str)
    {
    return strtr(base64_encode($str), '+/', '-_');
    }
    function decode_url($str)
    {
        return base64_decode(strtr($str, '-_', '+/'));
    }
    function my_simple_crypt( $string, $action = 'e' ) 
    {
	    // you may change these values to your own
	    $secret_key = 'my_simple_secret_key';
	    $secret_iv = 'my_simple_secret_iv';
	 
	    $output = false;
	    $encrypt_method = "AES-256-CBC";
	    $key = hash( 'sha256', $secret_key );
	    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
	 
	    if( $action == 'e' ) {
	        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
	    }
	    else if( $action == 'd' ){
	        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
	    }
	 
	    return $output;
	}
	// $encoded = encode("help me vanish" , "ticket_to_haven");
	// echo $encoded;
	// echo "\n";
	// $decoded = decode($encoded, "ticket_to_haven");
	// echo $decoded;
	function backup_tables()
	{
		$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
	    $domainName = $_SERVER['HTTP_HOST'] . '/';
		$prefs = array(     
	        'format'      => 'zip',             
	        'filename'    => $this->model_setting->getDomain().'-'.date('Y-m-d_H-i-s').'.sql'
	      );
		$this->load->dbutil();   
		$backup = $this->dbutil->backup($prefs);  
		$this->load->helper('file');
		write_file('upload/files/db/'.$this->model_setting->getDomain().'-'.date('Y-m-d_H-i-s').'.zip', $backup); 
	}
}