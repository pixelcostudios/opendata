<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Setting Social Network Editor</title>

    <link href="<?php echo base_url()?>themes/default/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/animate.css" rel="stylesheet">
    <!-- Toastr style -->
    <link href="<?php echo base_url()?>themes/default/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/custom.css" rel="stylesheet">

</head>

<body class="fixed-sidebar">

    <div id="wrapper">

    <?php $this->load->view('menu/nav');?>

    <div id="page-wrapper" class="gray-bg">
    <?php $this->load->view('menu/nav_top');?>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Setting Advanced </h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo base_url()?>panel/dashboard.html">Home</a>
                    </li>
                    <li>
                        <a>Setting Advanced</a>
                    </li>
                    <li class="active">
                        <strong>Setting Advanced Editor</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>
    <div class="wrapper wrapper-content">

        <div class="row">
        <form method="post" enctype="multipart/form-data">
            <div class="col-lg-12 padding-none">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <button type="submit" name="Pixel_Save" class="btn btn-primary float-right">Save Setting Advanced</button>
                        <h5>Setting Advanced Editor</h5>
                    </div>
                    <div class="ibox-content">
                    <div class="row">
                    <div class="col-sm-2">
                    	<div class="full-height-scroll">
	                    <ul class="list-group elements-list">
	                        <li class="list-group-item active">
	                            <a data-toggle="tab" href="#dashboard">
	                                <small class="pull-right text-muted"> 1</small>
	                                <strong>Dashboard</strong>
	                                <div class="small m-t-xs">
	                                    <p class="m-b-none">
	                                        <i class="fa fa-map-marker"></i> dashboard.html
	                                    </p>
	                                </div>
	                            </a>
	                        </li>
	                        <li class="list-group-item">
	                            <a data-toggle="tab" href="#post">
	                                <small class="pull-right text-muted"> 2</small>
	                                <strong>Post</strong>
	                                <div class="small m-t-xs">
	                                    <p class="m-b-none">

	                                        <span class="label pull-right label-primary">SPECIAL</span>
	                                        <i class="fa fa-map-marker"></i> post.html
	                                    </p>
	                                </div>
	                            </a>
	                        </li>
	                        <li class="list-group-item">
	                            <a data-toggle="tab" href="#pages">
	                                <small class="pull-right text-muted"> 3</small>
	                                <strong>Pages</strong>
	                                <div class="small m-t-xs">
	                                    <p class="m-b-none">
	                                        <i class="fa fa-map-marker"></i> pages.html
	                                    </p>
	                                </div>
	                            </a>
	                        </li>
	                        <li class="list-group-item">
	                            <a data-toggle="tab" href="#hidden_pages">
	                                <small class="pull-right text-muted"> 4</small>
	                                <strong>Hidden Pages</strong>
	                                <div class="small m-t-xs">
	                                    <p class="m-b-none">
	                                        <i class="fa fa-map-marker"></i> hidden_pages.html
	                                    </p>
	                                </div>
	                            </a>
	                        </li>
	                        <li class="list-group-item">
	                            <a data-toggle="tab" href="#comment">
	                                <small class="pull-right text-muted"> 5</small>
	                                <strong>Comment</strong>
	                                <div class="small m-t-xs">
	                                    <p class="m-b-none">
	                                        <i class="fa fa-map-marker"></i> comment.html
	                                    </p>
	                                </div>
	                            </a>
	                        </li>
	                        <li class="list-group-item">
	                            <a data-toggle="tab" href="#contact">
	                                <small class="pull-right text-muted"> 6</small>
	                                <strong>Contact</strong>
	                                <div class="small m-t-xs">
	                                    <p class="m-b-none">
	                                        <i class="fa fa-map-marker"></i> contact.html
	                                    </p>
	                                </div>
	                            </a>
	                        </li>
	                        <li class="list-group-item">
	                            <a data-toggle="tab" href="#banner">
	                                <small class="pull-right text-muted"> 7</small>
	                                <strong>Banner</strong>
	                                <div class="small m-t-xs">
	                                    <p class="m-b-none">
	                                        <i class="fa fa-map-marker"></i> banner.html
	                                    </p>
	                                </div>
	                            </a>
	                        </li>
	                        <li class="list-group-item">
	                            <a data-toggle="tab" href="#slideshow">
	                                <small class="pull-right text-muted"> 8</small>
	                                <strong>Slideshow</strong>
	                                <div class="small m-t-xs">
	                                    <p class="m-b-none">
	                                        <i class="fa fa-map-marker"></i> slideshow.html
	                                    </p>
	                                </div>
	                            </a>
	                        </li>
	                        <li class="list-group-item">
	                            <a data-toggle="tab" href="#media">
	                                <small class="pull-right text-muted"> 9</small>
	                                <strong>Media</strong>
	                                <div class="small m-t-xs">
	                                    <p class="m-b-none">
	                                        <i class="fa fa-map-marker"></i> media.html
	                                    </p>
	                                </div>
	                            </a>
	                        </li>
							<li class="list-group-item">
	                            <a data-toggle="tab" href="#member">
	                                <small class="pull-right text-muted"> 10</small>
	                                <strong>Member</strong>
	                                <div class="small m-t-xs">
	                                    <p class="m-b-none">
	                                        <i class="fa fa-map-marker"></i> member.html
	                                    </p>
	                                </div>
	                            </a>
	                        </li>
							<li class="list-group-item">
	                            <a data-toggle="tab" href="#themes">
	                                <small class="pull-right text-muted"> 11</small>
	                                <strong>Themes</strong>
	                                <div class="small m-t-xs">
	                                    <p class="m-b-none">
	                                        <i class="fa fa-map-marker"></i> themes.html
	                                    </p>
	                                </div>
	                            </a>
	                        </li>
	                        <li class="list-group-item">
	                            <a data-toggle="tab" href="#setting">
	                                <small class="pull-right text-muted"> 12</small>
	                                <strong>Setting</strong>
	                                <div class="small m-t-xs">
	                                    <p class="m-b-none">
	                                        <i class="fa fa-map-marker"></i> setting.html
	                                    </p>
	                                </div>
	                            </a>
	                        </li>
	                    </ul>
	                	</div>
                    </div>
                    <div class="col-sm-10">
                    	<div class="tab-content">
                    		<div id="dashboard" class="tab-pane active">
                            <h1>
                                Setting Dashboard
                            </h1>
                            <div class="row">
                            <?php foreach($menu_dashboard as $row_social){?>
							<div class="col-sm-2">
								<p>
	                                <?php echo str_replace('Setting ','',$row_social->setting_value);?>
	                            </p>
								<div class="switch margin-bottom-20">
	                                <div class="onoffswitch">
	                                    <input type="checkbox" value="1" name="<?php echo $row_social->setting_name;?>" <?php if($row_social->setting_status=='1'){?>checked<?php }?> class="onoffswitch-checkbox" id="<?php echo $row_social->setting_name;?>_toggle">
	                                    <label class="onoffswitch-label" for="<?php echo $row_social->setting_name;?>_toggle">
	                                        <span class="onoffswitch-inner"></span>
	                                        <span class="onoffswitch-switch"></span>
	                                    </label>
	                                </div>
	                            </div>
							</div>
							<?php }?>
							</div>
                            </div>
                    		<div id="post" class="tab-pane">
                            <h1>
                                Setting Post
                            </h1>
                            <div class="row">
                            <?php foreach($menu_post as $row_social){?>
							<div class="col-sm-2">
								<p>
	                                <?php echo str_replace('Setting ','',$row_social->setting_value);?>
	                            </p>
								<div class="switch margin-bottom-20">
	                                <div class="onoffswitch">
	                                    <input type="checkbox" value="1" name="<?php echo $row_social->setting_name;?>" <?php if($row_social->setting_status=='1'){?>checked<?php }?> class="onoffswitch-checkbox" id="<?php echo $row_social->setting_name;?>_toggle">
	                                    <label class="onoffswitch-label" for="<?php echo $row_social->setting_name;?>_toggle">
	                                        <span class="onoffswitch-inner"></span>
	                                        <span class="onoffswitch-switch"></span>
	                                    </label>
	                                </div>
	                            </div>
							</div>
							<?php }?>
							</div>
                            </div>
                    		<div id="pages" class="tab-pane">
                             <h1>
                                Setting Pages
                            </h1>
                            <div class="row">
                            <?php foreach($menu_pages as $row_social){?>
							<div class="col-sm-2">
								<p>
	                                <?php echo str_replace('Setting ','',$row_social->setting_value);?>
	                            </p>
								<div class="switch margin-bottom-20">
	                                <div class="onoffswitch">
	                                    <input type="checkbox" value="1" name="<?php echo $row_social->setting_name;?>" <?php if($row_social->setting_status=='1'){?>checked<?php }?> class="onoffswitch-checkbox" id="<?php echo $row_social->setting_name;?>_toggle">
	                                    <label class="onoffswitch-label" for="<?php echo $row_social->setting_name;?>_toggle">
	                                        <span class="onoffswitch-inner"></span>
	                                        <span class="onoffswitch-switch"></span>
	                                    </label>
	                                </div>
	                            </div>
							</div>
							<?php }?>
							</div>
                            </div>
                    		<div id="hidden_pages" class="tab-pane">
                            <h1>
                                Setting Hidden Pages
                            </h1>
                            <div class="row">
                            <?php foreach($menu_hidden_pages as $row_social){?>
							<div class="col-sm-2">
								<p>
	                                <?php echo str_replace('Setting ','',$row_social->setting_value);?>
	                            </p>
								<div class="switch margin-bottom-20">
	                                <div class="onoffswitch">
	                                    <input type="checkbox" value="1" name="<?php echo $row_social->setting_name;?>" <?php if($row_social->setting_status=='1'){?>checked<?php }?> class="onoffswitch-checkbox" id="<?php echo $row_social->setting_name;?>_toggle">
	                                    <label class="onoffswitch-label" for="<?php echo $row_social->setting_name;?>_toggle">
	                                        <span class="onoffswitch-inner"></span>
	                                        <span class="onoffswitch-switch"></span>
	                                    </label>
	                                </div>
	                            </div>
							</div>
							<?php }?>
							</div>
                            </div>
                    		<div id="comment" class="tab-pane">
                            <h1>
                                Setting Comment
                            </h1>
                            <div class="row">
                            <?php foreach($menu_comment as $row_social){?>
							<div class="col-sm-2">
								<p>
	                                <?php echo str_replace('Setting ','',$row_social->setting_value);?>
	                            </p>
								<div class="switch margin-bottom-20">
	                                <div class="onoffswitch">
	                                    <input type="checkbox" value="1" name="<?php echo $row_social->setting_name;?>" <?php if($row_social->setting_status=='1'){?>checked<?php }?> class="onoffswitch-checkbox" id="<?php echo $row_social->setting_name;?>_toggle">
	                                    <label class="onoffswitch-label" for="<?php echo $row_social->setting_name;?>_toggle">
	                                        <span class="onoffswitch-inner"></span>
	                                        <span class="onoffswitch-switch"></span>
	                                    </label>
	                                </div>
	                            </div>
							</div>
							<?php }?>
							</div>
                            </div>
                    		<div id="contact" class="tab-pane">
                            <h1>
                                Setting Contact
                            </h1>
                            <div class="row">
                            <?php foreach($menu_contact as $row_social){?>
							<div class="col-sm-2">
								<p>
	                                <?php echo str_replace('Setting ','',$row_social->setting_value);?>
	                            </p>
								<div class="switch margin-bottom-20">
	                                <div class="onoffswitch">
	                                    <input type="checkbox" value="1" name="<?php echo $row_social->setting_name;?>" <?php if($row_social->setting_status=='1'){?>checked<?php }?> class="onoffswitch-checkbox" id="_toggle<?php echo $row_social->setting_name;?>">
	                                    <label class="onoffswitch-label" for="_toggle<?php echo $row_social->setting_name;?>">
	                                        <span class="onoffswitch-inner"></span>
	                                        <span class="onoffswitch-switch"></span>
	                                    </label>
	                                </div>
	                            </div>
							</div>
							<?php }?>
							</div>
                            </div>
                    		<div id="banner" class="tab-pane">
                            <h1>
                                Setting Banner
                            </h1>
                            <div class="row">
                            <?php foreach($menu_banner as $row_social){?>
							<div class="col-sm-2">
								<p>
	                                <?php echo str_replace('Setting ','',$row_social->setting_value);?>
	                            </p>
								<div class="switch margin-bottom-20">
	                                <div class="onoffswitch">
	                                    <input type="checkbox" value="1" name="<?php echo $row_social->setting_name;?>" <?php if($row_social->setting_status=='1'){?>checked<?php }?> class="onoffswitch-checkbox" id="<?php echo $row_social->setting_name;?>_toggle">
	                                    <label class="onoffswitch-label" for="<?php echo $row_social->setting_name;?>_toggle">
	                                        <span class="onoffswitch-inner"></span>
	                                        <span class="onoffswitch-switch"></span>
	                                    </label>
	                                </div>
	                            </div>
							</div>
							<?php }?>
							</div>
                            </div>
                            <div id="slideshow" class="tab-pane">
                            <h1>
                                Setting Slideshow
                            </h1>
                            <div class="row">
                            <?php foreach($menu_slideshow as $row_social){?>
							<div class="col-sm-2">
								<p>
	                                <?php echo str_replace('Setting ','',$row_social->setting_value);?>
	                            </p>
								<div class="switch margin-bottom-20">
	                                <div class="onoffswitch">
	                                    <input type="checkbox" value="1" name="<?php echo $row_social->setting_name;?>" <?php if($row_social->setting_status=='1'){?>checked<?php }?> class="onoffswitch-checkbox" id="<?php echo $row_social->setting_name;?>_toggle">
	                                    <label class="onoffswitch-label" for="<?php echo $row_social->setting_name;?>_toggle">
	                                        <span class="onoffswitch-inner"></span>
	                                        <span class="onoffswitch-switch"></span>
	                                    </label>
	                                </div>
	                            </div>
							</div>
							<?php }?>
							</div>
                            </div>

                            <div id="media" class="tab-pane">
                            <h1>
                                Setting Media
                            </h1>
                            <div class="row">
                            <?php foreach($menu_media as $row_social){?>
							<div class="col-sm-2">
								<p>
	                                <?php echo str_replace('Setting ','',$row_social->setting_value);?>
	                            </p>
								<div class="switch margin-bottom-20">
	                                <div class="onoffswitch">
	                                    <input type="checkbox" value="1" name="<?php echo $row_social->setting_name;?>" <?php if($row_social->setting_status=='1'){?>checked<?php }?> class="onoffswitch-checkbox" id="<?php echo $row_social->setting_name;?>_toggle">
	                                    <label class="onoffswitch-label" for="<?php echo $row_social->setting_name;?>_toggle">
	                                        <span class="onoffswitch-inner"></span>
	                                        <span class="onoffswitch-switch"></span>
	                                    </label>
	                                </div>
	                            </div>
							</div>
							<?php }?>
							</div>
                            </div>

                            <div id="member" class="tab-pane">
                            <h1>
                                Setting Member
                            </h1>
                            <div class="row">
                            <?php foreach($menu_member as $row_social){?>
							<div class="col-sm-2">
								<p>
	                                <?php echo str_replace('Setting ','',$row_social->setting_value);?>
	                            </p>
								<div class="switch margin-bottom-20">
	                                <div class="onoffswitch">
	                                    <input type="checkbox" value="1" name="<?php echo $row_social->setting_name;?>" <?php if($row_social->setting_status=='1'){?>checked<?php }?> class="onoffswitch-checkbox" id="<?php echo $row_social->setting_name;?>_toggle">
	                                    <label class="onoffswitch-label" for="<?php echo $row_social->setting_name;?>_toggle">
	                                        <span class="onoffswitch-inner"></span>
	                                        <span class="onoffswitch-switch"></span>
	                                    </label>
	                                </div>
	                            </div>
							</div>
							<?php }?>
							</div>
                            </div>
                            <div id="themes" class="tab-pane">
                            <h1>
                                Setting Themes
                            </h1>
                            <div class="row">
                            <?php foreach($menu_themes as $row_social){?>
							<div class="col-sm-2">
								<p>
	                                <?php echo str_replace('Setting ','',$row_social->setting_value);?>
	                            </p>
								<div class="switch margin-bottom-20">
	                                <div class="onoffswitch">
	                                    <input type="checkbox" value="1" name="<?php echo $row_social->setting_name;?>" <?php if($row_social->setting_status=='1'){?>checked<?php }?> class="onoffswitch-checkbox" id="<?php echo $row_social->setting_name;?>_toggle">
	                                    <label class="onoffswitch-label" for="<?php echo $row_social->setting_name;?>_toggle">
	                                        <span class="onoffswitch-inner"></span>
	                                        <span class="onoffswitch-switch"></span>
	                                    </label>
	                                </div>
	                            </div>
							</div>
							<?php }?>
							</div>
                            </div>
							<div id="setting" class="tab-pane">
								<h1>
                                    Setting Menu
                                </h1>
                                <div class="row">
                                <?php foreach($menu_setting as $row_social){?>
								<div class="col-sm-2">
										<p>
			                                <?php echo str_replace('Setting ','',$row_social->setting_value);?>
			                            </p>
										<div class="switch margin-bottom-20">
			                                <div class="onoffswitch">
			                                    <input type="checkbox" value="1" name="<?php echo $row_social->setting_name;?>" <?php if($row_social->setting_status=='1'){?>checked<?php }?> class="onoffswitch-checkbox" id="<?php echo $row_social->setting_name;?>_toggle">
			                                    <label class="onoffswitch-label" for="<?php echo $row_social->setting_name;?>_toggle">
			                                        <span class="onoffswitch-inner"></span>
			                                        <span class="onoffswitch-switch"></span>
			                                    </label>
			                                </div>
			                            </div>
									</div>
								<?php }?>
								 </div>
								 <h1>
                                    Setting Contact
                                </h1>
                                <div class="row">
                                <?php foreach($company as $row_social){?>
								<div class="col-sm-2">
										<p>
			                                <?php echo str_replace('Setting ','',$row_social->setting_value);?>
			                            </p>
										<div class="switch margin-bottom-20">
			                                <div class="onoffswitch">
			                                    <input type="checkbox" value="1" name="<?php echo $row_social->setting_name;?>" <?php if($row_social->setting_status=='1'){?>checked<?php }?> class="onoffswitch-checkbox" id="<?php echo $row_social->setting_name;?>_toggle">
			                                    <label class="onoffswitch-label" for="<?php echo $row_social->setting_name;?>_toggle">
			                                        <span class="onoffswitch-inner"></span>
			                                        <span class="onoffswitch-switch"></span>
			                                    </label>
			                                </div>
			                            </div>
									</div>
								<?php }?>
								 </div>
                                <h1 class="clear">
                                    Setting Social Network
                                </h1>
                                <div class="row">
                                <?php foreach($social_network as $row_social){?>
								<div class="col-sm-2">
										<p>
			                                <?php echo $row_social->setting_explain;?>
			                            </p>
										<div class="switch margin-bottom-20">
			                                <div class="onoffswitch">
			                                    <input type="checkbox" value="1" name="<?php echo $row_social->setting_name;?>" <?php if($row_social->setting_status=='1'){?>checked<?php }?> class="onoffswitch-checkbox" id="<?php echo $row_social->setting_name;?>_toggle">
			                                    <label class="onoffswitch-label" for="<?php echo $row_social->setting_name;?>_toggle">
			                                        <span class="onoffswitch-inner"></span>
			                                        <span class="onoffswitch-switch"></span>
			                                    </label>
			                                </div>
			                            </div>
									</div>
								<?php }?>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
					<div class="clear"></div>
                    </div>
                </div>
            </div>
        </form>
        </div>


        </div>
    <div class="footer">
        <div class="pull-right">
            <strong><?php echo $this->model_nav->disk_size();?></strong> Free.
        </div>
        <div>
            <strong>Copyright</strong> <?php echo $this->model_setting->setting('company');?> &copy; <?php echo date('Y');?>
        </div>
    </div>

    </div>
    </div>



    <!-- Mainly scripts -->
    <script src="<?php echo base_url()?>themes/default/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url()?>themes/default/js/inspinia.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/pace/pace.min.js"></script>
    <!-- Toastr script -->
    <script src="<?php echo base_url()?>themes/default/js/plugins/toastr/toastr.min.js"></script>
    <script>
        $(function () {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": true,
                "preventDuplicates": false,
                "positionClass": "toast-bottom-right",
                "onclick": null,
                "showDuration": "400",
                "hideDuration": "1000",
                "timeOut": "7000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            //toastr.success('Successfully Update Profile', 'Update Profile');
            <?php if(isset($_COOKIE['status'])){ echo $this->model_status->status($_COOKIE['status']);}?>
            <?php if(isset($_COOKIE['status_second'])){ echo $this->model_status->status($_COOKIE['status_second']);}?>
        });
    </script>
</body>

</html>