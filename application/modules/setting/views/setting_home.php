<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Setting Home Editor</title>

    <link href="<?php echo base_url()?>themes/default/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/animate.css" rel="stylesheet">
    <!-- Toastr style -->
    <link href="<?php echo base_url()?>themes/default/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/custom.css" rel="stylesheet">

</head>

<body class="fixed-sidebar">

    <div id="wrapper">

    <?php $this->load->view('menu/nav');?>

        <div id="page-wrapper" class="gray-bg">
        <?php $this->load->view('menu/nav_top');?>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Setting Home </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>panel/dashboard.html">Home</a>
                        </li>
                        <li>
                            <a>Setting Home</a>
                        </li>
                        <li class="active">
                            <strong>Setting Home Editor</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content">

            <div class="row">
            <form method="post" enctype="multipart/form-data">
                <div class="col-lg-12 padding-none">
	                <div class="ibox float-e-margins">
	                    <div class="ibox-title">
	                        <button type="submit" name="Pixel_Save" class="btn btn-primary float-right">Save Setting</button>
	                        <h5>Setting Home Description Editor</h5>
	                    </div>
	                    <div class="ibox-content">
	                    <?php //print_r($setting_meta);?>
	                    <div class="form-group">
							<label for="title">Segera Terbit</label>
							<select class="form-control" name="segera_terbit">
                                <option value="1" <?php if($this->model_setting->setting_value('segera_terbit','setting_status')=='1'){?>selected=""<?php }?>>Yes</option>
                                <option value="0" <?php if($this->model_setting->setting_value('segera_terbit','setting_status')=='0'){?>selected=""<?php }?>>No</option>
                            </select>
						</div>
                        <div class="form-group">
                            <label for="title">Terbitan Terbaru</label>
                            <select class="form-control" name="terbitan_terbaru">
                                <option value="1" <?php if($this->model_setting->setting_value('terbitan_terbaru','setting_status')=='1'){?>selected=""<?php }?>>Yes</option>
                                <option value="0" <?php if($this->model_setting->setting_value('terbitan_terbaru','setting_status')=='0'){?>selected=""<?php }?>>No</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Katalog Terbitan</label>
                            <select class="form-control" name="katalog_terbitan">
                                <option value="1" <?php if($this->model_setting->setting_value('katalog_terbitan','setting_status')=='1'){?>selected=""<?php }?>>Yes</option>
                                <option value="0" <?php if($this->model_setting->setting_value('katalog_terbitan','setting_status')=='0'){?>selected=""<?php }?>>No</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">News</label>
                            <select class="form-control" name="news">
                                <option value="1" <?php if($this->model_setting->setting_value('news','setting_status')=='1'){?>selected=""<?php }?>>Yes</option>
                                <option value="0" <?php if($this->model_setting->setting_value('news','setting_status')=='0'){?>selected=""<?php }?>>No</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Coretan Sahabat</label>
                            <select class="form-control" name="coretan_sahabat">
                                <option value="1" <?php if($this->model_setting->setting_value('coretan_sahabat','setting_status')=='1'){?>selected=""<?php }?>>Yes</option>
                                <option value="0" <?php if($this->model_setting->setting_value('coretan_sahabat','setting_status')=='0'){?>selected=""<?php }?>>No</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Testimoni</label>
                            <select class="form-control" name="testimoni">
                                <option value="1" <?php if($this->model_setting->setting_value('testimoni','setting_status')=='1'){?>selected=""<?php }?>>Yes</option>
                                <option value="0" <?php if($this->model_setting->setting_value('testimoni','setting_status')=='0'){?>selected=""<?php }?>>No</option>
                            </select>
                        </div>
						<div class="clear"></div>
	                    </div>
	                </div>
	            </div>
            </form>
            </div>


            </div>
        <div class="footer">
            <div class="pull-right">
                <strong><?php echo $this->model_nav->disk_size();?></strong> Free.
            </div>
            <div>
                <strong>Copyright</strong> <?php echo $this->model_setting->setting('company');?> &copy; <?php echo date('Y');?>
            </div>
        </div>

        </div>
        </div>



    <!-- Mainly scripts -->
    <script src="<?php echo base_url()?>themes/default/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url()?>themes/default/js/inspinia.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/pace/pace.min.js"></script>
    <!-- Toastr script -->
    <script src="<?php echo base_url()?>themes/default/js/plugins/toastr/toastr.min.js"></script>
    <script>
        $(function () {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": true,
                "preventDuplicates": false,
                "positionClass": "toast-bottom-right",
                "onclick": null,
                "showDuration": "400",
                "hideDuration": "1000",
                "timeOut": "7000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            //toastr.success('Successfully Update Profile', 'Update Profile');
            <?php if(isset($_COOKIE['status'])){ echo $this->model_status->status($_COOKIE['status']);}?>
            <?php if(isset($_COOKIE['status_second'])){ echo $this->model_status->status($_COOKIE['status_second']);}?>
        });
    </script>
</body>

</html>