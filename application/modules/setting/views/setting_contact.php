<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Setting Contact Editor</title>

    <link href="<?php echo base_url()?>themes/default/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/animate.css" rel="stylesheet">
    <!-- Toastr style -->
    <link href="<?php echo base_url()?>themes/default/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/custom.css" rel="stylesheet">

</head>

<body class="fixed-sidebar">

    <div id="wrapper">

    <?php $this->load->view('menu/nav');?>

        <div id="page-wrapper" class="gray-bg">
        <?php $this->load->view('menu/nav_top');?>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Setting Contact </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>panel/dashboard.html">Home</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url()?>panel/setting_contact.html">Setting Contact</a>
                        </li>
                        <li class="active">
                            <strong>Contact Editor</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content">

            <div class="row">
            <form method="post" enctype="multipart/form-data">
                <div class="col-lg-12 padding-none">
	                <div class="ibox float-e-margins">
	                    <div class="ibox-title">
	                        <button type="submit" name="Pixel_Save" class="btn btn-primary float-right">Save Contact</button>
	                        <h5>Contact Editor</h5>
	                    </div>
	                    <div class="ibox-content">
	                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                        	<?php if($this->model_setting->setting_value('setting_company_1','setting_status')=='1'){?>
                            <li class="active"><a data-toggle="tab" href="#tab-1"> Company 1</a></li>
                            <?php }?>
                            <?php if($this->model_setting->setting_value('setting_company_2','setting_status')=='1'){?>
                            <li class=""><a data-toggle="tab" href="#tab-2">Company 2</a></li>
                            <?php }?>
                            <?php if($this->model_setting->setting_value('setting_company_3','setting_status')=='1'){?>
                            <li class=""><a data-toggle="tab" href="#tab-3">Company 3</a></li>
                            <?php }?>
                            <?php if($this->model_setting->setting_value('setting_company_4','setting_status')=='1'){?>
                            <li class=""><a data-toggle="tab" href="#tab-4">Company 4</a></li>
                            <?php }?>
                        </ul>
                        <div class="tab-content">
                        	<?php if($this->model_setting->setting_value('setting_company_1','setting_status')=='1'){?>
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body padding-left-0 padding-right-0">
                                   <?php  $i=0; foreach($company_1 as $row_value){ $i++; if($row_value->setting_status=='1'){ if($row_value->setting_name=='c1_map'){?>
				                    <div class="form-group">
										<label for="title"><?php echo $row_value->setting_explain;?></label>
										<textarea class="form-control"  name="<?php echo $row_value->setting_name;?>" placeholder="<?php echo $row_value->setting_explain;?>"><?php echo $row_value->setting_value;?></textarea>
									</div>
				                    <?php }else{?>
				                    <div class="form-group">
										<label for="title"><?php echo $row_value->setting_explain;?></label>
										<input type="text" class="form-control"  name="<?php echo $row_value->setting_name;?>" placeholder="<?php echo $row_value->setting_explain;?>" value="<?php echo $row_value->setting_value;?>">
									</div>
				                    <?php }}}?>
                                </div>
                            </div>
							<?php }?>
                            <?php if($this->model_setting->setting_value('setting_company_2','setting_status')=='1'){?>
                            <div id="tab-2" class="tab-pane">
                                <div class="panel-body padding-left-0 padding-right-0">
                                    <?php  $i=0; foreach($company_2 as $row_value){ $i++; if($row_value->setting_status=='1'){if($row_value->setting_name=='c2_map'){?>
				                    <div class="form-group">
										<label for="title"><?php echo $row_value->setting_explain;?></label>
										<textarea class="form-control"  name="<?php echo $row_value->setting_name;?>" placeholder="<?php echo $row_value->setting_explain;?>"><?php echo $row_value->setting_value;?></textarea>
									</div>
				                    <?php }else{?>
				                    <div class="form-group">
										<label for="title"><?php echo $row_value->setting_explain;?></label>
										<input type="text" class="form-control"  name="<?php echo $row_value->setting_name;?>" placeholder="<?php echo $row_value->setting_explain;?>" value="<?php echo $row_value->setting_value;?>">
									</div>
				                    <?php }}}?>
                                </div>
                            </div>
							<?php }?>
                            <?php if($this->model_setting->setting_value('setting_company_3','setting_status')=='1'){?>
                            <div id="tab-3" class="tab-pane">
                                <div class="panel-body padding-left-0 padding-right-0">
                                    <?php  $i=0; foreach($company_3 as $row_value){ $i++; if($row_value->setting_status=='1'){if($row_value->setting_name=='c3_map'){?>
				                    <div class="form-group">
										<label for="title"><?php echo $row_value->setting_explain;?></label>
										<textarea class="form-control"  name="<?php echo $row_value->setting_name;?>" placeholder="<?php echo $row_value->setting_explain;?>"><?php echo $row_value->setting_value;?></textarea>
									</div>
				                    <?php }else{?>
				                    <div class="form-group">
										<label for="title"><?php echo $row_value->setting_explain;?></label>
										<input type="text" class="form-control"  name="<?php echo $row_value->setting_name;?>" placeholder="<?php echo $row_value->setting_explain;?>" value="<?php echo $row_value->setting_value;?>">
									</div>
				                    <?php }}}?>
                                </div>
                            </div>
							<?php }?>
                            <?php if($this->model_setting->setting_value('setting_company_4','setting_status')=='1'){?>
                            <div id="tab-4" class="tab-pane">
                                <div class="panel-body padding-left-0 padding-right-0">
                                    <?php  $i=0; foreach($company_4 as $row_value){ $i++; if($row_value->setting_status=='1'){if($row_value->setting_name=='c4_map'){?>
				                    <div class="form-group">
										<label for="title"><?php echo $row_value->setting_explain;?></label>
										<textarea class="form-control"  name="<?php echo $row_value->setting_name;?>" placeholder="<?php echo $row_value->setting_explain;?>"><?php echo $row_value->setting_value;?></textarea>
									</div>
				                    <?php }else{?>
				                    <div class="form-group">
										<label for="title"><?php echo $row_value->setting_explain;?></label>
										<input type="text" class="form-control"  name="<?php echo $row_value->setting_name;?>" placeholder="<?php echo $row_value->setting_explain;?>" value="<?php echo $row_value->setting_value;?>">
									</div>
				                    <?php }}}?>
                                </div>
                            </div>
							<?php }?>
                        </div>


                    </div>
	                    
	                    
						<div class="clear"></div>
	                    </div>
	                </div>
	            </div>
            </form>
            </div>


            </div>
        <div class="footer">
            <div class="pull-right">
                <strong><?php echo $this->model_nav->disk_size();?></strong> Free.
            </div>
            <div>
                <strong>Copyright</strong> <?php echo $this->model_setting->setting('company');?> &copy; <?php echo date('Y');?>
            </div>
        </div>

        </div>
        </div>



    <!-- Mainly scripts -->
    <script src="<?php echo base_url()?>themes/default/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url()?>themes/default/js/inspinia.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/pace/pace.min.js"></script>
    <!-- Toastr script -->
    <script src="<?php echo base_url()?>themes/default/js/plugins/toastr/toastr.min.js"></script>
    <script>
        $(function () {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": true,
                "preventDuplicates": false,
                "positionClass": "toast-bottom-right",
                "onclick": null,
                "showDuration": "400",
                "hideDuration": "1000",
                "timeOut": "7000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            //toastr.success('Successfully Update Profile', 'Update Profile');
            <?php if(isset($_COOKIE['status'])){ echo $this->model_status->status($_COOKIE['status']);}?>
            <?php if(isset($_COOKIE['status_second'])){ echo $this->model_status->status($_COOKIE['status_second']);}?>
        });
    </script>
</body>

</html>