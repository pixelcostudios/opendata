<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

class model_status extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function status($status)
    {
        switch($status)
        {
            case 'added':
                $result =  "toastr.success('Successfully Added', 'Update');";
                break;

            case 'added_member':
                $result =  "toastr.success('New Member has been added', 'Add New Member');";
                break;

            case 'updated':
                $result =  "toastr.success('Successfully Update', 'Update');";
                break;

            case 'updated_member':
                $result =  "toastr.success('Successfully Update Member', 'Update Member');";
                break;

            case 'search':
                $result =  "toastr.success('Result Search', 'Searching...!');";
                break;

            case 'deleted':
                $result =  "toastr.error('Success Deleted', 'Deleted');";
                break;

            case 'deleted_member':
                $result =  "toastr.error('Success Deleted Member', 'Deleted Member');";
                break;

            case 'send':
                $result =  "toastr.success('Success Send', 'Sending...!');";
                break;
            //Second Notification
            case 'added_images':
                $result =  "toastr.success('Successfully Added Images', 'Update Images');";
                break;

            case 'added_pdf':
                $result =  "toastr.success('Successfully Added Files', 'Update PDF');";
                break;

            case 'added_files':
                $result =  "toastr.success('Successfully Added Files', 'Update Files');";
                break;

            case 'added_avatar':
                $result =  "toastr.success('Successfully Added Avatar', 'Update Avatar');";
                break;

            case 'updated_images':
                $result =  "toastr.success('Successfully Update Images', 'Update Images');";
                break;

            case 'updated_pdf':
                $result =  "toastr.success('Successfully Update PDF', 'Update PDF');";
                break;

            case 'updated_files':
                $result =  "toastr.success('Successfully Update Files', 'Update Files');";
                break;

            case 'updated_avatar':
                $result =  "toastr.success('Successfully Update Avatar', 'Update Avatar');";
                break;

            case 'deleted_images':
                $result =  "toastr.error('Success Deleted Images', 'Deleted Images');";
                break;

            case 'deleted_pdf':
                $result =  "toastr.error('Success Deleted PDF', 'Deleted PDF');";
                break;

            case 'deleted_files':
                $result =  "toastr.error('Success Deleted Files', 'Deleted Files');";
                break;

            case 'noupdated':
                $result =  "toastr.error('Error Updated', 'Updated');";
                break;

            case 'nofailed':
                $result =  "toastr.error('Error Added, Please Complete Field', 'Updated');";
                break;

            case 'nofiles':
                $result =  "toastr.error('Error Deleted Files', 'Deleted');";
                break;

            case 'noexist':
                $result =  "toastr.error('Error Exist', 'Updated');";
                break;

            case 'nousername':
                $result =  "toastr.error('Username Exist, Please Select Another', 'Username');";
                break;

            case 'noemail':
                $result =  "toastr.error('Email Address Exist, Please Select Another', 'Email Address');";
                break;

            case 'noalready':
                $result =  "toastr.error('Already, Please Select Another', 'Select');";
                break;

            case 'nosearch':
                $result =  "toastr.error('Search Error', 'Deleted');";
                break;

            case 'nodeleted':
                $result =  "toastr.error('Error Deleted', 'Deleted');";
                break;

            case 'noimages':
                $result =  "toastr.error('Error Deleted Images', 'Deleted');";
                break;

            case 'nofield':
                $result =  "toastr.error('Error Update Field', 'Error');";
                break;
        }
        return $result;
    }
    function status_pages($status)
    {
        switch($status)
        {
            case '404':
                $result = '<div class="middle-box margin-top-0 text-center animated fadeInDown">
                                <h1>404</h1>
                                <h3 class="font-bold">Page Not Found</h3>
                        
                                <div class="error-desc">
                                    Sorry, but the page you are looking for has note been found. Try checking the URL for error, then hit the refresh button on your browser or try found something else in our app.
                                    <form class="form-inline m-t" role="form">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Search for page">
                                        </div>
                                        <button type="submit" class="btn btn-primary">Search</button>
                                    </form>
                                </div>
                            </div>';
                break;
            case '500':
                $result = '<div class="middle-box margin-top-0 text-center animated fadeInDown">
                            <h1>500</h1>
                            <h3 class="font-bold">Internal Server Error</h3>

                            <div class="error-desc">
                                The server encountered something unexpected that didn\'t allow it to complete the request. We apologize.<br>
                                You can go back to main page: <br><a href="'.base_url().'panel/dashboard.html" class="btn btn-primary m-t">Dashboard</a>
                            </div>
                        </div>';
                break;
        }
        return $result;
    }
}