<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//require APPPATH . "third_party/MX/Controller.php";

	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
        $this->load->library('email');
        // $this->load->library('pdf');
        $this->load->helper(array('form'));
        $this->load->helper(array('url'));
		$this->load->helper(array('path'));
		$this->load->model("front/model_post");
		$this->load->model("model_comment");
		$this->load->model("front/model_function");

		$this->load->model('front/siteman_model');
		$this->load->model('front/lembaga_model');
		$this->load->model('front/publik_model');
		$this->load->model('front/post_model');
		$this->load->model('front/apbd_model');
	}
	public function index()
	{
		$data['meta_title'] 			= $this->model_function->setting('meta_title');
		$data['post_detail']			= $this->model_post->po_slug($this->uri->segment('1'));
		
		foreach($data['post_detail'] as $row)
    	{
			$gallery					= $this->model_post->ip_list($row->post_id,'post','ASC');
			$files						= $this->model_post->ip_list($row->post_id,'post_files','ASC');
    		$comment_list				= '';//$this->model_comment->co_cat_list($row->post_id,'comment','post','DESC');
    		$comment_count				= '';//$this->model_comment->co_cat_list_count($row->post_id,'comment','post');
    		
			switch($row->post_id)
			{		
				case '1': 
				$this->load->view('post_about', 
				array('meta_title'		=> $data['meta_title'],
					  'post_id' 		=> $row->post_id,
					  'cat_id'			=> $row->meta_dest,
					  'cat_slug'		=> $row->meta_slug,
					  'cat_title'		=> $row->meta_title,
					  'post_slug'		=> $row->post_slug,
				      'post_title' 		=> $row->post_title,
					  'post_content'	=> $row->post_content,
					  'post_summary'	=> $row->post_summary,
					  'post_video'		=> $row->post_video,
					  'post_count'		=> $row->post_count,
					  'post_key'		=> $row->post_key,
					  'post_desc'		=> $row->post_desc,
					  'about'           => $this->model_post->po_id('1'),
					  'date'			=> $row->date,
					  'images' 			=> $this->model_post->ip_list_limit($row->post_id,'post','ASC','1'),
					  'gallery'			=> $gallery,
					  'files'			=> $files,
					  'comment_list'	=> $comment_list,
					  'comment_count'	=> $comment_count,
					  'info' 			=> $this->siteman_model->configs(),
					  'page' 			=> array(
												'title'=> $row->post_title,
												'desc' =>'Open Data APBD Pemerintah Daerah ',
												'image' => base_url('themes/idea/assets/img/logo.png'))
				));
				break;
				
				case '2':
				$this->load->view('post_contact', 
				array('meta_title'		=> $data['meta_title'],
					  'post_id' 		=> $row->post_id,
					  'cat_id'			=> $row->meta_dest,
					  'cat_slug'		=> $row->meta_slug,
					  'cat_title'		=> $row->meta_title,
					  'post_slug'		=> $row->post_slug,
				      'post_title' 		=> $row->post_title,
					  'post_content'	=> $row->post_content,
					  'post_summary'	=> $row->post_summary,
					  'post_video'		=> $row->post_video,
					  'post_count'		=> $row->post_count,
					  'post_key'		=> $row->post_key,
					  'post_desc'		=> $row->post_desc,
					  'about'           => $this->model_post->po_id('1'),
					  'date'			=> $row->date,
					  'images' 			=> $this->model_post->ip_list_limit($row->post_id,'post','ASC','1'),
					  'gallery'			=> $gallery,
					  'files'			=> $files,
					  'comment_list'	=> $comment_list,
					  'comment_count'	=> $comment_count,
					  'info' 			=> $this->siteman_model->configs(),
					  'page' 			=> array(
												'title'=> $row->post_title,
												'desc' =>'Open Data APBD Pemerintah Daerah ',
												'image' => base_url('themes/idea/assets/img/logo.png'))
				));
				break;
				
				// case '8':
				// $this->load->view('themes/'.$this->config->item('themes').'/post_status');
				// break;
				
				default:
				switch($row->cat_id)
				{
					default:
					switch($row->post_type)
					{	
						case 'post':
						if(isset($_POST['Pixel_Comment']))
				        {
				            $user_id 		        = '0';//$this->input->post('user_id');
				            $post_id 		    	= $this->input->post('post_id');
				            $c_type 		    	= 'comment';
				            $c_division 		    = 'post';
				            $c_title 		    	= $this->input->post('title');
				            $c_name 				= $this->input->post('fullname');
				            $c_email 		    	= $this->input->post('email');
				            $c_content 		    	= $this->input->post('message');
				            $date 		        	= date('Y-m-d H:i:s');
				            $status 		        = '0';

				            $data_array = array(
				                'user_id' 	    	=> $user_id,
				                'post_id' 			=> $post_id,
				                'c_type' 			=> $c_type,
				                'c_division' 	    => $c_division,
				                'c_title' 			=> $c_title,
				                'c_name' 			=> $c_name,
				                'c_email' 			=> $c_email,
				                'c_content' 		=> $c_content,
				                'date' 				=> $date,
				                'status' 			=> $status
				            );
				            $this->model_comment->co_add($data_array);
				        	setcookie('status','updated', time()+2);
				            redirect(base_url(uri_string()).'.html');
				        }
						$this->load->view('post_default', 
						array('meta_title'		=> $data['meta_title'],
							  'post_id' 		=> $row->post_id,
							  'cat_id'			=> $row->meta_dest,
							  'cat_slug'		=> $row->meta_slug,
							  'cat_title'		=> $row->meta_title,
							  'post_slug'		=> $row->post_slug,
						      'post_title' 		=> $row->post_title,
							  'post_content'	=> $row->post_content,
							  'post_summary'	=> $row->post_summary,
							  'post_video'		=> $row->post_video,
							  'post_count'		=> $row->post_count,
							  'post_key'		=> $row->post_key,
							  'post_desc'		=> $row->post_desc,
							  'about'           => $this->model_post->po_id('1'),
							  'date'			=> $row->date,
							  'images' 			=> $this->model_post->ip_list_limit($row->post_id,'post','ASC','1'),
							  'gallery'			=> $gallery,
							  'files'			=> $files,
							  'comment_list'	=> $comment_list,
							  'comment_count'	=> $comment_count,
							  'info' 			=> $this->siteman_model->configs(),
							  'page' 			=> array(
												'title'=> $row->post_title,
												'desc' =>'Open Data APBD Pemerintah Daerah ',
												'image' => base_url('themes/idea/assets/img/logo.png'))
						));
						break;

						case 'pages':
						$this->load->view('post_default', 
						array('meta_title'		=> $data['meta_title'],
							'post_id' 			=> $row->post_id,
							'cat_id'			=> $row->meta_dest,
							'cat_slug'			=> $row->meta_slug,
							'cat_title'			=> $row->meta_title,
							'post_slug'			=> $row->post_slug,
							'post_title' 		=> $row->post_title,
							'post_content'		=> $row->post_content,
							'post_summary'		=> $row->post_summary,
							'post_video'		=> $row->post_video,
							'post_count'		=> $row->post_count,
							'post_key'			=> $row->post_key,
							'post_desc'			=> $row->post_desc,
							'images' 			=> $this->model_post->ip_list_limit($row->post_id,'post','ASC','1'),
							'files'				=> $files,
							'date'				=> $row->date,
							'info' 				=> $this->siteman_model->configs(),
							'page' 				=> array(
												'title'=> $row->post_title,
												'desc' =>'Open Data APBD Pemerintah Daerah ',
												'image' => base_url('themes/idea/assets/img/logo.png'))
						));
						break;
					}
					break;
				}
				break;
			}
		}
	}
}
