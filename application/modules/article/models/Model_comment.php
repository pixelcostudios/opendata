<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

class model_comment extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}
	function co_edit($c_id)
	{
		$this->db->select('c_id, my_cmt.user_id, my_cmt.post_id, c_type, c_division, c_link, c_title, c_name, c_phone, c_email, c_content, c_reply, my_cmt.date, date_reply, my_cmt.status, my_post.post_id, my_post.user_id, post_slug');
		$this->db->from("my_cmt");
		$this->db->where('c_id',$c_id);
        $this->db->join('my_post', 'my_post.post_id = my_cmt.post_id', 'left');
		$result	= $this->db->get();
		return $result->row();
	}
	function co_cat($c_type,$start,$limit)
	{
        $this->db->select('c_id, user_id, post_id, c_type, c_division, c_link, c_title, c_name, c_email, c_content, c_reply, date, date_reply, status');
        $this->db->from("my_cmt");
		$this->db->where('c_type',$c_type);
		$this->db->limit($start,$limit);
		$this->db->order_by('date','desc');
		$query	= $this->db->get();
		return $query->result();
	}
	function co_cat_count($c_type)
	{
        $this->db->select('c_id');
        $this->db->from("my_cmt");
        $this->db->where('c_type',$c_type);
        $this->db->order_by('date','desc');
		return $this->db->count_all_results();
	}
    function co_cat_type($c_type,$c_division,$start,$limit)
    {
        $this->db->select('c_id, user_id, post_id, c_type, c_division, c_link, c_title, c_name, c_email, c_content, c_reply, date, date_reply, status');
        $this->db->from("my_cmt");
        $this->db->where('c_type',$c_type);
        $this->db->where('c_division',$c_division);
        $this->db->limit($start,$limit);
        $this->db->order_by('date','desc');
        $query	= $this->db->get();
        return $query->result();
    }
    function co_cat_type_count($c_type,$c_division)
    {
        $this->db->select('c_id');
        $this->db->from("my_cmt");
        $this->db->where('c_type',$c_type);
        $this->db->where('c_division',$c_division);
        $this->db->order_by('date','desc');
        return $this->db->count_all_results();
    }
	function co_search($c_type,$cat,$start,$limit)
	{
        $this->db->select('c_id, user_id, post_id, c_type, c_division, c_link, c_title, c_name, c_email, c_content, c_reply, date, date_reply, status');
        $this->db->from("my_cmt");
        $this->db->group_start();
		$this->db->like('c_title');
        $this->db->or_like('c_name',$cat);
        $this->db->or_like('c_email',$cat);
        $this->db->or_like('c_content', $cat);
        $this->db->group_end();
        $this->db->where('c_type',$c_type);
		$this->db->limit($start,$limit);
		$this->db->order_by('date','desc');
		$query	= $this->db->get();
		return $query->result();
	}
	function co_search_count($c_type,$cat)
	{
        $this->db->select('c_id, user_id, post_id, c_type, c_division, c_link, c_title, c_name, c_email, c_content, c_reply, date, date_reply, status');
        $this->db->from("my_cmt");
        $this->db->group_start();
        $this->db->like('c_title');
        $this->db->or_like('c_name',$cat);
        $this->db->or_like('c_email',$cat);
        $this->db->or_like('c_content', $cat);
        $this->db->group_end();
        $this->db->where('c_type',$c_type);
        $this->db->order_by('date','desc');
		return $this->db->count_all_results();
	}
	function co_post_count($post_id,$c_type,$c_division)
    {
        $this->db->select('c_id');
        $this->db->from("my_cmt");
        $this->db->where('post_id',$post_id);
        $this->db->where('c_type',$c_type);
        $this->db->where('c_division',$c_division);
        $this->db->order_by('date','desc');
        return $this->db->count_all_results();
    }
	function co_add($data_array)
	{
		$this->db->insert('my_cmt',$data_array);
		return $this->db->insert_id();
	}
	function co_update($c_id,$data_array)
	{
		$this->db->where('c_id', $c_id);
		$this->db->update('my_cmt',$data_array);
		return true;
	}
	function co_update_status($c_id,$status)
	{
		$this->db->set('status', $status); 
		$this->db->where('c_id', $c_id);
		$this->db->update('my_cmt');
		return true;
	}
	function co_post_delete($post_id)
	{
		$this->db->where('post_id',$post_id);
		$this->db->delete('my_cmt');
		return true;
	}
	function co_delete($c_id)
	{
		$this->db->where('c_id',$c_id);
		$this->db->delete('my_cmt');
		return true;
	}
}