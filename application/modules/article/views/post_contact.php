<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $post_title;?> | <?php echo $meta_title;?></title>
    <meta name="description" content="<?php echo substr(strip_tags(str_replace("&nbsp;", "",$post_content)),0,160);?>" />
    <meta name="keywords" content="<?php echo $post_key;?>" />
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,400italic,700italic%7cPlayfair+Display:400,700%7cGreat+Vibes'
		  rel='stylesheet' type='text/css'><!-- Attach Google fonts -->
	<link rel="stylesheet" type="text/css" href="themes/<?php echo $this->config->item('themes');?>/assets/css/styles.css"><!-- Attach the main stylesheet file -->

</head>
<body>
	<div class="main-wrapper">
		<?php $this->load->view('front/menu_top');?>

		<!--Breadcrumb Section-->
		<section id="breadcrumb-section" data-bg-img="themes/<?php echo $this->config->item('themes');?>/assets/img/breadcrumb.jpg">
			<div class="inner-container container">
				<div class="ravis-title">
					<div class="inner-box">
						<div class="title"><?php echo $post_title;?></div>
						<div class="sub-title">Some description about your hotel</div>
					</div>
				</div>

				<div class="breadcrumb">
					<ul class="list-inline">
						<li><a href="<?php echo base_url();?>">Home</a></li>
						<li class="current"><a href="<?php echo base_url().$post_slug;?>.html"><?php echo $post_title;?></a></li>
					</ul>
				</div>
			</div>
		</section>
		<!--End of Breadcrumb Section-->

		<!--Contact Section-->
		<section id="contact-section">
			<div class="inner-container container">
				<div class="t-sec">
					<div class="ravis-title-t-2">
						<div class="title"><span>Contact Us</span></div>
						<div class="sub-title">Do not hesitate to contact me if you have any further questions</div>
					</div>
					<div class="content">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
						labore
						et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
						ut
						aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.
						sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis
						iste
						natus error sit voluptatem accusantium doloremque.
					</div>

					<div class="contact-info">
						<div class="contact-inf-box">
							<div class="icon-box">
								<i class="fa fa-home"></i>
							</div>
							<div class="text">
								 Jl. Palagan Tentara Pelajar, Sleman D.I. Yogyakarta 55581
							</div>
						</div>
						<div class="contact-inf-box">
							<div class="icon-box">
								<i class="fa fa-envelope"></i>
							</div>
							<div class="text">
								info@sofiayogya.com
							</div>
						</div>
						<div class="contact-inf-box">
							<div class="icon-box">
								<i class="fa fa-phone"></i>
							</div>
							<div class="text">
								 08124567890
							</div>
						</div>
					</div>
				</div>

				<div class="b-sec clearfix">
					<div class="contact-form col-md-6">
						<form action="#" id="contact-form-box">
							<div class="field-row">
								<input type="text" name="name" id="contact-name" placeholder="Name :" required>
							</div>
							<div class="field-row">
								<input type="text" name="phone" id="contact-phone" placeholder="Phone :">
							</div>
							<div class="field-row">
								<input type="email" name="email" id="contact-email" placeholder="Email :" required>
							</div>
							<div class="field-row">
								<textarea placeholder="Your Message" name="message" id="contact-message" required></textarea>
							</div>
							<div class="message-box"></div>
							<div class="field-row">
								<input type="submit" value="Submit">
							</div>
						</form>
					</div>
					<div id="google-map" class="col-md-6" data-marker="assets/img/marker.png"></div>
				</div>
			</div>
		</section>
		<!--End of Contact Section-->

		<?php $this->load->view('front/footer');?>

	</div>

	<!-- JS Include Section -->
	<script type="text/javascript" src="themes/<?php echo $this->config->item('themes');?>/assets/js/jquery-3.1.0.min.js"></script>
	<script type="text/javascript" src="themes/<?php echo $this->config->item('themes');?>/assets/js/helper.js"></script>
	<script type="text/javascript" src="themes/<?php echo $this->config->item('themes');?>/assets/js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="themes/<?php echo $this->config->item('themes');?>/assets/js/template.js"></script>
</body>
</html>