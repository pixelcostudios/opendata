<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $post_title;?> | <?php echo $meta_title;?></title>
    <meta name="description" content="<?php echo substr(strip_tags(str_replace("&nbsp;", "",$post_content)),0,160);?>" />
    <meta name="keywords" content="<?php echo $post_key;?>" />
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,400italic,700italic%7cPlayfair+Display:400,700%7cGreat+Vibes'
		  rel='stylesheet' type='text/css'><!-- Attach Google fonts -->
	<link rel="stylesheet" type="text/css" href="themes/<?php echo $this->config->item('themes');?>/assets/css/styles.css"><!-- Attach the main stylesheet file -->

</head>
<body class="room-detials">
	<div class="main-wrapper">
		<?php $this->load->view('front/menu_top');?>

		<section id="room-top-section">
			<!-- Event Slider -->
			<section id="room-slider">
				<div class="items">
					<div class="img-container" data-bg-img="themes/<?php echo $this->config->item('themes');?>/assets/img/slider/room/1.jpg"></div>
				</div>
				<div class="items">
					<div class="img-container" data-bg-img="themes/<?php echo $this->config->item('themes');?>/assets/img/slider/room/3.jpg"></div>
				</div>
				<div class="items">
					<div class="img-container" data-bg-img="themes/<?php echo $this->config->item('themes');?>/assets/img/slider/room/2.jpg"></div>
				</div>
				<div class="items">
					<div class="img-container" data-bg-img="themes/<?php echo $this->config->item('themes');?>/assets/img/slider/room/4.jpg"></div>
				</div>
			</section>
			<!-- End of Event Slider -->
			<div class="inner-container container">
				<div class="room-title-box">
					<h1 class="title"><?php echo $post_title;?></h1>
					<!--<div class="price">
						<div class="title">Starting from :</div>
						<div class="value">$350</div>
					</div>-->
				</div>
			</div>
		</section>

		<section class="room-desc">
			<div class="inner-container container">
				<div class="l-sec col-md-12">
					<div class="amenities">
						<ul class="list-inline clearfix">
							<li class="col-md-4">
								<div class="title">Breakfast :</div>
								<div class="value">Included</div>
							</li>
							<li class="col-md-4">
								<div class="title">Room Size :</div>
								<div class="value">60 sqm</div>
							</li>
							<li class="col-md-4">
								<div class="title">Max People :</div>
								<div class="value">3</div>
							</li>
							<li class="col-md-4">
								<div class="title">View :</div>
								<div class="value">Sea</div>
							</li>
							<li class="col-md-4">
								<div class="title">Facilities :</div>
								<div class="value">Free Wifi, Free Mini Bar, Room Security</div>
							</li>
						</ul>
					</div>
					
					<div class="description">
						<?php echo $post_content;?>
					</div>
				</div>

			</div>
		</section>

		<!--Other Rooms Section-->
		<section id="other-rooms">
			<div class="inner-container container">
				<div class="ravis-title">
					<div class="inner-box">
						<div class="title">Other Rooms</div>
						<div class="sub-title">List of other rooms that you might be interested</div>
					</div>
				</div>
				<div class="room-container clearfix">
					<?php foreach ($hotel as $row_post){ $images = $this->model_post->ip_list_limit($row_post->post_id,'post_thumb','ASC','1');?>
					<div class="room-box col-xs-6 col-md-3 animated-box" data-animation="fadeInUp">
						<div class="inner-box" <?php foreach ($images as $row_images){?>data-bg-img="<?php echo base_url();?>upload/<?php echo $row_images->ipost;?>"<?php }?>>
							<a href="room-details.html" class="more-info"></a>
							<div class="caption">
								<div class="title"><?php echo $row_post->post_title;?></div>
								<!--<div class="price">
									<div class="title">Starting from :</div>
									<div class="value">$320</div>
								</div>-->
								<div class="desc">
									<div class="inner-box">
										<?php echo $row_post->post_summary;?>
									</div>
								</div>
							</div>

						</div>
					</div>
					<?php }?>
				</div>
			</div>
		</section>
		<!--End of Other Rooms Section-->

		<?php $this->load->view('front/footer');?>

	</div>
	<!-- JS Include Section -->
	<script type="text/javascript" src="themes/<?php echo $this->config->item('themes');?>/assets/js/jquery-3.1.0.min.js"></script>
	<script type="text/javascript" src="themes/<?php echo $this->config->item('themes');?>/assets/js/helper.js"></script>
	<script type="text/javascript" src="themes/<?php echo $this->config->item('themes');?>/assets/js/select2.min.js"></script>
	<script type="text/javascript" src="themes/<?php echo $this->config->item('themes');?>/assets/js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="themes/<?php echo $this->config->item('themes');?>/assets/js/template.js"></script>
</body>
</html>