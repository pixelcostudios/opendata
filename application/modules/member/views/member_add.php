<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Member Editor</title>

    <link href="<?php echo base_url()?>themes/default/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <!-- Toastr style -->
    <link href="<?php echo base_url()?>themes/default/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link href="<?php echo base_url()?>themes/default/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/custom.css" rel="stylesheet">

</head>

<body class="fixed-sidebar">

    <div id="wrapper">
    <?php $this->load->view('menu/nav');?>

        <div id="page-wrapper" class="gray-bg">
        <?php $this->load->view('menu/nav_top');?>
        
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Member Editor</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>panel/dashboard.html">Home</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url()?>panel/member/manage.html">Member</a>
                        </li>
                        <li class="active">
                            <strong>Member Editor</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content">

            <div class="row">
            <form name="myform" method="post" enctype="multipart/form-data">
                <div class="col-lg-9 padding-none">
	                <div class="ibox float-e-margins">
	                    <div class="ibox-title">
	                    	<h5 class="pull-left margin-right-20">Member Editor</h5>
	                    	<a href="<?php echo base_url()?>panel/member/add.html" class="btn btn-default pull-left">Add New Member</a>
	                        <button type="submit" name="Pixel_Save" class="btn btn-primary float-right">Save Member</button>
	                    </div>
	                    <div class="ibox-content padding-top-0">
                        <div class="ibox-title-detail ibox-border-bottom-1 clear">
                            <h5 class="pull-left">Login Information</h5>
                        </div>

	                    <div class="form-group col-sm-6 padding-left-0">
                            <label for="title">Email Address </label>
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <input type="text" name="user_email" class="form-control" placeholder="Email Address" required="required">
                            </div>
                        </div>
                        <div class="form-group col-sm-6 padding-right-0">
                            <label for="title">Password </label>
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input type="password" name="user_password" class="form-control" placeholder="Password">
                            </div>
                        </div>
                        <div class="ibox-title-detail ibox-border-top-1 ibox-border-bottom-1 clear">
                            <h5 class="pull-left">Account Type</h5>
                        </div>
                            <div class="form-group col-sm-6 padding-left-0">
                                <label for="title">Member Role </label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-magic"></i></span>
                                    <select name="user_role" class="form-control">
                                        <option value="0">No Selected</option>
                                        <?php foreach($member_role as $row_user_role){?>
                                        <option value="<?php echo $row_user_role->role_id;?>"><?php echo $row_user_role->role_description;?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-sm-6 padding-right-0">
                                <label for="title">Status </label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-magic"></i></span>
                                    <select name="user_active" class="form-control">
                                        <option value="1">Confirm</option>
                                        <option value="0">Unconfirm</option>
                                    </select>
                                </div>
                            </div>
                        <div class="ibox-title-detail ibox-border-top-1 ibox-border-bottom-1 clear">
                            <h5 class="pull-left">Personal Information</h5>
                        </div>

                            <div class="form-group col-sm-6 padding-left-0">
                                <label for="title">Gender </label>
                                <select name="user_gender" class="form-control">
                                    <option value="Mr">Mr</option>
                                    <option value="Mrs">Mrs</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-6 padding-right-0">
                                <label for="title">Join Date </label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input disabled type="text" class="form-control" placeholder="Automatically Added">
                                </div>
                            </div>
                            <div class="form-group col-sm-6 padding-left-0">
                                <label for="title">First Name </label>
                                <input type="text" class="form-control" name="user_first_name" placeholder="First Name" required="required">
                            </div>
                            <div class="form-group col-sm-6 padding-right-0">
                                <label for="title">Last Name </label>
                                <input type="text" class="form-control" name="user_last_name" placeholder="Last Name">
                            </div>
                            <div class="form-group col-sm-6 padding-left-0">
                                <label for="title">Address </label>
                                <input type="text" class="form-control" name="user_address" placeholder="Address">
                            </div>
                            <div class="form-group col-sm-6 padding-right-0">
                                <label for="title">City</label>
                                <input type="text" class="form-control" name="user_city" placeholder="City">
                            </div>
                            <div class="form-group col-sm-6 padding-left-0">
                                <label for="title">Province</label>
                                <input type="text" class="form-control" name="user_province" placeholder="Province">
                            </div>
                            <div class="form-group col-sm-6 padding-right-0">
                                <label for="title">Country</label>
                                <input type="text" class="form-control" name="user_country" placeholder="Country">
                            </div>
                            <div class="form-group col-sm-6 padding-left-0">
                                <label for="title">Postal Code</label>
                                <input type="text" class="form-control" name="user_postal" placeholder="Postal Code">
                            </div>
                            <div class="form-group col-sm-6 padding-right-0 relative" id="data_3">
                                <label for="title">Birth Date </label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="user_birthday" class="form-control">
                                </div>
                            </div>
							<div class="form-group col-sm-6 padding-left-0">
                                <label for="title">Profession</label>
                                <input type="text" class="form-control" name="user_profession" placeholder="Profession">
                            </div>
                            <div class="form-group col-sm-6 padding-right-0">
                                <label for="title">Company</label>
                                <input type="text" class="form-control" name="user_company" placeholder="Company">
                            </div>
                            <div class="form-group clear">
                                <label for="title">Signature</label>
                                <textarea class="form-control" rows="3" name="user_signature" placeholder="Signature"></textarea>
                            </div>
                            <div class="form-group clear" style="display: none;">
                                <label for="title">Map Location</label>
                                <textarea class="form-control" rows="3" name="user_map" placeholder="Map Location"></textarea>
                            </div>
                            <div class="ibox-title-detail ibox-border-top-1 ibox-border-bottom-1 clear">
                                <h5 class="pull-left">Contact Information</h5>
                            </div>

                            <div class="form-group col-sm-6 padding-left-0">
                                <label for="title">Phone</label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                    <input type="text" name="user_phone" class="form-control" placeholder="Phone">
                                </div>
                            </div>
                            <div class="form-group col-sm-6 padding-right-0">
                                <label for="title">Mobile Phone</label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-mobile-phone"></i></span>
                                    <input type="text" name="user_mobile" class="form-control" placeholder="Mobile Phone">
                                </div>
                            </div>

                            <div class="ibox-title-detail ibox-border-top-1 ibox-border-bottom-1 clear">
                                <h5 class="pull-left">Social Network</h5>
                            </div>
                            <div class="form-group col-sm-6 padding-left-0">
                                <label for="title">Facebook</label>
                                <input type="text" class="form-control" name="user_facebook" placeholder="Facebook">
                            </div>
                            <div class="form-group col-sm-6 padding-right-0">
                                <label for="title">Twitter</label>
                                <input type="text" class="form-control" name="user_twitter" placeholder="Twitter">
                            </div>
                            <div class="form-group col-sm-6 padding-left-0">
                                <label for="title">Instagram</label>
                                <input type="text" class="form-control" name="user_instagram" placeholder="Instagram">
                            </div>
                            <div class="form-group col-sm-6 padding-left-0">
                                <label for="title">Google Plus</label>
                                <input type="text" class="form-control" name="user_google" placeholder="Google Plus">
                            </div>
						<div class="clear"></div>
	                    </div>
	                </div>
	            </div>
	            <div class="col-lg-3 padding-right-0">
	            <div class="bar-title white-bg">
                    <div class="ibox-title-sidebar ibox-border-bottom-1">
                        <h5 class="pull-left">Upload Avatar</h5>
                    </div>
	            	<div class="tabs-container">
                        <div class="tabs-left">
                            <div class="tab-content padding-10">
                                <div class="input-file"><input type="file" name="upload_photo[]" multiple="multiple" id="input_images"></div>
                            </div>

                        </div>

                    </div>
                    <!--End Tab-->
	            </div>
	            </div>
            </form>
            </div>


            </div>
        <div class="footer">
            <div class="pull-right">
                <strong><?php echo $this->model_nav->disk_size();?></strong> Free.
            </div>
            <div>
                <strong>Copyright</strong> <?php echo $this->model_setting->setting('company');?> &copy; <?php echo date('Y');?>
            </div>
        </div>

        </div>
        </div>

    <!-- Mainly scripts -->
    <script src="<?php echo base_url()?>themes/default/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url()?>themes/default/js/inspinia.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/pace/pace.min.js"></script>

    <!-- Data picker -->
    <script src="<?php echo base_url()?>themes/default/js/plugins/datapicker/bootstrap-datepicker.js"></script>

    <!--Files Scripts-->
	<script src="<?php echo base_url()?>themes/default/js/plugins/filestyle/filestyle.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/filestyle/aplication.js"></script>

    <!-- Toastr script -->
    <script src="<?php echo base_url()?>themes/default/js/plugins/toastr/toastr.min.js"></script>
    <script>
        $(function () {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": true,
                "preventDuplicates": false,
                "positionClass": "toast-bottom-right",
                "onclick": null,
                "showDuration": "400",
                "hideDuration": "1000",
                "timeOut": "7000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            //toastr.success('Successfully Update Profile', 'Update Profile');
            <?php if(isset($_COOKIE['status'])){ echo $this->model_status->status($_COOKIE['status']);}?>
            <?php if(isset($_COOKIE['status_second'])){ echo $this->model_status->status($_COOKIE['status_second']);}?>
        });
    </script>

    <script>
        $(document).ready(function(){
            $('#data_3 .input-group.date').datepicker({
                startView: 2,
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                format: "yyyy-mm-dd"
            });
        });
    </script>
</body>

</html>
