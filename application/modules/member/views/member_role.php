<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Member Role</title>

    <link href="<?php echo base_url()?>themes/default/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/custom.css" rel="stylesheet">

</head>

<body class="fixed-sidebar">
    <div id="wrapper">
    <?php $this->load->view('menu/nav');?>

        <div id="page-wrapper" class="gray-bg">
        <?php $this->load->view('menu/nav_top');?>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Member Role</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>panel/dashboard.html">Home</a>
                        </li>
                        <li>
                            <a>Member Role</a>
                        </li>
                        <li class="active">
                            <strong>All Member Role</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
            	<form method="post">
                <div class="col-lg-12 padding-none">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>All Member Role</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                            	<div class="col-sm-3">
                                    <div class="input-group"><input type="text" placeholder="Search" class="input-sm form-control"> <span class="input-group-btn">
                                        <button type="button" class="btn btn-sm btn-primary"> Go!</button> </span></div>
                                </div>
                                <div class="col-sm-6 m-b-xs">
                                    <div data-toggle="buttons" class="btn-group">
                                        <label onclick="location.href='<?php echo base_url()?>panel/member/role_add.html'" class="btn btn-sm btn-white"> <i class="fa fa-plus"></i> Add New Member </label>
                                        <!--<label class="btn btn-sm btn-white active"> <input type="radio" id="option2" name="options"> Week </label>
                                        <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="options"> Month </label>-->
                                    </div>
                                </div>
                                <div class="col-sm-3 m-b-xs">
                                <select class="input-sm form-control input-s-sm inline float-right category-show" id="select-view" onchange="window.location = jQuery('#select-view option:selected').val();">
                                    <option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo '1';}else{echo $this->uri->segment(3);}?>/<?php if($this->uri->segment(4)==''){echo '0';}else{echo $this->uri->segment(4);}?>/0/10.html" <?php if($this->uri->segment(6)=='10'){?>selected=""<?php }?>>10</option>
                                    <option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo '1';}else{echo $this->uri->segment(3);}?>/<?php if($this->uri->segment(4)==''){echo '0';}else{echo $this->uri->segment(4);}?>/0/25.html" <?php if($this->uri->segment(6)=='25'){?>selected=""<?php }?>>25</option>
                                    <option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo '1';}else{echo $this->uri->segment(3);}?>/<?php if($this->uri->segment(4)==''){echo '0';}else{echo $this->uri->segment(4);}?>/0/50.html" <?php if($this->uri->segment(6)=='50'){?>selected=""<?php }?>>50</option>
                                    <option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo '1';}else{echo $this->uri->segment(3);}?>/<?php if($this->uri->segment(4)==''){echo '0';}else{echo $this->uri->segment(4);}?>/0/100.html" <?php if($this->uri->segment(6)=='100'){?>selected=""<?php }?>>100</option>
                                </select>
                                </div>
                            </div>
                            <div class="table-responsive margin-top-10">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>

                                        <th width="1%">
                                        	<label>
	                                                <input type="checkbox" class="colored-green checkall">
	                                                <span class="text"></span>
	                                            </label>
                                        </th>
                                        <th>Role Name </th>
                                        <th>Role Description </th>
                                        <th width="5%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php for ($i = 0; $i < count($role_list); ++$i) { ?>
                                    <tr>
                                        <td>
                                        	<label>
                                                <input type="checkbox" name="post-check[]" value="<?php echo $role_list[$i]->role_id; ?>" class="colored-green">
                                                <span class="text"></span>
                                            </label>
                                        </td>
                                        <td><a href="<?php echo base_url()?>panel/member/role_edit/<?php echo $role_list[$i]->role_id; ?>.html"><?php echo $role_list[$i]->role_name; ?></a> </td>
                                        <td><?php echo $role_list[$i]->role_description; ?></td>
                                        <td>
                                        	<div class="btn-group">
	                                        <a class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
	                                            Action <i class="fa fa-angle-down"></i>
	                                        </a>
	                                        <ul class="dropdown-menu drop-right">
	                                            <li><a href="<?php echo base_url()?>panel/member/role_edit/<?php echo $role_list[$i]->role_id; ?>.html">Edit</a></li>
	                                            <li><a href="<?php echo base_url()?>panel/member/role_delete/<?php echo $role_list[$i]->role_id; ?>.html">Delete</a></li>
	                                        </ul>
                                			</div>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                                    <tfoot>
                                <tr>
                                    <td colspan="7" class="footable-visible">
                                    	<div class="pull-left margin-top-20">
	                                    	<div class="btn-group">
		                                    <div class="btn-group dropup">
		                                        <button type="button" class="btn btn-default btn-custom button-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
		                                            <i class="fa fa-ellipsis-horizontal"></i> Action <i class="fa fa-angle-up"></i>
		                                        </button>
		                                        <ul class="dropdown-menu dropdown-archive">
		                                        <li>
		                                        	<button type="submit" name="post-delete" value="delete" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i> Delete</button>                                 
		                                        </li>
		                                        </ul>
		                                    </div>
		                                </div>
	                                    	</div>
                                    	<div class="float-right">
                                    		<?php echo $pagination; ?>
                                    	</div>
                                    </td>
                                </tr>
                                </tfoot>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
				</form>
            </div>
        </div>
        <div class="footer">
            <div class="pull-right">
                10GB of <strong>250GB</strong> Free.
            </div>
            <div>
                <strong>Copyright</strong> Example Company &copy; 2014-2017
            </div>
        </div>

        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="<?php echo base_url()?>themes/default/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Peity -->
    <script src="<?php echo base_url()?>themes/default/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url()?>themes/default/js/inspinia.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/pace/pace.min.js"></script>

    <!-- Check -->
    <script src="<?php echo base_url()?>themes/default/js/check.js"></script>

    <!-- Peity -->
    <script src="<?php echo base_url()?>themes/default/js/demo/peity-demo.js"></script>

    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>

</body>

</html>
