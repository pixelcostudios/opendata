<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//require APPPATH . "third_party/MX/Controller.php";

	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
        $this->load->library('email');
        // $this->load->library('pdf');
        $this->load->helper(array('form'));
        $this->load->helper(array('url'));
		$this->load->helper(array('path'));
		$this->load->library("pagination");
		$this->load->model("menu/model_nav");
		$this->load->model("setting/model_setting");
        $this->load->model("model_member");
        $this->load->model("post/model_status");
        $this->load->model("post/model_ipost");
		// $this->load->model("model_meta");
    }
    public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		else {
			redirect(base_url().'panel/member/manage.html');
		}
	}
	public function manage()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		$data['list_post_count'] 	    = $this->model_nav->po_list_count('post','0');
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count'] 	    = $this->model_nav->us_list_count();
        $data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);
        $data['cat_list']               = $this->model_nav->ca_list('post','0');

        //pagination settings
        if(isset($_POST['Pixel_Search']))
        {
            setcookie('status','search', time()+2);
            redirect(base_url().'panel/member/manage/search/'.$this->model_setting->my_simple_crypt($this->input->post('search'),$action = 'e').'.html');
        }

        if($this->uri->segment(5)!='')
        {
            $config = array();
            $config["base_url"]         = base_url().'panel/member/manage/'.$this->uri->segment(4).'/';
            $total_row                  = $this->model_member->mb_cat_count();
            $config['suffix'] 			= '.html';
            $config['first_url']        = '1.html';
            $config["total_rows"]       = $total_row;
            $config["per_page"]         = ($this->uri->segment(4)!='') ? $this->uri->segment(4) : '10';
            $config['use_page_numbers'] = TRUE;
            $config['num_links']        = $total_row;
            $config['cur_tag_open']     = '&nbsp;<a class="active">';
            $config['cur_tag_close']    = '</a>';
            $config['next_link']        = 'Next';
            $config['prev_link']        = 'Prev';
            $config['num_links'] 		= 5;

            $this->pagination->initialize($config);
            if($this->uri->segment(5))
            {
                $page = ($this->uri->segment(5)-1)*$config["per_page"] ;
            }
            else
            {
                $page = 0;
            }
        }
        else 
        {
            $config = array();
            $config["base_url"]         = base_url().'panel/member/manage/';
            $total_row                  = $this->model_member->mb_cat_count();
            $config['suffix'] 			= '.html';
            $config['first_url']        = '1.html';
            $config["total_rows"]       = $total_row;
            $config["per_page"]         = '10';
            $config['use_page_numbers'] = TRUE;
            $config['num_links']        = $total_row;
            $config['cur_tag_open']     = '&nbsp;<a class="active">';
            $config['cur_tag_close']    = '</a>';
            $config['next_link']        = 'Next';
            $config['prev_link']        = 'Prev';
            $config['num_links'] 		= 5;

            $this->pagination->initialize($config);
            if($this->uri->segment(4))
            {
                $page = ($this->uri->segment(4)-1)*$config["per_page"] ;
            }
            else
            {
                $page = 0;
            }
        }

        $data["user_list"]          = $this->model_member->mb_cat($config["per_page"], $page); 

        $str_links                  = $this->pagination->create_links();
        $data["links"]              = explode('&nbsp;',$str_links );  

		if(isset($_POST['post-draft']) && $_POST['post-draft'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$user_id=$_POST['post-check'][$i];
				
				$this->model_member->us_update_status($user_id,'0');
			}
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/member/manage.html');
		}
        elseif(isset($_POST['post-publish']) && $_POST['post-publish'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$user_id=$_POST['post-check'][$i];
				
				$this->model_member->us_update_status($user_id,'1');
			}
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/member/manage.html');
		}
		elseif(isset($_POST['post-delete']) && $_POST['post-delete'] != "")
		{
            for ($i=0; $i<count($_POST['post-check']);$i++)
            {
                $user_id=$_POST['post-check'][$i];

                @unlink('./upload/avatar/'.$this->model_member->us_value($user_id,'user_avatar'));
                $this->model_member->us_delete($user_id);
            }
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/member/manage.html');
		}

        $this->load->view('member',$data);
	}
    public function edit()
    {
        if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        /**
         *
         * @var List Menu
         *
         */
        $data['list_post_count'] 	    = $this->model_nav->po_list_count('post','0');
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count'] 	    = $this->model_nav->us_list_count();
        $data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);

        /*Show Data*/
        $data['row']				= $this->model_member->us_edit_role($this->uri->segment(4));

        /*Member Role*/
        $data['member_role'] 	    = $this->model_nav->us_list_role();

        /*User List*/
        $data['user_list']			= $this->model_member->us_list();

        if(isset($_POST['Pixel_Save']))
        {
            $id						= $this->uri->segment(4);
            $user_role				= $this->input->post('user_role');
            $user_name				= $this->input->post('user_email');
            $user_password 			= $this->input->post('user_password');
            $user_email 			= $this->input->post('user_email');
            $user_active 			= $this->input->post('user_active');

            $user_gender            = $this->input->post('user_gender');
            $user_first_name        = $this->input->post('user_first_name');
            $user_last_name         = $this->input->post('user_last_name');
            $user_phone             = $this->input->post('user_phone');
            $user_mobile            = $this->input->post('user_mobile');
            $user_address           = $this->input->post('user_address');
            $user_city              = $this->input->post('user_city');
            $user_province          = $this->input->post('user_province');
            $user_country           = $this->input->post('user_country');
            $user_postal            = $this->input->post('user_postal');
            $user_map               = $this->input->post('user_map');
            $user_profession        = $this->input->post('user_profession');
            $user_company           = $this->input->post('user_company');
            $user_facebook          = $this->input->post('user_facebook');
            $user_twitter           = $this->input->post('user_twitter');
            $user_instagram         = $this->input->post('user_instagram');
            $user_google            = $this->input->post('user_google');
            $user_birthday          = $this->input->post('user_birthday');
            $user_signature         = $this->input->post('user_signature');

            $data_array = array(
                'user_role' 	    => $user_role,
                'user_name' 	    => $user_name,
                'user_password'     => $user_password,
                'user_email' 	    => $user_email,
                'user_gender'       => $user_gender,
                'user_first_name'   => $user_first_name,
                'user_last_name'    => $user_last_name,
                'user_phone'        => $user_phone,
                'user_mobile'       => $user_mobile,
                'user_address'      => $user_address,
                'user_city'         => $user_city,
                'user_province'     => $user_province,
                'user_country'      => $user_country,
                'user_postal'       => $user_postal,
                'user_map'          => $user_map,
                'user_profession'   => $user_profession,
                'user_company'      => $user_company,
                'user_facebook'     => $user_facebook,
                'user_twitter'      => $user_twitter,
                'user_instagram'    => $user_instagram,
                'user_google'       => $user_google,
                'user_birthday'     => $user_birthday,
                'user_signature'    => $user_signature
            );
            $this->ion_auth->update($id,$data_array);

            $this->model_member->us_update_status($id,$user_active);

            if($this->model_member->us_group_count($id)==0)
            {
                $data_array = array(
                    'user_id' 		=> $id,
                    'role_id' 		=> $user_role
                );
                $this->model_member->us_group_add($data_array);
            }
            else
            {
                $data_array = array(
                    'user_id' 		=> $id,
                    'role_id' 		=> $user_role
                );
                $this->model_member->us_group_update($id,$data_array);
            }

            /**
             * @var Upload Files
             */

            $this->model_ipost->avatar_upload('upload_photo',$id);
            setcookie('status','updated', time()+2);
            redirect(base_url(uri_string()).'.html');
        }
        $this->load->view('member_edit', $data);
    }
    public function add()
    {
        if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        /**
         *
         * @var List Menu
         *
         */
        $data['list_post_count'] 	    = $this->model_nav->po_list_count('post','0');
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count'] 	    = $this->model_nav->us_list_count();
        $data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);

        /*Show Data*/
        $data['row']				= $this->model_member->us_edit_role($this->uri->segment(3));

        /*Member Role*/
        $data['member_role'] 	    = $this->model_nav->us_list_role();

        /*User List*/
        $data['user_list']			= $this->model_member->us_list();

        if(isset($_POST['Pixel_Save']))
        {
            $user_role				= $this->input->post('user_role');
            $user_name				= $this->input->post('user_email');
            $user_password 			= $this->input->post('user_password');//($this->input->post('user_password')=='') ? '' : $this->input->post('user_password');
            $user_email 			= $this->input->post('user_email');
            $user_active 			= $this->input->post('user_active');

            $user_gender            = $this->input->post('user_gender');
            $user_first_name        = $this->input->post('user_first_name');
            $user_last_name         = $this->input->post('user_last_name');
            $user_phone             = $this->input->post('user_phone');
            $user_mobile            = $this->input->post('user_mobile');
            $user_address           = $this->input->post('user_address');
            $user_city              = $this->input->post('user_city');
            $user_province          = $this->input->post('user_province');
            $user_country           = $this->input->post('user_country');
            $user_postal            = $this->input->post('user_postal');
            $user_map               = $this->input->post('user_map');
            $user_profession        = $this->input->post('user_profession');
            $user_company           = $this->input->post('user_company');
            $user_facebook          = $this->input->post('user_facebook');
            $user_twitter           = $this->input->post('user_twitter');
            $user_instagram         = $this->input->post('user_instagram');
            $user_google            = $this->input->post('user_google');
            $user_birthday          = $this->input->post('user_birthday');
            $user_signature         = $this->input->post('user_signature');
            $user_join 		        = date('Y-m-d H:i:s');

            $data_array = array(
                'user_name' 	    => $user_name,
                'user_password' 	=> $user_password,
                'user_email' 	    => $user_email,
                'user_gender'       => $user_gender,
                'user_first_name'   => $user_first_name,
                'user_last_name'    => $user_last_name,
                'user_phone'        => $user_phone,
                'user_mobile'       => $user_mobile,
                'user_address'      => $user_address,
                'user_city'         => $user_city,
                'user_province'     => $user_province,
                'user_country'      => $user_country,
                'user_postal'       => $user_postal,
                'user_map'          => $user_map,
                'user_profession'   => $user_profession,
                'user_company'      => $user_company,
                'user_facebook'     => $user_facebook,
                'user_twitter'      => $user_twitter,
                'user_instagram'    => $user_instagram,
                'user_google'       => $user_google,
                'user_birthday'     => $user_birthday,
                'user_signature'    => $user_signature,
                'user_active' 		=> $user_active
            );
            $this->ion_auth->register($user_name, $user_password, $user_email, $data_array, $group_ids = array($user_role));
            //$this->ion_auth->register($data_array);
            $id 			    = $this->db->insert_id();

           /* $data_array = array(
                'user_id' 		=> $id,
                'role_id' 		=> $user_role
            );
            $this->model_member->us_group_add($data_array);*/

            /**
             * @var Upload Files
             */

            $this->model_ipost->avatar_upload('upload_photo',$id);
            setcookie('status','added_member', time()+2);
            redirect(base_url().'panel/member/edit/'.$id.'.html');
        }
        $this->load->view('member_add', $data);
    }
    public function delete()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		$this->load->model("model_member");
        $this->load->model("model_status");

		$this->model_member->us_delete($this->uri->segment(4));

        setcookie('status','deleted', time()+2);
		redirect(base_url().'panel/member/manage.html');
	}
    public function role()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		/**
		* 
		* @var List Menu
		* 
		*/
        $data['list_post_count'] 	    = $this->model_nav->po_list_count('post','0');
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count'] 	    = $this->model_nav->us_list_count();
        $data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);
		
		//pagination settings
        $page 						= $this->uri->segment(4);
        $config['base_url'] 		= base_url().'panel/member/role';
        $config['total_rows'] 		= $this->model_member->us_role_count();

        $config['use_page_numbers'] = TRUE;
        $config['per_page'] 		= ($this->uri->segment(6)) ? $this->uri->segment(6) : '10';
        //$config["uri_segment"] 		= 2;
        $choice 					= $config["total_rows"] / $config["per_page"];
        $config["num_links"] 		= floor($choice);
        $config_url 				= ($this->uri->segment(4)) ? '/'.$this->uri->segment(4).'.html' : '.html';
		
        //config for bootstrap pagination class integration
        $config['full_tag_open']    = '<ul class="pagination pagination-sm">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = '';
        $config['last_link']        = '';
        $config['suffix'] 			= $config_url;
        $config['first_tag_open']   = "<ul class=\"pagination pagination-sm\">";
        $config['first_tag_close']  = '</ul>';
        $config['prev_link']        = '&lsaquo; Prev';
        $config['prev_tag_open']    = '<li>';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = 'Next &rsaquo;</i>';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li> <a href="'.base_url().'panel/member_role/'.$this->uri->segment(3).'/'.$config["num_links"] .'/'.$this->uri->segment(5).'/1.html">Last &raquo;';
        $config['last_tag_close']   = '</a></li>';
        $config['cur_tag_open']     = '<li class="active"><a href="#">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        $config['num_links'] 		= 5;
        $page 						= ($page!='')? $page : 0;
        
        $this->pagination->initialize($config);
        
        $data['page'] 				= ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        
        if( $data['page'] == 0 ){
			$data['page'] = 0;
		}else{
			$data['page'] = ($data['page']-1)*$config['per_page'];
		}
		
        $data['role_list'] 			= $this->model_member->us_role($config["per_page"], $data['page']);
		
        $data['pagination'] 		= $this->pagination->create_links(); 
        
        if(isset($_POST['post-delete']) && $_POST['post-delete'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$role_id=$_POST['post-check'][$i];
				
				$this->model_member->us_role_delete($role_id);
			}
            setcookie('status','deleted', time()+2);
			redirect(base_url().'panel/member/role.html');
		}     

		$this->load->view('member_role',$data);
	}
	public function role_edit()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		/**
		* 
		* @var List Menu
		* 
		*/
		$data['list_post_count'] 	    = $this->model_nav->po_list_count('post','0');
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count'] 	    = $this->model_nav->us_list_count();
        $data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);
		
		/**
		* 
		* @var Data
		* 
		*/
		$data['row']				= $this->model_member->us_role_edit($this->uri->segment(4));
		
		if(isset($_POST['Pixel_Save']))
		{
			$id					= $this->uri->segment(4);
			$role_name 			= $this->input->post('role_name');
			$role_description 	= $this->input->post('role_description');
			
			$data_array = array(  
			'role_name' 		=> $role_name,  
			'role_description' 	=> $role_description 
			);  
			$this->model_member->us_role_update($id,$data_array);

            setcookie('status','updated', time()+2);
			redirect(base_url(uri_string()).'.html');
		}
		$this->load->view('member_role_edit', $data);
	}
	public function role_add()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		/**
		* 
		* @var List Menu
		* 
		*/
		$data['list_post_count'] 	    = $this->model_nav->po_list_count('post','0');
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count'] 	    = $this->model_nav->us_list_count();
        $data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);
		
		/**
		* 
		* @var Data
		* 
		*/
		$data['row']				= $this->model_member->us_role_edit($this->uri->segment(3));
		
		if(isset($_POST['Pixel_Save']))
		{
			$id					= $this->uri->segment(3);
			$role_name 			= $this->input->post('role_name');
			$role_description 	= $this->input->post('role_description');
			
			$data_array = array(  
			'role_name' 		=> $role_name,  
			'role_description' 	=> $role_description 
			);  
			$this->model_member->us_role_add($data_array);
			$id 				= $this->db->insert_id();

            setcookie('status','added', time()+2);
			redirect(base_url().'panel/member/role_edit/'.$id.'.html');
		}
		$this->load->view('member_role_add', $data);
	}
	public function role_delete()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		$this->load->model("model_member");
        $this->load->model("model_status");

		$this->model_member->us_role_delete($this->uri->segment(4));

        setcookie('status','deleted', time()+2);
		redirect(base_url().'panel/member/role.html');
    }
    public function type()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		$data['list_post_count'] 	    = $this->model_nav->po_list_count('post','0');
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count'] 	    = $this->model_nav->us_list_count();
        $data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);
        $data['cat_list']               = $this->model_nav->ca_list('post','0');

        //pagination settings
        if(isset($_POST['Pixel_Search']))
        {
            setcookie('status','search', time()+2);
            redirect(base_url().'panel/member/manage/search/'.$this->model_setting->my_simple_crypt($this->input->post('search'),$action = 'e').'.html');
        }

        if($this->uri->segment(5)!='')
        {
            $config = array();
            $config["base_url"]         = base_url().'panel/member/type/'.$this->uri->segment(4).'/';
            $total_row                  = $this->model_member->mb_cat_role_count($this->uri->segment(4));
            $config['suffix'] 			= '.html';
            $config['first_url']        = '1.html';
            $config["total_rows"]       = $total_row;
            $config["per_page"]         = ($this->uri->segment(4)!='') ? $this->uri->segment(4) : '10';
            $config['use_page_numbers'] = TRUE;
            $config['num_links']        = $total_row;
            $config['cur_tag_open']     = '&nbsp;<a class="active">';
            $config['cur_tag_close']    = '</a>';
            $config['next_link']        = 'Next';
            $config['prev_link']        = 'Prev';
            $config['num_links'] 		= 5;

            $this->pagination->initialize($config);
            if($this->uri->segment(5))
            {
                $page = ($this->uri->segment(5)-1)*$config["per_page"] ;
            }
            else
            {
                $page = 0;
            }
        }
        else 
        {
            $config = array();
            $config["base_url"]         = base_url().'panel/member/type/';
            $total_row                  = $this->model_member->mb_cat_role_count($this->uri->segment(4));
            $config['suffix'] 			= '.html';
            $config['first_url']        = '1.html';
            $config["total_rows"]       = $total_row;
            $config["per_page"]         = '10';
            $config['use_page_numbers'] = TRUE;
            $config['num_links']        = $total_row;
            $config['cur_tag_open']     = '&nbsp;<a class="active">';
            $config['cur_tag_close']    = '</a>';
            $config['next_link']        = 'Next';
            $config['prev_link']        = 'Prev';
            $config['num_links'] 		= 5;

            $this->pagination->initialize($config);
            if($this->uri->segment(4))
            {
                $page = ($this->uri->segment(4)-1)*$config["per_page"] ;
            }
            else
            {
                $page = 0;
            }
        }

        $data["user_list"]          = $this->model_member->mb_cat_role($this->uri->segment(4),$config["per_page"], $page); 

        $str_links                  = $this->pagination->create_links();
        $data["links"]              = explode('&nbsp;',$str_links );  

		if(isset($_POST['post-draft']) && $_POST['post-draft'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$user_id=$_POST['post-check'][$i];
				
				$this->model_member->us_update_status($user_id,'0');
			}
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/member/manage.html');
		}
        elseif(isset($_POST['post-publish']) && $_POST['post-publish'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$user_id=$_POST['post-check'][$i];
				
				$this->model_member->us_update_status($user_id,'1');
			}
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/member/manage.html');
		}
		elseif(isset($_POST['post-delete']) && $_POST['post-delete'] != "")
		{
            for ($i=0; $i<count($_POST['post-check']);$i++)
            {
                $user_id=$_POST['post-check'][$i];

                @unlink('./upload/avatar/'.$this->model_member->us_value($user_id,'user_avatar'));
                $this->model_member->us_delete($user_id);
            }
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/member/manage.html');
		}

        $this->load->view('member',$data);
    }
    public function logout() 
	{
		$this->ion_auth->logout();
		redirect(base_url().'panel.html');
    }
    public function profile()
    {
        if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        /**
         *
         * @var List Menu
         *
         */
        $data['list_post_count'] 	    = $this->model_nav->po_list_count('post','0');
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count'] 	    = $this->model_nav->us_list_count();
        $data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);

        /*Show Data*/
        $data['row']				= $this->model_member->us_edit_role($_SESSION['user_id']);

        /*Member Role*/
        $data['member_role'] 	    = $this->model_nav->us_list_role();

        /*User List*/
        $data['user_list']			= $this->model_member->us_list();

        if(isset($_POST['Pixel_Save']))
        {
            $id						= $_SESSION['user_id'];
            $user_password 			= $this->input->post('user_password');

            $user_gender 			= $this->input->post('user_gender');
            $user_first_name 		= $this->input->post('user_first_name');
            $user_last_name 		= $this->input->post('user_last_name');
            $user_phone 		    = $this->input->post('user_phone');
            $user_mobile 		    = $this->input->post('user_mobile');
            $user_address 		    = $this->input->post('user_address');
            $user_city 		        = $this->input->post('user_city');
            $user_province 		    = $this->input->post('user_province');
            $user_country 		    = $this->input->post('user_country');
            $user_postal 		    = $this->input->post('user_postal');
            $user_map               = $this->input->post('user_map');
            $user_profession 		= $this->input->post('user_profession');
            $user_company           = $this->input->post('user_company');
            $user_facebook          = $this->input->post('user_facebook');
            $user_twitter           = $this->input->post('user_twitter');
            $user_instagram         = $this->input->post('user_instagram');
            $user_google 		    = $this->input->post('user_google');
            $user_birthday 		    = $this->input->post('user_birthday');
            $user_signature 		= $this->input->post('user_signature');

            $data_array = array(
                'user_password' 	=> $user_password,
                'user_gender' 	    => $user_gender,
                'user_first_name' 	=> $user_first_name,
                'user_last_name' 	=> $user_last_name,
                'user_phone' 		=> $user_phone,
                'user_mobile' 		=> $user_mobile,
                'user_address' 	    => $user_address,
                'user_city' 	    => $user_city,
                'user_province' 	=> $user_province,
                'user_country' 		=> $user_country,
                'user_postal' 	    => $user_postal,
                'user_map'          => $user_map,
                'user_profession' 	=> $user_profession,
                'user_company'      => $user_company,
                'user_facebook'     => $user_facebook,
                'user_twitter'      => $user_twitter,
                'user_instagram'    => $user_instagram,
                'user_google' 	    => $user_google,
                'user_birthday' 	=> $user_birthday,
                'user_signature' 	=> $user_signature
            );
            $this->ion_auth->update($id,$data_array);

            /**
             * @var Upload Files
             */

            $this->model_ipost->avatar_upload('upload_photo',$id);
            setcookie('status','updated_member', time()+2);
            redirect(base_url(uri_string()).'.html');
        }
        $this->load->view('profile', $data);
    }
}
