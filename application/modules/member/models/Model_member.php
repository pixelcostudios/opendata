<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

class model_member extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}
	function us_list()
	{
		$this->db->select('user_id, user_name, user_password, user_email, user_avatar, user_gender, user_first_name, user_last_name, user_join, user_activity');
		$this->db->from("my_user");
		$query	= $this->db->get();
		return $query->result();
	}
	function us_list_type($role_name)
	{
		$this->db->select('my_user.user_id, user_name, user_email, user_avatar, user_gender, user_first_name, user_last_name, user_join, user_active, role_name, role_description');
		$this->db->from("my_user");
		$this->db->join('my_user_group', 'my_user.user_id = my_user_group.user_id','left');
		$this->db->join('my_user_role', 'my_user_role.role_id = my_user_group.role_id','left');
		$this->db->where('my_user_role.role_name',$role_name);
		$query	= $this->db->get();
		return $query->result();
	}
	function us_value($user_id,$user_value)
	{
		$this->db->select('user_id, user_name, user_password, user_email, user_avatar, user_gender, user_first_name, user_last_name, user_join');
		$this->db->from("my_user");
		$this->db->where('user_id',$user_id);
		$result	= $this->db->get();
		return $result->row($user_value);
	}
	function us_edit($user_id)
	{
		$this->db->select('user_id, user_name, user_password, user_email, user_avatar, user_gender, user_first_name, user_last_name, user_phone, user_mobile, user_signature, user_birthday, user_address, user_city, user_province, user_country, user_postal, user_profession, user_company, user_join, user_active');
		$this->db->from("my_user");
		$this->db->where('user_id',$user_id);
		$result	= $this->db->get();
		return $result->row();
	}
	function us_edit_username($user_email)
	{
		$this->db->select('user_id, user_name, user_password, user_email, user_avatar, user_gender, user_first_name, user_last_name, user_phone, user_mobile, user_signature, user_birthday, user_address, user_city, user_province, user_country, user_postal, user_profession, user_company, user_join');
		$this->db->from("my_user");
		$this->db->where('user_email',$user_email);
		$result	= $this->db->get();
		return $result->row();
	}
    function us_edit_role($user_id)
    {
        $this->db->select('*');
        $this->db->from("my_user");
        $this->db->join('my_user_group', 'my_user.user_id = my_user_group.user_id', 'left');
        $this->db->join('my_user_role', 'my_user_role.role_id = my_user_group.role_id', 'left');
        $this->db->where('my_user.user_id',$user_id);
        $result	= $this->db->get();
        return $result->row();
    }
	function us_cat($start,$limit)
	{
		$this->db->select('user_id, user_name, user_password, user_email, user_avatar, user_gender, user_first_name, user_last_name, user_join');
		$this->db->from("my_user");
		$this->db->limit($start,$limit);
		$query	= $this->db->get();
		return $query->result();
	}
	function us_cat_count()
	{
		$this->db->select('user_id, user_name, user_password, user_email, user_avatar, user_gender, user_first_name, user_last_name, user_join');
		$this->db->from("my_user");
		return $this->db->count_all_results();
	}
	function us_check_count($where_array)
	{
		$this->db->select('user_name, user_email');
		$this->db->from("my_user");
		$this->db->where($where_array);
		return $this->db->count_all_results();
	}
	function us_forgot_count($user_forgotten_password_code)
	{
		$this->db->select('user_forgotten_password_code');
		$this->db->from("my_user");
		$this->db->where('user_forgotten_password_code',$user_forgotten_password_code);
		return $this->db->count_all_results();
	}
	function us_registration_count($user_activation_code)
	{
		$this->db->select('user_activation_code');
		$this->db->from("my_user");
		$this->db->where('user_activation_code',$user_activation_code);
		return $this->db->count_all_results();
	}
	function us_forgot_user_id($user_forgotten_password_code)
	{
		$this->db->select('user_id,user_forgotten_password_code');
		$this->db->from("my_user");
		$this->db->where('user_forgotten_password_code',$user_forgotten_password_code);
		$result	= $this->db->get();
		return $result->row('user_id');
	}
	function us_registration_user_id($user_activation_code)
	{
		$this->db->select('user_id,user_activation_code');
		$this->db->from("my_user");
		$this->db->where('user_activation_code',$user_activation_code);
		$result	= $this->db->get();
		return $result->row('user_id');
	}
	function us_add($data_array)
	{
		$this->db->insert('my_user',$data_array);
		return $this->db->insert_id();
	}
	function us_update($user_id,$data_array)
	{
		$this->db->where('user_id', $user_id);
		$this->db->update('my_user',$data_array);
		return true;
	}
	function us_update_status($user_id,$user_active)
	{
		$this->db->set('user_active', $user_active); 
		$this->db->where('user_id', $user_id);
		$this->db->update('my_user');
		return true;
	}
	function us_delete($user_id)
	{
		$this->db->where('user_id',$user_id);
		$this->db->delete('my_user');
		return true;
	}
	/*
	 * module Group
	 * */
    function us_group_count($user_id)
    {
        $this->db->select('group_id, user_id, role_id');
        $this->db->from("my_user_group");
        $this->db->where('user_id', $user_id);
        return $this->db->count_all_results();
    }
    function us_group_list($user_id)
    {
        $this->db->select('group_id, user_id, role_id');
        $this->db->from("my_user_group");
        $this->db->where('user_id', $user_id);
        $query	= $this->db->get();
		return $query->result();
    }
    function us_group_role_count($user_id,$role_id)
    {
        $this->db->select('group_id, user_id, role_id');
        $this->db->from("my_user_group");
        $this->db->where('user_id', $user_id);
        $this->db->where('role_id', $role_id);
        return $this->db->count_all_results();
    }
    function us_group_add($data_array)
    {
        $this->db->insert('my_user_group',$data_array);
        return $this->db->insert_id();
    }
    function us_group_update($user_id,$data_array)
    {
        $this->db->where('user_id', $user_id);
        $this->db->update('my_user_group',$data_array);
        return true;
    }
	/**
	* module Member
	*/
	function mb_cat($start,$limit)
	{
		$this->db->select('my_user.user_id, user_name, user_email, user_avatar, user_gender, user_first_name, user_last_name, user_join, user_active, role_name, role_description');
		$this->db->from("my_user");
		$this->db->join('my_user_group', 'my_user.user_id = my_user_group.user_id', 'left');
		$this->db->join('my_user_role', 'my_user_role.role_id = my_user_group.role_id', 'left');
		$this->db->limit($start,$limit);
		$this->db->order_by('my_user.user_id','ASC');
		$query	= $this->db->get();
		return $query->result();
	}
	function mb_cat_count()
	{
		$this->db->select('my_user.user_id, user_name, user_email, user_avatar, user_gender, user_first_name, user_last_name, user_join, user_active');
		$this->db->from("my_user");
		$this->db->join('my_user_group', 'my_user.user_id = my_user_group.user_id','left');
		$this->db->join('my_user_role', 'my_user_role.role_id = my_user_group.role_id','left');
		$this->db->order_by('my_user.user_id','ASC');
		return $this->db->count_all_results();
	}
	function mb_cat_role($role_name,$start,$limit)
	{
		$this->db->select('my_user.user_id, user_name, user_email, user_avatar, user_gender, user_first_name, user_last_name, user_company, user_join, user_active, role_name, role_description');
		$this->db->from("my_user");
		$this->db->join('my_user_group', 'my_user.user_id = my_user_group.user_id','left');
		$this->db->join('my_user_role', 'my_user_role.role_id = my_user_group.role_id','left');
		$this->db->where('my_user_role.role_name',$role_name);
		$this->db->order_by('user_join','DESC');
		$this->db->limit($start,$limit);
		$query	= $this->db->get();
		return $query->result();
	}
	function mb_cat_role_count($role_name)
	{
        $this->db->select('my_user.user_id, user_name, user_email, user_avatar, user_gender, user_first_name, user_last_name, user_company, user_join, user_active, role_name, role_description');
        $this->db->from("my_user");
        $this->db->join('my_user_group', 'my_user.user_id = my_user_group.user_id','left');
        $this->db->join('my_user_role', 'my_user_role.role_id = my_user_group.role_id','left');
        $this->db->where('my_user_role.role_name',$role_name);
        $this->db->order_by('user_join','DESC');
		return $this->db->count_all_results();
	}
	function mb_search($role_name,$cat,$start,$limit)
	{
		$this->db->select('my_user.user_id, user_name, user_email, user_avatar, user_gender, user_first_name, user_last_name, user_phone, user_mobile, user_signature, user_address, user_city, user_province, user_country, user_postal, user_profession, user_company, user_join, user_active, role_name, role_description');
		$this->db->from("my_user");
		$this->db->group_start();
		$this->db->like('user_name',$cat);
		$this->db->or_like('user_email',$cat);
		$this->db->or_like('user_first_name',$cat);
		$this->db->or_like('user_last_name',$cat);
		$this->db->or_like('user_phone',$cat);
		$this->db->or_like('user_mobile',$cat);
		$this->db->or_like('user_signature',$cat);
		$this->db->or_like('user_address',$cat);
		$this->db->or_like('user_city',$cat);
		$this->db->or_like('user_province',$cat);
		$this->db->or_like('user_country',$cat); 
		$this->db->or_like('user_postal',$cat);
		$this->db->or_like('user_profession',$cat);
		$this->db->or_like('user_company',$cat);
		$this->db->group_end();
		$this->db->join('my_user_group', 'my_user.user_id = my_user_group.user_id','left');
		$this->db->join('my_user_role', 'my_user_role.role_id = my_user_group.role_id','left');
		$this->db->where('my_user_role.role_name',$role_name);
		$this->db->group_by('my_user_role.role_id');
		$this->db->limit($start,$limit);
		$query	= $this->db->get();
		return $query->result();
	}
	function mb_search_count($role_name,$cat)
	{
        $this->db->select('my_user.user_id, user_name, user_email, user_avatar, user_gender, user_first_name, user_last_name, user_phone, user_mobile, user_signature, user_address, user_city, user_province, user_country, user_postal, user_profession, user_company, user_join, user_active, role_name, role_description');
		$this->db->from("my_user");
		$this->db->group_start();
		$this->db->like('user_name',$cat);
		$this->db->or_like('user_email',$cat);
		$this->db->or_like('user_first_name',$cat);
		$this->db->or_like('user_last_name',$cat);
		$this->db->or_like('user_phone',$cat);
		$this->db->or_like('user_mobile',$cat);
		$this->db->or_like('user_signature',$cat);
		$this->db->or_like('user_address',$cat);
		$this->db->or_like('user_city',$cat);
		$this->db->or_like('user_province',$cat);
		$this->db->or_like('user_country',$cat); 
		$this->db->or_like('user_postal',$cat);
		$this->db->or_like('user_profession',$cat);
		$this->db->or_like('user_company',$cat);
		$this->db->group_end();
		$this->db->join('my_user_group', 'my_user.user_id = my_user_group.user_id','left');
		$this->db->join('my_user_role', 'my_user_role.role_id = my_user_group.role_id','left');
		$this->db->where('my_user_role.role_name',$role_name);
		$this->db->group_by('my_user_role.role_id');
		return $this->db->count_all_results();
	}
	function us_role($start,$limit)
	{
		$this->db->select('role_id, role_name, role_setting, role_description');
		$this->db->from("my_user_role");
		$this->db->limit($start,$limit);
		$query	= $this->db->get();
		return $query->result();
	}
	function us_role_count()
	{
		$this->db->select('role_id');
		$this->db->from("my_user_role");
		return $this->db->count_all_results();
	}
	function us_role_add($data_array)
	{
		$this->db->insert('my_user_role',$data_array);
		return $this->db->insert_id();
	}
	function us_role_edit($role_id)
	{
		$this->db->select('role_id, role_name, role_setting, role_description');
		$this->db->from("my_user_role");
		$this->db->where('role_id',$role_id);
		$result	= $this->db->get();
		return $result->row();
	}
	function us_role_update($role_id,$data_array)
	{
		$this->db->where('role_id', $role_id);
		$this->db->update('my_user_role',$data_array);
		return true;
	}
	function us_role_delete($role_id)
	{
		$this->db->where('role_id',$role_id);
		$this->db->delete('my_user_role');
		return true;
	}
	//User with Outlet
	function us_outlet_list($role_name)
	{
		$this->db->select('*');
		$this->db->from("my_user");
		$this->db->join("my_user_group","my_user.user_id=my_user_group.user_id");
		$this->db->join("my_user_role","my_user_group.role_id=my_user_role.role_id");
		$this->db->join("my_outlet_group","my_user.user_id=my_outlet_group.user_id",'left');
		$this->db->join("my_outlet","my_outlet_group.outlet_id=my_outlet.outlet_id", 'left');
		$this->db->where("my_user_role.role_name",$role_name);
		$query	= $this->db->get();
		return $query->result();
	}
	function us_outlet_status_list($user_active,$role_name)
	{
		$this->db->select('*');
		$this->db->from("my_user");
		$this->db->join("my_user_group","my_user.user_id=my_user_group.user_id");
		$this->db->join("my_user_role","my_user_group.role_id=my_user_role.role_id");
		$this->db->join("my_outlet_group","my_user.user_id=my_outlet_group.user_id");
		$this->db->join("my_outlet","my_outlet_group.outlet_id=my_outlet.outlet_id");
		$this->db->where("my_user.user_active",$user_active);
		$this->db->where("my_user_role.role_name",$role_name);
		$query	= $this->db->get();
		return $query->result();
	}
}