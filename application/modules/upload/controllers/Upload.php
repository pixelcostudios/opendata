<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//require APPPATH . "third_party/MX/Controller.php";

	function __construct()
	{
		parent::__construct();
        $this->load->helper(array('url'));
		$this->load->helper(array('path'));
	}
	public function index()
	{
		echo 'uploaded';
	}
	public function crop()
    {	
		$this->load->model('m_upload');
		$width 	= ((int)$this->uri->segment(3)) ? $this->uri->segment(3) : '300';
		$height = ((int)$this->uri->segment(4)) ? $this->uri->segment(4) : '300';

		if($this->uri->segment(5)!='' AND $width AND $height)
    	{
			if (file_exists('upload/'.$this->uri->segment(5)))
			{
				$result = base_url().'upload/'.$this->uri->segment(5);
			}
			else 
			{
				$result	= base_url().'themes/default/img/default.png';
			}
		}
		else 
		{
			$result	= base_url().'themes/default/img/default.png';
		}
		
		$handle = curl_init($result);
		curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
		$response = curl_exec($handle);
		$httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);

		$this->m_upload->iCrop_Browser($result, $width, $height);
		curl_close($handle);
    }
    public function resize()
    {
		$this->load->model('m_upload');	
		$width 	= ((int)$this->uri->segment(3)) ? $this->uri->segment(3) : '300';
		if($this->uri->segment(4)!='' AND $width)
		{
			if (file_exists('upload/'.$this->uri->segment(4)))
			{
				$result = base_url().'upload/'.$this->uri->segment(4);
			}
			else 
			{
				$result	= base_url().'themes/default/img/default.png';
			}
		}
		else 
		{
			$result	= base_url().'themes/default/img/default.png';
		}
		
		$handle = curl_init($result);
		curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
		$response = curl_exec($handle);
		$httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);

		$this->m_upload->iResize_Browser($result, $width);
		curl_close($handle);
    }
}
