<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Panel extends MX_Controller {
	function __construct()
    {
        parent::__construct();
		$this->load->library('ion_auth');
        $this->load->helper(array('form', 'url', 'string'));
		$this->load->helper(array('path'));
    }
	public function index()
	{
		//$this->load->model("back/model_setting");

		if ($this->ion_auth->logged_in())
		{
			redirect(base_url().'panel/dashboard.html');
		}
		$this->load->library('form_validation');
        $this->form_validation->set_rules('user_name', 'Username', 'trim|required');
        $this->form_validation->set_rules('user_password', 'Password', 'required');
        if ($this->form_validation->run() === FALSE)
        {
			$this->load->helper('form');
            $this->load->view('login');
        }
        else
        {
            $remember = (bool) $this->input->post('remember');
            if ($this->ion_auth->login($this->input->post('user_name'), $this->input->post('user_password'), $remember))
            {
            	$this->ion_auth->increase_login_attempts($this->input->post('user_name'));
                redirect(base_url().'panel/dashboard.html');
            }
            else
            {   // $_SESSION['auth_message'] = $this->ion_auth->errors(); // $this->session->mark_as_flash('auth_message');
                $this->session->set_flashdata('auth_message', $this->ion_auth->errors());
                redirect(base_url().'panel');
            }
        }
	}
	public function login()
	{
		if ($this->ion_auth->logged_in())
		{
			redirect(base_url().'panel/dashboard.html');
		}
		$this->load->library('form_validation');
        $this->form_validation->set_rules('user_name', 'Username', 'trim|required');
        $this->form_validation->set_rules('user_password', 'Password', 'required');
        if ($this->form_validation->run() === FALSE)
        {
			$this->load->helper('form');
            $this->load->view('login');
        }
        else
        {
            $remember = (bool) $this->input->post('remember');
            if ($this->ion_auth->login($this->input->post('user_name'), $this->input->post('user_password'), $remember))
            {
            	$this->ion_auth->increase_login_attempts($this->input->post('user_name'));
                redirect(base_url().'panel/dashboard.html');
            }
            else
            {   // $_SESSION['auth_message'] = $this->ion_auth->errors(); // $this->session->mark_as_flash('auth_message');
                $this->session->set_flashdata('auth_message', $this->ion_auth->errors());
                redirect(base_url().'panel');
            }
        }
	}
	public function logout()
    {
        $this->ion_auth->logout();
		redirect(base_url().'panel');
    }
    public function register()
    {
        echo 'samples';
    }
}
