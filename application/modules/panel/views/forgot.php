<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Forgot password</title>

    <link href="<?php echo base_url()?>themes/default/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo base_url()?>themes/default/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/style.css" rel="stylesheet">
    <style type="text/css">
        .margin-bottom-20{
            margin-bottom: 20px;
        }
        .btn-primary:hover{
            background: #369212;
            border: 1px solid #369212;
        }
        .btn-primary{
            background: #dc4202;
            border: 1px solid #dc4202;
        }
        .passwordBox {
            padding: 0px 20px 20px 20px;
        }
        .middle-box h1 {
            margin-top: 0;
        }
    </style>
</head>

<body class="gray-bg">

    <div class="passwordBox text-center loginscreen  animated fadeInDown">
        <div class="row text-center">

            <div class="col-md-12">
            <div>

                <h1 class="logo-name margin-bottom-20"><img src="<?php echo base_url()?>themes/default/img/logo.svg" width="150"></h1>

            </div>
            <h3 class="margin-bottom-20">Welcome to <?php echo $this->model_setting->setting('c1_company');?></h3>
                <div class="ibox-content">

                    <h2 class="font-bold">Forgot password</h2>

                    <p>
                        <?php echo isset($_SESSION['auth_status']) ? $_SESSION['auth_status'] : FALSE; ?>
                    </p>

                    <p>
                        <?php echo isset($_SESSION['auth_message']) ? $_SESSION['auth_message'] : FALSE; ?>
                    </p>

                    <div class="row">

                        <div class="col-lg-12">
                            <form class="m-t" role="form" method="post">
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Email address" name="email" required="">
                                </div>

                                <button type="submit" name="Pixel_Forgot" class="btn btn-primary block full-width m-b">Send new password</button>
								<p class="text-muted text-center"><small>Already have an account?</small></p>
                				<a class="btn btn-sm btn-white btn-block" href="<?php echo base_url()?>panel/">Login</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-8 text-left">
                Copyright <?php echo $this->model_setting->setting('c1_company');?>
            </div>
            <div class="col-md-4 text-right">
               <small>© <?php echo date('Y')?></small>
            </div>
        </div>
    </div>

</body>

</html>
