<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link href="<?php echo base_url()?>themes/default/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/style.css" rel="stylesheet">
    <style type="text/css">
        .margin-bottom-20{
            margin-bottom: 20px;
        }
        .btn-primary:hover{
            background: #369212;
            border: 1px solid #369212;
        }
        .btn-primary{
            background: #dc4202;
            border: 1px solid #dc4202;
        }
        .middle-box h1 {
            margin-top: 0;
        }
    </style>
</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name margin-bottom-20"><img src="<?php echo base_url()?>themes/default/img/logo.png" width="150"></h1>

            </div>
            <div class="ibox-content">
            <h3>Welcome to Login Area <?php //echo $this->model_setting->setting('c1_company');?></h3>
            
            <!--<p>Perfectly designed and precisely prepared admin theme with over 50 pages with extra new web app views.</p>-->
            
            <p>Login in. To see it in action.</p>
            <p>
                <?php
                echo isset($_SESSION['auth_message']) ? $_SESSION['auth_message'] : FALSE;
                ?>
            </p>
            <form class="m-t" action="<?php echo base_url(); ?>panel" method="post">
                <div class="form-group">
                    <input type="text" name="user_name" class="form-control" placeholder="Username" required="">
                </div>
                <div class="form-group">
                    <input type="password" name="user_password" class="form-control" placeholder="Password" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                <a href="<?php echo base_url()?>panel/forgot.html"><small>Forgot password?</small></a>
                <!--<p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="<?php echo base_url()?>panel/register.html">Create an account</a>-->
            </form>
            </div>
            <!--<p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small> </p>-->
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="<?php echo base_url()?>themes/default/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/bootstrap.min.js"></script>

</body>

</html>