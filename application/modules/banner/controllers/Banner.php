<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//require APPPATH . "third_party/MX/Controller.php";

	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
        $this->load->library('email');
        // $this->load->library('pdf');
        $this->load->helper(array('form'));
        $this->load->helper(array('url'));
		$this->load->helper(array('path'));
		$this->load->library("pagination");
		// $this->load->model("model_meta");
    }
    public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		else {
			redirect(base_url().'panel/banner/manage.html');
		}
	}
	public function manage()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		// $this->load->model("model_setting");
		$this->load->model("menu/model_nav");	
		$this->load->model("categories/model_categories");	
		$this->load->model("post/model_ipost");
        $this->load->model("model_status");
        $this->load->model("model_meta");
		/**
		* 
		* @var List Menu
		* 
		*/
        $data['list_post_count'] 	    = $this->model_nav->po_list_count('post','0');
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count'] 	    = $this->model_nav->us_list_count();
        $data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);
		
		/*categories List*/
		$data['cat_list'] 			= $this->model_categories->ca_list('banner','0','DESC');

        $post_type 					= 'banner';

        //pagination settings
        if(isset($_POST['Pixel_Search']))
        {
            setcookie('status','search', time()+2);
            redirect(base_url().'panel/banner_search/'.$post_type.'/'.$this->input->post('search').'.html');
        }

        if($this->uri->segment(5)!='')
        {
            $config = array();
            $config["base_url"]         = base_url().'panel/banner/manage/'.$this->uri->segment(4).'/';
            $total_row                  = $this->model_ipost->ip_post_all_count($post_type);
            $config['suffix'] 			= '.html';
            $config['first_url']        = '1.html';
            $config["total_rows"]       = $total_row;
            $config["per_page"]         = ($this->uri->segment(4)!='') ? $this->uri->segment(4) : '10';
            $config['use_page_numbers'] = TRUE;
            $config['num_links']        = $total_row;
            $config['cur_tag_open']     = '&nbsp;<a class="active">';
            $config['cur_tag_close']    = '</a>';
            $config['next_link']        = 'Next';
            $config['prev_link']        = 'Prev';
            $config['num_links'] 		= 5;

            $this->pagination->initialize($config);
            if($this->uri->segment(5))
            {
                $page = ($this->uri->segment(5)-1)*$config["per_page"] ;
            }
            else
            {
                $page = 0;
            }
        }
        else 
        {
            $config = array();
            $config["base_url"]         = base_url().'panel/banner/manage/';
            $total_row                  = $this->model_ipost->ip_post_all_count($post_type);
            $config['suffix'] 			= '.html';
            $config['first_url']        = '1.html';
            $config["total_rows"]       = $total_row;
            $config["per_page"]         = '10';
            $config['use_page_numbers'] = TRUE;
            $config['num_links']        = $total_row;
            $config['cur_tag_open']     = '&nbsp;<a class="active">';
            $config['cur_tag_close']    = '</a>';
            $config['next_link']        = 'Next';
            $config['prev_link']        = 'Prev';
            $config['num_links'] 		= 5;

            $this->pagination->initialize($config);
            if($this->uri->segment(4))
            {
                $page = ($this->uri->segment(4)-1)*$config["per_page"] ;
            }
            else
            {
                $page = 0;
            }
        }

        $data["post_list"]          = $this->model_ipost->ip_post_all($post_type,'DESC',$config["per_page"], $page);

        $str_links                  = $this->pagination->create_links();
        $data["links"]              = explode('&nbsp;',$str_links );
	    
	    if(isset($_POST['post-draft']) && $_POST['post-draft'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$post_id=$_POST['post-check'][$i];
				
				$this->model_ipost->ip_update_status($post_id,'0');
			}
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/banner.html');
		}
	    elseif(isset($_POST['post-publish']) && $_POST['post-publish'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$post_id=$_POST['post-check'][$i];
				
				$this->model_ipost->ip_update_status($post_id,'1');
			}
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/banner.html');
		}
		elseif(isset($_POST['post-delete']) && $_POST['post-delete'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$post_id=$_POST['post-check'][$i];
				
				$this->model_ipost->ip_delete($post_id);
			}
            setcookie('status','deleted', time()+2);
			redirect(base_url().'panel/banner.html');
		}
		$this->load->view('banner',$data);
	}
    public function edit()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		// $this->load->model("back/model_setting");
		$this->load->model("menu/model_nav");
		$this->load->model("categories/model_categories");
		$this->load->model("post/model_ipost");
		$this->load->model("member/model_member");
        $this->load->model("model_status");
		$this->load->model("model_meta");
		/**
		* 
		* @var List Menu
		* 
		*/
        $data['list_post_count'] 	    = $this->model_nav->po_list_count('post','0');
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count'] 	    = $this->model_nav->us_list_count();
        $data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);
		
		/**
		* 
		* @var Data
		* 
		*/
		$data['row']				= $this->model_ipost->ip_edit($this->uri->segment(4));
		
		$data['images']				= $this->model_ipost->ip_list($this->uri->segment(4),'banner','DESC');
		
		/*categories List*/
        $data['cat_list']           = $this->model_categories->ca_list('banner','0','DESC');
        $data['cat_array']          = $this->model_meta->me_source_list('categories_banner',$this->uri->segment(4));
        
        if(count($data['cat_array'])>0)
        {
            foreach ($data['cat_array'] as $key_cat) 
            {
                $array_cat[] = $key_cat->meta_dest;
            }
        }
        else
        {
            $array_cat = array('0' => 0);
        }
        
        $data['array_cat'] = $array_cat;
		
		/*User List*/
		$data['user_list']			= $this->model_member->us_list();
		
		if(isset($_POST['Pixel_Save']))
		{
			$id						= $this->uri->segment(4);
			$user_id 				= $this->input->post('user_id');
			//$post_all 				= $this->input->post('cat_id');
			$post_slug 				= strtolower(url_title($this->input->post('ipost_title')));
			$post_title 			= $this->input->post('ipost_title');
			$post_content 			= $this->input->post('ipost_content');
			$post_embed 			= $this->input->post('ipost_embed');
			$post_link 				= $this->input->post('ipost_link');
			$post_comment 			= $this->input->post('ipost_comment');
			$date 					= ($this->input->post('date')=='') ? date('Y-m-d H:i:s') : $this->input->post('date');
			$status 				= $this->input->post('status');
			
			$data_array = array(  
			'user_id' 		=> $user_id,  
			//'ipost_all' 	=> $post_all,
			'ipost_slug' 	=> $post_slug,
			'ipost_title' 	=> $post_title,
			'ipost_content' => $post_content,  
			'ipost_embed' 	=> $post_embed,  
			'ipost_link' 	=> $post_link,  
			'ipost_comment' => $post_comment,  
			'date' 			=> $date,  
			'status' 		=> $status 
			);  
			$this->model_ipost->ip_update($this->uri->segment(3),$data_array);

            if(count($this->input->post('categories'))>0)
            {
                foreach ($this->input->post('categories') as $categories)
                {

                    if($this->model_meta->me_source_check('categories_banner',$id,$categories)=='0')
                    {
                        $data_array = array(  
                            'meta_type'     => 'categories_banner',  
                            'meta_source'   => $id,  
                            'meta_dest'     => $categories,
                            'meta_title'    => $this->model_categories->ca_title($categories),
                            'meta_slug'     => $this->model_categories->ca_slug($categories),
                            'meta_date'     => date('Y-m-d H:i:s'),
                            'meta_status'   => '1'
                        ); 
                        $this->model_meta->me_add($data_array);
                    }
                    else
                    {
                        $this->model_meta->me_update_status('categories_banner',$id,$categories,'1'); 

                        $meta_list = $this->model_meta->me_list_not_in('categories_banner',$id,$this->input->post('categories'));
                        //print_r($meta_list);
                        foreach ($meta_list as $key) 
                        {
                            $this->model_meta->me_update_status('categories_banner',$id,$key->meta_dest,'0');
                        }
                    }
                }
            }
			
			/**
			* @var Upload Files
			*/
			
			$this->model_ipost->ibanner_upload('upload_photo','post',$id);
			$this->model_ipost->ibanner_upload('upload_swf','swf',$id);

            setcookie('status','updated', time()+2);
			redirect(base_url(uri_string()).'.html');
		}
		$this->load->view('banner_edit', $data);
	}
	public function add()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		// $this->load->model("back/model_setting");
		$this->load->model("menu/model_nav");
		$this->load->model("categories/model_categories");
		$this->load->model("post/model_ipost");
		$this->load->model("member/model_member");
        $this->load->model("model_status");
        $this->load->model("model_meta");
		/**
		* 
		* @var List Menu
		* 
		*/
        $data['list_post_count'] 	    = $this->model_nav->po_list_count('post','0');
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count'] 	    = $this->model_nav->us_list_count();
        $data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);
		
		/**
		* 
		* @var Data
		* 
		*/
		$data['row']				= $this->model_ipost->ip_edit($this->uri->segment(3));
		
		$data['images']				= $this->model_ipost->ip_list($this->uri->segment(3),'banner','DESC');
		
		/*categories List*/
		$cat_type					= ($this->uri->segment(4)=='') ? 'banner' : $this->uri->segment(4);
		$data['cat_list'] 			= $this->model_categories->ca_list($cat_type,'0','DESC');
		
		/*User List*/
		$data['user_list']			= $this->model_member->us_list();
		
		if(isset($_POST['Pixel_Save']))
		{
			$user_id 				= $this->input->post('user_id');
            $post_type              = 'banner';
			$post_slug 				= strtolower(url_title($this->input->post('ipost_title')));
			$post_title 			= $this->input->post('ipost_title');
			$post_content 			= $this->input->post('ipost_content');
			$post_embed 			= $this->input->post('ipost_embed');
			$post_link 				= $this->input->post('ipost_link');
			$post_comment 			= $this->input->post('ipost_comment');
			$date 					= ($this->input->post('date')=='') ? date('Y-m-d H:i:s') : $this->input->post('date');
			$status 				= $this->input->post('status');
			
			$data_array = array(  
    			'user_id' 		=> $user_id,  
                'ipost_type'    => $post_type,
    			'ipost_slug' 	=> $post_slug,
    			'ipost_title' 	=> $post_title,
    			'ipost_content' => $post_content,  
    			'ipost_embed' 	=> $post_embed,  
    			'ipost_link' 	=> $post_link,  
    			'ipost_comment' => $post_comment,  
    			'date' 			=> $date,  
    			'status' 		=> $status 
			);  
			$this->model_ipost->ip_add($data_array);
			$id 			= $this->db->insert_id();

            if(count($this->input->post('categories'))>0)
            {
                foreach ($this->input->post('categories') as $categories)
                {

                    $data_array = array(  
                        'meta_type'     => 'categories_banner',  
                        'meta_source'   => $id,  
                        'meta_dest'     => $categories,
                        'meta_title'    => $this->model_categories->ca_title($categories),
                        'meta_slug'     => $this->model_categories->ca_slug($categories),
                        'meta_date'     => date('Y-m-d H:i:s'),
                        'meta_status'   => '1'
                    ); 
                    $this->model_meta->me_add($data_array);
                }
            }
			
			/**
			* @var Upload Files
			*/
			
			$this->model_ipost->ibanner_upload('upload_photo','post',$id);
			$this->model_ipost->ibanner_upload('upload_swf','swf',$id);

            setcookie('status','added', time()+2);
			redirect(base_url().'panel/banner/edit/'.$id.'.html');
		}
		$this->load->view('banner_add', $data);
    }
    public function delete() 
    {
        $this->load->model("post/model_ipost");
        $this->model_ipost->ip_delete($this->uri->segment(4));
        setcookie('status','deleted', time()+2);
        redirect(base_url().'panel/banner.html');
    }
}
