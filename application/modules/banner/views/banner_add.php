<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Banner Editor</title>

    <link href="<?php echo base_url()?>themes/default/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/js/plugins/multiselect/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>themes/default/js/plugins/datetime/bootstrap-datetimepicker.css" rel="stylesheet" />
	<link href="<?php echo base_url()?>themes/default/css/plugins/select2/select2.min.css" rel="stylesheet">
    <!-- Toastr style -->
    <link href="<?php echo base_url()?>themes/default/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/custom.css" rel="stylesheet">

</head>

<body class="fixed-sidebar">

    <div id="wrapper">
    <?php $this->load->view('menu/nav');?>

        <div id="page-wrapper" class="gray-bg">
        <?php $this->load->view('menu/nav_top');?>
        
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Banner Editor</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>panel/dashboard.html">Home</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url()?>panel/banner.html">Banner</a>
                        </li>
                        <li class="active">
                            <strong>Banner Editor</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content">

            <div class="row">
            <form name="myform" method="post" enctype="multipart/form-data">
                <div class="col-lg-9 padding-none">
	                <div class="ibox float-e-margins">
	                    <div class="ibox-title">
	                    	<h5 class="pull-left margin-right-20">Banner Editor</h5>
	                    	<a href="<?php echo base_url()?>panel/banner/add.html" class="btn btn-default pull-left">Add New Banner</a>
	                        <button type="submit" name="Pixel_Save" class="btn btn-primary float-right">Save Banner</button>
	                    </div>
	                    <div class="ibox-content">
	                    <div class="form-group">
							<label for="title">Title ID </label>
							<input type="text" class="form-control" id="title" name="ipost_title" placeholder="Title ID" required="required">
						</div>
                        <div class="form-group col-lg-4 col-sm-4 col-xs-12 padding-left-0 padding-xs-none multi_select clearfix">
                        <label for="category">Category</label><br>
							<select id="example-multiple-selected" multiple="multiple" class="form-control" name="category[]">
                            <?php foreach($cat_list as $row_cat){?>
                            <option value="<?php echo $row_cat->cat_id;?>"><?php echo $row_cat->cat_title;?></option>
                                <?php $cat_list_sub = $this->model_categories->ca_list($row_cat->cat_type,$row_cat->cat_id,'DESC'); foreach($cat_list_sub as $row_cat_sub){?>
                                    <option value="<?php echo $row_cat_sub->cat_id;?>">|-- <?php echo $row_cat_sub->cat_title;?></option>
                                    <?php $cat_list_sub1 = $this->model_categories->ca_list($row_cat_sub->cat_type,$row_cat_sub->cat_id,'DESC'); foreach($cat_list_sub1 as $row_cat_sub1){?>
                                        <option value="<?php echo $row_cat_sub1->cat_id;?>">|--|-- <?php echo $row_cat_sub1->cat_title;?></option>
                                    <?php }?>
                                <?php }?>
                            <?php }?>
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-sm-4 col-xs-12 padding-none padding-xs-none">
                            <label for="status">Status</label>
                            <select name="status" class="form-control">
                            	<option value="1">Publish</option>
								<option value="0">Draft</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-sm-4 col-xs-12 padding-right-0 padding-xs-none">
                            <label for="date">Date</label>
                            <div class="input-group">
                                <input name="date" class="form-control date-picker" type="text" data-date-format="yyyy-mm-dd hh:mm">
                                <span class="input-group-addon tooltip-demo">
                                    <i class="fa fa-calendar" data-toggle="tooltip" data-placement="top" title="Calendar"></i>
                                </span>
                            </div>
                        </div>
						<div class="form-group clear">
							<label for="title">Website Link </label>
							<input type="text" class="form-control" name="ipost_link" placeholder="Website Link">
						</div>
						<div class="form-group clear">
							<label for="title">Embed Code </label>
							<textarea rows="3" name="ipost_embed" class="form-control"></textarea>
						</div>
						<div class="form-group clear">
							<label for="content">Content ID</label>
                            <textarea id="content_id" name="ipost_content"></textarea>
						</div>
						<div class="form-group col-lg-6 col-sm-6 col-xs-12 padding-left-0 padding-xs-none">
			                <label for="Comment">Comment</label>
			                <select name="ipost_comment" class="form-control">
			                	<option value="1">Yes</option>
								<option value="0">No</option>
			                </select>
			            </div>
			            <div class="form-group col-lg-6 col-sm-6 col-xs-12 padding-right-0 padding-xs-none">
			                <label for="Username">Username</label>
			                <select name="user_id" class="form-control">
			                <?php foreach($user_list as $row_user){?>
							<option value="<?php echo $row_user->user_id;?>"><?php echo $row_user->user_name;?> | <?php echo $row_user->user_first_name;?> <?php echo $row_user->user_first_name;?></option>
							<?php }?>
			                </select>
			            </div>
						<div class="clear"></div>
	                    </div>
	                </div>
	            </div>
	            <div class="col-lg-3 padding-right-0">
	            <div class="bar-title white-bg">
	            	<div class="tabs-container">
                        <div class="tabs-left">
                            <ul class="nav nav-tabs tooltip-demo">
                                <li class="active"><a data-toggle="tab" href="#tab_images" aria-expanded="true"> <i class="fa fa-picture-o" data-toggle="tooltip" data-placement="top" title="Images"></i></a></li>
                                <li class=""><a data-toggle="tab" href="#tab_flash" aria-expanded="false"><i class="fa fa-flash" data-toggle="tooltip" data-placement="top" title="Flash"></i></a></li>
                            </ul>
                            <div class="tab-content ">
                                <div id="tab_images" class="tab-pane active">
                                    <div class="panel-body">
                                        <div class="input-file"><input type="file" name="upload_photo[]" multiple="multiple" id="input_images"></div>
                                    </div>
                                </div>
                                <div id="tab_flash" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="input-file"><input type="file" name="upload_swf[]" multiple="multiple" id="input_icon"></div>
										
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    <!--End Tab-->
	            </div>
	            </div>
            </form>
            </div>


            </div>
        <div class="footer">
	        <div class="pull-right">
	            <strong><?php echo $this->model_nav->disk_size();?></strong> Free.
	        </div>
	        <div>
	            <strong>Copyright</strong> <?php //echo $this->model_setting->setting('company');?> &copy; <?php echo date('Y');?>
	        </div>
	    </div>

        </div>
        </div>

    <!-- Mainly scripts -->
    <script src="<?php echo base_url()?>themes/default/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url()?>themes/default/js/inspinia.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/pace/pace.min.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/multiselect/dist/js/bootstrap-multiselect.js" type="text/javascript" ></script>
    
    <!--CK Editor-->
    <script src="<?php echo base_url()?>themes/default/js/plugins/ckeditor/ckeditor.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/ckeditor/en.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/ckeditor/id.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/ckeditor/ck.js"></script>

	<script src="<?php echo base_url()?>themes/default/js/plugins/select2/select2.full.min.js"></script>
	
	<!--Files Scripts-->
	<script src="<?php echo base_url()?>themes/default/js/plugins/filestyle/filestyle.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/filestyle/aplication.js"></script>
	
	<!-- Check -->
    <script src="<?php echo base_url()?>themes/default/js/check.js"></script>

	<!--Bootstrap Date Picker-->
	<script src="<?php echo base_url()?>themes/default/js/plugins/datetime/bootstrap-datetimepicker.min.js"></script>
    <!-- Toastr script -->
    <script src="<?php echo base_url()?>themes/default/js/plugins/toastr/toastr.min.js"></script>
    <script>
        $(function () {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": true,
                "preventDuplicates": false,
                "positionClass": "toast-bottom-right",
                "onclick": null,
                "showDuration": "400",
                "hideDuration": "1000",
                "timeOut": "7000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            //toastr.success('Successfully Update Profile', 'Update Profile');
            <?php if(isset($_COOKIE['status'])){ echo $this->model_status->status($_COOKIE['status']);}?>
            <?php if(isset($_COOKIE['status_second'])){ echo $this->model_status->status($_COOKIE['status_second']);}?>
        });
    </script>
	<script>
	    $(".date-picker").datetimepicker({format: 'yyyy-mm-dd hh:ii:ss',autoclose: true,});
	</script>

	<script type="text/javascript">
    $(document).ready(function() {
    	$(".select_parent").select2({
            placeholder: "Select a Parent",
            allowClear: true
        });
        $('#example-multiple-selected').multiselect();
    });
    </script>

</body>

</html>
