<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Media extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//require APPPATH . "third_party/MX/Controller.php";

	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
        $this->load->library('email');
        // $this->load->library('pdf');
        $this->load->helper(array('form'));
        $this->load->helper(array('url'));
		$this->load->helper(array('path'));
		$this->load->library("pagination");
		$this->load->model("menu/model_nav");
		$this->load->model("post/model_post");
		$this->load->model("setting/model_setting");
		$this->load->model("post/model_ipost");
        $this->load->model("setting/model_meta");
        $this->load->model("member/model_member");
        $this->load->model("model_media");
	}
	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url().'panel');
		}
		$data['list_post_count'] 	    = $this->model_nav->po_list_count('post','0');
        $data['list_pages'] 		    = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count'] 	    = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count'] 	= $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role'] 	    = $this->model_nav->us_list_role();
        $data['list_member_count'] 	    = $this->model_nav->us_list_count();
        $data['row_user'] 	    		= $this->model_nav->us_profile($_SESSION['user_id']);
        $data['cat_list']               = $this->model_nav->ca_list('post','0');

        //pagination settings
        if(isset($_POST['Pixel_Search']))
        {
            setcookie('status','search', time()+2);
            redirect(base_url().'panel/media/search/'.$this->model_setting->my_simple_crypt($this->input->post('search'),$action = 'e').'.html');
        }

        if($this->uri->segment(3)=='')
        {
            $config = array();
            $config["base_url"]         = base_url().'panel/media/manage/10/';
            $total_row                  = $this->model_media->ip_post_all_count();
            $config['suffix']           = '.html';
            $config['first_url']        = '1.html';
            $config["total_rows"]       = $total_row;
            $config["per_page"]         = '10';
            $config['use_page_numbers'] = TRUE;
            $config['num_links']        = $total_row;
            $config['cur_tag_open']     = '&nbsp;<a class="active">';
            $config['cur_tag_close']    = '</a>';
            $config['next_link']        = 'Next';
            $config['prev_link']        = 'Prev';
            $config['num_links']        = 5;

            $this->pagination->initialize($config);
            if($this->uri->segment(4))
            {
                $page = ($this->uri->segment(4)-1)*$config["per_page"] ;
            }
            else
            {
                $page = 0;
            }
        }
        elseif($this->uri->segment(3)=='all')
        {
            $config = array();
            $config["base_url"]         = base_url().'panel/media/manage/'.$this->uri->segment(4).'/';
            $total_row                  = $this->model_media->ip_post_all_count();
            $config['suffix']           = '.html';
            $config['first_url']        = '1.html';
            $config["total_rows"]       = $total_row;
            $config["per_page"]         = ($this->uri->segment(4)) ? $this->uri->segment(4) : '10';
            $config['use_page_numbers'] = TRUE;
            $config['num_links']        = $total_row;
            $config['cur_tag_open']     = '&nbsp;<a class="active">';
            $config['cur_tag_close']    = '</a>';
            $config['next_link']        = 'Next';
            $config['prev_link']        = 'Prev';
            $config['num_links']        = 5;

            $this->pagination->initialize($config);
            if($this->uri->segment(5))
            {
                $page = ($this->uri->segment(5)-1)*$config["per_page"] ;
            }
            else
            {
                $page = 0;
            }
        }

        $data["post_list"]          = $this->model_media->ip_post_all('DESC',$config["per_page"], $page);

        $str_links                  = $this->pagination->create_links();
        $data["links"]              = explode('&nbsp;',$str_links );

        if(isset($_POST['post-draft']) && $_POST['post-draft'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$ipost_id=$_POST['post-check'][$i];
				
				$this->model_media->ip_update_status($ipost_id,'0');
			}
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/media/manage.html');
		}
	    elseif(isset($_POST['post-publish']) && $_POST['post-publish'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$ipost_id=$_POST['post-check'][$i];
				
				$this->model_media->ip_update_status($ipost_id,'1');
			}
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/media/manage.html');
		}
		elseif(isset($_POST['post-delete']) && $_POST['post-delete'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$ipost_id=$_POST['post-check'][$i];
				
				$this->model_media->ip_delete($ipost_id);
			}
            setcookie('status','deleted', time()+2);
			redirect(base_url().'panel/media/manage.html');
		}
        $this->load->view('media',$data);

	}
    public function manage()
    {
        if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        //echo $this->uri->segment(3);
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);
        $data['cat_list']               = $this->model_nav->ca_list('post','0');

        //pagination settings
        if(isset($_POST['Pixel_Search']))
        {
            setcookie('status','search', time()+2);
            redirect(base_url().'panel/media/search/'.$this->model_setting->my_simple_crypt($this->input->post('search'),$action = 'e').'.html');
        }

        if($this->uri->segment(5)!='')
        {
            $config = array();
            $config["base_url"]         = base_url().'panel/media/manage/'.$this->uri->segment(4).'/';
            $total_row                  = $this->model_media->ip_post_all_count();
            $config['suffix']           = '.html';
            $config['first_url']        = '1.html';
            $config["total_rows"]       = $total_row;
            $config["per_page"]         = ($this->uri->segment(4)) ? $this->uri->segment(4) : '10';
            $config['use_page_numbers'] = TRUE;
            $config['num_links']        = $total_row;
            $config['cur_tag_open']     = '&nbsp;<a class="active">';
            $config['cur_tag_close']    = '</a>';
            $config['next_link']        = 'Next';
            $config['prev_link']        = 'Prev';
            $config['num_links']        = 5;

            $this->pagination->initialize($config);
            if($this->uri->segment(5))
            {
                $page = ($this->uri->segment(5)-1)*$config["per_page"] ;
            }
            else
            {
                $page = 0;
            }
        }
        else 
        {
            $config = array();
            $config["base_url"]         = base_url().'panel/media/manage/';
            $total_row                  = $this->model_media->ip_post_all_count();
            $config['suffix']           = '.html';
            $config['first_url']        = '1.html';
            $config["total_rows"]       = $total_row;
            $config["per_page"]         = '10';
            $config['use_page_numbers'] = TRUE;
            $config['num_links']        = $total_row;
            $config['cur_tag_open']     = '&nbsp;<a class="active">';
            $config['cur_tag_close']    = '</a>';
            $config['next_link']        = 'Next';
            $config['prev_link']        = 'Prev';
            $config['num_links']        = 5;

            $this->pagination->initialize($config);
            if($this->uri->segment(4))
            {
                $page = ($this->uri->segment(4)-1)*$config["per_page"] ;
            }
            else
            {
                $page = 0;
            }
        }

        $data["post_list"]          = $this->model_media->ip_post_all('DESC',$config["per_page"], $page);

        $str_links                  = $this->pagination->create_links();
        $data["links"]              = explode('&nbsp;',$str_links );

        if(isset($_POST['post-draft']) && $_POST['post-draft'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$ipost_id=$_POST['post-check'][$i];
				
				$this->model_media->ip_update_status($ipost_id,'0');
			}
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/media/manage.html');
		}
	    elseif(isset($_POST['post-publish']) && $_POST['post-publish'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$ipost_id=$_POST['post-check'][$i];
				
				$this->model_media->ip_update_status($ipost_id,'1');
			}
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/media/manage.html');
		}
		elseif(isset($_POST['post-delete']) && $_POST['post-delete'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$ipost_id=$_POST['post-check'][$i];
				
				$this->model_media->ip_delete($ipost_id);
			}
            setcookie('status','deleted', time()+2);
			redirect(base_url().'panel/media/manage.html');
		}
        $this->load->view('media',$data);
    }
    public function type()
    {
        if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        //echo $this->uri->segment(3);
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);
        $data['cat_list']               = $this->model_nav->ca_list('post','0');

        //pagination settings
        if(isset($_POST['Pixel_Search']))
        {
            setcookie('status','search', time()+2);
            redirect(base_url().'panel/media/search/'.$this->model_setting->my_simple_crypt($this->input->post('search'),$action = 'e').'.html');
        }

        if($this->uri->segment(6)!='')
        {
            $config = array();
            $config["base_url"]         = base_url().'panel/media/type/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
            $total_row                  = $this->model_media->ip_post_type_count($this->uri->segment(4));
            $config['suffix']           = '.html';
            $config['first_url']        = '1.html';
            $config["total_rows"]       = $total_row;
            $config["per_page"]         = ($this->uri->segment(5)) ? $this->uri->segment(5) : '10';
            $config['use_page_numbers'] = TRUE;
            $config['num_links']        = $total_row;
            $config['cur_tag_open']     = '&nbsp;<a class="active">';
            $config['cur_tag_close']    = '</a>';
            $config['next_link']        = 'Next';
            $config['prev_link']        = 'Prev';
            $config['num_links']        = 5;

            $this->pagination->initialize($config);
            if($this->uri->segment(6))
            {
                $page = ($this->uri->segment(6)-1)*$config["per_page"] ;
            }
            else
            {
                $page = 0;
            }
        }
        else 
        {
            $config = array();
            $config["base_url"]         = base_url().'panel/media/type/'.$this->uri->segment(4).'/';
            $total_row                  = $this->model_media->ip_post_type_count($this->uri->segment(4));
            $config['suffix']           = '.html';
            $config['first_url']        = '1.html';
            $config["total_rows"]       = $total_row;
            $config["per_page"]         = ($this->uri->segment(4)) ? $this->uri->segment(4) : '10';
            $config['use_page_numbers'] = TRUE;
            $config['num_links']        = $total_row;
            $config['cur_tag_open']     = '&nbsp;<a class="active">';
            $config['cur_tag_close']    = '</a>';
            $config['next_link']        = 'Next';
            $config['prev_link']        = 'Prev';
            $config['num_links']        = 5;

            $this->pagination->initialize($config);
            if($this->uri->segment(5))
            {
                $page = ($this->uri->segment(5)-1)*$config["per_page"] ;
            }
            else
            {
                $page = 0;
            }
        }
        
        $data["post_list"]          = $this->model_media->ip_post_type($this->uri->segment(4),'DESC',$config["per_page"], $page);

        $str_links                  = $this->pagination->create_links();
        $data["links"]              = explode('&nbsp;',$str_links );

        if(isset($_POST['post-draft']) && $_POST['post-draft'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$ipost_id=$_POST['post-check'][$i];
				
				$this->model_media->ip_update_status($ipost_id,'0');
			}
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/media/manage.html');
		}
	    elseif(isset($_POST['post-publish']) && $_POST['post-publish'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$ipost_id=$_POST['post-check'][$i];
				
				$this->model_media->ip_update_status($ipost_id,'1');
			}
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/media/manage.html');
		}
		elseif(isset($_POST['post-delete']) && $_POST['post-delete'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$ipost_id=$_POST['post-check'][$i];
				
				$this->model_media->ip_delete($ipost_id);
			}
            setcookie('status','deleted', time()+2);
			redirect(base_url().'panel/media/manage.html');
		}
        $this->load->view('media',$data);
    }
    public function search()
    {
        if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        //echo $this->uri->segment(3);
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);
        $data['cat_list']               = $this->model_nav->ca_list('post','0');

        //pagination settings
        if(isset($_POST['Pixel_Search']))
        {
            setcookie('status','search', time()+2);
            redirect(base_url().'panel/media/search/'.$this->model_setting->my_simple_crypt($this->input->post('search'),$action = 'e').'.html');
        }

        $query = $this->model_setting->my_simple_crypt($this->uri->segment(4),$action = 'd');

        if($this->uri->segment(6)!='')
        {
            $config = array();
            $config["base_url"]         = base_url().'panel/media/search/'.$this->uri->segment(4).'/'.$this->uri->segment(5).'/';
            $total_row                  = $this->model_media->ip_search_count($query);
            $config['suffix']           = '.html';
            $config['first_url']        = '1.html';
            $config["total_rows"]       = $total_row;
            $config["per_page"]         = ($this->uri->segment(5)) ? $this->uri->segment(5) : '10';
            $config['use_page_numbers'] = TRUE;
            $config['num_links']        = $total_row;
            $config['cur_tag_open']     = '&nbsp;<a class="active">';
            $config['cur_tag_close']    = '</a>';
            $config['next_link']        = 'Next';
            $config['prev_link']        = 'Prev';
            $config['num_links']        = 5;

            $this->pagination->initialize($config);
            if($this->uri->segment(6))
            {
                $page = ($this->uri->segment(6)-1)*$config["per_page"] ;
            }
            else
            {
                $page = 0;
            }
        }
        else 
        {
            $config = array();
            $config["base_url"]         = base_url().'panel/media/search/'.$this->uri->segment(4).'/';
            $total_row                  = $this->model_media->ip_search_count($query);
            $config['suffix']           = '.html';
            $config['first_url']        = '1.html';
            $config["total_rows"]       = $total_row;
            $config["per_page"]         = ($this->uri->segment(4)) ? $this->uri->segment(4) : '10';
            $config['use_page_numbers'] = TRUE;
            $config['num_links']        = $total_row;
            $config['cur_tag_open']     = '&nbsp;<a class="active">';
            $config['cur_tag_close']    = '</a>';
            $config['next_link']        = 'Next';
            $config['prev_link']        = 'Prev';
            $config['num_links']        = 5;

            $this->pagination->initialize($config);
            if($this->uri->segment(5))
            {
                $page = ($this->uri->segment(5)-1)*$config["per_page"] ;
            }
            else
            {
                $page = 0;
            }
        }

        $data["post_list"]          = $this->model_media->ip_search('DESC',$query,$config["per_page"], $page);

        $str_links                  = $this->pagination->create_links();
        $data["links"]              = explode('&nbsp;',$str_links );

        if(isset($_POST['post-draft']) && $_POST['post-draft'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$ipost_id=$_POST['post-check'][$i];
				
				$this->model_media->ip_update_status($ipost_id,'0');
			}
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/media/manage.html');
		}
	    elseif(isset($_POST['post-publish']) && $_POST['post-publish'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$ipost_id=$_POST['post-check'][$i];
				
				$this->model_media->ip_update_status($ipost_id,'1');
			}
            setcookie('status','updated', time()+2);
			redirect(base_url().'panel/media/manage.html');
		}
		elseif(isset($_POST['post-delete']) && $_POST['post-delete'] != "")
		{
			for ($i=0; $i<count($_POST['post-check']);$i++) 
			{
				$ipost_id=$_POST['post-check'][$i];
				
				$this->model_media->ip_delete($ipost_id);
			}
            setcookie('status','deleted', time()+2);
			redirect(base_url().'panel/media/manage.html');
		}
        $this->load->view('media',$data);
    }
	function add()
	{
		if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        $this->load->model("model_status");
        $this->load->model("model_meta");
        /**
        * 
        * @var List Menu
        * 
        */
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);
      
        /*User List*/
        $data['user_list']          = $this->model_member->us_list();

        if(isset($_POST['Pixel_Save']))
        {
            $user_id                = $this->input->post('user_id');
            // $cat_id                 = ($this->input->post('cat_id')=='') ? '0' : $this->input->post('cat_id');
            $post_type             = 'pages';
            $post_up                = ($this->input->post('post_up')=='') ? '0' : $this->input->post('post_up');
            $post_slug              = strtolower(url_title($this->input->post('post_title')));
            $post_title             = $this->input->post('post_title');         
            $post_content           = $this->input->post('post_content');
            $post_summary           = $this->input->post('post_summary');
            $post_link              = $this->input->post('post_link');
            $post_video             = $this->input->post('post_video');
            $post_key               = $this->input->post('post_key');
            $post_desc              = $this->input->post('post_desc');
            $post_comment           = $this->input->post('post_comment');
            $date                   = ($this->input->post('date')=='') ? date('Y-m-d H:i:s') : $this->input->post('date');
            $status                 = $this->input->post('status');
            
            $data_array = array(  
                'user_id'       => $user_id,  
                'post_type'     => $post_type,  
                'post_up'       => $post_up,
                'post_slug'     => $post_slug,
                'post_title'    => $post_title,
                'post_content'  => $post_content,  
                'post_summary'  => $post_summary,  
                'post_link'     => $post_link,  
                'post_video'    => $post_video,  
                'post_key'      => $post_key,  
                'post_desc'     => $post_desc,  
                'post_comment'  => $post_comment,  
                'date'          => $date,  
                'status'        => $status 
            );  
            $this->model_post->po_add($data_array);
			$id 			= $this->db->insert_id();
            
            /**
            * @var Upload Files
            */
            $this->model_ipost->ipost_upload('upload_photo','post',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_icon','post_icon',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_thumb','post_thumb',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_background','post_background',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_pdf','post_pdf',$id,$_SESSION['user_id']);
            $this->model_ipost->ipost_upload('upload_files','post_files',$id,$_SESSION['user_id']);
            
            if($this->uri->segment(5)=='sub')
            {
                setcookie('status','updated', time()+2);
                redirect(base_url().'panel/pages/edit/'.$id.'.html');
            }
            else
            {
                setcookie('status','added', time()+2);
			    redirect(base_url().'panel/pages/edit/'.$id.'.html');
            }
        }
        $this->load->view('pages_add', $data);
	}
	function edit()
	{
		if (!$this->ion_auth->logged_in())
        {
            redirect(base_url().'panel');
        }
        $this->load->model("model_status");
        $this->load->model("model_meta");
        /**
        * 
        * @var List Menu
        * 
        */
        $data['list_post_count']        = $this->model_nav->po_list_count('post','0');
        $data['list_pages']             = $this->model_nav->pg_list('pages','0');
        $data['list_pages_count']       = $this->model_nav->po_list_count('pages','0');
        $data['list_slideshow_count']   = $this->model_nav->po_list_count('slideshow','0');
        $data['list_member_role']       = $this->model_nav->us_list_role();
        $data['list_member_count']      = $this->model_nav->us_list_count();
        $data['row_user']               = $this->model_nav->us_profile($_SESSION['user_id']);
        
        /**
        * 
        * @var Data
        * 
        */
        $data['row']					= $this->model_ipost->ip_edit($this->uri->segment(4));
		$data['row_post']				= $this->model_post->po_edit($data['row']->ipost_all);
		$data['media_type'] 	    	= $this->model_setting->setting_list('media');
		$data['user_list']				= $this->model_member->us_list();
		
		switch($data['row']->ipost_type)
		{
			case 'cat':
			case 'post':
			case 'cat_icon':
			case 'post_icon':
			case 'cat_thumb':
			case 'post_thumb':
			case 'cat_background':
			case 'post_background':
			case 'banner':
			$data['files']					= ($data['row']->ipost=='') ? '' : base_url().'upload/'.$data['row']->ipost;
			$data['thumbnail']				= '<img src="'.base_url().'upload/'.$data['row']->ipost.'" width="100%"/>';
			break;
			
			case 'cat_pdf':
			case 'post_pdf':
			case 'cat_files':
			case 'post_files':
			case 'download':
			$data['files']					= ($data['row']->ipost=='') ? '' :base_url().'upload/files/'.$data['row']->ipost;
			$data['thumbnail']				= '';
			break;
			
			default:
			$data['files']					= '';
			$data['thumbnail']				= '';
			break;
		}
		
		if(isset($_POST['Pixel_Save']))
		{
			$id						= $this->uri->segment(4);
			$user_id 				= $this->input->post('user_id');
			$ipost_type 			= $this->input->post('ipost_type');
			$post_slug 				= strtolower(url_title($this->input->post('ipost_title')));
			$post_title 			= $this->input->post('ipost_title');
			$post_content 			= $this->input->post('ipost_content');
			$post_embed 			= $this->input->post('ipost_embed');
			$ipost_copyright 		= $this->input->post('ipost_copyright');
			$post_link 				= $this->input->post('ipost_link');
			$post_comment 			= $this->input->post('ipost_comment');
			$date 					= ($this->input->post('date')=='') ? date('Y-m-d H:i:s') : $this->input->post('date');
			$status 				= $this->input->post('status');
			
			$data_array = array(  
                'user_id' 				=> $user_id,
                'ipost_type' 			=> $ipost_type,
                'ipost_slug' 			=> $post_slug,
                'ipost_title' 			=> $post_title,
                'ipost_content' 		=> $post_content,  
                'ipost_embed' 			=> $post_embed,  
                'ipost_copyright' 		=> $ipost_copyright,  
                'ipost_link' 			=> $post_link,  
                'ipost_comment' 		=> $post_comment,  
                'date' 					=> $date,  
                'status' 				=> $status 
			);  
			$this->model_ipost->ip_update($this->uri->segment(4),$data_array);
			
			/**
			* @var Upload Files
			*/
			
			$this->model_ipost->media_upload('upload_photo',$ipost_type,$id);

            setcookie('status','updated', time()+2);
			redirect(base_url(uri_string()).'.html');
		}
		if(isset($_POST['Pixel_Save_Parent']))
		{
			$id						= $this->uri->segment(3);
			$post_all 				= $this->input->post('post_parent');
			
			$data_array = array(  
			'ipost_all' 			=> $post_all
			);  
			$this->model_ipost->ip_update($this->uri->segment(3),$data_array);
			setcookie('status','updated', time()+2);
			redirect(base_url(uri_string()).'.html');
		}
        $this->load->view('media_edit', $data);
	}
    function delete()
    {
        $this->model_ipost->ip_delete($this->uri->segment(4));
        setcookie('status','deleted', time()+2);
		redirect(base_url().'panel/media/manage.html');
    }
}
