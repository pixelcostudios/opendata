<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Media Editor</title>

    <link href="<?php echo base_url()?>themes/default/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/js/plugins/multiselect/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>themes/default/js/plugins/datetime/bootstrap-datetimepicker.css" rel="stylesheet" />
	<link href="<?php echo base_url()?>themes/default/css/plugins/select2/select2.min.css" rel="stylesheet">
    <!-- Toastr style -->
    <link href="<?php echo base_url()?>themes/default/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/custom.css" rel="stylesheet">

</head>

<body class="fixed-sidebar">

    <div id="wrapper">
    <?php $this->load->view('menu/nav');?>

        <div id="page-wrapper" class="gray-bg">
        <?php $this->load->view('menu/nav_top');?>
        
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Media Editor</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>panel/dashboard.html">Home</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url()?>panel/media.html">Media</a>
                        </li>
                        <li class="active">
                            <strong>Media Editor</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content">

            <div class="row">
            <form name="myform" method="post" enctype="multipart/form-data">
            <?php if(count($row)=='0'){ echo $this->model_status->status_pages('500');}else{?>
                <div class="col-lg-9 padding-none">
	                <div class="ibox float-e-margins">
	                    <div class="ibox-title">
	                    	<h5 class="pull-left margin-right-20">Media Editor</h5>
	                    	<!--<a href="<?php echo base_url()?>panel/<?php if($this->uri->segment(5)=='sub'){echo 'banner_add/0/banner/sub/'.$this->uri->segment(6).'.html';}else{echo 'banner_add.html';}?>" class="btn btn-default pull-left">Add New Banner</a>-->
	                        <button type="submit" name="Pixel_Save" class="btn btn-primary float-right">Save Media</button>
	                    </div>
	                    <div class="ibox-content">
	                    <div class="form-group">
							<label for="title">Attach To</label>
							<div class="input-group">
								<input type="text" class="form-control" placeholder="Files" disabled="" value="<?php if($row_post){echo $row_post->post_title;}else{echo 'No Attach Post';}?>">
								<span class="input-group-addon tooltip-demo" title="Edit Post" data-toggle="modal" data-target="#myModal4"><a href="#"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit Post"></i></a></span>
								<span class="input-group-addon tooltip-demo" title="Preview Post"><a target="_blank" href="<?php if($row_post){echo base_url().'panel/'.$row_post->post_type.'_edit/'.$row_post->post_id.'.html';}else{echo '#';}?>"><i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="Preview Link ID"></i></a></span>
							</div>
							

							<div class="modal inmodal" id="myModal4" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content animated fadeIn">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <!--<i class="fa fa-clock-o modal-icon"></i>-->
                                            <h4 class="modal-title">Attach Post Editor</h4>
                                            <small>Please Select Post to Attach Media</small>
                                        </div>
                                        <div class="modal-body">
                                           <label for="title">Type a keyword : </label>
							                <div class="form-group">
							                    <input type="text" class="select2-search__field form-control" onkeyup="autocomplet()">
							                    <input type="hidden" name="post_parent" class="search_id">
							                    <ul class="select2-results__options"></ul>
							                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                            <button type="submit" name="Pixel_Save_Parent" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
						
	                    <div class="form-group">
							<label for="title">Title ID </label>
							<input type="text" class="form-control" id="title" name="ipost_title" placeholder="Title ID" required="required" value="<?php echo $row->ipost_title;?>">
						</div>
                        <div class="form-group col-lg-4 col-sm-4 col-xs-12 padding-left-0 padding-xs-none multi_select clearfix">
                        <label for="category">Type</label><br>
							<select class="form-control" name="ipost_type">
							<?php foreach($media_type as $row_type){?>
							<option value="<?php echo $row_type->setting_name;?>" <?php if($row_type->setting_name==$row->ipost_type){?>selected<?php }?>><?php echo $row_type->setting_value;?></option>
							<?php }?>
			                </select>
                        </div>
                        <div class="form-group col-lg-4 col-sm-4 col-xs-12 padding-none padding-xs-none">
                            <label for="status">Status</label>
                            <select name="status" class="form-control">
                            	<option value="1" <?php if($row->status=='1'){?>selected<?php }?>>Publish</option>
								<option value="0" <?php if($row->status=='0'){?>selected<?php }?>>Draft</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-sm-4 col-xs-12 padding-right-0 padding-xs-none">
                            <label for="date">Date</label>
                            <div class="input-group">
                                <input name="date" value="<?php echo $row->date;?>" class="form-control date-picker" type="text" data-date-format="yyyy-mm-dd hh:mm">
                                <span class="input-group-addon tooltip-demo">
                                    <i class="fa fa-calendar" data-toggle="tooltip" data-placement="top" title="Calendar"></i>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
							<label for="title">Files</label>
							<div class="input-group">
								<input type="text" class="form-control" id="title" placeholder="Files" disabled="" value="<?php echo $files;?>">
								<span class="input-group-addon tooltip-demo" title="Preview Link ID"><a target="_blank" href="<?php echo $files;?>"><i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="Preview Link ID"></i></a></span>
							</div>
						</div>
						<div class="form-group clear">
							<label for="title">Website Link </label>
							<input type="text" class="form-control" name="ipost_link" placeholder="Website Link" value="<?php echo $row->ipost_link;?>">
						</div>
						<div class="form-group clear">
							<label for="title">Embed Code </label>
							<textarea rows="3" name="ipost_embed" class="form-control"><?php echo $row->ipost_embed;?></textarea>
						</div>
						<div class="form-group clear">
							<label for="title">Copyright</label>
							<textarea rows="3" name="ipost_copyright" class="form-control"><?php echo $row->ipost_copyright;?></textarea>
						</div>
						<div class="form-group clear">
							<label for="content">Content ID</label>
                            <textarea id="content_id" name="ipost_content"><?php echo $row->ipost_content;?></textarea>
						</div>
						<div class="form-group col-lg-6 col-sm-6 col-xs-12 padding-left-0 padding-xs-none">
			                <label for="Comment">Comment</label>
			                <select name="ipost_comment" class="form-control">
			                	<option value="1" <?php if($row->ipost_comment=='1'){?>selected<?php }?>>Yes</option>
								<option value="0" <?php if($row->ipost_comment=='0'){?>selected<?php }?>>No</option>
			                </select>
			            </div>
			            <div class="form-group col-lg-6 col-sm-6 col-xs-12 padding-right-0 padding-xs-none">
			                <label for="Username">Username</label>
			                <select name="user_id" class="form-control">
			                <?php foreach($user_list as $row_user){?>
							<option value="<?php echo $row_user->user_id;?>"  <?php if($row_user->user_id==$row->user_id){?>selected<?php }?>><?php echo $row_user->user_name;?> | <?php echo $row_user->user_first_name;?> <?php echo $row_user->user_first_name;?></option>
							<?php }?>
			                </select>
			            </div>
						<div class="clear"></div>
	                    </div>
	                </div>
	            </div>
	            <div class="col-lg-3 padding-right-0">
	            <div class="bar-title white-bg">
	            	<div class="tabs-container">
                        <div class="tabs-left">
                            <div class="tab-content padding-10">
                                <div class="input-file"><input type="file" name="upload_photo[]" multiple="multiple" id="input_images"></div>
                                <?php if($thumbnail!=''){?>
                                <div class="margin-top-10">
                                    <?php echo $thumbnail;?>
                                </div>
                                <?php }?>
                            </div>
                        </div>

                    </div>
                    <!--End Tab-->
	            </div>
	            </div>
            <?php }?>
            </form>
            </div>


            </div>
        <div class="footer">
            <div class="pull-right">
                <strong><?php echo $this->model_nav->disk_size();?></strong> Free.
            </div>
            <div>
                <strong>Copyright</strong> <?php echo $this->model_setting->setting('company');?> &copy; <?php echo date('Y');?>
            </div>
        </div>

        </div>
        </div>

    <!-- Mainly scripts -->
    <script src="<?php echo base_url()?>themes/default/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url()?>themes/default/js/inspinia.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/pace/pace.min.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/multiselect/dist/js/bootstrap-multiselect.js" type="text/javascript" ></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    
    <!--CK Editor-->
    <script src="<?php echo base_url()?>themes/default/js/plugins/ckeditor/ckeditor.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/ckeditor/en.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/ckeditor/id.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/ckeditor/ck.js"></script>

	<script src="<?php echo base_url()?>themes/default/js/plugins/select2/select2.full.min.js"></script>
	
	<!--Files Scripts-->
	<script src="<?php echo base_url()?>themes/default/js/plugins/filestyle/filestyle.js"></script>
	<script src="<?php echo base_url()?>themes/default/js/plugins/filestyle/aplication.js"></script>
	
	<!-- Check -->
    <script src="<?php echo base_url()?>themes/default/js/check.js"></script>
    <!-- Typeahead -->
    <script src="<?php echo base_url()?>themes/default/js/script.js"></script>

	<!--Bootstrap Date Picker-->
	<script src="<?php echo base_url()?>themes/default/js/plugins/datetime/bootstrap-datetimepicker.min.js"></script>
    <!-- Toastr script -->
    <script src="<?php echo base_url()?>themes/default/js/plugins/toastr/toastr.min.js"></script>
    <script>
        $(function () {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": true,
                "preventDuplicates": false,
                "positionClass": "toast-bottom-right",
                "onclick": null,
                "showDuration": "400",
                "hideDuration": "1000",
                "timeOut": "7000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            //toastr.success('Successfully Update Profile', 'Update Profile');
            <?php if(isset($_COOKIE['status'])){ echo $this->model_status->status($_COOKIE['status']);}?>
            <?php if(isset($_COOKIE['status_second'])){ echo $this->model_status->status($_COOKIE['status_second']);}?>
        });
    </script>
	<script>
	    $(".date-picker").datetimepicker({format: 'yyyy-mm-dd hh:ii:ss',autoclose: true,});
	</script>
	<script type="text/javascript">
	    $(document).ready(function(){
	    	$('.select2-search__field').attr('onkeyup', 'autocomplet()');
	        
	    });
	</script> 
	<script type="text/javascript">
    $(document).ready(function() {
    	$(".select_parent").select2({
            placeholder: "Select a Parent",
            allowClear: true
        });
        $('#example-multiple-selected').multiselect();
    });
    </script>

</body>

</html>
