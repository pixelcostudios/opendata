<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Media</title>
    <link href="<?php echo base_url()?>themes/default/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/animate.css" rel="stylesheet">
    <!-- Toastr style -->
    <link href="<?php echo base_url()?>themes/default/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url()?>themes/default/css/custom.css" rel="stylesheet">

</head>

<body class="fixed-sidebar">
    <div id="wrapper">
    <?php $this->load->view('menu/nav');?>

        <div id="page-wrapper" class="gray-bg">
        <?php $this->load->view('menu/nav_top');?>
        
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Media</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url();?>">Home</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>panel/media/manage.html">Media</a>
                        </li>
                        <li class="active">
                            <strong>All Media</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
            <form method="post">
                <div class="col-lg-12 padding-none">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>All Media</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config 1</a>
                                    </li>
                                    <li><a href="#">Config 2</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                            	<div class="col-sm-3">
                                    <div class="input-group"><input type="text" name="search" placeholder="Search" class="input-sm form-control"> <span class="input-group-btn">
                                        <button type="submit" name="Pixel_Search" class="btn btn-sm btn-primary"> Go!</button> </span></div>
                                </div>
                                <div class="col-sm-6 m-b-xs">
                                    <div data-toggle="buttons" class="btn-group">
                                        <!--<label onclick="location.href='<?php echo base_url()?>panel/media/add.html'" class="btn btn-sm btn-white"> <i class="fa fa-plus"></i> Add New Media </label>-->
                                        <!--<label class="btn btn-sm btn-white active"> <input type="radio" id="option2" name="options"> Week </label>
                                        <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="options"> Month </label>-->
                                    </div>
                                </div>
                                <div class="col-sm-3 m-b-xs">
                                    <?php if($this->uri->segment(2)=='media'){?>
                                        <select class="input-sm form-control input-s-sm inline float-left category" id="select-cat" onchange="window.location = jQuery('#select-cat option:selected').val();">
                                            <option value="<?php echo base_url()?>panel/media.html">All Type</option>
                                            <?php $list_media = array('post','post_icon','post_thumb','post_background','post_pdf','post_files'); foreach($list_media as $row_type){?>
                                                <option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo 'media';}else{echo $this->uri->segment(3);}?>/<?php echo $row_type;?>/<?php if($this->uri->segment(5)==''){echo '10';}else{echo $this->uri->segment(5);}?>/<?php if($this->uri->segment(6)==''){echo '1';}else{echo $this->uri->segment(6);}?>.html" <?php if($row_type==$this->uri->segment(4)){?>selected=""<?php }?> ><?php echo $row_type;?></option>
                                            <?php }?>
                                        </select>
                                    <?php }?>
                                    <?php if($this->uri->segment(3)=='manage'){?>
                                    <select class="input-sm form-control input-s-sm inline float-right category-show" id="select-view" onchange="window.location = jQuery('#select-view option:selected').val();">
                                        <option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo 'media';}else{echo $this->uri->segment(3);}?>/10/<?php if($this->uri->segment(5)==''){echo '1';}else{echo $this->uri->segment(5);}?>.html" <?php if($this->uri->segment(5)=='10'){?>selected=""<?php }?>>10</option>
                                        <option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo 'media';}else{echo $this->uri->segment(3);}?>/25/<?php if($this->uri->segment(5)==''){echo '1';}else{echo $this->uri->segment(5);}?>.html" <?php if($this->uri->segment(5)=='25'){?>selected=""<?php }?>>25</option>
                                        <option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo 'media';}else{echo $this->uri->segment(3);}?>/50/<?php if($this->uri->segment(5)==''){echo '1';}else{echo $this->uri->segment(5);}?>.html" <?php if($this->uri->segment(5)=='50'){?>selected=""<?php }?>>50</option>
                                        <option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo 'media';}else{echo $this->uri->segment(3);}?>/100/<?php if($this->uri->segment(5)==''){echo '1';}else{echo $this->uri->segment(5);}?>.html" <?php if($this->uri->segment(5)=='100'){?>selected=""<?php }?>>100</option>
                                    </select>
                                    <?php }else{?>
                                    <select class="input-sm form-control input-s-sm inline float-right category-show" id="select-view" onchange="window.location = jQuery('#select-view option:selected').val();">
                                        <option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo 'media';}else{echo $this->uri->segment(3);}?>/<?php if($this->uri->segment(4)==''){echo '0';}else{echo $this->uri->segment(4);}?>/10/<?php if($this->uri->segment(6)==''){echo '1';}else{echo $this->uri->segment(6);}?>.html" <?php if($this->uri->segment(5)=='10'){?>selected=""<?php }?>>10</option>
                                        <option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo 'media';}else{echo $this->uri->segment(3);}?>/<?php if($this->uri->segment(4)==''){echo '0';}else{echo $this->uri->segment(4);}?>/25/<?php if($this->uri->segment(6)==''){echo '1';}else{echo $this->uri->segment(6);}?>.html" <?php if($this->uri->segment(5)=='25'){?>selected=""<?php }?>>25</option>
                                        <option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo 'media';}else{echo $this->uri->segment(3);}?>/<?php if($this->uri->segment(4)==''){echo '0';}else{echo $this->uri->segment(4);}?>/50/<?php if($this->uri->segment(6)==''){echo '1';}else{echo $this->uri->segment(6);}?>.html" <?php if($this->uri->segment(5)=='50'){?>selected=""<?php }?>>50</option>
                                        <option value="<?php echo base_url()?>panel/<?php echo $this->uri->segment(2);?>/<?php if($this->uri->segment(3)==''){echo 'media';}else{echo $this->uri->segment(3);}?>/<?php if($this->uri->segment(4)==''){echo '0';}else{echo $this->uri->segment(4);}?>/100/<?php if($this->uri->segment(6)==''){echo '1';}else{echo $this->uri->segment(6);}?>.html" <?php if($this->uri->segment(5)=='100'){?>selected=""<?php }?>>100</option>
                                    </select>
                                    <?php }?>
                                </div>
                            </div>
                            <div class="table-responsive margin-top-10">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>

                                        <th width="1%">
                                        	<label>
	                                                <input type="checkbox" class="colored-green checkall">
	                                                <span class="text"></span>
	                                            </label>
                                        </th>
                                        <th width="5%">Thumb </th>
                                        <th>Title </th>
                                        <th>Type </th>
                                        <th width="13%">Date</th>
                                        <th width="5%">Status</th>
                                        <th width="5%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php for ($i = 0; $i < count($post_list); ++$i) { 
                                    switch($post_list[$i]->ipost_type){
										case 'post':
										case 'post_icon':
										case 'post_thumb':
										case 'post_background':
										case 'cat':
										case 'cat_icon':
										case 'cat_thumb':
										case 'cat_background':
										$icon	= base_url().'upload/'.$post_list[$i]->ipost;
										$files	= base_url().'upload/'.$post_list[$i]->ipost;
											break;
											
										case 'post_pdf':
										case 'post_files':
										case 'cat_pdf':
										case 'cat_files':
										case 'download':
										$icon	= base_url().'themes/default/img/mimetypes/48/'.$this->model_ipost->ext_filename($post_list[$i]->ipost).'.png';
										$files	= base_url().'upload/files/'.$post_list[$i]->ipost;
											break;
										
										case 'post_link':
										$icon	= base_url().'themes/default/img/mimetypes/48/link.png';
										$files	= $post_list[$i]->ipost_link;
											break;
										
										case 'video_link':
										case 'post_video':
										$icon	= base_url().'themes/default/img/mimetypes/48/video.png';
										$files	= $post_list[$i]->ipost_link;
											break;
											
										default:
										$icon	= base_url().'upload/default.png';
										$files	= '#';
											break;
									}
                                    
                                    ?>
                                    <tr>
                                        <td>
                                        	<label>
                                                <input type="checkbox" name="post-check[]" value="<?php echo $post_list[$i]->ipost_id; ?>" class="colored-green">
                                                <span class="text"></span>
                                            </label>
                                        </td>
                                        <td align="center"><img src="<?php echo $icon;?>" width="20" height="20"></td>
                                        <td><a href="<?php echo base_url()?>panel/media/edit/<?php echo $post_list[$i]->ipost_id; ?>/<?php echo $post_list[$i]->ipost_type; ?>.html"><?php echo $post_list[$i]->ipost_title; ?></a> &nbsp;&nbsp;<a target="_blank" href="<?php echo $files; ?>"><i class="fa fa-search"></i></a></td>
                                        <td><?php echo $post_list[$i]->ipost_type;?></td>
                                        <td><?php echo $post_list[$i]->date; ?></td>
                                        <td><?php if($post_list[$i]->status=='1'){ ?><span class="label label-primary">Publish</span><?php }else{?><span class="label label-warning">Draft</span><?php }?></td>
                                        <td>
                                        	<div class="btn-group">
	                                        <a class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
	                                            Action <i class="fa fa-angle-down"></i>
	                                        </a>
	                                        <ul class="dropdown-menu drop-right">
	                                            <li><a href="<?php echo base_url()?>panel/media/edit/<?php echo $post_list[$i]->ipost_id; ?>/<?php echo $post_list[$i]->ipost_type; ?>.html">Edit</a></li>
	                                            <li><a href="<?php echo base_url()?>panel/media/delete/<?php echo $post_list[$i]->ipost_id; ?>/<?php echo $post_list[$i]->ipost_type; ?>.html">Delete</a></li>
	                                        </ul>
                                			</div>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                                    <?php if(count($post_list)>0){?>
                                    <tfoot>
	                                <tr>
	                                    <td colspan="7" class="footable-visible">
	                                    	<div class="pull-left margin-top-20">
		                                    	<div class="btn-group">
			                                    <div class="btn-group dropup">
			                                        <button type="button" class="btn btn-default btn-custom button-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
			                                            <i class="fa fa-ellipsis-horizontal"></i> Action <i class="fa fa-angle-up"></i>
			                                        </button>
			                                        <ul class="dropdown-menu dropdown-archive">
			                                        <li class="margin-bottom-5">
			                                        	<button type="submit" name="post-publish" value="publish" class="btn btn-default btn-sm"><i class="fa fa-check"></i> Publish</button>
			                                        </li>
			                                        <li class="margin-bottom-5">
			                                            <button type="submit" name="post-draft" value="draft" class="btn btn-default btn-sm"><i class="fa fa-pencil-square-o"></i> Draft</button>
			                                        </li>
			                                        <li>
			                                        	<button type="submit" name="post-delete" value="delete" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i> Delete</button>                                 
			                                        </li>
			                                        </ul>
			                                    </div>
			                                </div>
		                                    	</div>
	                                    	<div class="float-right">
                                                <ul class="pagination pull-right">

                                                    <!-- Show pagination links -->
                                                    <?php foreach ($links as $link) {
                                                        echo "<li>". $link."</li>";
                                                    } ?>
	                                    	</div>
	                                    </td>
	                                </tr>
	                                </tfoot>
                                   <?php }?>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
			</form>
            </div>
        </div>
        <div class="footer">
            <div class="pull-right">
                <strong><?php echo $this->model_nav->disk_size();?></strong> Free.
            </div>
            <div>
                <strong>Copyright</strong> Example Company &copy; 2014-2017
            </div>
        </div>

        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="<?php echo base_url()?>themes/default/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Peity -->
    <script src="<?php echo base_url()?>themes/default/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url()?>themes/default/js/inspinia.js"></script>
    <script src="<?php echo base_url()?>themes/default/js/plugins/pace/pace.min.js"></script>

    <!-- Check -->
    <script src="<?php echo base_url()?>themes/default/js/check.js"></script>

    <!-- Peity -->
    <script src="<?php echo base_url()?>themes/default/js/demo/peity-demo.js"></script>
    <!-- Toastr script -->
    <script src="<?php echo base_url()?>themes/default/js/plugins/toastr/toastr.min.js"></script>
    <script>
        $(function () {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": true,
                "preventDuplicates": false,
                "positionClass": "toast-bottom-right",
                "onclick": null,
                "showDuration": "400",
                "hideDuration": "1000",
                "timeOut": "7000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            //toastr.success('Successfully Update Profile', 'Update Profile');
            <?php if(isset($_COOKIE['status'])){ echo $this->model_status->status($_COOKIE['status']);}?>
            <?php if(isset($_COOKIE['status_second'])){ echo $this->model_status->status($_COOKIE['status_second']);}?>
        });
    </script>
    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>

</body>

</html>
