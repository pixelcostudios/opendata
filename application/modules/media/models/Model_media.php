<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

class model_media extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}
	function ip_post_all($order,$start,$limit)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		$this->db->order_by('date',$order);
		$this->db->limit($start,$limit);
		$query	= $this->db->get();
		return $query->result();
	}
	function ip_post_all_count()
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		return $this->db->count_all_results();
	}
	function ip_post_type($ipost_type,$order,$start,$limit)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		$this->db->where('ipost_type',$ipost_type);
		$this->db->order_by('date',$order);
		$this->db->limit($start,$limit);
		$query	= $this->db->get();
		return $query->result();
	}
	function ip_post_type_count($ipost_type)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		$this->db->where('ipost_type',$ipost_type);
		return $this->db->count_all_results();
	}
	function ip_update($ipost_id,$data_array)
	{
		$this->db->where('ipost_id', $ipost_id);
		$this->db->update('my_ipost',$data_array);
		return true;
	}
	function ip_update_type($ipost_type,$data_array)
	{
		$this->db->where('ipost_type', $ipost_type);
		$this->db->update('my_ipost',$data_array);
		return true;
	}
	function ip_update_status($ipost_id,$status)
	{
		$this->db->set('status', $status); 
		$this->db->where('ipost_id', $ipost_id);
		$this->db->update('my_ipost');
		return true;
	}
	function ip_delete($ipost_id)
	{
		$this->db->where('ipost_id',$ipost_id);
		$this->db->delete('my_ipost');
		return true;
	}
	function ip_search($order,$cat,$start,$limit)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		$this->db->group_start();
		$this->db->like('ipost',$cat);
		$this->db->or_like('ipost_files',$cat);
		$this->db->or_like('ipost_link',$cat);
		$this->db->or_like('ipost_slug',$cat);
		$this->db->or_like('ipost_title',$cat);
		$this->db->or_like('ipost_content',$cat);
		$this->db->or_like('ipost_embed',$cat);
		$this->db->or_like('ipost_copyright',$cat);
		$this->db->group_end();
		$this->db->order_by('date',$order);
		$this->db->limit($start,$limit);
		$query	= $this->db->get();
		return $query->result();
	}
	function ip_search_count($cat)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		$this->db->group_start();
		$this->db->like('ipost',$cat);
		$this->db->or_like('ipost_files',$cat);
		$this->db->or_like('ipost_link',$cat);
		$this->db->or_like('ipost_slug',$cat);
		$this->db->or_like('ipost_title',$cat);
		$this->db->or_like('ipost_content',$cat);
		$this->db->or_like('ipost_embed',$cat);
		$this->db->or_like('ipost_copyright',$cat);
		$this->db->group_end();
		return $this->db->count_all_results();
	}
	function ip_search_type($ipost_type,$order,$cat,$start,$limit)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		$this->db->group_start();
		$this->db->like('ipost',$cat);
		$this->db->or_like('ipost_files',$cat);
		$this->db->or_like('ipost_link',$cat);
		$this->db->or_like('ipost_slug',$cat);
		$this->db->or_like('ipost_title',$cat);
		$this->db->or_like('ipost_content',$cat);
		$this->db->or_like('ipost_embed',$cat);
		$this->db->or_like('ipost_copyright',$cat);
		$this->db->group_end();
        $this->db->where('ipost_type',$ipost_type);
		$this->db->order_by('date',$order);
		$this->db->limit($start,$limit);
		$query	= $this->db->get();
		return $query->result();
	}
	function ip_search_type_count($ipost_type,$cat)
	{
		$this->db->select('ipost_id, user_id, ipost_all, ipost_type, ipost, ipost_files, ipost_link, ipost_slug, ipost_title, ipost_content, ipost_embed, ipost_copyright, ipost_comment, date, status');
		$this->db->from("my_ipost");
		$this->db->group_start();
		$this->db->like('ipost',$cat);
		$this->db->or_like('ipost_files',$cat);
		$this->db->or_like('ipost_link',$cat);
		$this->db->or_like('ipost_slug',$cat);
		$this->db->or_like('ipost_title',$cat);
		$this->db->or_like('ipost_content',$cat);
		$this->db->or_like('ipost_embed',$cat);
		$this->db->or_like('ipost_copyright',$cat);
		$this->db->group_end();
        $this->db->where('ipost_type',$ipost_type);
		return $this->db->count_all_results();
	}
}